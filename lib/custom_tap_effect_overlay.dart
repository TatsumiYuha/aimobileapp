import 'package:flutter/material.dart';

class CustomTapEffectOverlay extends StatefulWidget {
  final Widget child;
  CustomTapEffectOverlay({required this.child});

  @override
  _CustomTapEffectOverlayState createState() => _CustomTapEffectOverlayState();
}

class _CustomTapEffectOverlayState extends State<CustomTapEffectOverlay> {
  bool _showOverlay = false;
  Alignment _tapPosition = Alignment.center;
  double _overlaySize = 0.0;

  void _showTapEffect(TapDownDetails details, BuildContext context) {
    final RenderBox box = context.findRenderObject() as RenderBox;
    final Offset localOffset = box.globalToLocal(details.globalPosition);
    final double dx = localOffset.dx / box.size.width * 2 - 1;
    final double dy = localOffset.dy / box.size.height * 2 - 1;

    setState(() {
      _showOverlay = true;
      _tapPosition = Alignment(dx, dy);
      _overlaySize = 0.0;
    });

    Future.delayed(Duration(milliseconds: 10), () {
      setState(() {
        _overlaySize = 60.0;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (details) => _showTapEffect(details, context),
      onTapUp: (_) => setState(() => _showOverlay = false),
      onTapCancel: () => setState(() => _showOverlay = false),
      child: Stack(
        fit: StackFit.expand,
        children: [
          widget.child,
          if (_showOverlay)
            Positioned.fill(
              child: AnimatedAlign(
                alignment: _tapPosition,
                duration: const Duration(milliseconds: 300),
                child: AnimatedContainer(
                  width: _overlaySize,
                  height: _overlaySize,
                  duration: Duration(milliseconds: 300),
                  curve: Curves.easeOutQuart,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey.withOpacity(0.4),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
