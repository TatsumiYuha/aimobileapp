import 'package:flutter/material.dart';

class AnimatedImageDisplay extends StatefulWidget {
  final bool animate;
  final Alignment alignment;
  final EdgeInsets padding;
  final double width;
  final double height;
  final String imagePath;

  const AnimatedImageDisplay({
    Key? key,
    required this.animate,
    required this.alignment,
    required this.padding,
    required this.width,
    required this.height,
    required this.imagePath,
  }) : super(key: key);

  @override
  _AnimatedImageDisplayState createState() => _AnimatedImageDisplayState();
}

class _AnimatedImageDisplayState extends State<AnimatedImageDisplay>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();
    if (widget.animate) {
      _controller = AnimationController(
        duration: const Duration(seconds: 1),
        vsync: this,
      )..repeat(reverse: true);

      _opacityAnimation =
          Tween<double>(begin: 1.0, end: 0.3).animate(_controller);
    }
  }

  @override
  void dispose() {
    if (widget.animate) {
      _controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: true,
      child: Padding(
        padding: widget.padding,
        child: Align(
          alignment: widget.alignment,
          child: widget.animate
              ? FadeTransition(
                  opacity: _opacityAnimation,
                  child: Image.asset(
                    widget.imagePath,
                    width: widget.width,
                    height: widget.height,
                  ),
                )
              : Image.asset(
                  widget.imagePath,
                  width: widget.width,
                  height: widget.height,
                ),
        ),
      ),
    );
  }
}
