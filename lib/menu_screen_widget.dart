import 'dart:async';
import 'package:ai_mobile_app/achievement_screen.dart';
import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/inquiry_form_screen_widget.dart';
import 'package:ai_mobile_app/question_debugging_screen_widget.dart';
import 'package:ai_mobile_app/question_description_screen_widget.dart';
import 'package:ai_mobile_app/question_programming_screen_widget.dart';
import 'package:ai_mobile_app/xp_gain_overlay.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'auth_service.dart';
import 'level_data.dart';

/// デイリーミッション用のデータモデル
class DailyMission {
  final String id;
  final String title;
  final String description;
  final int current;
  final int total;
  final int reward;
  final IconData icon;

  DailyMission({
    required this.id,
    required this.title,
    required this.description,
    required this.current,
    required this.total,
    required this.reward,
    required this.icon,
  });

  /// ミッションが完了しているかどうか
  bool get isCompleted => current >= total;
}

/// メニュー画面 (StatefulWidget)
class MenuScreenWidget extends StatefulWidget {
  const MenuScreenWidget({Key? key}) : super(key: key);

  @override
  State<MenuScreenWidget> createState() => _MenuScreenWidgetState();
}

class _MenuScreenWidgetState extends State<MenuScreenWidget> {
  late PageController _pageController;
  int _currentPage = 0;
  Timer? _autoSlideTimer;

  final User? currentUser = FirebaseAuth.instance.currentUser;
  bool _obscurePassword = true;
  bool _obscureConfirmPassword = true;

  // Firestoreから取得した値を保存する変数
  String _userName = '...'; // 表示名
  int _level = 1; // 現在のレベル
  double _currentXP = 0; // レベル内の進捗
  double _nextLevelXP = 100; // レベル内の必要総量
  int _streak = 0; // 連続ログイン日数
  int _point = 0; // ポイント

  // デイリーミッションオーバーレイ表示フラグ
  bool _showDailyMissionOverlay = false;

  // ★ デイリーミッションのサンプルデータ
  List<DailyMission> _dailyMissions = [
    DailyMission(
      id: "AI",
      title: "AIへの質問",
      description: "AQuiZと5回話す",
      current: 0,
      total: 5,
      reward: 10,
      icon: Icons.chat,
    ),
    DailyMission(
      id: "Glossary",
      title: "用語集の確認",
      description: "用語集ページにて10種類の用語を確認する",
      current: 0,
      total: 10,
      reward: 10,
      icon: Icons.book,
    ),
    DailyMission(
      id: "Question",
      title: "今日の問題",
      description: "『今日の問題』で提供される問題を解く",
      current: 0,
      total: 1,
      reward: 50,
      icon: Icons.question_answer,
    ),
  ];

  final gamificationService = GamificationService();
  StreamSubscription<DocumentSnapshot>? _userDocSubscription;

  @override
  void initState() {
    super.initState();

    // 初回ログイン確認
    _checkFirstLogin();

    _pageController = PageController();

    // ★ 3ページになったので、8秒おきに 0→1→2→0→1→2... のループ
    _autoSlideTimer = Timer.periodic(const Duration(seconds: 8), (timer) {
      setState(() {
        _currentPage = (_currentPage + 1) % 3;
      });
      _pageController.animateToPage(
        _currentPage,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
    });

    // ▼ Firestoreからユーザー情報をロード
    _loadUserData();
    if (currentUser != null) {
      _userDocSubscription = FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser!.uid)
          .snapshots()
          .listen((docSnap) {
        if (docSnap.exists) {
          final data = docSnap.data()!;
          final int dailyAICount = data['dailyXpCount'] ?? 0;

          final List<dynamic> dailyGlossaryNumbers =
              data['dailyMissionGlossary_numbers'] ?? [];
          final int dailyGlossaryCount = dailyGlossaryNumbers.length;

          final int dailyQuestion = data['dailyQuestion'] ?? 0;

          setState(() {
            _dailyMissions = _dailyMissions.map((mission) {
              if (mission.id == "AI") {
                return DailyMission(
                  id: mission.id,
                  title: mission.title,
                  description: mission.description,
                  current: dailyAICount,
                  total: mission.total,
                  reward: mission.reward,
                  icon: mission.icon,
                );
              }
              if (mission.id == "Glossary") {
                return DailyMission(
                  id: mission.id,
                  title: mission.title,
                  description: mission.description,
                  current: dailyGlossaryCount,
                  total: mission.total,
                  reward: mission.reward,
                  icon: mission.icon,
                );
              }
              if (mission.id == "Question") {
                return DailyMission(
                  id: mission.id,
                  title: mission.title,
                  description: mission.description,
                  current: dailyQuestion,
                  total: mission.total,
                  reward: mission.reward,
                  icon: mission.icon,
                );
              }
              return mission;
            }).toList();
          });
        }
      });
    }
  }

  @override
  void dispose() {
    _autoSlideTimer?.cancel();
    _pageController.dispose();
    _userDocSubscription?.cancel();
    super.dispose();
  }

  /// Firestoreからユーザー情報を取得し、レベルやXP進捗を計算
  Future<void> _loadUserData() async {
    if (currentUser == null) return;
    final docRef =
        FirebaseFirestore.instance.collection('users').doc(currentUser!.uid);

    final docSnap = await docRef.get();
    if (!docSnap.exists) return;

    final data = docSnap.data()!;
    final int experience = data['experience'] ?? 0;
    final String userNameFromDB =
        data['userName'] ?? currentUser!.email ?? '名無し';
    final int streakFromDB = data['streak'] ?? 0;
    final int pointFromDB = data['point'] ?? 0;

    final int dailyAICount = data['dailyXpCount'] ?? 0;
    final List<dynamic> dailyGlossaryNumbers =
        data['dailyMissionGlossary_numbers'] ?? [];
    final int dailyGlossaryCount = dailyGlossaryNumbers.length;
    // 現在のレベル
    final userLevel = getLevelFromXp(experience);

    // レベル内の進捗 (currentProgress) と、そのレベルで必要な合計値 (maxProgress)
    final progressMap = getCurrentLevelProgress(experience);
    final currentProgress = progressMap['currentProgress'] ?? 0;
    final maxProgress = progressMap['maxProgress'] ?? 1; // 0除算回避用

    setState(() {
      _userName = userNameFromDB;
      _streak = streakFromDB;
      _point = pointFromDB;
      _level = userLevel;
      _currentXP = currentProgress.toDouble();
      _nextLevelXP = maxProgress.toDouble();
      _dailyMissions = _dailyMissions.map((mission) {
        if (mission.id == "AI") {
          return DailyMission(
            id: mission.id,
            title: mission.title,
            description: mission.description,
            current: dailyAICount,
            total: mission.total,
            reward: mission.reward,
            icon: mission.icon,
          );
        }
        if (mission.id == "Glossary") {
          return DailyMission(
            id: mission.id,
            title: mission.title,
            description: mission.description,
            current: dailyGlossaryCount, // Firestore の値で更新
            total: mission.total,
            reward: mission.reward,
            icon: mission.icon,
          );
        }
        return mission;
      }).toList();
    });
  }

  /// 初回ログイン時にパスワード変更ダイアログを表示
  Future<void> _checkFirstLogin() async {
    if (currentUser != null && currentUser!.uid.isNotEmpty) {
      final doc = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser!.uid)
          .get();

      if (doc['loginCount'] == 1 &&
          !(doc.data()?['passwordChanged'] ?? false)) {
        _showChangePasswordDialog();
      }
    }
  }

  /// 今日の問題データを Firestore から取得する関数
  Future<Map<String, dynamic>> getDailyQuestion() async {
    final FirebaseFirestore firestore = FirebaseFirestore.instance;

    // ① 日付から dailyCount を算出
    // 開始日：4月3日（例：2023年4月3日）
    DateTime startDate = DateTime(2025, 2, 21);

    // サーバー時刻を取得するためのドキュメント（適宜コレクション・ドキュメント名は変更可）
    final serverTimeDocRef = firestore.collection('meta').doc('serverTimeDoc');

// サーバータイムスタンプをセット（ドキュメントが存在しない場合は自動作成）
    await serverTimeDocRef.set({
      'timestamp': FieldValue.serverTimestamp(),
    }, SetOptions(merge: true));

// ドキュメントからサーバー時刻を取得
    final serverTimeSnap = await serverTimeDocRef.get();
    final serverTimestamp = serverTimeSnap['timestamp'] as Timestamp?;
    if (serverTimestamp == null) {
      throw Exception('サーバー時刻が取得できませんでした');
    }

// サーバー時刻をDateTimeに変換し、JSTに補正（UTCから9時間加算）
    final serverDateTime = serverTimestamp.toDate();
    final jstDateTime = serverDateTime.toUtc().add(const Duration(hours: 9));

    int diffDays = jstDateTime.difference(startDate).inDays;
    int dailyCount = diffDays + 1;
    // dailyCount の上限（dailyCount 運用の場合は最大 130 とする）
    if (dailyCount > 130) dailyCount = 130;
    if (dailyCount < 0) dailyCount = 0;

    // ② カテゴリと各カテゴリ内の出現回数を決定
    String category;
    int appearanceCount; // 該当カテゴリ内の連番
    if (dailyCount % 3 == 1) {
      category = "Debugging";
      appearanceCount = ((dailyCount - 1) ~/ 3) + 1;
    } else if (dailyCount % 3 == 2) {
      category = "Descriptive";
      appearanceCount = ((dailyCount - 2) ~/ 3) + 1;
    } else {
      // dailyCount % 3 == 0
      category = "Implementation";
      appearanceCount = (dailyCount ~/ 3);
    }

    Map<String, dynamic>? questionData = {};

    // ③ 各カテゴリごとの線形補完計算に基づく問題選出と Firestore からの取得
    if (category == "Debugging") {
      // Debugging の場合：問題番号 = round( 2 + 81×(appearanceCount–1)/33 )
      int questionNum = (2 + (81 * (appearanceCount - 1) / 33)).round();
      // 例: questionNum を2桁表示 "question01", "question02", …
      String questionId = "question" + questionNum.toString().padLeft(2, '0');

      DocumentSnapshot docSnap = await firestore
          .collection('questions')
          .doc('Debugging')
          .collection('quiz')
          .doc(questionId)
          .get();

      if (docSnap.exists) {
        questionData = docSnap.data() as Map<String, dynamic>;
        questionData['category'] = 2;
        questionData['documentId'] = questionId;
        questionData['questionName'] = "今日の問題";
      }
    } else if (category == "Implementation") {
      // Implementation の場合：問題番号 = round( 3 + 89×(appearanceCount–1)/32 )
      int questionNum = (3 + (89 * (appearanceCount - 1) / 32)).round();
      String questionId = "question" + questionNum.toString().padLeft(2, '0');

      DocumentSnapshot docSnap = await firestore
          .collection('questions')
          .doc('Implementation')
          .collection('quiz')
          .doc(questionId)
          .get();

      if (docSnap.exists) {
        questionData = docSnap.data() as Map<String, dynamic>;
        questionData['category'] = 1;
        questionData['documentId'] = questionId;
        questionData['questionName'] = "今日の問題";
      }
    } else if (category == "Descriptive") {
      // Descriptive の場合：
      // 　・ doc番号 = ((appearanceCount–1) mod 13) + 1
      // 　・ fieldインデックス = floor((appearanceCount–1) / 13)
      int docNumber = ((appearanceCount - 1) % 13) + 1;
      int fieldIndex = ((appearanceCount - 1) ~/ 13);
      String docId = "questionList" + docNumber.toString(); // 例："questionList1"

      DocumentSnapshot docSnap = await firestore
          .collection('questions')
          .doc('Descriptive')
          .collection('quiz')
          .doc(docId)
          .get();

      if (docSnap.exists) {
        Map<String, dynamic> docData = docSnap.data() as Map<String, dynamic>;
        // 配列フィールド "questionList" から該当インデックスの問題を取得
        List<dynamic> questionList = docData['questionList'];
        if (fieldIndex < questionList.length) {
          docData['questionList'] = [
            questionList[fieldIndex] as Map<String, dynamic>
          ];
          questionData = docData;
          questionData['category'] = 0;
          questionData['documentId'] = docId;
          questionData['fieldIndex'] = fieldIndex;
          questionData['questionListName'] = "今日の問題";
        }
      }
    }

    return questionData;
  }

  /// パスワード再設定ダイアログの表示
  void _showChangePasswordDialog() {
    final TextEditingController newPasswordController = TextEditingController();
    final TextEditingController confirmPasswordController =
        TextEditingController();

    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (ctx) {
        return StatefulBuilder(
          builder: (BuildContext context, StateSetter setState) {
            return AlertDialog(
              title: const Text('パスワードの再設定'),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    obscureText: _obscurePassword,
                    controller: newPasswordController,
                    decoration: InputDecoration(
                      labelText: '新しいパスワードを入力',
                      suffixIcon: IconButton(
                        icon: Icon(
                          _obscurePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            _obscurePassword = !_obscurePassword;
                          });
                        },
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  TextField(
                    obscureText: _obscureConfirmPassword,
                    controller: confirmPasswordController,
                    decoration: InputDecoration(
                      labelText: '確認用パスワードを入力',
                      suffixIcon: IconButton(
                        icon: Icon(
                          _obscureConfirmPassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                        onPressed: () {
                          setState(() {
                            _obscureConfirmPassword = !_obscureConfirmPassword;
                          });
                        },
                      ),
                    ),
                  ),
                ],
              ),
              actions: [
                TextButton(
                  child: const Text('再設定'),
                  onPressed: () async {
                    final newPassword = newPasswordController.text.trim();
                    final confirmPassword =
                        confirmPasswordController.text.trim();

                    if (newPassword.isEmpty || confirmPassword.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('パスワードを入力してください。'),
                        ),
                      );
                      return;
                    }

                    if (newPassword != confirmPassword) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('パスワードが一致しません。'),
                        ),
                      );
                      return;
                    }

                    try {
                      await currentUser!.updatePassword(newPassword);
                      await FirebaseFirestore.instance
                          .collection('users')
                          .doc(currentUser!.uid)
                          .update({'passwordChanged': true});
                      Navigator.of(ctx).pop();
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('パスワードが更新されました。'),
                        ),
                      );
                    } catch (e) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(
                          content: Text('パスワードの更新に失敗しました。'),
                        ),
                      );
                    }
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  /// デイリーミッション確認ボタンタップ時にオーバーレイを表示/非表示
  void _toggleDailyMissionOverlay() {
    setState(() {
      _showDailyMissionOverlay = !_showDailyMissionOverlay;
    });
  }

  /// デイリーミッション確認ボタン
  Widget _buildDailyMissionButton() {
    return GestureDetector(
      onTap: _toggleDailyMissionOverlay,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        decoration: BoxDecoration(
          color: const Color(0xFF6A1B9A),
          borderRadius: BorderRadius.circular(30),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(Icons.flag, color: Colors.white),
            SizedBox(width: 8),
            Text(
              "デイリーミッション確認",
              style: TextStyle(
                fontFamily: 'Nunito',
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// 画面全体を使ったデイリーミッションオーバーレイ
  Widget _buildDailyMissionOverlay() {
    return Positioned.fill(
      child: AnimatedOpacity(
        opacity: _showDailyMissionOverlay ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 250),
        curve: Curves.easeInOut,
        child: IgnorePointer(
          ignoring: !_showDailyMissionOverlay,
          child: Container(
            color: Colors.black54,
            child: SafeArea(
              child: Center(
                child: Container(
                  margin: const EdgeInsets.all(16),
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(16),
                  ),
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // ヘッダー (タイトル & 閉じる)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            "デイリーミッション",
                            style: TextStyle(
                              fontFamily: 'Nunito',
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          GestureDetector(
                            onTap: _toggleDailyMissionOverlay,
                            child:
                                const Icon(Icons.close, color: Colors.black54),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      // ミッション一覧
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: _dailyMissions.map((m) {
                              return MissionItemWidget(
                                key: ValueKey(m.id),
                                mission: m,
                                onClaimed: _onMissionClaimed,
                              );
                            }).toList(),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  /// ミッションをタップしてクレーム（消す）した際の処理
  Future<void> _onMissionClaimed(DailyMission mission) async {
    setState(() {
      _dailyMissions.remove(mission);
    });

    final int oldXp = await gamificationService.getExperience();
    final oldprogressMap = getCurrentLevelProgress(oldXp);
    final oldprogressXp = oldprogressMap['currentProgress'] ?? 0;
    final oldLevel = getLevelFromXp(oldXp);

    await gamificationService.addExperience(mission.reward);

    final int newXp = await gamificationService.getExperience();
    final newLevel = getLevelFromXp(newXp);
    final newprogressMap = getCurrentLevelProgress(newXp);
    final newprogressXp = newprogressMap['currentProgress'] ?? 0;
    final xpNeededForNextLevel = newprogressMap['maxProgress'] ?? 0;

    showXpGainOverlay(
      context: context,
      oldLevel: oldLevel,
      newLevel: newLevel,
      oldXp: oldprogressXp,
      newXp: newprogressXp,
      xpNeededForNextLevel: xpNeededForNextLevel,
      gainedXp: mission.reward,
    );

    setState(() {
      _level = newLevel;
      _currentXP = newprogressXp.toDouble();
      _nextLevelXP = xpNeededForNextLevel.toDouble();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color(0xFF211C54),
            Color(0xFF3B327F),
            Color(0xFF4E4376),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: const IconThemeData(color: Colors.black),
              title: const Text(
                'ホーム',
                style: TextStyle(color: Colors.black),
              ),
              centerTitle: true,
              actions: [
                IconButton(
                  onPressed: () => {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return AlertDialog(
                          title: const Text('ログアウト'),
                          content: const Text('本当にログアウトしますか？'),
                          actions: <Widget>[
                            TextButton(
                              child: const Text('キャンセル'),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                            TextButton(
                              child: const Text('ログアウト'),
                              onPressed: () {
                                Navigator.of(context).pop();
                                AuthService.logout(context);
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  },
                  icon: const Icon(
                    Icons.logout,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 16.0, vertical: 16.0),
                child: Column(
                  children: [
                    // ユーザー情報
                    _buildUserInfoSection(
                      userName: _userName,
                      level: _level,
                      currentXP: _currentXP,
                      nextLevelXP: _nextLevelXP,
                      streak: _streak,
                      point: _point,
                    ),
                    // デイリーミッション確認ボタン
                    _buildDailyMissionButton(),
                    const SizedBox(height: 12),
                    // PageView
                    SizedBox(
                      height: 360,
                      child: PageView(
                        controller: _pageController,
                        onPageChanged: (index) {
                          setState(() {
                            _currentPage = index;
                          });
                        },
                        children: [
                          // 今日の問題
                          _buildDailyQuestionCard(),
                          // ダッシュボード
                          _buildCompactDashboardCard(),
                          // 問い合わせ
                          _buildInquiryCard(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          // オーバーレイ表示
          if (_showDailyMissionOverlay) _buildDailyMissionOverlay(),
        ],
      ),
    );
  }

  // ─────────────────────────────────────────────────────────
  // ユーザー情報カード
  // ─────────────────────────────────────────────────────────
  Widget _buildUserInfoSection({
    required String userName,
    required int level,
    required double currentXP,
    required double nextLevelXP,
    required int streak,
    required int point,
  }) {
    return Stack(
      children: [
        // 背景半透明カード
        Container(
          margin: const EdgeInsets.only(top: 8),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.12),
            borderRadius: BorderRadius.circular(16),
          ),
          padding: const EdgeInsets.only(
            top: 16,
            left: 16,
            right: 16,
            bottom: 16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                userName,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 6),
              // レベル & XP
              Text(
                'レベル: $level   |   XP: ${currentXP.toInt()} / ${nextLevelXP.toInt()}',
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              const SizedBox(height: 16),
              // ストリーク & ポイント
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      const Icon(
                        Icons.whatshot,
                        color: Colors.orangeAccent,
                        size: 32,
                      ),
                      const SizedBox(height: 4),
                      Text(
                        '$streak 日連続',
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const Text(
                        'ストリーク',
                        style: TextStyle(
                          color: Colors.white70,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      const Icon(
                        Icons.monetization_on,
                        color: Colors.yellowAccent,
                        size: 32,
                      ),
                      const SizedBox(height: 4),
                      Text(
                        '$point pt',
                        style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const Text(
                        'ポイント',
                        style: TextStyle(
                          color: Colors.white70,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 16),
              // アチーブメントボタン
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => AchievementScreen()),
                  );
                },
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: const [
                      Icon(
                        Icons.military_tech,
                        color: Colors.purpleAccent,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'アチーブメント',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  // ─────────────────────────────────────────────────────────
  // ページ0: 「今日の問題」カード
  // ─────────────────────────────────────────────────────────
  Widget _buildDailyQuestionCard() {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      color: Colors.white.withOpacity(0.9),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: const Text(
                  'TOPIC!',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 12),
            const Icon(
              Icons.calendar_today,
              size: 80,
              color: Colors.blue,
            ),
            const SizedBox(height: 12),
            const Text(
              '今日の問題',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Colors.black87,
              ),
            ),
            const SizedBox(height: 8),
            const Text(
              '日替わりで出題される問題にチャレンジ',
              style: TextStyle(
                fontSize: 14,
                color: Colors.black54,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 16),
            ElevatedButton.icon(
              onPressed: () async {
                final questionData = await getDailyQuestion();
                // ※ 本来はサーバーやデータベースから今日の問題データを取得する
                // ここではダミーデータを用いて、'type' で問題の種類を指定しています。
                // type: 0 → 用語記述問題, 1 → 実装問題, 2 → デバッグ問題

                int type = questionData['category'] ?? 0;

                if (type == 0) {
                  // 用語記述問題画面へ遷移
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => QuestionDescriptionScreenWidget(
                        questionData: questionData,
                        interruptions: const {},
                      ),
                    ),
                  );
                } else if (type == 1) {
                  // 実装問題画面へ遷移
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => QuestionProgrammingScreenWidget(
                        questionData: questionData,
                        interruption: const {},
                        resumeFromInterruption: false,
                      ),
                    ),
                  );
                } else if (type == 2) {
                  // デバッグ問題画面へ遷移
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => QuestionDebuggingScreenWidget(
                        questionData: questionData,
                        interruption: const {},
                      ),
                    ),
                  );
                }
              },
              icon: const Icon(Icons.play_arrow),
              label: const Text('今すぐ解く'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // ─────────────────────────────────────────────────────────
  // ページ1: ダッシュボードカード (進捗バー付き)
  // ─────────────────────────────────────────────────────────
  Widget _buildCompactDashboardCard() {
    // ダミーデータ: 全体進捗70%, 正答率80%, バッジ数5
    final double overallProgress = 0.70;
    final String accuracy = '80%';
    final int badges = 5;

    // 問題達成の進捗
    final List<_ProblemProgress> problems = [
      _ProblemProgress(title: "用語記述問題", done: 23, total: 80),
      _ProblemProgress(title: "実装問題", done: 12, total: 40),
      _ProblemProgress(title: "デバッグ問題", done: 24, total: 100),
    ];

    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      color: Colors.white.withOpacity(0.9),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // タイトル
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: const Text(
                  'DASHBOARD',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            const Icon(
              Icons.assessment,
              size: 80,
              color: Colors.green,
            ),
            // 全体進捗
            Text(
              '全体進捗: ${(overallProgress * 100).toStringAsFixed(0)}%',
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black87,
              ),
            ),
            const SizedBox(height: 8),
            // 問題進捗バー
            for (var p in problems)
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 2.0),
                child: _buildProgressRow(p.title, p.done, p.total),
              ),
            const SizedBox(height: 4),
            ElevatedButton.icon(
              onPressed: () {
                // 詳細ダッシュボードへ
              },
              icon: const Icon(Icons.arrow_forward),
              label: const Text('詳細を確認'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: Colors.green,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // ─────────────────────────────────────────────────────────
  // ページ2: 問い合わせ画面へのカード (OTHER, オレンジ)
  // ─────────────────────────────────────────────────────────
  Widget _buildInquiryCard() {
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      color: Colors.white.withOpacity(0.9),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // 右上タイトル
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
                decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: const Text(
                  'OTHER',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 12),
            // アイコン
            const Icon(
              Icons.contact_support,
              size: 80,
              color: Colors.orange,
            ),
            const SizedBox(height: 12),
            // 見出し
            const Text(
              'お問い合わせ',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Colors.black87,
              ),
            ),
            const SizedBox(height: 8),
            // 説明文
            const Text(
              '不明点や要望、ご意見は\nこちらからお問い合わせください。',
              style: TextStyle(fontSize: 14, color: Colors.black54),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 16),
            ElevatedButton.icon(
              onPressed: () {
                // 実際の問い合わせ画面へ遷移
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => InquiryFormScreenWidget(),
                  ),
                );
              },
              icon: const Icon(Icons.mail_outline),
              label: const Text('問い合わせフォーム'),
              style: ElevatedButton.styleFrom(
                foregroundColor: Colors.white,
                backgroundColor: Colors.orange,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// 進捗バー（1行）
  Widget _buildProgressRow(String title, int done, int total) {
    final double ratio = (total == 0) ? 0 : (done / total);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '$title  $done / $total',
          style: const TextStyle(fontSize: 14, color: Colors.black87),
        ),
        const SizedBox(height: 4),
        ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: LinearProgressIndicator(
            value: ratio,
            minHeight: 8,
            backgroundColor: Colors.grey[300],
            color: Colors.green,
          ),
        ),
      ],
    );
  }
}

/// 問題達成の進捗モデル
class _ProblemProgress {
  final String title;
  final int done;
  final int total;

  _ProblemProgress({
    required this.title,
    required this.done,
    required this.total,
  });
}

/// 「ミッション完了時に淡く光る & タップでフェードアウト」するWidget
class MissionItemWidget extends StatefulWidget {
  final DailyMission mission;
  final Function(DailyMission) onClaimed;

  const MissionItemWidget({
    Key? key,
    required this.mission,
    required this.onClaimed,
  }) : super(key: key);

  @override
  State<MissionItemWidget> createState() => _MissionItemWidgetState();
}

class _MissionItemWidgetState extends State<MissionItemWidget>
    with TickerProviderStateMixin {
  late AnimationController _glowController;
  late Animation<double> _glowAnimation;

  late AnimationController _scaleController;
  late Animation<double> _scaleAnimation;

  bool _isDismissed = false; // フェードアウト完了フラグ

  @override
  void initState() {
    super.initState();
    // 淡く光るアニメーション (BoxShadow用)
    _glowController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );
    _glowAnimation = Tween<double>(begin: 0, end: 8).animate(
      CurvedAnimation(
        parent: _glowController,
        curve: Curves.easeInOut,
      ),
    );

    // ミッションが完了していればリピート開始
    if (widget.mission.isCompleted) {
      _glowController.repeat(reverse: true);
    }
    // ★ ピンチアウト用のコントローラとアニメーション初期化
    _scaleController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _scaleAnimation = Tween<double>(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(
        parent: _scaleController,
        curve: Curves.easeInOut,
      ),
    );

    // アニメーション完了後にリストから削除
    _scaleController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        widget.onClaimed(widget.mission);
      }
    });
  }

  @override
  void didUpdateWidget(covariant MissionItemWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    // ミッション完了状態が変わった場合にアニメーションを開始/停止
    if (widget.mission.isCompleted && !_glowController.isAnimating) {
      _glowController.repeat(reverse: true);
    } else if (!widget.mission.isCompleted && _glowController.isAnimating) {
      _glowController.stop();
    }
  }

  @override
  void dispose() {
    _glowController.dispose();
    _scaleController.dispose();
    super.dispose();
  }

  /// 完了ミッションをタップしたらフェードアウト
  void _onTap() {
    if (widget.mission.isCompleted && !_isDismissed) {
      _scaleController.forward();
      setState(() {
        _isDismissed = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // 通常時: 光なし
    // 完了時: アニメーションするボックスシャドウ
    return ScaleTransition(
      scale: _scaleAnimation,
      child: GestureDetector(
        onTap: _onTap,
        child: AnimatedBuilder(
          animation: _glowAnimation,
          builder: (context, child) {
            double glow = widget.mission.isCompleted ? _glowAnimation.value : 0;
            return Container(
              margin: const EdgeInsets.symmetric(vertical: 8),
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                color: Colors.grey[100],
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  if (glow > 0)
                    BoxShadow(
                      color: Colors.deepPurple.withOpacity(0.5),
                      blurRadius: glow,
                      spreadRadius: 1,
                    ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // タイトル行
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(widget.mission.icon, color: Colors.deepPurple),
                          const SizedBox(width: 8),
                          Text(
                            widget.mission.title,
                            style: const TextStyle(
                              fontFamily: 'Nunito',
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Colors.black87,
                            ),
                          ),
                        ],
                      ),
                      // 報酬
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8, vertical: 4),
                        decoration: BoxDecoration(
                          color: Colors.green.shade400,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Text(
                          '+${widget.mission.reward}EXP',
                          style: const TextStyle(
                            fontFamily: 'Nunito',
                            color: Colors.white,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(height: 8),
                  // 説明文 + 進捗
                  Text(
                    '${widget.mission.description} '
                    '(${widget.mission.current}/${widget.mission.total})',
                    style: TextStyle(
                      fontFamily: 'Nunito',
                      color: Colors.grey[700],
                      fontSize: 14,
                    ),
                  ),
                  const SizedBox(height: 8),
                  // カスタムプログレスバー
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: LinearProgressIndicator(
                      value: (widget.mission.total == 0)
                          ? 0
                          : widget.mission.current / widget.mission.total,
                      minHeight: 8,
                      backgroundColor: Colors.grey[300],
                      color: Colors.deepPurple,
                    ),
                  ),
                  // ヒント：この下に「タップして受け取る」的な文言を入れても良い
                  if (widget.mission.isCompleted)
                    const Padding(
                      padding: EdgeInsets.only(top: 4),
                      child: Text(
                        'タップして報酬を受け取る',
                        style: TextStyle(
                          fontFamily: 'Nunito',
                          fontSize: 12,
                          color: Colors.deepPurple,
                        ),
                      ),
                    ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
