import 'package:flutter/material.dart';
import 'performance_management_screen_service.dart'; // サービスをインポート
import 'package:get/get.dart'; // Getを使用するためにインポート
import 'question_result_screen_widget.dart';

class PerformanceManagementScreen extends StatefulWidget {
  final int questionId; // 問題識別IDを渡す

  PerformanceManagementScreen({required this.questionId});

  @override
  _PerformanceManagementScreenState createState() =>
      _PerformanceManagementScreenState();
}

class _PerformanceManagementScreenState
    extends State<PerformanceManagementScreen> with TickerProviderStateMixin {
  int _selectedQuestion = 0; // プルダウンで選択された問題ID
  bool _isAscending = true; // ソート順（昇順 or 降順）
  String _sortBy = 'timestamp'; // ソート基準
  int? _selectedQuestionId;
  late String _selectedQuestionName;
  List<Map<String, dynamic>> _performanceHistory = [];
  late AnimationController _animationController;
  final PerformanceManagementScreenService _service =
      PerformanceManagementScreenService();

  late Future<List<Map<String, dynamic>>> _questionsFuture;
  late Future<List<Map<String, dynamic>>> _performanceHistoryFuture;

  @override
  void initState() {
    super.initState();
    // アニメーションコントローラの初期化
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    // 初期状態で問題リストと履歴を取得
    _selectedQuestion = widget.questionId;
    _questionsFuture = _service.getDescriptiveQuestions();

    // 質問リストが取得できたら、選択された質問名を設定
    _questionsFuture.then((questions) {
      final selectedQuestion = questions.firstWhere(
        (question) => question['questionId'] == widget.questionId,
        orElse: () => {'questionId': 0, 'questionName': '不明な質問'},
      );

      if (selectedQuestion != null &&
          selectedQuestion['questionName'] is String) {
        setState(() {
          _selectedQuestionName = selectedQuestion['questionName'];
        });
      } else {
        setState(() {
          _selectedQuestionName = '不明な質問';
        });
      }
    }).catchError((error) {
      print('質問リストの取得中にエラーが発生しました: $error');
      setState(() {
        _selectedQuestionName = 'エラー';
      });
    });

    _performanceHistoryFuture = _service
        .getPerformanceDescriptiveHistory(widget.questionId); // 渡されたIDで履歴を取得
  }

  @override
  void dispose() {
    _animationController.dispose(); // 必ずコントローラーを破棄する
    super.dispose();
  }

  // 問題が選択された時に履歴を更新
  void _onQuestionSelected(int questionId) {
    setState(() {
      _selectedQuestion = questionId;
      _performanceHistoryFuture =
          _service.getPerformanceDescriptiveHistory(_selectedQuestion);
    });
  }

  // ソート機能
  void _sortList(String sortBy) {
    setState(() {
      _isAscending = !_isAscending;
      _sortBy = sortBy;
      _animationController.reset();
      _animationController.forward(); // アニメーションを再生
      _performanceHistory.sort((a, b) {
        int compare;
        if (sortBy == 'timestamp') {
          compare = a['timestamp'].compareTo(b['timestamp']);
        } else {
          compare = a['correctCount'].compareTo(b['correctCount']);
        }
        return _isAscending ? compare : -compare;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      child: Scaffold(
        appBar: AppBar(
          title: Text('成績管理'),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () async {
              Get.until((route) {
                return route.settings.name == '/';
              });
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: FutureBuilder<List<Map<String, dynamic>>>(
                  future: _questionsFuture,
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return CircularProgressIndicator();
                    } else if (snapshot.hasError) {
                      return Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('質問リストの取得に失敗しました。'),
                            SizedBox(height: 10),
                            ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  _questionsFuture =
                                      _service.getDescriptiveQuestions();
                                });
                              },
                              child: Text('再試行'),
                            ),
                          ],
                        ),
                      );
                    } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                      return Text('問題がありません');
                    } else {
                      List<Map<String, dynamic>> questions = snapshot.data!;
                      return _buildStyledDropdownMenu(questions);
                    }
                  },
                ),
              ),
              // ソートボタン
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildSortButton('timestamp', '日付でソート'),
                    _buildSortButton('correctCount', '正解数でソート'),
                  ],
                ),
              ),
              // 成績履歴の表示
              FutureBuilder<List<Map<String, dynamic>>>(
                future: _performanceHistoryFuture,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return CircularProgressIndicator();
                  } else if (snapshot.hasError) {
                    return Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text('成績履歴の取得に失敗しました。'),
                          SizedBox(height: 10),
                          ElevatedButton(
                            onPressed: () {
                              setState(() {
                                _performanceHistoryFuture =
                                    _service.getPerformanceDescriptiveHistory(
                                        _selectedQuestion);
                              });
                            },
                            child: Text('再試行'),
                          ),
                        ],
                      ),
                    );
                  } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
                    return Text('成績履歴がありません');
                  } else {
                    _performanceHistory = snapshot.data!;
                    return AnimatedList(
                      key: GlobalKey(),
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      initialItemCount: _performanceHistory.length,
                      itemBuilder: (context, index, animation) {
                        return _buildAnimatedHistoryItem(
                            _performanceHistory[index], animation);
                      },
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  // ソートボタンのウィジェット
  Widget _buildSortButton(String sortBy, String label) {
    return ElevatedButton(
      onPressed: () => _sortList(sortBy),
      child: Row(
        children: [
          Text(label),
          Icon(_isAscending ? Icons.arrow_upward : Icons.arrow_downward),
        ],
      ),
    );
  }

  // アニメーション付きの成績履歴の項目を表示するウィジェット
  Widget _buildAnimatedHistoryItem(
      Map<String, dynamic> historyItem, Animation<double> animation) {
    return ScaleTransition(
      scale: animation,
      child: GestureDetector(
        onTap: () async {
          try {
            var questionAnswerPairs =
                await _service.fetchSingleDocument(historyItem['docId']);
            if (questionAnswerPairs != null) {
              /*Get.to(() => QuestionResultScreenWidget(
                    questionAnswerPairs: questionAnswerPairs,
                    quesTionListName: _selectedQuestionName,
                    originId: 2,
                    questionId: _selectedQuestion,
                  ));*/
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => QuestionResultScreenWidget(
                          questionAnswerPairs: questionAnswerPairs,
                          quesTionListName: _selectedQuestionName,
                          originId: 2,
                          questionId: _selectedQuestion,
                          xpResult: null,
                        )),
              );
            } else {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text('回答データが見つかりませんでした。')));
            }
          } catch (e) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('データの取得に失敗しました: $e')));
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: [
              BoxShadow(
                color: Colors.black12,
                blurRadius: 6,
                spreadRadius: 1,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // 成績のスコア部分
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '回答日時: ${historyItem['timestamp']}',
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: Colors.grey[600]),
                  ),
                  SizedBox(height: 6),
                  Text(
                    '正解数: ${historyItem['correctCount']}',
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
              // スコア表示
              CircleAvatar(
                radius: 24,
                backgroundColor: _getColorForScore(historyItem['correctCount']),
                child: Text(
                  '${historyItem['correctCount']}',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // スコアに応じた色を返すメソッド
  Color _getColorForScore(int score) {
    if (score >= 8) return Colors.green;
    if (score >= 5) return Colors.orange;
    return Colors.red;
  }

  // ドロップダウンメニューのウィジェット
  Widget _buildStyledDropdownMenu(List<Map<String, dynamic>> questions) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 4,
            spreadRadius: 2,
          ),
        ],
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<int>(
          value: _selectedQuestion,
          hint: Text('Select a question'),
          isExpanded: true,
          icon: Icon(Icons.arrow_drop_down, color: Colors.grey[700]),
          items: questions.map((question) {
            return DropdownMenuItem<int>(
              value: question['questionId'],
              child: Text(question['questionName']),
            );
          }).toList(),
          onChanged: (int? newValue) {
            if (newValue != null) {
              // 選択されたquestionIdに対応するquestionNameを取得
              final selectedQuestion = questions.firstWhere(
                (question) => question['questionId'] == newValue,
                orElse: () => {},
              );

              final questionName =
                  selectedQuestion['questionName'] ?? 'Unknown';

              // 状態を更新
              setState(() {
                _selectedQuestionId = newValue;
                _selectedQuestionName = questionName;
              });
              _onQuestionSelected(newValue);
            }
          },
        ),
      ),
    );
  }
}
