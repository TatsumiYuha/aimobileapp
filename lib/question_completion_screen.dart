import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/xp_gain_overlay.dart';
import 'package:flutter/material.dart';
import 'auth_service.dart';

class CompletionScreen extends StatefulWidget {
  final String compilation;
  final String execution;
  final String specialCheck;
  final VoidCallback onNextChallenge;
  final VoidCallback onFinish;
  final int? debuggingQuestionId;
  final int? implementiveQuestionId;
  final XpGainResult? xpResult;
  final String? questionName;

  CompletionScreen({
    required this.compilation,
    required this.execution,
    required this.specialCheck,
    required this.onNextChallenge,
    required this.onFinish,
    this.debuggingQuestionId,
    this.implementiveQuestionId,
    required this.xpResult,
    this.questionName,
  });

  @override
  State<CompletionScreen> createState() => _CompletionScreenState();
}

class _CompletionScreenState extends State<CompletionScreen> {
  // Overlay表示を管理するための変数
  OverlayEntry? _overlayEntry;
  GamificationService gamificationService = GamificationService();

  @override
  void initState() {
    super.initState();

    if (widget.questionName == "今日の問題") {
      gamificationService.addDailyQuestion();
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _showXpOverlay();
    });
  }

  Future<void> _showXpOverlay() async {
    // ここでは仮にユーザーの現状 XP を 100 と仮定
    // ※ xpResult が null ならデフォルト値を使うなど適宜処理
    final xp = widget.xpResult;
    final oldXp = xp?.oldXp ?? 0;
    final newXp = xp?.newXp ?? 0;
    final oldLevel = xp?.oldLevel ?? 1;
    final newLevel = xp?.newLevel ?? 1;
    final xpNeededForNextLevel = xp?.xpNeededForNextLevel ?? 100;
    final xpGain = (newXp - oldXp);

    await showXpGainOverlay(
      context: context,
      oldLevel: oldLevel,
      newLevel: newLevel,
      oldXp: oldXp,
      newXp: newXp,
      xpNeededForNextLevel: xpNeededForNextLevel,
      gainedXp: xpGain,
    ).then((_) {
      // オーバーレイが消えた後にスナックバーを表示する
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('ポイントを取得しました！')),
      );
    });
    ;
  }

  /// "success" → 緑, "failure" → 赤でハイライトする TextSpan を作る
  List<TextSpan> _buildHighlightedSpans(String text) {
    final List<TextSpan> spans = [];
    // success / failure の出現を正規表現で検索
    final pattern = RegExp(r'(success|failure|passed|failed)');
    int lastIndex = 0;

    for (final match in pattern.allMatches(text)) {
      // マッチ開始位置より前の通常テキストを追加
      if (match.start > lastIndex) {
        spans.add(TextSpan(text: text.substring(lastIndex, match.start)));
      }
      final matchedText = match.group(0);
      Color highlightColor = Colors.white;
      if (matchedText == 'success' || matchedText == 'passed') {
        highlightColor = Colors.green;
      } else if (matchedText == 'failure' || matchedText == 'failed') {
        highlightColor = Colors.red;
      }
      spans.add(TextSpan(
        text: matchedText,
        style: TextStyle(color: highlightColor),
      ));
      lastIndex = match.end;
    }

    // 最後のマッチ以降の文字列を追加
    if (lastIndex < text.length) {
      spans.add(TextSpan(text: text.substring(lastIndex)));
    }
    return spans;
  }

  /// OverlayEntryを生成して返す
  OverlayEntry _createOverlayEntry() {
    return OverlayEntry(
      builder: (context) {
        // 背景をタップしたら Overlay を閉じる
        return GestureDetector(
          onTap: () {
            _overlayEntry?.remove();
            _overlayEntry = null;
          },
          behavior: HitTestBehavior.translucent,
          child: Stack(
            children: [
              // 半透明の背景
              Container(color: Colors.black54),
              // 画面中央にコメントを表示
              Center(
                child: Material(
                  color: Colors.transparent,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    margin: EdgeInsets.all(16.0),
                    padding: EdgeInsets.all(16.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // compilation はそのまま表示
                          Text(
                            widget.compilation,
                            style: TextStyle(fontSize: 14, color: Colors.black),
                          ),
                          SizedBox(height: 8),
                          // execution は一部ハイライト
                          RichText(
                            text: TextSpan(
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black),
                              children:
                                  _buildHighlightedSpans(widget.execution),
                            ),
                          ),
                          SizedBox(height: 8),
                          // specialCheck は一部ハイライト
                          RichText(
                            text: TextSpan(
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black),
                              children:
                                  _buildHighlightedSpans(widget.specialCheck),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  /// Overlayを表示
  void _showOverlay() {
    if (_overlayEntry != null) return; // 連続タップ防止
    _overlayEntry = _createOverlayEntry();
    Overlay.of(context).insert(_overlayEntry!);
  }

  @override
  void dispose() {
    // 画面破棄時にOverlayが残らないようにする
    _overlayEntry?.remove();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final debuggingQuestionId = widget.debuggingQuestionId;
    final implementiveQuestionId = widget.implementiveQuestionId;

    return PopScope(
      canPop: false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            'タスク完了',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          actions: [],
          automaticallyImplyLeading: false,
        ),
        body: Container(
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 20.0),
          color: Colors.white, // 背景色を白に維持
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.task_alt,
                size: 120,
                color: Colors.indigo[600], // アイコンの色
              ),
              SizedBox(height: 30.0),

              // 最後のタスクかどうかで表示メッセージを分ける
              if (debuggingQuestionId != 83 && implementiveQuestionId != 94)
                Text(
                  'タスクが正常に完了しました。',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                  ),
                )
              else
                Text(
                  '最後のタスクが正常に完了しました。',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 22.0,
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              SizedBox(height: 40.0),

              // ▼▼▼ ここでOverlayを使ってコメント表示 ▼▼▼
              GestureDetector(
                onTap: () => _showOverlay(),
                child: Container(
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    color: Colors.grey[100],
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    'タップしてコメントを表示',
                    style: TextStyle(fontSize: 18.0, color: Colors.black87),
                  ),
                ),
              ),

              SizedBox(height: 40.0),

              // 次のタスク or 終了ボタン
              if (debuggingQuestionId != 83 &&
                  implementiveQuestionId != 94 &&
                  widget.questionName != "今日の問題")
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.symmetric(horizontal: 60, vertical: 18),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                  ),
                  onPressed: () {
                    try {
                      widget.onNextChallenge();
                    } catch (e) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content: Text('次のタスクへの移行中にエラーが発生しました: $e')),
                      );
                    }
                  },
                  child: Text(
                    '次のタスクに進む',
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                ),

              SizedBox(height: 20.0),
              TextButton(
                onPressed: () {
                  try {
                    widget.onFinish();
                  } catch (e) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('ホームへの移動中にエラーが発生しました: $e')),
                    );
                  }
                },
                child: Text(
                  '終了してホームに戻る',
                  style: TextStyle(
                    color: Colors.indigo[600],
                    fontSize: 16.0,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
