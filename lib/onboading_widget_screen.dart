import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Onboarding',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BaseScreen(),
    );
  }
}

class BaseScreen extends StatefulWidget {
  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _showOnboarding(context);
    });
  }

  void _showOnboarding(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) => Dialog(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: MediaQuery.of(context).size.height * 0.8,
          child: OnboardingScreen(),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Base Screen')),
      body: Center(
        child: Text('Welcome to Base Screen'),
      ),
    );
  }
}

class OnboardingScreen extends StatefulWidget {
  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  int _currentPage = 0;
  final _controller = PageController();
  final List<Widget> _pages = [
    _buildPage(image: 'assets/Chats.png', message: 'Welcome to Page 1'),
    _buildPage(image: 'assets/Home.png', message: 'This is Page 2'),
    _buildPage(image: 'assets/Profile.png', message: 'Here is Page 3'),
    _buildPage(image: 'assets/Search.png', message: 'Finally, Page 4'),
  ];

  static _buildPage({required String image, required String message}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(image, height: 200.0),
        SizedBox(height: 20.0),
        Text(message),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView.builder(
            itemCount: _pages.length,
            controller: _controller,
            onPageChanged: (index) {
              setState(() {
                _currentPage = index;
              });
            },
            itemBuilder: (_, index) => _pages[index],
          ),
          if (_currentPage == 3)
            Positioned(
              top: 40,
              right: 20,
              child: CloseButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
          Positioned(
            bottom: 30,
            left: MediaQuery.of(context).size.width * 0.5 - 40,
            child: Row(
              children: List.generate(_pages.length, _buildDot),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDot(int index) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 300),
      margin: EdgeInsets.symmetric(horizontal: 5.0),
      height: 10,
      width: 10,
      decoration: BoxDecoration(
        color: _currentPage == index ? Colors.blue : Colors.grey,
        borderRadius: BorderRadius.circular(5),
      ),
    );
  }
}
