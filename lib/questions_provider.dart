import 'package:flutter/material.dart';
import 'question_selection_screen_service.dart';

class QuestionsProvider with ChangeNotifier {
  final Map<int, List<Map<String, dynamic>>> _cache = {};
  final Map<int, List<dynamic>> _completeListCache = {};

  QuestionSelectionScreenService service = QuestionSelectionScreenService();

  Future<List<Map<String, dynamic>>> fetchQuestions(int category) async {
    if (!_cache.containsKey(category)) {
      // カテゴリに基づいて異なるクエリを実行
      var questions = await service.getQuestionsByCategory(category);
      _cache[category] = questions;
      notifyListeners();
    }
    return _cache[category]!;
  }

  void setQuestions(int category, List<Map<String, dynamic>> questions) {
    _cache[category] = questions;
    notifyListeners();
  }

  Future<List<dynamic>> fetchCompleteLists(int category) async {
    // カテゴリに基づいて異なるクエリを実行
    var completeList = await service.getUserCompleteListByCategory(category);
    _completeListCache[category] = completeList;
    notifyListeners();

    return _completeListCache[category]!;
  }

  /// ユーザーの完了リストをリセットするメソッド。
  Future<void> reset() async {
    try {
      // Firestore上の永続的なデータをリセット
      await service.resetUserCompleteLists();

      // メモリ内のキャッシュをクリア
      _cache.clear();
      _completeListCache.clear();

      // UIを更新
      notifyListeners();
    } catch (e) {
      print('Error during reset: $e');
      // 必要に応じてエラーハンドリングを追加
      throw e;
    }
  }
}
