// password_reset_screen.dart
import 'dart:async'; // タイマーを使用するためにインポート
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'auth_service.dart';

class PasswordResetScreen extends StatefulWidget {
  @override
  _PasswordResetScreenState createState() => _PasswordResetScreenState();
}

class _PasswordResetScreenState extends State<PasswordResetScreen> {
  final AuthService _authService = AuthService();
  final TextEditingController _emailController = TextEditingController();
  bool _isLoading = false; // ローディング状態を管理するフラグ
  Timer? _debounceTimer; // デバウンス用のタイマー
  static const Duration _debounceDuration = Duration(seconds: 3); // デバウンス期間

  void _sendPasswordResetEmail() async {
    if (_isLoading) return; // 既にローディング中の場合は処理を中断
    if (_debounceTimer?.isActive ?? false) return; // デバウンス期間中は処理を中断

    setState(() {
      _isLoading = true; // ローディング開始
    });

    // デバウンスタイマーを開始
    _debounceTimer = Timer(_debounceDuration, () {
      setState(() {
        _isLoading = false; // ローディング終了
      });
    });

    try {
      // パスワードリセットメールの送信処理
      await _authService.sendPasswordResetEmail(_emailController.text);

      // 成功メッセージの表示
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('パスワード再設定メールを送信しました。')),
      );

      // 必要に応じて画面を閉じる
      // Navigator.of(context).pop();
    } on FirebaseAuthException catch (e) {
      // FirebaseAuthException を個別にキャッチ
      String errorMessage = 'メールの送信に失敗しました。';
      if (e.code == 'user-not-found') {
        errorMessage = '入力されたメールアドレスのユーザーが見つかりません。';
      } else if (e.code == 'invalid-email') {
        errorMessage = '無効なメールアドレスです。';
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(errorMessage)),
      );
    } catch (e) {
      // エラーハンドリング
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('メールの送信に失敗しました: $e')),
      );
    } finally {
      // ローディング終了はタイマーで管理
      // setState(() {
      //   _isLoading = false;
      // });
      // タイマーが終了した際にローディングを終了するため、ここでは何もしない
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'パスワード再設定',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        centerTitle: true,
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                controller: _emailController,
                keyboardType: TextInputType.emailAddress, // キーボードタイプを追加
                inputFormatters: [
                  FilteringTextInputFormatter.allow(
                    RegExp('[a-zA-Z0-9@._-]'),
                  ), // 半角英数字と特定の記号のみを許可
                ],
                decoration: InputDecoration(labelText: 'メールアドレス'),
              ),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: (_isLoading || (_debounceTimer?.isActive ?? false))
                    ? null
                    : _sendPasswordResetEmail, // パスワードリセットメールの送信
                child: _isLoading
                    ? SizedBox(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(
                          strokeWidth: 2,
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                      )
                    : Text(
                        '再設定メールを送信',
                        style: TextStyle(color: Colors.white),
                      ),
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(Colors.indigo),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _debounceTimer?.cancel(); // タイマーをキャンセル
    super.dispose();
  }
}
