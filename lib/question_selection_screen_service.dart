// question_service.dart
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class QuestionSelectionScreenService {
  FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final User? currentUser = FirebaseAuth.instance.currentUser;

  Future<List<Map<String, dynamic>>> getQuestionsByCategory(
      int category) async {
    try {
      String collectionName;
      String orderBy;
      switch (category) {
        case 0:
          collectionName = 'Descriptive';
          orderBy = 'questionListId';
          break;
        case 1:
          collectionName = 'Implementation';
          orderBy = 'questionId';
          break;
        case 2:
          collectionName = 'Debugging';
          orderBy = 'questionId';
          break;
        default:
          throw Exception('Invalid category');
      }

      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc(collectionName)
          .collection('quiz')
          .orderBy(orderBy)
          .get();

      return querySnapshot.docs
          .map((doc) => doc.data() as Map<String, dynamic>)
          .toList();
    } catch (e) {
      print('Error fetching questions for category $category: $e');
      return [];
    }
  }

  Future<List<dynamic>> getUserCompleteListByCategory(int category) async {
    try {
      String collectionName;
      switch (category) {
        case 0:
          collectionName = 'Descriptive';
          break;
        case 1:
          collectionName = 'Implementation';
          break;
        case 2:
          collectionName = 'Debugging';
          break;
        default:
          throw Exception('Invalid category');
      }

      DocumentSnapshot docSnapshot = await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('userComplete')
          .doc(collectionName)
          .get();

      if (docSnapshot.exists) {
        var data = docSnapshot.data() as Map<String, dynamic>;
        return data['completeList'] ?? [];
      } else {
        return [];
      }
    } catch (e) {
      print(
          'Error fetching complete list for user $currentUser and category $category: $e');
      return [];
    }
  }

  /// ユーザーの完了リストをリセットし、過去ログとして保存するメソッド。
  Future<void> resetUserCompleteLists() async {
    try {
      if (currentUser == null) {
        throw Exception('No user is currently logged in.');
      }

      // カテゴリごとのコレクション名
      final List<String> collectionNames = [
        'Descriptive',
        'Implementation',
        'Debugging'
      ];

      for (String collectionName in collectionNames) {
        DocumentReference currentDocRef = _firestore
            .collection('users')
            .doc(currentUser!.uid)
            .collection('userComplete')
            .doc(collectionName);

        // 現在の完了リストを取得
        DocumentSnapshot currentDocSnapshot = await currentDocRef.get();
        if (currentDocSnapshot.exists) {
          var currentData = currentDocSnapshot.data() as Map<String, dynamic>;
          List<dynamic> completeList = currentData['completeList'] ?? [];

          if (completeList.isNotEmpty) {
            // 過去ログ用コレクションへの参照
            CollectionReference pastLogsCollection = _firestore
                .collection('users')
                .doc(currentUser!.uid)
                .collection('pastCompleteLogs');

            // 過去ログ用ドキュメントを作成
            await pastLogsCollection.add({
              'category': collectionName,
              'timestamp': FieldValue.serverTimestamp(),
              'completeList': completeList,
            });

            print('Archived complete list for category: $collectionName');
          }

          // 現在の完了リストを削除
          await currentDocRef.delete();
          print('Deleted complete list for category: $collectionName');
        } else {
          print(
              'No existing complete list found for category: $collectionName');
        }
      }

      print('All user complete lists have been reset and archived.');
    } catch (e) {
      print('Error resetting user complete lists: $e');
      throw e; // エラーを再スローして上位で処理
    }
  }

  Future<List<Map<String, dynamic>>> fetchDescriptiveQuestions() async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc('Descriptive')
          .collection('quiz')
          .orderBy('questionListId')
          .get();

      return querySnapshot.docs
          .map((doc) => doc.data() as Map<String, dynamic>)
          .toList();
    } catch (e) {
      print('Error fetching descriptive questions: $e');
      return [];
    }
  }

  Future<List<Map<String, dynamic>>> fetchImplementiveQuestions() async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc('Implementation')
          .collection('quiz')
          .orderBy('questionId')
          .get();

      return querySnapshot.docs
          .map((doc) => doc.data() as Map<String, dynamic>)
          .toList();
    } catch (e) {
      print('Error fetching implementive questions: $e');
      return [];
    }
  }

  Future<Map<String, dynamic>> fetchImplementiveNextQuestion(int id) async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc('Implementation')
          .collection('quiz')
          .where('questionId', isEqualTo: id + 1) // questionIdフィールドをチェック
          .limit(1) // 一致する最初のドキュメントのみを取得
          .get();

      if (querySnapshot.docs.isNotEmpty) {
        return querySnapshot.docs.first.data() as Map<String, dynamic>;
      } else {
        print('No question found with id: ${id + 1}');
        return {};
      }
    } catch (e) {
      print('Error fetching implementive question with id ${id + 1}: $e');
      return {};
    }
  }

  Future<List<Map<String, dynamic>>> fetchDebuggingQuestions() async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc('Debugging')
          .collection('quiz')
          .orderBy('questionId')
          .get();
      return querySnapshot.docs
          .map((doc) => doc.data() as Map<String, dynamic>)
          .toList();
    } catch (e) {
      print('Error fetching debugging questions: $e');
      return [];
    }
  }

  Future<Map<String, dynamic>> fetchDebuggingNextQuestion(int id) async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc('Debugging')
          .collection('quiz')
          .where('questionId', isEqualTo: id + 1) // questionIdフィールドをチェック
          .limit(1) // 一致する最初のドキュメントのみを取得
          .get();

      if (querySnapshot.docs.isNotEmpty) {
        return querySnapshot.docs.first.data() as Map<String, dynamic>;
      } else {
        print('No question found with id: ${id + 1}');
        return {};
      }
    } catch (e) {
      print('Error fetching debugging question with id ${id + 1}: $e');
      return {};
    }
  }

  Future<Map<String, dynamic>> getUserPerformance(
      int selectedQuestionType) async {
    String questionType;

    switch (selectedQuestionType) {
      case 0:
        questionType = 'Descriptive';
        break;
      case 1:
        questionType = 'Implementation';
        break;
      case 2:
        questionType = 'Debugging';
        break;
      default:
        throw Exception(' invalid selectedQuestionType');
    }
    var performanceSnapshot = await _firestore
        .collection('users')
        .doc(currentUser?.uid)
        .collection('userPerformance')
        .where('questionType', isEqualTo: questionType)
        .get();

    if (performanceSnapshot.docs.isNotEmpty) {
      return await calculatePerformance(performanceSnapshot);
    } else {
      return {'answeredCount': 0, 'correctCount': 0, 'totalQuestions': 0};
    }
  }

  Future<Map<String, double>> calculatePerformance(
      QuerySnapshot performanceSnapshot) async {
    num totalAnswered = 0;
    num totalCorrect = 0;
    num totalQuestions = 0;

    for (var doc in performanceSnapshot.docs) {
      var data = doc.data() as Map<String, dynamic>;
      totalAnswered += data['answeredCount'] ?? 0;
      totalCorrect += data['correctCount'] ?? 0;
      totalQuestions += data['totalQuestions'] ?? 0;
    }

    if (totalQuestions > 0) {
      double answeredRate = (totalAnswered / totalQuestions) * 100;
      double correctRate = (totalCorrect / totalQuestions) * 100;
      return {'answeredRate': answeredRate, 'correctRate': correctRate};
    } else {
      return {'answeredRate': 0, 'correctRate': 0};
    }
  }
}
