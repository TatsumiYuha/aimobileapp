// auth_service.dart
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'auth_screen_widget.dart';
import 'dart:math';

class AuthService {
  // シングルトンインスタンス
  static final AuthService _instance = AuthService._internal();
  factory AuthService() {
    return _instance;
  }
  AuthService._internal();
  static final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _db = FirebaseFirestore.instance;
  final FlutterSecureStorage _storage = FlutterSecureStorage();

  // ログイン成功時に呼び出される
  static Future<void> _setLoginFlag(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isLoggedIn', value);
  }

  /// ランダムなセッションIDを生成するメソッド
  String _generateSessionId() {
    const chars =
        'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    Random rnd = Random();
    return String.fromCharCodes(Iterable.generate(
        20, (_) => chars.codeUnitAt(rnd.nextInt(chars.length))));
  }

  /// ログインロジックを実行するメソッド
  /// 成功した場合は {'success': true} を返し、失敗した場合は {'success': false, 'message': 'エラーメッセージ'} を返します。
  Future<Map<String, dynamic>> login(String email, String password) async {
    try {
      final UserCredential userCredential =
          await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      // セッションIDを生成
      String sessionId = _generateSessionId();

      // FirestoreにセッションIDを保存
      DocumentReference userRef =
          _db.collection('users').doc(userCredential.user!.uid);
      await userRef.set(
        {
          'loginCount': FieldValue.increment(1),
          'currentSessionId': sessionId,
        },
        SetOptions(merge: true),
      );

      // ローカルにセッションIDを保存
      await _storage.write(key: 'sessionId', value: sessionId);

      // ログイン成功時にフラグをセット
      await _setLoginFlag(true);

      // 入力されたメールアドレスとパスワードをセキュアストレージに保存
      await _storage.write(key: 'email', value: email);
      await _storage.write(key: 'password', value: password);

      // セッション監視を開始
      startSessionListener(userCredential.user!.uid, sessionId); // メソッド名を修正

      return {'success': true}; // ログイン成功
    } on FirebaseAuthException catch (e) {
      // Firebase特有のエラーハンドリング
      String errorMessage = 'ログインに失敗しました。';
      if (e.code == 'user-not-found') {
        errorMessage = 'ユーザーが見つかりません。';
      } else if (e.code == 'wrong-password') {
        errorMessage = 'パスワードが間違っています。';
      } else if (e.code == 'network-request-failed') {
        errorMessage = 'ネットワークエラーが発生しました。インターネット接続を確認してください。'; // 追加行
      }
      // エラーメッセージを返す
      return {'success': false, 'message': errorMessage};
    } catch (e) {
      // その他のエラーハンドリング
      return {'success': false, 'message': 'ログインに失敗しました。'};
    }
  }

  // セッションIDの監視を行うメソッド（パブリックに変更）
  void startSessionListener(String uid, String sessionId) {
    _db.collection('users').doc(uid).snapshots().listen((snapshot) async {
      if (snapshot.exists) {
        String? currentSessionId = snapshot.data()!['currentSessionId'];
        String? localSessionId = await _storage.read(key: 'sessionId');
        if (currentSessionId != null &&
            localSessionId != null &&
            currentSessionId != localSessionId) {
          // セッションが異なる場合、強制ログアウト
          logoutForced();
        }
      }
    });
  }

  // 強制ログアウトを実行するメソッド（既にパブリック）
  void logoutForced() async {
    // ログアウト時にフラグをクリア
    await _setLoginFlag(false);

    // SharedPreferencesの設定をリセット
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('activeBottomBarItem', 'チャット');

    // セキュアストレージのセッションIDを削除
    await _storage.delete(key: 'sessionId');

    // すべてのルートをクリアして、ログインページに遷移します。
    Get.offAll(() => AuthScreenWidget());

    // ユーザーに通知
    Get.snackbar(
      '強制ログアウト',
      '別の端末からログインされたため、ログアウトされました。',
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: Colors.redAccent,
      colorText: Colors.white,
    );
  }

  // ログアウトメソッドの追加
  static Future<void> logout(BuildContext context) async {
    await _auth.signOut();

    // ログアウト時にフラグをクリア
    await _setLoginFlag(false);

    // SharedPreferencesの設定をリセット
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('activeBottomBarItem', 'チャット');

    // セキュアストレージのセッションIDを削除
    final FlutterSecureStorage storage = FlutterSecureStorage();
    await storage.delete(key: 'sessionId');

    // すべてのルートをクリアして、ログインページに遷移します。
    Get.offAll(() => AuthScreenWidget());
  }

  // パスワード再設定用メソッドの追加
  Future<void> sendPasswordResetEmail(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      // メール送信成功
    } on FirebaseAuthException catch (e) {
      // 修正行：FirebaseAuthException をキャッチ
      String errorMessage = 'メールの送信に失敗しました。';
      if (e.code == 'network-request-failed') {
        // 追加行
        errorMessage = 'ネットワークエラーが発生しました。インターネット接続を確認してください。'; // 追加行
      }
      throw Exception(errorMessage); // 修正行
    } catch (error) {
      // メール送信失敗
      throw Exception("メールの送信に失敗しました");
    }
  }

  // ログインとトークンを確認するメソッド
  Future<bool> isUserAuthenticatedAndTokenValid() async {
    final user = FirebaseAuth.instance.currentUser;
    if (user != null) {
      try {
        final idTokenResult = await user.getIdTokenResult(true);
        // トークンが正常に取得できれば、ユーザーは認証され、アカウントも有効
        return idTokenResult.token != null;
      } catch (e) {
        // トークン取得時にエラーが発生した場合
        print('Error getting token: $e');
        return false;
      }
    } else {
      // ユーザーがログインしていない
      return false;
    }
  }
}
