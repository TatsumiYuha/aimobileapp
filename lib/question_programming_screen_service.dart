import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'auth_service.dart';

class QuestionProgrammingScreenService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final User? currentUser = FirebaseAuth.instance.currentUser;
  final authService = AuthService();
  // 粒度高の問題の場合
  Future<String> sendProgrammingHighTask(
      http.Client client,
      List<Map<String, dynamic>> subTasks,
      int currentSubTaskIndex,
      String code) async {
    try {
      List<dynamic> subTaskList = [];
      for (Map<String, dynamic> subTask in subTasks) {
        subTaskList.add(subTask['subTask']);
      }

      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        return "認証トークンが無効です。再ログインしてください。"; // ここで処理を終了
      }
      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp/judge_high'),
        body: utf8.encode(jsonEncode({
          'sub_tasks': subTaskList,
          'currentSubTaskIndex': currentSubTaskIndex,
          'code': code,
        })),
        headers: {"Content-Type": "application/json"},
      );
      if (response.statusCode == 200) {
        return utf8.decode(response.bodyBytes);
      } else {
        throw Exception('Failed to load post');
      }
    } catch (e) {
      print(e);
      throw Exception('Error occurred while sending data: $e');
    }
  }

  // ====================================================
  //採点結果をSharedPreferencesに保存
  // ====================================================
  Future<void> storeGradingResult(int questionId, String rawJson) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    // 「採点結果JSON」を保存
    await prefs.setString('implementation_gradingResult_$questionId', rawJson);

    // 未読フラグをtrueに設定
    await prefs.setBool('implementation_gradingResultUnread_$questionId', true);
  }

  // ====================================================
  // 採点結果JSONを取得
  // ====================================================
  Future<String?> getGradingResult(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('implementation_gradingResult_$questionId');
  }

  // ====================================================
  // 採点結果が未読かどうかを確認
  // ====================================================
  Future<bool> isGradingResultUnread(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('implementation_gradingResultUnread_$questionId') ??
        false;
  }

  // ====================================================
  // 採点結果を既読とする
  // ====================================================
  Future<void> markGradingResultAsRead(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(
        'implementation_gradingResultUnread_$questionId', false);
  }

  // 粒度低の問題の場合
  Future<String> sendProgrammingLowTask(
      http.Client client, String subTask, String userInput) async {
    try {
      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        return "認証トークンが無効です。再ログインしてください。"; // ここで処理を終了
      }
      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp/judge_low'),
        body: utf8.encode(jsonEncode({
          'sub_task': subTask,
          'user_input': userInput,
        })),
        headers: {"Content-Type": "application/json"},
      );
      if (response.statusCode == 200) {
        return utf8.decode(response.bodyBytes);
      } else {
        throw Exception('Failed to load post');
      }
    } catch (e) {
      throw Exception('Error occurred while sending data: $e');
    }
  }

  // コードを保存するメソッド
  Future<void> saveUserCode(String code, int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('programming_${questionId}_user_code', code);
  }

  // コードを読み込むメソッド
  Future<String> loadUserCode(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('programming_${questionId}_user_code') ?? '';
  }

  Future<void> addNumberToCompleteList(int questionId) async {
    DocumentReference userCompleteRef = _firestore
        .collection('users')
        .doc(currentUser?.uid)
        .collection('userComplete')
        .doc('Implementation');

    try {
      // ドキュメントの現在のデータを取得
      DocumentSnapshot snapshot = await userCompleteRef.get();

      if (snapshot.exists) {
        Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
        List<dynamic> currentList = data['completeList'] as List<dynamic>;
        //リストに既に同一のIDが存在する場合は登録しない
        if (!currentList.contains(questionId)) {
          // 新しい数値をリストに追加
          currentList.add(questionId);
          // 更新されたリストをFirestoreに保存
          await userCompleteRef.update({'completeList': currentList});
        }
      } else {
        // ドキュメントが存在しない場合、新しいドキュメントを作成
        await userCompleteRef.set({
          'completeList': [questionId]
        });
      }
    } catch (e) {
      print('Error adding number to complete list: $e');
      throw e; // またはエラーを適切に処理
    }
  }
}
