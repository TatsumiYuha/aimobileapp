import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'auth_service.dart';
import 'custom_bottom_app_bar.dart';
import 'package:provider/provider.dart';
import 'inquiry_provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'inquiry_list_screen_widget.dart';

class InquiryFormScreenWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => InquiryFormProvider(),
      child: PopScope(
        canPop: false,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: Colors.black),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                // 前の画面に戻る
                Get.back();
              },
            ),
            title: Text(
              'お問い合わせ',
              style: TextStyle(color: Colors.black),
            ),
            centerTitle: true,
            actions: [],
          ),
          body: _InquiryFormScreenWidget(),
        ),
      ),
    );
  }
}

class _InquiryFormScreenWidget extends StatelessWidget {
  final _formKey = GlobalKey<FormBuilderState>();
  final FirebaseAuth _auth = FirebaseAuth.instance;

  void _submitForm(BuildContext context) {
    final provider = Provider.of<InquiryFormProvider>(context, listen: false);

    if (_formKey.currentState?.saveAndValidate() ?? false) {
      provider.startLoading(); // ローディング開始

      final formData = _formKey.currentState?.value;
      final User? user = _auth.currentUser;
      final userEmail = user?.email ?? 'Unknown';

      // Firestore へのデータ保存を直接行う
      FirebaseFirestore.instance.collection('inquiries').add({
        'timestamp': FieldValue.serverTimestamp(),
        'userEmail': userEmail, // Emailを取得するが、表示はしない
        'type': formData?['type'], // カテゴリー（例：要望、不具合、その他）
        'content': formData?['content'], // 問い合わせの内容
        'status': '新規', // デフォルトの状態
        'severity': '', //重度、初期は空白
        'subtype': '未分類', // サブカテゴリー、初期は空白とする
        'adminComment': '', // 初期は空白とする
        'title': '' // 初期は空白とする
      }).then((value) {
        provider.stopLoading(); // ローディング終了
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('登録完了'),
            content: Text('問い合わせが正常に登録されました。'),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('OK'),
              ),
            ],
          ),
        );
      }).catchError((error) {
        provider.stopLoading(); // ローディング終了
        // エラー時の処理
        // エラー時の処理
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('エラー'),
            content: Text('問い合わせの登録中にエラーが発生しました。'),
            actions: [
              TextButton(
                onPressed: () => Navigator.of(context).pop(),
                child: Text('OK'),
              ),
            ],
          ),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final isLoading = Provider.of<InquiryFormProvider>(context).isLoading;

    return GestureDetector(
      onTap: () {
        // キーボードを閉じる
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(16.0), // ここで全体のパディングを設定
          child: isLoading
              ? Center(child: CircularProgressIndicator())
              : Column(
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: () {
                        // 要望リスト画面へ遷移する
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => InquiryListScreenWidget()),
                        );
                      },
                      child: Text('要望リストを見る'),
                    ),
                    SizedBox(height: 16.0),
                    Expanded(
                      child: FormBuilder(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  bottom: 8.0), // 各ウィジェット間のスペース
                              child: FormBuilderDropdown(
                                name: 'type',
                                decoration: InputDecoration(
                                  labelText: '問い合わせの種類',
                                  hintText: '選択してください',
                                  border: OutlineInputBorder(), // フィールドに境界線を追加
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 5), // フィールド内のパディング
                                ),
                                validator: FormBuilderValidators.required(
                                    errorText: 'このフィールドの入力は必須です'),
                                items: ['要望', '不具合', 'その他']
                                    .map((type) => DropdownMenuItem(
                                        value: type, child: Text("$type")))
                                    .toList(),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: FormBuilderTextField(
                                enableSuggestions: true,
                                name: 'content',
                                keyboardType: TextInputType.multiline,
                                decoration: InputDecoration(
                                  labelText: '内容',
                                  border: OutlineInputBorder(), // フィールドに境界線を追加
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 10,
                                      vertical: 5), // フィールド内のパディング),
                                ),
                                maxLines: 5,
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(
                                      errorText: 'このフィールドの入力は必須です'),
                                ]),
                              ),
                            ),
                            ElevatedButton(
                              onPressed: () => _submitForm(context),
                              child: Text('送信'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
        ),
      ),
    );
  }
}
