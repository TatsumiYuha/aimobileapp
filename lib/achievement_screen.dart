import 'dart:ui'; // ぼかし効果に必要なImageFilter用
import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/level_data.dart';
import 'package:ai_mobile_app/xp_gain_overlay.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart'; // ステータスバーのスタイル変更に使用
import 'package:cloud_firestore/cloud_firestore.dart';

// アチーブメントデータのモデルクラス
class AchievementData {
  final String id;
  final String title; // アチーブメント名
  final String condition; // 解除条件の説明
  final int current; // 現在の進捗値
  final int goal; // 達成に必要な値
  final int rewardXp;
  final int rewardPoint;
  final bool claimed;
  AchievementData({
    required this.id,
    required this.title,
    required this.condition,
    required this.current,
    required this.goal,
    required this.rewardXp,
    required this.rewardPoint,
    this.claimed = false,
  });
  bool get isUnlocked => current >= goal;
}

// 各アチーブメントカード（達成済みの場合はアニメーション付き）
class AchievementItem extends StatefulWidget {
  final Future<void> Function(AchievementData) onClaimed;
  final AchievementData achievement;
  const AchievementItem(
      {Key? key, required this.achievement, required this.onClaimed})
      : super(key: key);

  @override
  _AchievementItemState createState() => _AchievementItemState();
}

class _AchievementItemState extends State<AchievementItem>
    with TickerProviderStateMixin {
  late final AnimationController _glowController;
  late final Animation<double> _glowAnimation;

  bool get _shouldAnimate =>
      widget.achievement.isUnlocked && !widget.achievement.claimed;

  @override
  void initState() {
    super.initState();
    if (_shouldAnimate) {
      // 達成済みの場合、グローとスケールのアニメーションを設定
      _glowController = AnimationController(
        vsync: this,
        duration: const Duration(seconds: 1),
      )..repeat(reverse: true);

      _glowAnimation = Tween<double>(begin: 0, end: 20).animate(
        CurvedAnimation(parent: _glowController, curve: Curves.easeInOut),
      );
    } else {
      // 未達成の場合は固定値とする
      _glowController = AnimationController(
          vsync: this, duration: const Duration(milliseconds: 0));
      _glowAnimation = AlwaysStoppedAnimation(0.0);
    }
  }

  @override
  void didUpdateWidget(covariant AchievementItem oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (_shouldAnimate !=
        (oldWidget.achievement.isUnlocked && !oldWidget.achievement.claimed)) {
      if (_shouldAnimate) {
        _glowController.duration = const Duration(seconds: 1);
        _glowController.repeat(reverse: true);
      } else {
        _glowController.stop();
      }
    }
  }

  @override
  void dispose() {
    _glowController.dispose();
    super.dispose();
  }

  void _onTap() async {
    if (_shouldAnimate) {
      // 達成済みならタップ時に報酬獲得などの処理を実行（ここではデモとしてprint）
      await widget.onClaimed(widget.achievement);
    }
  }

  @override
  Widget build(BuildContext context) {
    // カード内コンテンツ
    Widget cardContent = Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          // アイコンとタイトル、条件の行
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(
                Icons.emoji_events,
                size: 40,
                color: widget.achievement.isUnlocked
                    ? Colors.amber.shade300
                    : Colors.grey.shade800,
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.achievement.title,
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ),
                    const SizedBox(height: 4),
                    Text(
                      widget.achievement.condition,
                      style: const TextStyle(
                        fontSize: 14,
                        color: Colors.black54,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(height: 12),
          // 進捗バーと数値表示
          Row(
            children: [
              Expanded(
                child: LinearProgressIndicator(
                  value: widget.achievement.current / widget.achievement.goal,
                  minHeight: 8,
                  backgroundColor: Colors.white.withOpacity(0.3),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    widget.achievement.isUnlocked
                        ? Colors.amber.shade400
                        : Colors.purpleAccent,
                  ),
                ),
              ),
              const SizedBox(width: 8),
              Text(
                '${widget.achievement.current}/${widget.achievement.goal}',
                style: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                  color: Colors.black87,
                ),
              ),
            ],
          ),
          // 達成済みの場合はヒント表示
          if (_shouldAnimate)
            const Padding(
              padding: EdgeInsets.only(top: 8.0),
              child: Text(
                'タップして報酬を獲得',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.deepPurple,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
        ],
      ),
    );

    // 達成済みの場合はアニメーション付きのウィジェットでラップ
    if (_shouldAnimate) {
      return GestureDetector(
        onTap: _onTap,
        child: AnimatedBuilder(
          animation: _glowAnimation,
          builder: (context, child) {
            double glowValue = _glowAnimation.value;
            return Container(
              margin: const EdgeInsets.symmetric(vertical: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  if (glowValue > 0)
                    BoxShadow(
                      color: Colors.deepPurple.withOpacity(0.5),
                      blurRadius: glowValue,
                      spreadRadius: 1,
                    ),
                ],
              ),
              child: child,
            );
          },
          child: Hero(
            tag: widget.achievement.id,
            child: Container(
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                border: Border.all(color: Colors.white.withOpacity(0.3)),
                color: Colors.white.withOpacity(0.2),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
                child: Material(
                  color: Colors.transparent,
                  child: cardContent,
                ),
              ),
            ),
          ),
        ),
      );
    }
    // 未達成の場合は従来の見た目で表示
    return Hero(
      tag: widget.achievement.id,
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              blurRadius: 10,
              offset: const Offset(0, 4),
            ),
          ],
        ),
        clipBehavior: Clip.antiAlias,
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Colors.white.withOpacity(0.3)),
              color: Colors.white.withOpacity(0.2),
            ),
            child: Material(
              color: Colors.transparent,
              child: cardContent,
            ),
          ),
        ),
      ),
    );
  }
}

class AchievementScreen extends StatefulWidget {
  const AchievementScreen({Key? key}) : super(key: key);

  @override
  _AchievementScreenState createState() => _AchievementScreenState();
}

class _AchievementScreenState extends State<AchievementScreen> {
  final gamificationService = GamificationService();
  final User? currentUser = FirebaseAuth.instance.currentUser;
  bool isLoading = true;
  // アチーブメントリストは初期状態では空リスト
  List<AchievementData> achievements = [];

  // -------------------------------
  // タップ時に報酬獲得処理を実行
  // （gamificationService等は既実装の前提）
  // -------------------------------
  Future<void> _onAchievementClaimed(AchievementData achievement) async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser?.uid)
        .collection('achievements')
        .doc(achievement.id)
        .set({'claimed': true}, SetOptions(merge: true));

    setState(() {
      int index = achievements.indexWhere((a) => a.id == achievement.id);
      if (index != -1) {
        achievements[index] = AchievementData(
          id: achievement.id,
          title: achievement.title,
          condition: achievement.condition,
          current: achievement.current,
          goal: achievement.goal,
          rewardXp: achievement.rewardXp,
          rewardPoint: achievement.rewardPoint,
          claimed: true,
        );
      }
    });

    final int oldXp = await gamificationService.getExperience();
    final oldProgressMap = getCurrentLevelProgress(oldXp);
    final oldProgressXp = oldProgressMap['currentProgress'] ?? 0;
    final oldLevel = getLevelFromXp(oldXp);

    await gamificationService.addExperience(achievement.rewardXp);
    await gamificationService.addPoint(achievement.rewardPoint);

    final int newXp = await gamificationService.getExperience();
    final newLevel = getLevelFromXp(newXp);
    final newProgressMap = getCurrentLevelProgress(newXp);
    final newProgressXp = newProgressMap['currentProgress'] ?? 0;
    final xpNeededForNextLevel = newProgressMap['maxProgress'] ?? 0;

    showXpGainOverlay(
      context: context,
      oldLevel: oldLevel,
      newLevel: newLevel,
      oldXp: oldProgressXp,
      newXp: newProgressXp,
      xpNeededForNextLevel: xpNeededForNextLevel,
      gainedXp: achievement.rewardXp,
    ).then((_) {
      // オーバーレイが消えた後にスナックバーを表示する
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('ポイントを取得しました！')),
      );
    });
    ;
  }

  Future<int> fetchCurrentProgress(String category) async {
    final userDoc = FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser?.uid); // 現在のユーザーIDを使用
    final achievementDoc =
        await userDoc.collection('userComplete').doc(category).get();

    if (achievementDoc.exists) {
      final data = achievementDoc.data();
      if (data != null && data['completeList'] is List) {
        return (data['completeList'] as List).length;
      }
    }
    return 0;
  }

  Future<int> fetchMaxStreak() async {
    final userDoc =
        FirebaseFirestore.instance.collection('users').doc(currentUser?.uid);
    final snapshot = await userDoc.get();
    if (snapshot.exists) {
      final data = snapshot.data();
      // もし maxStreak が未登録の場合は 0 を返す
      return data?['maxStreak'] ?? 0;
    }
    return 0;
  }

  @override
  void initState() {
    super.initState();
    _loadAchievements();
  }

  Future<void> _loadAchievements() async {
    final descriptiveProgress = await fetchCurrentProgress('Descriptive');
    final implementationProgress = await fetchCurrentProgress('Implementation');
    final debuggingProgress = await fetchCurrentProgress('Debugging');
    final maxStreak = await fetchMaxStreak();

    final achievementSnapshot = await FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser?.uid)
        .collection('achievements')
        .get();

    final Map<String, bool> claimedMap = {};
    for (var doc in achievementSnapshot.docs) {
      claimedMap[doc.id] = (doc.data()['claimed'] ?? false) as bool;
    }

    setState(() {
      achievements = [
        AchievementData(
          id: 'description1',
          title: 'Basic Description',
          condition: '用語記述問題を1問コンプリートする',
          current: descriptiveProgress,
          goal: 1,
          rewardXp: 50,
          rewardPoint: 50,
          claimed: claimedMap['description1'] ?? false,
        ),
        AchievementData(
          id: 'description2',
          title: 'Concept Description',
          condition: '用語記述問題を3問コンプリートする',
          current: descriptiveProgress,
          goal: 3,
          rewardXp: 100,
          rewardPoint: 100,
          claimed: claimedMap['description2'] ?? false,
        ),
        AchievementData(
          id: 'description3',
          title: 'Applied Description',
          condition: '用語記述問題を5問コンプリートする',
          current: descriptiveProgress,
          goal: 5,
          rewardXp: 150,
          rewardPoint: 150,
          claimed: claimedMap['description3'] ?? false,
        ),
        AchievementData(
          id: 'description4',
          title: 'Wordsmith',
          condition: '用語記述問題をすべてコンプリートする',
          current: descriptiveProgress,
          goal: 13,
          rewardXp: 200,
          rewardPoint: 200,
          claimed: claimedMap['description4'] ?? false,
        ),
        AchievementData(
          id: 'implementation1',
          title: 'Basic Implementation',
          condition: '実装問題を10問コンプリートする',
          current: implementationProgress,
          goal: 10,
          rewardXp: 50,
          rewardPoint: 50,
          claimed: claimedMap['implementation1'] ?? false,
        ),
        AchievementData(
          id: 'implementation2',
          title: 'Standard Implementation',
          condition: '実装問題を30問コンプリートする',
          current: implementationProgress,
          goal: 30,
          rewardXp: 100,
          rewardPoint: 100,
          claimed: claimedMap['implementation2'] ?? false,
        ),
        AchievementData(
          id: 'implementation3',
          title: 'Advanced Implementation',
          condition: '実装問題を50問コンプリートする',
          current: implementationProgress,
          goal: 50,
          rewardXp: 150,
          rewardPoint: 150,
          claimed: claimedMap['implementation3'] ?? false,
        ),
        AchievementData(
          id: 'implementation4',
          title: 'Optimization',
          condition: '実装問題をすべてコンプリートする',
          current: implementationProgress,
          goal: 92,
          rewardXp: 200,
          rewardPoint: 200,
          claimed: claimedMap['implementation4'] ?? false,
        ),
        AchievementData(
          id: 'debugging1',
          title: 'Initial Verification',
          condition: 'デバッグ問題を10問コンプリートする',
          current: debuggingProgress,
          goal: 10,
          rewardXp: 50,
          rewardPoint: 50,
          claimed: claimedMap['debugging1'] ?? false,
        ),
        AchievementData(
          id: 'debugging2',
          title: 'Detailed Analysis',
          condition: 'デバッグ問題を30問コンプリートする',
          current: debuggingProgress,
          goal: 30,
          rewardXp: 100,
          rewardPoint: 100,
          claimed: claimedMap['debugging2'] ?? false,
        ),
        AchievementData(
          id: 'debugging3',
          title: 'Implemented Correction',
          condition: 'デバッグ問題を50問コンプリートする',
          current: debuggingProgress,
          goal: 50,
          rewardXp: 150,
          rewardPoint: 150,
          claimed: claimedMap['debugging3'] ?? false,
        ),
        AchievementData(
          id: 'debugging4',
          title: 'Resolution',
          condition: 'デバッグ問題をすべてコンプリートする',
          current: debuggingProgress,
          goal: 83,
          rewardXp: 200,
          rewardPoint: 200,
          claimed: claimedMap['debugging4'] ?? false,
        ),
        // ストリーク系は別途固定値のまま
        AchievementData(
          id: 'streak1',
          title: 'First Step',
          condition: '3日間連続でアプリを起動する',
          current: maxStreak,
          goal: 3,
          rewardXp: 50,
          rewardPoint: 50,
          claimed: claimedMap['streak1'] ?? false,
        ),
        AchievementData(
          id: 'streak2',
          title: 'Halfway There',
          condition: '7日間連続でアプリを起動する',
          current: maxStreak,
          goal: 7,
          rewardXp: 100,
          rewardPoint: 100,
          claimed: claimedMap['streak2'] ?? false,
        ),
        AchievementData(
          id: 'streak3',
          title: 'Steady Ascent',
          condition: '14日間連続でアプリを起動する',
          current: maxStreak,
          goal: 14,
          rewardXp: 150,
          rewardPoint: 150,
          claimed: claimedMap['streak3'] ?? false,
        ),
        AchievementData(
          id: 'streak4',
          title: 'Forward',
          condition: '30日間連続でアプリを起動する',
          current: maxStreak,
          goal: 30,
          rewardXp: 200,
          rewardPoint: 200,
          claimed: claimedMap['streak4'] ?? false,
        ),
      ];
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    // 背景用のグラデーション（ブルーからパープル）
    final backgroundGradient = LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        const Color(0xFF8EC5FC), // 明るい青
        const Color(0xFFE0C3FC), // 明るい紫
      ],
    );

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: const Text(
          'アチーブメント',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        // ステータスバーの文字色をダークモード（背景が明るいため黒系に）
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: isLoading
          ? const Center(child: CircularProgressIndicator())
          : Container(
              decoration: BoxDecoration(gradient: backgroundGradient),
              child: ListView.builder(
                padding: const EdgeInsets.fromLTRB(16, 100, 16, 32),
                itemCount: achievements.length,
                itemBuilder: (context, index) {
                  return AchievementItem(
                    achievement: achievements[index],
                    onClaimed: _onAchievementClaimed,
                  );
                },
              ),
            ),
    );
  }
}
