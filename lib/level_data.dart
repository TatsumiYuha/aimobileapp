class LevelData {
  final int level;
  final int cumulativeXp; // 累積経験値
  final int xpForThisLevel; // このレベルに到達するために必要な経験値量（Lv毎の差分）

  LevelData({
    required this.level,
    required this.cumulativeXp,
    required this.xpForThisLevel,
  });
}

// アプリ内の定数として保持
final List<LevelData> levelTable = [
  LevelData(level: 1, cumulativeXp: 0, xpForThisLevel: 25),
  LevelData(level: 2, cumulativeXp: 25, xpForThisLevel: 25),
  LevelData(level: 3, cumulativeXp: 50, xpForThisLevel: 50),
  LevelData(level: 4, cumulativeXp: 100, xpForThisLevel: 50),
  LevelData(level: 5, cumulativeXp: 150, xpForThisLevel: 75),
  LevelData(level: 6, cumulativeXp: 225, xpForThisLevel: 75),
  LevelData(level: 7, cumulativeXp: 300, xpForThisLevel: 100),
  LevelData(level: 8, cumulativeXp: 400, xpForThisLevel: 100),
  LevelData(level: 9, cumulativeXp: 500, xpForThisLevel: 150),
  LevelData(level: 10, cumulativeXp: 650, xpForThisLevel: 150),
  LevelData(level: 11, cumulativeXp: 800, xpForThisLevel: 175),
  LevelData(level: 12, cumulativeXp: 975, xpForThisLevel: 175),
  LevelData(level: 13, cumulativeXp: 1150, xpForThisLevel: 200),
  LevelData(level: 14, cumulativeXp: 1350, xpForThisLevel: 200),
  LevelData(level: 15, cumulativeXp: 1550, xpForThisLevel: 250),
  LevelData(level: 16, cumulativeXp: 1800, xpForThisLevel: 250),
  LevelData(level: 17, cumulativeXp: 2050, xpForThisLevel: 300),
  LevelData(level: 18, cumulativeXp: 2350, xpForThisLevel: 300),
  LevelData(level: 19, cumulativeXp: 2650, xpForThisLevel: 300),
  LevelData(level: 20, cumulativeXp: 2950, xpForThisLevel: 350),
  LevelData(level: 21, cumulativeXp: 3300, xpForThisLevel: 400),
  LevelData(level: 22, cumulativeXp: 3700, xpForThisLevel: 400),
  LevelData(level: 23, cumulativeXp: 4100, xpForThisLevel: 400),
  LevelData(level: 24, cumulativeXp: 4500, xpForThisLevel: 500),
  LevelData(level: 25, cumulativeXp: 5000, xpForThisLevel: 500),
  LevelData(level: 26, cumulativeXp: 5500, xpForThisLevel: 750),
  LevelData(level: 27, cumulativeXp: 6250, xpForThisLevel: 1000),
  LevelData(level: 28, cumulativeXp: 7250, xpForThisLevel: 1250),
  LevelData(level: 29, cumulativeXp: 8500, xpForThisLevel: 1500),
  LevelData(level: 30, cumulativeXp: 10000, xpForThisLevel: 20000),
];

// ユーザーの累積経験値から現在レベルを判定する関数
int getLevelFromXp(int userXp) {
  // レベルテーブルを後ろから走査して、一番小さいcumulativeXpを超えているレベルを返す
  for (int i = levelTable.length - 1; i >= 0; i--) {
    if (userXp >= levelTable[i].cumulativeXp) {
      return levelTable[i].level;
    }
  }
  return 1; // 万が一全て下回る場合はレベル1
}

/// 現在レベルの進捗(レベル内の獲得経験値)と、レベル内の必要経験値(上限)を取得
Map<String, int> getCurrentLevelProgress(int userXp) {
  final currentLevel = getLevelFromXp(userXp);
  // 現在レベルのデータを取得
  final currentData = levelTable.firstWhere(
    (l) => l.level == currentLevel,
    orElse: () => levelTable.first,
  );

  // 最終レベルの場合は「次のレベル」は存在しないため、xpForThisLevel=0 とする
  // （最大レベル到達でカンスト扱い）
  if (currentLevel == levelTable.last.level) {
    return {
      'currentProgress': currentData.xpForThisLevel, // あるいは 0
      'maxProgress': currentData.xpForThisLevel, // あるいは 0
    };
  }

  // レベル内の開始地点
  final levelStartXp = currentData.cumulativeXp;
  // レベル内の進捗
  final currentProgress = userXp - levelStartXp;
  // レベル内に必要な総量
  final maxProgress = currentData.xpForThisLevel;

  return {
    'currentProgress': currentProgress,
    'maxProgress': maxProgress,
  };
}
