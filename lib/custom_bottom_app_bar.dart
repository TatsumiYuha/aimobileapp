import 'package:flutter/material.dart';
import 'bottom_nav_controller.dart';
import 'package:get/get.dart';
import 'chat_room_selection_screen_widget.dart';
import 'question_selection_screen_widget.dart';
import 'glossary_screen_widget.dart';
import 'inquiry_form_screen_widget.dart';

class CustomBottomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final BottomNavController bottomNavController =
        Get.find<BottomNavController>();
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      notchMargin: 4.0,
      child: Container(
        height: 60,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            _buildIconButton(
              iconData: Icons.emoji_emotions,
              label: "チャット",
              activeItem: bottomNavController.activeItem.value,
              onTap: () {
                if (bottomNavController.activeItem.value != "チャット") {
                  bottomNavController.setActiveItem("チャット");
                  Get.offAll(() => ChatRoomSelectionWidget());
                }
              },
            ),
            _buildIconButton(
              iconData: Icons.widgets,
              label: "問題",
              activeItem: bottomNavController.activeItem.value,
              onTap: () {
                if (bottomNavController.activeItem.value != "問題") {
                  bottomNavController.setActiveItem("問題");
                  Get.offAll(() => QuestionSelectionScreenWidget());
                }
              },
            ),
            _buildIconButton(
              iconData: Icons.book,
              label: "用語集",
              activeItem: bottomNavController.activeItem.value,
              onTap: () {
                if (bottomNavController.activeItem.value != "用語集") {
                  bottomNavController.setActiveItem("用語集");
                  Get.offAll(() => GlossaryScreenWidget());
                }
              },
            ),
            _buildIconButton(
              iconData: Icons.contact_mail, // 問い合わせフォームを示唆するアイコン
              label: "要望", // ボタンのラベル
              activeItem: bottomNavController.activeItem.value, // 現在のアクティブなアイテム
              onTap: () {
                // 現在のアクティブなアイテムが「問い合わせフォーム」でない場合に限り実行
                if (bottomNavController.activeItem.value != "要望") {
                  bottomNavController
                      .setActiveItem("要望"); // アクティブなアイテムを「問い合わせフォーム」に設定
                  Get.offAll(
                      () => InquiryFormScreenWidget()); // 問い合わせフォームの画面に遷移
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  void _showNotImplementedDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("未実装の機能"),
          content: Text("この機能は現在実装中です。"),
          actions: <Widget>[
            TextButton(
              child: Text("閉じる"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildIconButton({
    required IconData iconData,
    required String label,
    required String activeItem,
    void Function()? onTap,
  }) {
    return InkWell(
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            iconData,
            color: activeItem == label ? Colors.blue : null,
          ),
          Text(
            label,
            style: TextStyle(color: activeItem == label ? Colors.blue : null),
          ),
        ],
      ),
    );
  }
}
