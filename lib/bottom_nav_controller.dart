import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BottomNavController extends GetxController {
  // アクティブなアイテムを表す RxString
  var activeItem = "チャット".obs;

  BottomNavController() {
    loadInitialActiveItem();
  }

  // SharedPreferences から取得した値で更新
  Future<void> loadInitialActiveItem() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? savedItem = prefs.getString('activeBottomBarItem');
    if (savedItem != null) {
      activeItem.value = savedItem;
    }
  }

  // アクティブなアイテムを更新するメソッド
  void setActiveItem(String item) {
    activeItem.value = item;
    _saveToPrefs(item);
  }

  Future<void> _saveToPrefs(String item) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('activeBottomBarItem', item);
  }
}
