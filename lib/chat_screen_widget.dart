// chat_screen_widget.dart
import 'package:ai_mobile_app/xp_gain_overlay.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'chat_screen_service.dart'; // Serviceクラスのインポート
import 'dart:async'; // StreamControllerのためのインポート
import 'package:shared_preferences/shared_preferences.dart'; // 検索履歴用
import 'auth_service.dart';
import 'route_observer.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

// チュートリアル関連補助ファイル
import 'tutorial_progress_manager.dart';
import 'blue_square_overlay.dart';
import 'help_overlay_service.dart';
import 'animation_build_display.dart';

class ChatScreenWidget extends StatefulWidget {
  // 通常時に必要なプロパティ
  final String roomId; // チャットルームのID
  final String roomName; // チャットルームの名前

  // リザルトから遷移時に必要なプロパティ
  final bool showDetail;
  final bool? isCorrect;
  final String? questionText;
  final String? userAnswer;
  final String? commentary;
  bool isDetail;
  final String? modelAnswer;

  // 通常時コンストラクタ
  ChatScreenWidget({
    required this.roomId,
    required this.roomName,
  })  : showDetail = false,
        isDetail = false,
        isCorrect = null,
        questionText = null,
        userAnswer = null,
        commentary = null,
        modelAnswer = null;

  // リザルトから遷移時のコンストラクタ
  ChatScreenWidget.detail({
    required this.roomId,
    required this.roomName,
    required this.isDetail,
    this.showDetail = true,
    this.isCorrect,
    this.questionText,
    this.userAnswer,
    this.commentary,
    this.modelAnswer,
  });

  @override
  _ChatScreenWidgetState createState() => _ChatScreenWidgetState();
}

class _ChatScreenWidgetState extends State<ChatScreenWidget>
    with RouteAware, WidgetsBindingObserver {
  final TextEditingController _controller = TextEditingController();
  final ChatScreenService _service = ChatScreenService(); // Serviceクラスのインスタンス
  final User? currentUser = FirebaseAuth.instance.currentUser;

  final StreamController<List<QueryDocumentSnapshot>> _streamController =
      StreamController<List<QueryDocumentSnapshot>>.broadcast();
  int _dotIndex = 0; // 現在表示しているドットのインデックス
  bool _isSending = false; // メッセージ送信時のローディング
  bool _isFetchingMessages = false; // メッセージリストのローディング
  bool _hasText = false; // テキストフィールドに入力があるか
  bool _showScrollButton = false; // スクロールボタンの表示状態
  DocumentSnapshot? _lastDocument; // ページネーション用の最後のドキュメント
  List<QueryDocumentSnapshot<Object?>> _allMessages = []; // 全メッセージリスト
  bool _isToggleVisible = false; // 詳細セクションの表示状態
  bool isAnswerVisible = false; // 模範解答セクションの表示状態

  // 検索機能関連の変数
  bool _isSearching = false; // 検索バーの表示状態
  TextEditingController _searchController =
      TextEditingController(); // 検索入力用コントローラー
  String _searchQuery = ""; // 現在の検索クエリ
  List<int> _searchResults = []; // 検索結果のインデックスリスト
  int _currentSearchIndex = 0; // 現在選択中の検索結果インデックス

  // 検索履歴
  List<String> _searchHistory = [];

  // スクロールすべきメッセージのインデックス（遅延スクロール用）
  int? _pendingScrollToIndex;

  // スクロールコントローラーの置き換え
  final ItemScrollController _itemScrollController = ItemScrollController();
  final ItemPositionsListener _itemPositionsListener =
      ItemPositionsListener.create();

  Timer? _debounce; // デバウンスタイマー

  // クイックレスポンス関連の変数
  static List<String> staticQuickResponses = [
    "Javaの型とは？",
    "継承はどう使うのか？",
    "例外処理の書き方は？",
    "パッケージ構成例は？",
    "if文は何ができる？"
  ];
  List<String> _quickResponses = [];

  int _quickResponseTapCount = 0; //チュートリアル時にクイックチャットをタップした回数

  ////////////// チュートリアル／オンボーディング用の変数 //////////////
  final tutorialProgressManager _tutorial = tutorialProgressManager();
  bool _isTutorialActive = false; // 操作説明（チュートリアル）が実行中か
  bool _isOnboadingActive = false; // オンボーディング中か
  bool _isShowDialog = false; // 「操作説明を開始しますか？」のダイアログ表示
  bool _isCondition = false; // 操作が完了したかの一時フラグ
  bool showHelp = false; // ヘルプオーバーレイ表示用
  bool _isVisible = false; // 操作説明での青い枠表示制御

  int _currentStep = 0; // 操作説明（チュートリアル）進行管理
  int _guidanceNum = 0; // ガイダンス進行管理（オンボーディング用）

  // ガイダンス用のメッセージ（オンボーディング初期表示）
  final List<String> _guidance = [
    "ようこそ！この画面ではチャットを楽しむことができます。",
    "下部の入力欄にメッセージを入力し、送信ボタンをタップしてみましょう。",
    "クイックレスポンスボタンを使えば、メッセージをすぐに送信できます。",
    "上部の検索アイコンをタップすると、チャット内のメッセージを探せます。",
    "画面を下に引っ張ると、最新のメッセージに更新されます。",
    "左上の戻るボタンで、前の画面に戻ることができます。",
    "以上で操作方法のご説明は終了です。",
  ];

  // 各操作説明（チュートリアル）用のステップ
  final List<Map<String, dynamic>> _steps = [
    {'text': 'まずは、下部の入力欄に「Java」入力してみましょう。', 'requires': 'message'},
    {'text': '送信ボタンをタップして、入力したメッセージを送信してください。', 'requires': 'send'},
    {
      'text': 'クイックレスポンスボタンを5回タップして、メッセージを送信してみましょう。',
      'requires': 'quick_response'
    },
    {'text': '上部の検索アイコンをタップしてみましょう。', 'requires': 'search_icon'},
    {'text': '上部の検索バーに「Java」と入力し、メッセージを検索してみましょう。', 'requires': 'search'},
    {'text': '表示された検索結果をタップして内容を確認してください。', 'requires': 'search_result'},
    {
      'text': '右下の最新メッセージボタンをタップして、最新のメッセージにジャンプしてみましょう。',
      'requires': 'latest_message'
    },
    {'text': '再度、上部の検索アイコンをタップして検索モードに入り、', 'requires': 'search_icon'},
    {'text': '表示される検索履歴から過去のキーワードを選択してみましょう。', 'requires': 'search_history'},
    {'text': '検索バー内の「×」アイコンをタップして、検索内容をクリアしてください。', 'requires': 'search_clear'},
    {'text': 'これでチャット画面の基本操作は終了です。', 'requires': null},
  ];

  // ヘルプオーバーレイ用メッセージなど（そのまま）
  final List<String> helpMessages = [
    "検索バー: チャットルームを検索できます",
    "並び替えオプション: リストの順序を変更できます",
    "リスト: 各チャットルームの詳細が確認できます",
    "スワイプ: 削除・編集・お気に入り設定が可能です",
    "ドラッグ: チャットルームの並び替えができます",
    "FAB: 新しいチャットルームを作成します",
  ];

  final List<String> backgroundImages = [
    'assets/Help_search.png',
    'assets/Help_sort.png',
    'assets/Help_list.png',
    'assets/Help_slidable.png',
    'assets/Help_reorder.png',
    'assets/Help_fab.png',
  ];

  final List<Alignment> messageAlignment = [
    Alignment.topCenter,
    Alignment.topCenter,
    Alignment.center,
    Alignment.center,
    Alignment.center,
    Alignment.bottomCenter,
  ];

  final List<EdgeInsets> margin = [
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(bottom: 20),
  ];

  // ヘルプ用：ハイライト矩形（例示用の固定値）
  final List<Rect> rectangleSteps = [
    Rect.fromLTWH(20, 100, 360, 50), // 検索バー
    Rect.fromLTWH(20, 160, 360, 50), // 並び替えドロップダウン
    Rect.fromLTWH(20, 220, 360, 400), // チャットルームリスト
    Rect.fromLTWH(300, 600, 60, 60), // FAB（新規作成ボタン）
    Rect.fromLTWH(0, 0, 0, 0), // （オプション）
    Rect.fromLTWH(0, 0, 0, 0), // （オプション）
  ];

////////// 対象ウィジェットのGlobalKey ////////////
  final GlobalKey _searchIconKey = GlobalKey(); // 検索アイコン
  final GlobalKey _searchBarKey = GlobalKey(); // 検索バー（キーワード入力領域）
  final GlobalKey _searchClearKey = GlobalKey(); // 検索バー内の「×」アイコン
  final GlobalKey _searchHistoryKey = GlobalKey(); // 検索履歴リスト（またはリスト全体のコンテナ）
  final GlobalKey _searchResultItemKey =
      GlobalKey(); // 検索結果の各メッセージ（例：先頭の結果を対象とする）
  final GlobalKey _latestMessageButtonKey = GlobalKey(); // 最新メッセージに戻るための右下ボタン
  final GlobalKey _messageKey = GlobalKey(); // 入力欄
  final GlobalKey _sendKey = GlobalKey(); // 送信アイコン
  final GlobalKey _messageSendKey = GlobalKey();
  final GlobalKey _quickResponseKey = GlobalKey();
  final GlobalKey _backKey = GlobalKey();
////////// ここまで

  //////////// ガイダンス用オーバーレイ変数 ////////////
  double _guidanceOverlayTop = 0.0;
  double _guidanceOverlayLeft = 0.0;
  double _guidanceOverlayWidth = 0.0;
  double _guidanceOverlayHeight = 0.0;
  //////////// ここまで

  //////////// 操作説明用オーバーレイ変数 ////////////
  double _tutorialOverlayTop = 0.0;
  double _tutorialOverlayLeft = 0.0;
  double _tutorialOverlayWidth = 0.0;
  double _tutorialOverlayHeight = 0.0;
  //////////// ここまで

  GlobalKey? _getGuidanceTargetKey() {
    switch (_guidanceNum) {
      case 1:
        return _messageSendKey; // 検索バーの説明
      case 2:
        return _quickResponseKey; // 検索結果の表示（タップ対象）
      case 3:
        return _searchIconKey; // 検索アイコン // 最新メッセージに戻るボタン
      case 4:
        return null;
      case 5:
        return _backKey; // 戻るアイコン
      default:
        return null;
    }
  }

  // ガイダンス用の対象ウィジェットの位置とサイズを更新する
  void _updateGuidanceOverlayPosition() {
    GlobalKey? targetKey = _getGuidanceTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _guidanceOverlayTop = globalPosition.dy;
      _guidanceOverlayLeft = globalPosition.dx;
      _guidanceOverlayWidth = size.width;
      _guidanceOverlayHeight = size.height;
    });
  }

  //////////// 操作説明用の対象ウィジェットのGlobalKeyを返す関数 ////////////
  GlobalKey? _getTutorialTargetKey() {
    final String? req = _steps[_currentStep]['requires'];
    if (req == null) return null;
    switch (req) {
      case 'message_send':
        return _messageSendKey;
      case 'quick_response':
        return _quickResponseKey;
      case 'search':
        return _searchBarKey;
      case 'search_clear':
      case 'search_icon':
        return _searchIconKey;
      case 'search_history':
        return _searchHistoryKey;
      case 'search_result':
        return _searchResultItemKey;
      case 'latest_message':
        return _latestMessageButtonKey;
      case 'back':
        return _backKey;
      case 'message':
        return _messageKey;
      case 'send':
        return _sendKey;
      default:
        return null;
    }
  }

  // 操作説明用の対象ウィジェットの位置とサイズを更新する（新規）
  void _updateTutorialOverlayPosition() {
    GlobalKey? targetKey = _getTutorialTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      if (_steps[_currentStep]['requires'] == 'slidable') {
        Future.delayed(Duration(milliseconds: 100), () {
          _updateTutorialOverlayPosition();
        });
      }
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _tutorialOverlayTop = globalPosition.dy;
      _tutorialOverlayLeft = globalPosition.dx;
      _tutorialOverlayWidth = size.width;
      _tutorialOverlayHeight = size.height;
    });
  }
  //////////// ここまで操作説明用

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    if (widget.isDetail) {
      _quickResponses = ["この問題を解説してください"];
      //_sendDetailMessage();
    }

    if (widget.isDetail) {
      setState(() {
        _quickResponses = ["この問題を解説してください"];
      });
    } else {
      setState(() {
        _quickResponses = staticQuickResponses;
      });
    }

    _loadData();
    _loadSearchHistory();

    // テキストフィールドの内容を監視して、入力があるかをチェック
    _controller.addListener(() {
      setState(() {
        _hasText = _controller.text.trim().isNotEmpty;
      });
    });

    // ItemPositionsListenerを使用してスクロール位置を監視
    _itemPositionsListener.itemPositions.addListener(_scrollListener);

    // 検索コントローラーのリスナー
    _searchController.addListener(() {
      setState(() {
        _searchQuery = _searchController.text.trim();
        _performSearch();
      });
      // デバウンス処理の開始
      if (_debounce?.isActive ?? false) _debounce!.cancel();
      _debounce = Timer(const Duration(milliseconds: 3500), () {
        if (_searchQuery.isNotEmpty && !_searchHistory.contains(_searchQuery)) {
          setState(() {
            _searchHistory.add(_searchQuery);
            // 検索履歴のサイズを制限（オプション）
            if (_searchHistory.length > 10) {
              _searchHistory.removeAt(0);
              print('Search history exceeded 10 items. Removed oldest entry.');
            }
            _saveSearchHistory();
            print('Added "$_searchQuery" to search history.');
          });
        }
      });
    });

    // デバッグ用・初回起動時として強制的にオンボーディング（ガイダンス）を開始
    _checkAndStartTutorial();
    _loadProgress();
  }

  Future<void> _checkAndStartTutorial() async {
    // ここでは画面ごとに一意なキーを用いる（例：ChatScreenWidget の場合はクラス名や固有のID）
    // 複数の画面で管理する場合、各画面で異なるキーを使用してください。
    final String tutorialKey = 'tutorialShown_${this.runtimeType}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstTime = prefs.getBool(tutorialKey) ?? true;
    if (isFirstTime) {
      // 初回の場合のみチュートリアル／ガイダンスを発火させる
      setState(() {
        _isOnboadingActive = true;
        _guidanceNum = 0;
      });
      // 画面描画後にオーバーレイ位置を更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
      // 一度発火したのでフラグを更新（2回目以降は発火しない）
      await prefs.setBool(tutorialKey, false);
    }
  }

  Future<void> _loadProgress() async {
    int progress = await _tutorial.getProgress('currentStep');
    if (progress < _steps.length) {
      setState(() {
        _currentStep = progress;
      });
    }
  }

  Future<void> _saveProgress(int step) async {
    await _tutorial.saveProgress('currentStep', step);
  }

  Future<void> _resetProgress() async {
    setState(() {
      _currentStep = 0;
    });
    await _tutorial.resetProgress();
  }

  Future<void> _saveTutorialStatus() async {
    await _tutorial.saveTutorialBoolStatus(_isTutorialActive);
    await _tutorial.saveOnboadingBoolStatus(_isOnboadingActive);
    await _tutorial.saveShowDialogBoolStatus(_isShowDialog);
  }

  void _getTutorial() async {
    bool futureTutorialActive = await _tutorial.loadTutorialBoolStatus();
    bool futureOnboardingActive = await _tutorial.loadOnboadingBoolStatus();
    bool futureShowDialogActive = await _tutorial.loadShowDialogBoolStatus();
    setState(() {
      _isTutorialActive = futureTutorialActive;
      _isOnboadingActive = futureOnboardingActive;
      _isShowDialog = futureShowDialogActive;
    });
  }

  void _startTutorial() {
    setState(() {
      _currentStep = 0;
      _isTutorialActive = true;
      _isOnboadingActive = false;
      _isShowDialog = false;
    });
    _saveProgress(0);
    _saveTutorialStatus();
    _checkVisible();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _endTutorial() {
    setState(() {
      _isTutorialActive = false;
      _isOnboadingActive = false;
      _isShowDialog = false;
      _currentStep = 0;
      _isVisible = false;
    });
    _resetProgress();
    _saveTutorialStatus();
  }

  void _guidanceStep() {
    if (_guidanceNum < _guidance.length - 1) {
      setState(() {
        _guidanceNum++;
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
    } else {
      setState(() {
        _isShowDialog = true;
        _isOnboadingActive = true;
      });
    }
  }

  void _noTap() {
    setState(() {
      _currentStep = _steps.length - 1;
      _isTutorialActive = true;
    });
  }

  void _endGuidance() {
    setState(() {
      _isShowDialog = false;
      _isOnboadingActive = false;
    });
  }

  void _nextStep() {
    if (_currentStep < _steps.length - 1) {
      setState(() {
        _currentStep++;
        _isCondition = false;
      });
      _saveProgress(_currentStep);
    } else {
      _endTutorial();
      _saveProgress(_currentStep);
    }
    _checkVisible();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _checkAndProceed() {
    final requires = _steps[_currentStep]['requires'];
    if (requires == null || _isCondition) {
      _nextStep();
    }
  }

  void _conditionMet() {
    if (_isTutorialActive) {
      setState(() {
        _isCondition = true;
      });
      _nextStep();
    }
  }

  // 従来の青い枠表示制御（操作説明用）
  void _checkVisible() {
    if (_currentStep >= 0 && _currentStep <= 9) {
      setState(() {
        _isVisible = true;
      });
    } else {
      setState(() {
        _isVisible = false;
      });
    }
  }
  //////////////////////////////////////////////////////////////////////

  void _scrollListener() {
    // 現在表示されているアイテムの位置を取得
    final positions = _itemPositionsListener.itemPositions.value;

    if (positions.isNotEmpty) {
      // 最下部（最新メッセージ）が表示されているかどうかを判定
      bool isAtTop = positions.any((position) => position.index == 0);

      // スクロール位置が一定以上離れている場合にボタンを表示
      if (!isAtTop && !_showScrollButton) {
        setState(() {
          _showScrollButton = true;
        });
      } else if (isAtTop && _showScrollButton) {
        setState(() {
          _showScrollButton = false;
        });
      }
    }
  }

  @override
  void didChangeMetrics() {
    super.didChangeMetrics();
    // キーボードの表示／非表示を検知（viewInsets.bottom が0ならキーボード非表示）
    final bottomInset = WidgetsBinding.instance.window.viewInsets.bottom;
    if (bottomInset == 0.0) {
      // キーボードが下がったので、オーバーレイ位置を再更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateTutorialOverlayPosition();
      });
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context)!);
  }

  // ルートからポップされたときにルームの存在をチェック
  @override
  void didPop() {
    super.didPop();
    _checkAndDeleteRoom();
  }

  // ルートがポップされた際のチェック
  @override
  void didPopNext() {
    super.didPopNext();
    _checkAndDeleteRoom();
  }

  // 新しいルートがプッシュされた際のチェック
  @override
  void didPushNext() {
    super.didPushNext();
    _checkAndDeleteRoom();
  }

  // ルームの存在をチェックし、条件に応じて削除
  void _checkAndDeleteRoom() async {
    if (currentUser == null) return;

    final chatRoomRef = FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser!.uid)
        .collection('chatRooms')
        .doc(widget.roomId);

    final chatRoomData = await chatRoomRef.get();

    if (chatRoomData.exists && !(chatRoomData['isNameUpdated'] ?? false)) {
      await chatRoomRef.delete();
      print('Chat room ${widget.roomId} deleted.');
    } else {
      print('Chat room ${widget.roomId} exists and isNameUpdated is true.');
    }
  }

  // 検索履歴をロード
  Future<void> _loadSearchHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _searchHistory = prefs.getStringList('searchHistory') ?? [];
      print('Loaded search history: $_searchHistory');
    });
  }

  // 検索履歴を保存
  Future<void> _saveSearchHistory() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setStringList('searchHistory', _searchHistory);
    print('Saved search history: $_searchHistory');
  }

  // スクロール位置をリセット
  void _resetScrollPosition() {
    // リストの最後のインデックスにスクロールして画面の上部に移動
    _itemScrollController.scrollTo(
      index: 0,
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
    );
    print('Scroll position reset to top.');
  }

  // メッセージリストを結合し、重複を排除
  List<QueryDocumentSnapshot> combineLists(
      List<QueryDocumentSnapshot> list1, List<QueryDocumentSnapshot> list2) {
    List<QueryDocumentSnapshot> combinedList = [];
    final seenIds = Set<String>();

    for (var doc in list1) {
      if (!seenIds.contains(doc.id)) {
        seenIds.add(doc.id);
        combinedList.add(doc);
      }
    }

    for (var doc in list2) {
      if (!seenIds.contains(doc.id)) {
        seenIds.add(doc.id);
        combinedList.add(doc);
      }
    }

    return combinedList;
  }

  // メッセージデータをロード
  void _loadData() {
    if (!_streamController.isClosed) {
      setState(() {
        _isFetchingMessages = true; // メッセージ取得開始
      });
      _service.getChatMessages(widget.roomId, currentUser).listen((docs) {
        if (!_streamController.isClosed) {
          setState(() {
            _allMessages = combineLists(docs, _allMessages);
            // docsが空でないことを確認
            if (docs.isNotEmpty) {
              _lastDocument = docs.last; // 最後のメッセージのドキュメントを格納
            } else {
              _lastDocument = null; // または適切なデフォルト値を設定
              print('docs is empty. _lastDocument is set to null.');
            }
            print(
                'Loaded ${docs.length} new messages. Total messages: ${_allMessages.length}');
          });

          if (docs.isNotEmpty) {
            final latestMessage = docs.first;
            final gptResponse = latestMessage['chat_gpt_response'];

            if (gptResponse != null && _isSending) {
              setState(() {
                _isSending = false; // メッセージ送信完了
                print('GPT response received. Stopping loading animation.');
              });
            }
          }

          // 検索クエリがある場合、再度検索を実行
          if (_searchQuery.isNotEmpty) {
            _performSearch();
          }

          // StreamController に全メッセージを流す
          _streamController.add(_allMessages);
          print('StreamController updated with all messages.');

          setState(() {
            _isFetchingMessages = false; // メッセージ取得完了
          });
        }
      }, onError: (error) {
        setState(() {
          _isFetchingMessages = false; // エラー時も完了とする
        });
        // エラーハンドリングを追加（ユーザーに通知）
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('メッセージの取得に失敗しました: $error')),
        );
        print('Error fetching messages: $error');
      });
    }
  }

  @override
  void dispose() {
    _checkAndDeleteRoom(); // ウィジェットが破棄されるときにもチェック
    routeObserver.unsubscribe(this);
    _streamController.close();
    _controller.dispose();
    _searchController.dispose();
    _debounce?.cancel();
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  // ローディングアニメーションのウィジェット
  Widget _loadingAnimation() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List.generate(3, (index) {
        return AnimatedContainer(
          duration: Duration(milliseconds: 300),
          margin: EdgeInsets.symmetric(horizontal: 2.0),
          width: 12.0,
          height: 12.0,
          decoration: BoxDecoration(
            color: _dotIndex == index ? Colors.blue : Colors.grey,
            shape: BoxShape.circle,
          ),
        );
      }),
    );
  }

  // ローディングアニメーションを開始
  void _startLoadingAnimation() {
    Future.delayed(Duration(milliseconds: 400), () {
      if (_isSending) {
        setState(() {
          _dotIndex = (_dotIndex + 1) % 3;
          print('Loading animation tick: $_dotIndex');
        });
        _startLoadingAnimation();
      }
    });
  }

  // Pull-to-Refreshの処理
  Future<void> _handleRefresh() async {
    await Future.delayed(Duration(seconds: 2));

    final newMessages = await _service
        .getChatMessages(widget.roomId, currentUser, startAfter: _lastDocument)
        .first;

    if (newMessages.isNotEmpty) {
      setState(() {
        _allMessages = combineLists(_allMessages, newMessages); // 新しいメッセージを追加
        _lastDocument = newMessages.last; // 最後のメッセージを更新
        print('Pulled to refresh: Loaded ${newMessages.length} new messages.');
      });

      // StreamController に全メッセージを再度流す
      _streamController.add(_allMessages);
      print('StreamController updated with refreshed messages.');
    }
  }

  // メッセージ送信の処理
  void _sendMessage(String message) async {
    _controller.clear();
    print('Sending message: $message');

    setState(() {
      _isSending = true; // メッセージ送信中に設定
      print('Message send initiated. Loading animation started.');
    });

    _startLoadingAnimation(); // アニメーションを開始

    try {
      Map<String, dynamic> response =
          await _service.sendMessage(message, currentUser, widget.roomId);
      print('Message sent successfully.');

      // クイックレスポンスの更新
      _updateQuickResponses(response);
    } catch (e) {
      setState(() {
        _isSending = false; // 送信失敗時にリセット
        print('Failed to send message: $e');
      });
      String errorMessage = 'メッセージの送信に失敗しました: $e';
      if (e.toString().contains('No Internet connection')) {
        // エラー内容に基づく分岐
        errorMessage = 'ネットワークエラーが発生しました。インターネット接続を確認してください。';
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(errorMessage)),
      );
      return; // エラー発生時は以降の処理をスキップ
    }

    setState(() {
      _isSending = false; // ローディング完了後にリセット
      print('Message send completed. Loading animation stopped.');
    });

    // 検索結果をリセット
    if (_isSearching) {
      setState(() {
        _searchQuery = "";
        _searchResults = [];
        _currentSearchIndex = 0;
        _isSearching = false;
        _searchController.clear();
        _isFetchingMessages = false; // 必要に応じてリセット
        _loadData(); // メッセージデータの再取得
        print('Search reset after sending message.');
      });
    }
  }

  // クイックレスポンスの更新
  void _updateQuickResponses(Map<String, dynamic> response) async {
    // 予測クイックレスポンスを取得
    List<String> predictedResponses =
        await _service.getPredictedQuickResponses(response);
    setState(() {
      _quickResponses = predictedResponses;
      print('Predicted quick responses set: $predictedResponses');
    });
  }

  // 詳細メッセージ送信の処理
  void _sendDetailMessage(String message) async {
    _controller.clear();
    print('Sending detail message.');

    setState(() {
      _isSending = true; // メッセージ送信中に設定
      print('Detail message send initiated. Loading animation started.');
    });

    _startLoadingAnimation(); // アニメーションを開始

    try {
      Map<String, dynamic> response =
          await _service.sendDetailMessage(message, currentUser, widget.roomId);
      await _service.isDetailed(currentUser, widget.roomId);
      _updateQuickResponses(response);
      print('Detail message sent successfully.');
    } catch (e) {
      setState(() {
        _isSending = false; // 送信失敗時にリセット
        print('Failed to send detail message: $e');
      });
      String errorMessage = '詳細メッセージの送信に失敗しました: $e';
      if (e.toString().contains('No Internet connection')) {
        errorMessage = 'ネットワークエラーが発生しました。インターネット接続を確認してください。';
      }
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text(errorMessage)),
      );
      return; // エラー発生時は以降の処理をスキップ
    }

    setState(() {
      _isSending = false; // ローディング完了後にリセット
      print('Detail message send completed. Loading animation stopped.');
    });
  }

  // 検索を実行し、検索結果のインデックスを更新
  void _performSearch() {
    if (_searchQuery.isEmpty) {
      setState(() {
        _searchResults = [];
        print('Search query is empty. Cleared search results.');
      });
      return;
    }

    List<int> results = [];
    for (int i = 0; i < _allMessages.length; i++) {
      String userMessage = _allMessages[i]['user_message'] ?? "";
      String gptResponse = _allMessages[i]['chat_gpt_response'] ?? "";

      // 複数キーワードのサポート (OR検索)
      List<String> keywords = _searchQuery.toLowerCase().split(' ');
      bool matches = keywords.any((keyword) =>
          userMessage.toLowerCase().contains(keyword) ||
          gptResponse.toLowerCase().contains(keyword));

      if (matches) {
        results.add(i);
      }
    }

    setState(() {
      _searchResults = results.reversed.toList();
      print('Search performed. Found ${_searchResults.length} results.');
    });

    if (_searchResults.isNotEmpty) {
      // 最初の検索結果にスクロール
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _scrollToMessage(_searchResults.first);
        print('Scrolled to first search result.');
      });
    }
  }

  // 指定したインデックスのメッセージにスクロール
  void _scrollToMessage(int index) {
    if (!_isSearching) {
      // 通常モードの場合
      if (!_itemScrollController.isAttached) {
        print('Normal ScrollController has no clients. Cannot scroll.');
        return;
      }

      // チャットリストはreverse: trueなので、計算が必要
      int targetIndex = index;

      _itemScrollController.scrollTo(
        index: targetIndex,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
      print('Scrolling to message index $index (target index: $targetIndex).');

      setState(() {
        _currentSearchIndex = 0;
      });
    } else {
      // 検索モードの場合
      if (!_itemScrollController.isAttached) {
        print('Search ScrollController has no clients. Cannot scroll.');
        return;
      }

      int targetIndex = _searchResults[_currentSearchIndex];

      _itemScrollController.scrollTo(
        index: targetIndex,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
      print(
          'Scrolling to search result index $_currentSearchIndex (target index: $targetIndex).');
    }
  }

  // メッセージ内の検索単語をハイライトする関数
  Widget _highlightText(String text) {
    if (_searchQuery.isEmpty) {
      return SelectableText(
        text,
        style: TextStyle(
          color: Colors.black,
        ),
      );
    }

    // 複数キーワード対応 (OR検索)
    List<String> keywords = _searchQuery.toLowerCase().split(' ');
    String lowerText = text.toLowerCase();

    List<TextSpan> spans = [];
    int start = 0;

    while (start < text.length) {
      int? earliestMatchIndex;
      String? matchedKeyword;

      for (String keyword in keywords) {
        int index = lowerText.indexOf(keyword, start);
        if (index != -1 &&
            (earliestMatchIndex == null || index < earliestMatchIndex)) {
          earliestMatchIndex = index;
          matchedKeyword = keyword;
        }
      }

      if (earliestMatchIndex == null) {
        // No more matches
        spans.add(TextSpan(
          text: text.substring(start),
          style: TextStyle(color: Colors.black),
        ));
        break;
      }

      if (earliestMatchIndex > start) {
        spans.add(TextSpan(
          text: text.substring(start, earliestMatchIndex),
          style: TextStyle(color: Colors.black),
        ));
      }

      spans.add(TextSpan(
        text: text.substring(
            earliestMatchIndex, earliestMatchIndex + matchedKeyword!.length),
        style: TextStyle(
          color: Colors.black,
          backgroundColor: Colors.yellow,
          fontWeight: FontWeight.bold,
        ),
      ));

      start = earliestMatchIndex + matchedKeyword.length;
    }

    return RichText(
      text: TextSpan(
        children: spans,
      ),
    );
  }

  // メッセージの強調表示とハイライト
  Widget buildMessage(String? message, bool isUserMessage) {
    if (_searchQuery.isEmpty) {
      return SelectableText(
        message ?? "",
        style: TextStyle(
          color: getMessageTextColor(isUserMessage),
        ),
      );
    }

    return _highlightText(message ?? "");
  }

  // 検索結果を表示するウィジェット
  Widget _buildSearchResults() {
    if (_searchQuery.isEmpty) {
      if (_searchHistory.isEmpty) {
        return Center(child: Text('検索履歴がありません'));
      }
      // 検索履歴を表示
      return ListView.builder(
        itemCount: _searchHistory.length,
        itemBuilder: (context, index) {
          String historyItem =
              _searchHistory[_searchHistory.length - 1 - index];
          return ListTile(
            key: index == 0 ? _searchHistoryKey : null,
            leading: Icon(Icons.history),
            title: Text(historyItem),
            onTap: () {
              setState(() {
                _searchQuery = historyItem;
                _searchController.text = historyItem;
                _performSearch();
              });
              if (_isTutorialActive &&
                  _steps[_currentStep]['requires'] == 'search_history') {
                _conditionMet();
              }
            },
            trailing: IconButton(
              icon: Icon(Icons.clear),
              onPressed: () {
                setState(() {
                  _searchHistory.removeAt(_searchHistory.length - 1 - index);
                  _saveSearchHistory();
                  print('Removed search history item: $historyItem');
                });
              },
            ),
          );
        },
      );
    }

    if (_searchResults.isEmpty) {
      return Center(child: Text('該当するメッセージがありません'));
    }

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Text(
            '${_searchResults.length} 件の結果が見つかりました',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemCount: _searchResults.length,
            itemBuilder: (context, index) {
              int messageIndex = _searchResults[index];
              String userMessage =
                  _allMessages[messageIndex]['user_message'] ?? "";
              String gptResponse =
                  _allMessages[messageIndex]['chat_gpt_response'] ?? "";

              return ListTile(
                key: index == 0 ? _searchResultItemKey : null,
                leading: CircleAvatar(
                  child: Text('${index + 1}'),
                  backgroundColor:
                      _currentSearchIndex == index ? Colors.blue : Colors.grey,
                ),
                title: Text(
                  'メッセージ ${index + 1}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    buildMessage(userMessage, true),
                    buildMessage(gptResponse, false),
                  ],
                ),
                selected: _currentSearchIndex == index,
                onTap: () {
                  setState(() {
                    _isSearching = false;
                    _searchQuery = "";
                    _searchResults = [];
                    _currentSearchIndex = 0;
                    _searchController.clear();
                    _pendingScrollToIndex = messageIndex; // スクロールすべきインデックスを設定
                    // StreamController に全メッセージを再度流す
                    _streamController.add(_allMessages);
                    if (_isTutorialActive &&
                        _steps[_currentStep]['requires'] == 'search_result') {
                      _conditionMet();
                    }
                  });

                  // スクロールを実行するためにビルド後にコールバック
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    if (_pendingScrollToIndex != null) {
                      _scrollToMessage(_pendingScrollToIndex!);
                      _pendingScrollToIndex = null; // スクロール後にリセット
                    }
                  });
                },
              );
            },
          ),
        ),
      ],
    );
  }

  // メッセージの背景とテキストの色を設定する関数
  Color getMessageBackgroundColor(bool isUserMessage) {
    return isUserMessage ? Colors.blueGrey : Colors.black;
  }

  Color getMessageTextColor(bool isUserMessage) {
    return isUserMessage ? Colors.white : Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    // 画面を構築。テキストフィールド、送信ボタン、応答表示エリアを含む
    return StreamBuilder<DocumentSnapshot>(
        stream: FirebaseFirestore.instance
            .collection('users')
            .doc(currentUser?.uid)
            .collection('chatRooms')
            .doc(widget.roomId)
            .snapshots(),
        builder: (context, snapshot) {
          String appBarTitle = '無題'; // デフォルト値

          // Firestoreのドキュメントが存在し、nameフィールドも存在する場合、タイトルを更新
          if (snapshot.hasData) {
            Map<String, dynamic>? dataMap =
                snapshot.data!.data() as Map<String, dynamic>?; // null許容の型を使用

            if (dataMap != null && dataMap.containsKey('name')) {
              appBarTitle = dataMap['name'];
            }
          }

          // ウィジェットのビルド後にスクロールを実行
          WidgetsBinding.instance.addPostFrameCallback((_) {
            if (_pendingScrollToIndex != null) {
              _scrollToMessage(_pendingScrollToIndex!);
              _pendingScrollToIndex = null; // スクロール後にリセット
              print(
                  'Post-frame callback: Scrolled to message index $_pendingScrollToIndex.');
            }
          });

          return Stack(
            children: [
              PopScope(
                canPop: false,
                child: Scaffold(
                  appBar: AppBar(
                    backgroundColor: Colors.white,
                    iconTheme: IconThemeData(color: Colors.black),
                    leading: IconButton(
                      key: _backKey,
                      icon: Icon(Icons.arrow_back),
                      onPressed: () async {
                        Get.back();
                      },
                    ),
                    title: _isSearching
                        ? TextField(
                            key: _searchBarKey,
                            controller: _searchController,
                            autofocus: true,
                            onChanged: (value) {
                              // 余分な空白を取り除く
                              final trimmedValue = value.trim();
                              if (trimmedValue == "java") {
                                // 現在のステップが 'search' で、チュートリアルが有効なら _conditionMet() を呼び出す
                                if (_isTutorialActive &&
                                    _steps[_currentStep]['requires'] ==
                                        'search') {
                                  _conditionMet();
                                }
                              }
                            },
                            decoration: InputDecoration(
                              hintText: "検索ワードを入力",
                              border: InputBorder.none,
                              hintStyle: TextStyle(color: Colors.grey),
                            ),
                            style:
                                TextStyle(color: Colors.black, fontSize: 16.0),
                          )
                        : Text(
                            appBarTitle,
                            style: TextStyle(
                              color: Colors.black,
                            ),
                          ),
                    centerTitle: true,
                    actions: [
                      if (!_isSearching)
                        IconButton(
                          icon: Icon(Icons.help_outline,
                              color: _isTutorialActive
                                  ? Colors.grey
                                  : Colors.black),
                          onPressed: () {
                            if (!_isTutorialActive) {
                              // オンボーディング（ガイダンス）を開始
                              WidgetsBinding.instance.addPostFrameCallback((_) {
                                setState(() {
                                  _isOnboadingActive = true;
                                  _guidanceNum = 0;
                                });
                                _updateGuidanceOverlayPosition();
                              });

                              _loadProgress();
                            }
                          },
                        ),
                      IconButton(
                        key: _searchIconKey, // グローバルキーを追加
                        icon: Icon(
                          _isSearching ? Icons.close : Icons.search,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          setState(() {
                            if (_isSearching) {
                              _searchQuery = "";
                              _searchResults = [];
                              _currentSearchIndex = 0;
                              _isSearching = false;
                              _searchController.clear();
                              _isFetchingMessages = false; // ローディング状態をリセット
                              _streamController
                                  .add(_allMessages); // 全メッセージを再度流す
                            } else {
                              _isSearching = true;
                            }
                          });
                          // 操作説明中で「search_icon」のステップの場合
                          if (_isTutorialActive) {
                            String? req = _steps[_currentStep]['requires'];
                            if (req == 'search_icon' || req == 'search_clear') {
                              _conditionMet();
                            }
                          }
                          // スクロールを実行するためにビルド後にコールバック
                          WidgetsBinding.instance.addPostFrameCallback((_) {
                            if (_pendingScrollToIndex != null) {
                              _scrollToMessage(_pendingScrollToIndex!);
                              _pendingScrollToIndex = null; // スクロール後にリセット
                              print(
                                  'Post-frame callback after AppBar action: Scrolled to message index $_pendingScrollToIndex.');
                            }
                          });
                        },
                      ),
                    ],
                  ),
                  body: GestureDetector(
                    onTap: () {
                      FocusScope.of(context).unfocus();
                      print('Tapped outside to unfocus.');
                    },
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Visibility(
                            visible: !_isSearching && widget.showDetail,
                            child: widget.showDetail
                                ? Container(
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Colors.blueGrey[50], // 背景色
                                      borderRadius:
                                          BorderRadius.circular(10), // 角の丸み
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0, 3),
                                        ),
                                      ],
                                    ),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                                "詳細な情報を表示する"), // トグルの状態に応じたテキスト
                                            IconButton(
                                              icon: Icon(_isToggleVisible
                                                  ? Icons.toggle_on
                                                  : Icons.toggle_off_outlined),
                                              onPressed: () {
                                                setState(() {
                                                  _isToggleVisible =
                                                      !_isToggleVisible; // 表示状態を切り替える
                                                });
                                              },
                                            ),
                                          ],
                                        ),
                                        if (_isToggleVisible)
                                          _buildQuestionSection(), // 問題セクションの構築
                                        if (_isToggleVisible)
                                          _buildAnswerSection(), // 回答セクションの構築
                                      ],
                                    ),
                                  )
                                : SizedBox.shrink(),
                          ),
                          // 通常モードと検索モードのListViewをIndexedStackで管理
                          Expanded(
                            child: IndexedStack(
                              index: _isSearching ? 1 : 0,
                              children: [
                                // 通常モードのメッセージリスト
                                StreamBuilder<List<QueryDocumentSnapshot>>(
                                  stream: _streamController.stream,
                                  builder: (context, snapshot) {
                                    if (_isFetchingMessages) {
                                      return Center(
                                          child: CircularProgressIndicator());
                                    }
                                    if (snapshot.hasError) {
                                      return Center(
                                          child:
                                              Text('エラー: ${snapshot.error}'));
                                    }
                                    if (!snapshot.hasData ||
                                        snapshot.data!.isEmpty) {
                                      return Center(
                                          child: Text('メッセージを送ってみましょう'));
                                    }

                                    // デバッグ用: ストリームから受け取ったメッセージ数を表示
                                    print(
                                        'StreamBuilder received ${snapshot.data!.length} messages.');

                                    return Stack(
                                      children: [
                                        RefreshIndicator(
                                          onRefresh: _handleRefresh,
                                          child:
                                              ScrollablePositionedList.builder(
                                            itemScrollController:
                                                _itemScrollController, // 通常モード用
                                            itemPositionsListener:
                                                _itemPositionsListener,
                                            reverse: true,
                                            itemCount: snapshot.data!.length,
                                            itemBuilder: (context, index) {
                                              final doc = snapshot.data![index];
                                              final bool isLatestMessage =
                                                  index == 0;
                                              final String? userMessage =
                                                  doc['user_message'];
                                              final String? gptResponse =
                                                  doc['chat_gpt_response'];

                                              // 吹き出しのデザインをカスタマイズするための関数
                                              Widget bubbleDesign(
                                                  String? message,
                                                  bool isUserMessage,
                                                  [Widget? child]) {
                                                return Align(
                                                  alignment: isUserMessage
                                                      ? Alignment.centerRight
                                                      : Alignment.centerLeft,
                                                  child: IntrinsicWidth(
                                                    child: Container(
                                                      constraints: BoxConstraints(
                                                          maxWidth: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.7),
                                                      margin: isUserMessage
                                                          ? const EdgeInsets
                                                              .only(
                                                              left: 50,
                                                              right: 10,
                                                              top: 10,
                                                              bottom: 10)
                                                          : const EdgeInsets
                                                              .only(
                                                              left: 10,
                                                              right: 50,
                                                              top: 10,
                                                              bottom: 10),
                                                      padding: const EdgeInsets
                                                          .symmetric(
                                                          vertical: 10,
                                                          horizontal: 20),
                                                      decoration: BoxDecoration(
                                                        color:
                                                            getMessageBackgroundColor(
                                                                isUserMessage),
                                                        borderRadius:
                                                            isUserMessage
                                                                ? const BorderRadius
                                                                    .only(
                                                                    topRight: Radius
                                                                        .circular(
                                                                            20),
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            20),
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            20),
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            0),
                                                                  )
                                                                : const BorderRadius
                                                                    .only(
                                                                    topRight: Radius
                                                                        .circular(
                                                                            20),
                                                                    topLeft: Radius
                                                                        .circular(
                                                                            20),
                                                                    bottomRight:
                                                                        Radius.circular(
                                                                            20),
                                                                    bottomLeft:
                                                                        Radius.circular(
                                                                            0),
                                                                  ),
                                                      ),
                                                      child: child ??
                                                          SelectableText(
                                                            message ?? "",
                                                            style: TextStyle(
                                                              color: getMessageTextColor(
                                                                  isUserMessage),
                                                            ),
                                                          ),
                                                    ),
                                                  ),
                                                );
                                              }

                                              // 最新のメッセージかつレスポンスがない場合アニメーションを表示
                                              if (isLatestMessage &&
                                                  gptResponse == null) {
                                                return Column(
                                                  children: [
                                                    bubbleDesign(
                                                        userMessage, true),
                                                    _isSending
                                                        ? bubbleDesign(
                                                            null,
                                                            false,
                                                            _loadingAnimation())
                                                        : const SizedBox
                                                            .shrink(),
                                                  ],
                                                );
                                              }

                                              // 最新のメッセージのレスポンスが存在する場合、または他のメッセージ
                                              return Column(
                                                children: [
                                                  bubbleDesign(
                                                      userMessage,
                                                      true,
                                                      buildMessage(
                                                          userMessage, true)),
                                                  bubbleDesign(
                                                      gptResponse,
                                                      false,
                                                      buildMessage(
                                                          gptResponse, false)),
                                                ],
                                              );
                                            },
                                          ),
                                        ),
                                        if (_showScrollButton)
                                          Positioned(
                                            right: 10.0,
                                            bottom: 10.0,
                                            child: FloatingActionButton(
                                              key: _latestMessageButtonKey,
                                              onPressed: () {
                                                _resetScrollPosition();
                                                if (_isTutorialActive &&
                                                    _steps[_currentStep]
                                                            ['requires'] ==
                                                        'latest_message') {
                                                  _conditionMet();
                                                }
                                              },
                                              child: Icon(Icons.arrow_downward),
                                              mini: true, // 小さいFABを使用
                                              backgroundColor: Colors.grey,
                                            ),
                                          ),
                                      ],
                                    );
                                  },
                                ),
                                // 検索モードのメッセージリスト
                                _buildSearchResults(),
                              ],
                            ),
                          ),
                          // クイックレスポンスセクションの追加（メッセージ入力欄の直上に移動）
                          Visibility(
                            key: _quickResponseKey,
                            visible:
                                !_isSearching && _quickResponses.isNotEmpty,
                            child: Container(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: _quickResponses.map((response) {
                                    return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 4.0),
                                      child: ElevatedButton(
                                        onPressed: _isSending
                                            ? null // メッセージ送信中はボタンを無効化
                                            : () {
                                                if (widget.isDetail) {
                                                  _sendDetailMessage(
                                                      response); // sendDetailMessageを実行
                                                } else {
                                                  _sendMessage(
                                                      response); // sendMessageを実行

                                                  if (_isTutorialActive &&
                                                      _steps[_currentStep]
                                                              ['requires'] ==
                                                          'quick_response') {
                                                    _quickResponseTapCount++; // タップ回数をインクリメント
                                                    if (_quickResponseTapCount >=
                                                        5) {
                                                      _conditionMet(); // 5回タップされたら条件完了
                                                      _quickResponseTapCount =
                                                          0; // カウンターをリセット
                                                    }
                                                  }
                                                }
                                              },
                                        child: Text(response),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                          // メッセージ入力フィールドと送信ボタン
                          Visibility(
                            key: _messageSendKey,
                            visible: !_isSearching,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(
                                8.0,
                                8.0,
                                8.0,
                                MediaQuery.of(context)
                                    .padding
                                    .bottom, // システムの下部余白分を確保
                              ),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Expanded(
                                    child: TextField(
                                      key: _messageKey,
                                      controller: _controller,
                                      onChanged: (value) {
                                        // 余分な空白を取り除く
                                        final trimmedValue = value.trim();
                                        if (trimmedValue == "java") {
                                          // 現在のステップが 'search' で、チュートリアルが有効なら _conditionMet() を呼び出す
                                          if (_isTutorialActive &&
                                              _steps[_currentStep]
                                                      ['requires'] ==
                                                  'message') {
                                            _conditionMet();
                                          }
                                        }
                                      },
                                      onSubmitted: (message) {
                                        if (_hasText) {
                                          _sendMessage(message.trim());
                                        }
                                      },
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      decoration: const InputDecoration(
                                        hintText: "メッセージを入力してください",
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 10.0, horizontal: 10.0),
                                        border: OutlineInputBorder(),
                                      ),
                                    ),
                                  ),
                                  IconButton(
                                    key: _sendKey,
                                    icon: Icon(
                                        _isSending
                                            ? Icons.hourglass_empty
                                            : Icons.send,
                                        color: _isSending || !_hasText
                                            ? Colors.grey
                                            : null), // 「メッセージを送信する」アイコン
                                    onPressed: _isSending || !_hasText
                                        ? null
                                        : () {
                                            _sendMessage(
                                                _controller.text.trim());
                                            if (_isTutorialActive &&
                                                _steps[_currentStep]
                                                        ['requires'] ==
                                                    'send') {
                                              _conditionMet();
                                            }
                                          },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              // チュートリアル／オンボーディングオーバーレイ（ガイダンス部分）
              if (_isOnboadingActive && !_isShowDialog)
                Center(child: _buildBackgroundOverlay()),
              if (_isOnboadingActive && !_isTutorialActive)
                GestureDetector(
                  onTap: _guidanceStep,
                  child: Center(
                    child: Stack(
                      children: [
                        _buildBottomPanel(_guidance[_guidanceNum]),
                        if (_currentStep < _steps.length - 1)
                          Positioned(
                            right: 0,
                            top: 0,
                            child: ElevatedButton.icon(
                              onPressed: () {
                                if (_isOnboadingActive) {
                                  _endGuidance();
                                } else if (_isTutorialActive) {
                                  _endTutorial();
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.white,
                                padding: EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 8),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                  side: BorderSide(
                                      color: Colors.black, width: 2.5),
                                ),
                              ),
                              icon: Image.asset(
                                'assets/Skip.png',
                                width: 25,
                                height: 25,
                              ),
                              label: Text(
                                "スキップ",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  ),
                ),
              if (_isOnboadingActive && _isShowDialog)
                Center(child: _buildBackgroundOverlay()),
              if (_isShowDialog) Center(child: _buildTutorialDialog()),
              // 操作説明（チュートリアル）パネル
              if (_isTutorialActive && !_isOnboadingActive)
                Builder(
                  builder: (context) {
                    // 現在のステップが "search" または "search_result" なら上部、それ以外は下部に表示
                    final String? currentReq = _steps[_currentStep]['requires'];
                    final bool isSearchRelated = (currentReq == 'search' ||
                        currentReq == 'search_result' ||
                        currentReq == 'search_clear');
                    return GestureDetector(
                      onTap: _checkAndProceed,
                      child: Padding(
                        padding: isSearchRelated
                            ? EdgeInsets.only(bottom: 250)
                            : EdgeInsets.only(
                                bottom: 400), // FABと被らないよう下部に余白を追加
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Stack(
                            children: [
                              _buildBottomPanel(_steps[_currentStep]['text']),
                              if (_currentStep < _steps.length - 1)
                                Positioned(
                                  right: 0,
                                  top: 0,
                                  child: ElevatedButton.icon(
                                    onPressed: () {
                                      if (_isOnboadingActive) {
                                        _endGuidance();
                                      } else if (_isTutorialActive) {
                                        _endTutorial();
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.white,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 16, vertical: 8),
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        side: BorderSide(
                                            color: Colors.black, width: 2.5),
                                      ),
                                    ),
                                    icon: Image.asset(
                                      'assets/Skip.png',
                                      width: 25,
                                      height: 25,
                                    ),
                                    label: Text(
                                      "スキップ",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              // FABタップ促進のアニメーション
              // ヘルプオーバーレイ
              if (showHelp)
                HelpOverlay(
                  existingWidget: Container(),
                  helpMessages: helpMessages,
                  backgroundImages: backgroundImages,
                  onSkip: () {
                    setState(() {
                      showHelp = false;
                    });
                  },
                  messageAlignment: messageAlignment,
                  margin: margin,
                  rectangleSteps: rectangleSteps,
                ),
              // ガイダンス中なら、ガイダンス用の青い枠を表示する
              if (_isOnboadingActive)
                BlueSquareOverlay(
                  top: _guidanceOverlayTop,
                  left: _guidanceOverlayLeft,
                  width: _guidanceOverlayWidth,
                  height: _guidanceOverlayHeight,
                  flashing: true,
                  isVisible: true,
                ),

              // 操作説明用の青い枠（動的に対象ウィジェットの位置を取得）
              if (_isTutorialActive && _isVisible)
                BlueSquareOverlay(
                  top: _tutorialOverlayTop,
                  left: _tutorialOverlayLeft,
                  width: _tutorialOverlayWidth,
                  height: _tutorialOverlayHeight,
                  flashing: true,
                  isVisible: _isVisible,
                ),
            ],
          );
        });
  }

  Widget _buildBackgroundOverlay() {
    return Container(
      color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Widget _buildBottomPanel(String text) {
    String displayText = text;
    // 現在のステップが quick_response の場合、残りタップ回数を表示
    if (_isTutorialActive &&
        _steps[_currentStep]['requires'] == 'quick_response') {
      int remaining = 5 - _quickResponseTapCount;
      displayText = "$text\nあと: $remaining 回";
    }
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.lightBlue, width: 3),
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: Text(
        displayText,
        style: TextStyle(
            fontSize: 16,
            color: Colors.black,
            fontWeight: FontWeight.w600,
            decoration: TextDecoration.none),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTutorialDialog() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 252, 246, 251),
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black26, blurRadius: 10.0, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '操作説明を開始しますか？',
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.black87,
                fontWeight: FontWeight.normal,
                decoration: TextDecoration.none),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  _startTutorial();
                  _endGuidance();
                },
                child: Text(
                  'はい',
                  style: TextStyle(
                      color: Colors.purple,
                      fontSize: 16.0,
                      decoration: TextDecoration.none),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _noTap();
                  _endGuidance();
                },
                child: Text(
                  'いいえ',
                  style: TextStyle(
                      color: Colors.purple,
                      fontSize: 16.0,
                      decoration: TextDecoration.none),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // 質問セクションのウィジェット
  Widget _buildQuestionSection() {
    return Card(
      elevation: 4.0,
      margin: EdgeInsets.all(8.0), // カードの外側のマージンを設定
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SelectableText('問題文:',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
            const SizedBox(height: 8.0),
            SelectableText(widget.questionText ?? ""),
          ],
        ),
      ),
    );
  }

  // 回答セクションのウィジェット
  Widget _buildAnswerSection() {
    // 画面の高さの30%を計算
    double cardHeight = MediaQuery.of(context).size.height * 0.30;
    return Container(
      // CardをContainerでラップ
      width: double.infinity, // 横幅を画面いっぱいに設定
      height: cardHeight, // カードの高さを固定
      child: Card(
        elevation: 4.0,
        margin: EdgeInsets.all(8.0), // カードの外側のマージンを設定
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            // スクロール可能にする
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SelectableText('あなたの回答:',
                    style:
                        TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
                const SizedBox(height: 8.0),
                SelectableText(widget.userAnswer ?? ""),
                const SizedBox(height: 8.0),
                SelectableText(widget.isCorrect! ? '正解' : '不正解',
                    style: TextStyle(
                      color: widget.isCorrect! ? Colors.green : Colors.red,
                    )),
                const SizedBox(height: 8.0),
                SelectableText('採点者からのコメント:',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(height: 8.0),
                SelectableText(widget.commentary ?? ""),
                const SizedBox(height: 16.0), // 少しスペースを空ける
                // 模範解答を表示/非表示するボタン
                GestureDetector(
                  onTap: () {
                    setState(() {
                      // タップで表示/非表示を切り替える
                      isAnswerVisible = !isAnswerVisible;
                      print(
                          'Model answer visibility toggled: $isAnswerVisible');
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10.0),
                    color: Colors.blue, // ボタンの背景色
                    child: Center(
                      child: Text(
                        isAnswerVisible ? '模範解答を隠す' : '模範解答を表示',
                        style: TextStyle(color: Colors.white, fontSize: 16.0),
                      ),
                    ),
                  ),
                ),
                // 模範解答セクション
                if (isAnswerVisible)
                  Container(
                    padding: EdgeInsets.all(8.0),
                    margin: EdgeInsets.only(top: 16.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      color: Colors.grey.shade200, // 背景色
                    ),
                    child: SelectableText(
                      widget.modelAnswer ?? "",
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
