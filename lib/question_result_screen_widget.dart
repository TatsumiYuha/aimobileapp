import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/performance_management_screen_widget.dart';
import 'package:ai_mobile_app/question_selection_screen_widget.dart';
import 'package:ai_mobile_app/xp_gain_overlay.dart';
import 'package:flutter/material.dart';
import 'auth_service.dart';
import 'package:get/get.dart';
import 'custom_bottom_app_bar.dart';
import 'custom_tap_effect_overlay.dart';
import 'question_result_screen_service.dart';
import 'chat_room_selection_service.dart';
import 'package:firebase_auth/firebase_auth.dart';

class QuestionResultScreenWidget extends StatelessWidget {
  final List<Map<String, dynamic>> questionAnswerPairs;
  final String quesTionListName;
  final int originId; // 遷移元を示す　1:問題　2:成績履歴
  final int questionId; // 成績履歴に戻る際に必要な問題識別ID、記述から遷移する際には使わない
  final XpGainResult? xpResult;
  final String? questionListName;

  QuestionResultScreenWidget({
    required this.questionAnswerPairs,
    required this.quesTionListName,
    required this.originId,
    required this.questionId,
    this.xpResult,
    this.questionListName,
  });

  double calculateTotalScore(
      List<Map<String, dynamic>> gradedQuestionAnswerPairs) {
    double totalScore = 0.0;
    for (var pair in gradedQuestionAnswerPairs) {
      double score = pair['grade'] ?? 0.0;
      totalScore += score;
    }
    return totalScore;
  }

  @override
  Widget build(BuildContext context) {
    // 修正ポイント：questionsを適切な型で抽出
    List<Map<String, dynamic>> questions = questionAnswerPairs.map((pair) {
      return {
        'questionText': pair['question'],
        // 他に必要なデータがあればここに追加
      };
    }).toList();

    List<String> answers =
        questionAnswerPairs.map((pair) => pair["answer"] as String).toList();

    var service = QuestionResultScreenService(
      questions: questions,
      answers: answers,
    );

    // 採点結果に基づいて各問題が正解かどうかを判断
    List<bool> gradedAnswers = questionAnswerPairs.map((pair) {
      bool score = pair['grade'];
      return score; // スコアが1.00以上であれば true、それ以外は false
    }).toList();

    return PopScope(
      canPop: false,
      child: Scaffold(
        body: AnswerScreenWidget(
          service: service,
          gradedAnswers: gradedAnswers,
          questionAnswerPairs: questionAnswerPairs,
          questionListName: quesTionListName,
          originId: originId,
          questionId: questionId,
          xpResult: xpResult,
        ),
      ),
    );
  }
}

class AnswerScreenWidget extends StatefulWidget {
  final QuestionResultScreenService service;
  final List<bool> gradedAnswers;
  final List<Map<String, dynamic>> questionAnswerPairs;
  final String questionListName;
  final String sessionId = UniqueKey().toString(); // セッションIDの生成
  final int originId;
  final int questionId; // 成績履歴に戻る際に必要な問題識別ID、記述から遷移する際には使わない
  final XpGainResult? xpResult;

  AnswerScreenWidget({
    required this.service,
    required this.gradedAnswers,
    required this.questionAnswerPairs,
    required this.questionListName,
    required this.originId,
    required this.questionId,
    this.xpResult,
  });

  @override
  _AnswerScreenWidgetState createState() => _AnswerScreenWidgetState();
}

class _AnswerScreenWidgetState extends State<AnswerScreenWidget>
    with TickerProviderStateMixin {
  // アニメーション関連の変数
  late AnimationController _scoreFadeInController;
  late Animation<double> _scoreFadeInAnimation;

  // 「FutureDelayedAnimation を indexごとに管理」するマップ
  final Map<int, FutureDelayedAnimation> _controllers = {};

  // ChatRoomSelectionServiceのインスタンスを作成
  final ChatRoomSelectionService service = ChatRoomSelectionService();

  @override
  void initState() {
    super.initState();
    _scoreFadeInController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    );
    _scoreFadeInAnimation =
        Tween<double>(begin: 0.0, end: 1.0).animate(_scoreFadeInController);
  }

  @override
  void dispose() {
    _scoreFadeInController.dispose();

    // 2. リストアイテム用に生成したコントローラを全て破棄
    _controllers.forEach((_, controller) => controller.dispose());
    _controllers.clear();
    super.dispose();
  }

  // 模範解答を表示するためのメソッド
  void _showModelAnswer(BuildContext context, String modelAnswer) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent, // 背景を透明に
      builder: (BuildContext context) {
        return Container(
          padding: EdgeInsets.all(16.0),
          decoration: BoxDecoration(
            color: Colors.brown.shade50, // 薄いベージュ色
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '模範解答',
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              Text(modelAnswer, style: TextStyle(fontSize: 18)),
              SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  Navigator.pop(context); // 閉じるボタン
                },
                child: Text('閉じる'),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // スコアバーのフェードインアニメーションを開始
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _scoreFadeInController.forward();
    });

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          '${widget.questionListName} 結果',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: CustomTapEffectOverlay(
        child: Column(
          children: [
            //_buildScoreBar(context),
            Expanded(
              child: ListView.builder(
                itemCount: widget.gradedAnswers.length,
                itemBuilder: (context, index) {
                  FutureDelayedAnimation animation;
                  if (_controllers.containsKey(index)) {
                    // 既に生成済みなら再利用
                    animation = _controllers[index]!;
                  } else {
                    // 新規生成してマップに保持
                    animation = FutureDelayedAnimation(
                      delay: Duration(milliseconds: 100 * index),
                      vsync: this,
                    );
                    _controllers[index] = animation;
                    if (widget.originId == 1) if (index ==
                        widget.gradedAnswers.length - 1) {
                      animation.addStatusListener((status) {
                        if (status == AnimationStatus.completed) {
                          // ここでオーバーレイを表示するだけ

                          // ※ xpResult が null ならデフォルト値を使うなど適宜処理
                          final xp = widget.xpResult;
                          final oldXp = xp?.oldXp ?? 0;
                          final newXp = xp?.newXp ?? 0;
                          final oldLvl = xp?.oldLevel ?? 1;
                          final newLvl = xp?.newLevel ?? 1;
                          final nextXp = xp?.xpNeededForNextLevel ?? 100;
                          final gained = (newXp - oldXp);

                          // オーバーレイ呼び出し
                          if (gained != 0)
                            showXpGainOverlay(
                              context: context,
                              oldLevel: oldLvl,
                              newLevel: newLvl,
                              oldXp: oldXp,
                              newXp: newXp,
                              xpNeededForNextLevel: nextXp,
                              gainedXp: gained,
                            ).then((_) {
                              // オーバーレイが消えた後にスナックバーを表示する
                              ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text('ポイントを取得しました！')),
                              );
                            });
                        }
                      });
                    }
                  }
                  return ScaleTransition(
                    scale: Tween<double>(
                      begin: 0,
                      end: 1,
                    ).animate(
                      CurvedAnimation(
                        parent: animation,
                        curve: Curves.bounceOut,
                      ),
                    ),
                    child: Card(
                      child: ListTile(
                        leading: Icon(
                          widget.gradedAnswers[index]
                              ? Icons.check
                              : Icons.close,
                          color: widget.gradedAnswers[index]
                              ? Colors.green
                              : Colors.red,
                        ),
                        title: Row(
                          children: [
                            Expanded(
                              child: Text('問題${index + 1}'),
                            ),
                            // Text(
                            //   '${widget.questionAnswerPairs[index]['grade']} / 1.00',
                            //   style: TextStyle(
                            //     decoration: TextDecoration.underline, // 下線を引く
                            //   ),
                            // ),
                          ],
                        ),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            ElevatedButton(
                              onPressed: () async {
                                bool exists =
                                    await service.checkIfChatRoomExists(
                                  FirebaseAuth.instance.currentUser!,
                                  widget.questionAnswerPairs[index]["question"],
                                  widget.sessionId,
                                );
                                if (exists) {
                                  await service.detailExistChatRoom(
                                    context,
                                    widget.questionListName +
                                        ' 問題${(index + 1).toString()}' +
                                        ' 解説',
                                    FirebaseAuth.instance.currentUser,
                                    widget.sessionId,
                                    widget.gradedAnswers[index],
                                    widget.questionAnswerPairs[index]
                                        ["question"],
                                    widget.questionAnswerPairs[index]["answer"],
                                    widget.questionAnswerPairs[index]
                                        ["commentary"],
                                  );
                                } else {
                                  await service.detailCreateChatRoom(
                                    context,
                                    widget.questionListName +
                                        ' 問題${(index + 1).toString()}' +
                                        ' 解説',
                                    FirebaseAuth.instance.currentUser,
                                    widget.sessionId,
                                    widget.gradedAnswers[index],
                                    widget.questionAnswerPairs[index]
                                        ["question"],
                                    widget.questionAnswerPairs[index]["answer"],
                                    widget.questionAnswerPairs[index]
                                        ["commentary"],
                                    widget.questionAnswerPairs[index]
                                        ['modelAnswer'],
                                  );
                                }
                              },
                              child: Text(
                                '詳細',
                                style: TextStyle(color: Colors.white),
                              ),
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                              ).copyWith(
                                backgroundColor:
                                    MaterialStateProperty.all(Colors.indigo),
                              ),
                            ),
                            SizedBox(width: 8),
                            ElevatedButton(
                              onPressed: () {
                                String modelAnswer =
                                    widget.questionAnswerPairs[index]
                                            ["modelAnswer"] ??
                                        "模範解答はありません";
                                _showModelAnswer(context, modelAnswer);
                              },
                              child: Text(
                                '模範解答',
                                style: TextStyle(color: Colors.black),
                              ),
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 10),
                              ).copyWith(
                                backgroundColor: MaterialStateProperty.all(
                                    Colors.brown.shade50),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            if (widget.originId == 1)
              ElevatedButton(
                child: Text(
                  '問題選択画面へ戻る',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  Get.offAllNamed(
                    '/',
                    arguments: {'initialIndex': 2},
                  ); // GetXを使用しての遷移とトランジションを追加
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      Color.fromARGB(255, 21, 55, 78)), // 背景色を青色に設定
                ),
              ),
            if (widget.originId == 2)
              ElevatedButton(
                child: Text(
                  '成績履歴画面へ戻る',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  /*Get.to(
                      () => PerformanceManagementScreen(
                            questionId: widget.questionId,
                          ),
                      transition: Transition.fadeIn,
                      duration: const Duration(
                          seconds: 1));*/ // GetXを使用しての遷移とトランジションを追加
                  // 要望リスト画面へ遷移する
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PerformanceManagementScreen(
                              questionId: widget.questionId,
                            )),
                  );
                },
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(
                      Color.fromARGB(255, 21, 55, 78)), // 背景色を青色に設定
                ),
              ),
          ],
        ),
      ),
    );
  }
}

class FutureDelayedAnimation extends AnimationController {
  FutureDelayedAnimation({required this.delay, required TickerProvider vsync})
      : super(vsync: vsync, duration: Duration(seconds: 1)) {
    _initialize();
  }

  final Duration delay;

  _initialize() async {
    await Future.delayed(delay);
    forward();
  }
}
