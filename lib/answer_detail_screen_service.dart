import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AnswerDetailScreenService {
  // メッセージのリストを取得する
  Stream<List<QueryDocumentSnapshot>> getMessages(String roomId) {
    try {
      return FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .collection('chatRooms')
          .doc(roomId)
          .collection('chats')
          .orderBy('timestamp', descending: true)
          .snapshots()
          .map((snapshot) => snapshot.docs);
    } catch (e) {
      throw Exception('Failed to fetch messages: $e');
    }
  }

  // メッセージを送信する
  Future<void> sendMessage(String message, String roomId) async {
    try {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .collection('chatRooms')
          .doc(roomId)
          .collection('chats')
          .add({
        'user_message': message,
        'timestamp': DateTime.now(),
      });
    } catch (e) {
      throw Exception('Failed to send message: $e');
    }
  }
}
