import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'chat_screen_widget.dart';

class ChatRoomSelectionService {
  // 通常チャットルームをFirestoreに保存するメソッド
  Future<void> createChatRoom(
      BuildContext context, String roomName, User? currentUser) async {
    if (currentUser != null) {
      DocumentReference roomRef = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .add({
        'name': '無題',
        'timestamp': DateTime.now(),
        'isDeleted': false,
        'isNameUpdated': false,
        'isFavorite': false,
        'order': 1,
      });
      /*Get.to(
        () => ChatScreenWidget(roomId: roomRef.id, roomName: '無題'),
      );*/
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                ChatScreenWidget(roomId: roomRef.id, roomName: '無題')),
      );
      // ChatScreenWidgetに遷移
    }
  }

  // 問題解説チャットルームをFirestoreに保存するメソッド
  Future<void> detailCreateChatRoom(
      BuildContext context,
      String roomName,
      User? currentUser,
      String sessionId,
      bool isCorrect,
      String questionText,
      String userAnswer,
      String commentary,
      String modelAnswer) async {
    if (currentUser != null) {
      DocumentReference roomRef = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .add({
        'sessionId': sessionId,
        'name': roomName,
        'timestamp': DateTime.now(),
        'isDeleted': false,
        'isDetail': true, //detail時のリクエスト用
        'isNameUpdated': true,
        'showDetails': true, //ここから通常時と異なるフィールド
        'isCorrect': isCorrect,
        'questionText': questionText,
        'userAnswer': userAnswer,
        'commentary': commentary,
        'modelAnswer': modelAnswer,
        'isFavorite': false,
        'order': 1,
      });

      //遷移する
      /*Get.to(
        () => ChatScreenWidget.detail(
          roomId: roomRef.id,
          roomName: roomName,
          isDetail: true,
          isCorrect: isCorrect,
          questionText: questionText,
          userAnswer: userAnswer,
          commentary: commentary,
          modelAnswer: modelAnswer,
        ),
      ); // ChatScreenWidgetに遷移*/

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChatScreenWidget.detail(
                  roomId: roomRef.id,
                  roomName: roomName,
                  isDetail: true,
                  isCorrect: isCorrect,
                  questionText: questionText,
                  userAnswer: userAnswer,
                  commentary: commentary,
                  modelAnswer: modelAnswer,
                )),
      );
    }
  }

  Future<void> detailExistChatRoom(
      BuildContext context,
      String roomName,
      User? currentUser,
      String sessionId,
      bool isCorrect,
      String questionText,
      String userAnswer,
      String commentary) async {
    if (currentUser != null) {
      // Firestoreから既存の問題詳細チャットルームを検索
      QuerySnapshot existingRooms = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .where('questionText', isEqualTo: questionText)
          .where('sessionId', isEqualTo: sessionId)
          .get();
      if (existingRooms.docs.isNotEmpty) {
        // 既存のルームのIDを取得
        String existingRoomId = existingRooms.docs.first.id;
        // 既存のルームに遷移
        /*Get.to(
          () => ChatScreenWidget.detail(
            roomId: existingRoomId,
            roomName: roomName,
            isDetail: false,
            isCorrect: isCorrect,
            questionText: questionText,
            userAnswer: userAnswer,
            commentary: commentary,
          ),
        );*/
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatScreenWidget.detail(
                    roomId: existingRoomId,
                    roomName: roomName,
                    isDetail: false,
                    isCorrect: isCorrect,
                    questionText: questionText,
                    userAnswer: userAnswer,
                    commentary: commentary,
                  )),
        );
      }
    }
  }

  Future<bool> checkIfChatRoomExists(
      User? currentUser, String questionText, String sessionId) async {
    try {
      QuerySnapshot existingRooms = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser?.uid)
          .collection('chatRooms')
          .where('questionText', isEqualTo: questionText)
          .where('sessionId', isEqualTo: sessionId)
          .get();

      return existingRooms.docs.isNotEmpty;
    } catch (e) {
      print('Error checking for existing chat rooms: $e');
      return false;
    }
  }

  Future<void> deleteChatRoom(String chatRoomId, User? currentUser) async {
    if (chatRoomId.isNotEmpty && currentUser != null) {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .doc(chatRoomId)
          .update({'isDeleted': true});
    }
  }

  // ルームIDに基づいてルームの詳細を取得し、適切な画面へ遷移するメソッド
  Future<void> navigateToChatRoom(BuildContext context, String roomId) async {
    try {
      User? currentUser = FirebaseAuth.instance.currentUser;

      // Firestore からルームの詳細を取得
      DocumentSnapshot roomSnapshot = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser?.uid)
          .collection('chatRooms')
          .doc(roomId)
          .get();

      Map<String, dynamic>? roomData =
          roomSnapshot.data() as Map<String, dynamic>?;

      if (roomSnapshot.exists && roomData?['showDetails'] == true) {
        /*Get.to(() => ChatScreenWidget.detail(
              roomId: roomId,
              roomName: roomData?['name'] ?? '無題',
              // その他の詳細情報
              isDetail: roomData?['isDetail'],
              isCorrect: roomData?['isCorrect'],
              questionText: roomData?['questionText'],
              userAnswer: roomData?['userAnswer'],
              commentary: roomData?['commentary'],
              modelAnswer: roomData?['modelAnswer'],
            ));*/
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatScreenWidget.detail(
                    roomId: roomId,
                    roomName: roomData?['name'] ?? '無題',
                    // その他の詳細情報
                    isDetail: roomData?['isDetail'],
                    isCorrect: roomData?['isCorrect'],
                    questionText: roomData?['questionText'],
                    userAnswer: roomData?['userAnswer'],
                    commentary: roomData?['commentary'],
                    modelAnswer: roomData?['modelAnswer'],
                  )),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ChatScreenWidget(
                    roomId: roomId,
                    roomName: roomData?['name'] ?? '無題',
                  )),
        );
        /*Get.to(
          () => ChatScreenWidget(
            roomId: roomId,
            roomName: roomData?['name'] ?? '無題',
          ),
          popGesture: false,
        );*/
      }
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('チャットルームの読み込みに失敗しました。')));
    }
  }

  Future<void> toggleFavoriteChatRoom(
      String roomId, User? currentUser, bool isFavorite) async {
    if (currentUser == null) return;

    await FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .collection('chatRooms')
        .doc(roomId)
        .update({'isFavorite': !isFavorite});
  }

  //チャットルーム名を変更するメソッド
  void changeChatRoomName(
      BuildContext context, String chatRoomId, User? currentUser) async {
    final TextEditingController _controller = TextEditingController();

    // ダイアログに表示するための初期データをセット
    _controller.text = await _getChatRoomNameById(
        chatRoomId, currentUser); // このメソッドはチャットルームIDから既存のルーム名を取得します

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('ルーム名を変更'),
          content: TextField(
            controller: _controller,
            decoration: InputDecoration(
              hintText: '新しいルーム名を入力',
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () async {
                try {
                  await _updateChatRoomName(
                      chatRoomId, _controller.text, currentUser);
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('チャットルーム名が更新されました。')));
                } catch (e) {
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('チャットルーム名の更新に失敗しました。')));
                }
                Navigator.of(context).pop();
              },
              child: Text('変更'),
            ),
            TextButton(
              onPressed: () {
                // キャンセルボタンが押されたときの処理
                Navigator.of(context).pop(); // ダイアログを閉じる
              },
              child: Text('キャンセル'),
            ),
          ],
        );
      },
    );
  }

// チャットルーム名をIDで取得するダミー関数
  Future<String> _getChatRoomNameById(
      String chatRoomId, User? currentUser) async {
    try {
      DocumentSnapshot doc = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser!.uid)
          .collection('chatRooms')
          .doc(chatRoomId)
          .get();

      if (doc.exists) {
        // ドキュメントのデータを Map としてキャスト
        Map<String, dynamic>? data = doc.data() as Map<String, dynamic>?;
        return data?['name'] ??
            '無題'; // 'name' フィールドの値を返します。データがない場合は '無題' を返します。
      } else {
        return '無題'; // ドキュメントが存在しない場合
      }
    } catch (e) {
      print('Error fetching chat room name: $e');
      return '無題'; // エラーが発生した場合
    }
  }

// チャットルーム名を更新する関数
  Future<void> _updateChatRoomName(
      String chatRoomId, String newName, User? currentUser) async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser?.uid)
        .collection('chatRooms')
        .doc(chatRoomId)
        .update({
      'name': newName,
      'isNameUpdated': true,
    });
    print('チャットルームID: $chatRoomId の名前を "$newName" に変更しました');
  }
}
