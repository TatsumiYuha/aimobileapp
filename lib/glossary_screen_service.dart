import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';

class GlossaryScreenService {
  // Firestore インスタンスを取得
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  // glossary コレクションからデータを取得するメソッド
  Future<List<Map<String, dynamic>>> getGlossary() async {
    try {
      // glossary コレクションからデータを取得
      QuerySnapshot querySnapshot =
          await _firestore.collection('glossary').get();

      // ドキュメントのデータを List<Map<String, dynamic>> に変換
      return querySnapshot.docs
          .map((doc) => doc.data() as Map<String, dynamic>)
          .toList();
    } catch (e) {
      print(e);
      return [];
    }
  }
}
