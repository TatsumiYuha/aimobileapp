// history_icon_button.dart
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'performance_management_screen_widget.dart';
import 'performance_management_screen_service.dart'; // PerformanceManagementScreenService のインポート

class HistoryIconButton extends StatelessWidget {
  final int questionId;
  final int selectedQuestionType;

  const HistoryIconButton({
    Key? key,
    required this.questionId,
    required this.selectedQuestionType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PerformanceManagementScreenService _service =
        Get.find<PerformanceManagementScreenService>();
    // キャッシュを監視し、変更があった場合のみ再ビルド
    return Obx(() {
      bool? hasHistory = _service.hasHistoryCache[questionId];

      // キャッシュがない場合はロード中の状態を表示
      if (hasHistory == null) {
        // 初回のみデータを取得
        _service.hasPerformanceDescriptiveHistory(questionId);
        return Icon(
          Icons.history,
          color: Colors.grey.withOpacity(0.6),
        );
      }

      return IconButton(
        icon: Icon(
          Icons.history,
          color: hasHistory
              ? const Color.fromARGB(255, 24, 224, 74) // 成績履歴がある場合は緑色
              : Colors.grey, // 成績履歴がない場合は灰色
        ),
        onPressed: () {
          if (hasHistory) {
            /*Get.to(() => PerformanceManagementScreen(
                  questionId: questionId,
                ));*/
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PerformanceManagementScreen(
                        questionId: questionId,
                      )),
            );
          } else {
            _showDialog(context);
          }
        },
      );
    });
  }

  /// 成績履歴がない場合に表示するダイアログ
  void _showDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("通知"),
          content: Text("成績履歴が存在しません。"),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("OK"),
            ),
          ],
        );
      },
    );
  }
}
