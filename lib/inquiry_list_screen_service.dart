import 'package:cloud_firestore/cloud_firestore.dart';

class InquiryListScreenService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Future<Map<String, Map<String, List<Map<String, dynamic>>>>>
      getInquiries() async {
    // Firestoreコレクションからデータを取得する
    QuerySnapshot snapshot = await _firestore.collection('inquiries').get();

    // カテゴリごとにデータを分類する
    Map<String, Map<String, List<Map<String, dynamic>>>> inquiries = {};

    for (var doc in snapshot.docs) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      String mainCategory = data['type'] ?? '未分類';
      String subCategory = data['subcategory'] ?? '未分類';

      if (!inquiries.containsKey(mainCategory)) {
        inquiries[mainCategory] = {};
      }

      if (!inquiries[mainCategory]!.containsKey(subCategory)) {
        inquiries[mainCategory]![subCategory] = [];
      }

      inquiries[mainCategory]![subCategory]!.add({
        'title': data['title'] ?? data['timestamp'].toDate().toString(),
        'details': data['content'] ?? '',
        'date': data['timestamp'].toDate().toString(),
        'status': data['status'] ?? '',
        'severity': data['severity'] ?? '',
        'adminComment': data['adminComment'] ?? '',
      });
    }

    return inquiries;
  }
}
