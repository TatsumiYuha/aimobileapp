import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';

class PerformanceManagementScreenService extends GetxService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final User? currentUser = FirebaseAuth.instance.currentUser;

  // キャッシュのためのマップ
  final RxMap<int, bool> hasHistoryCache = <int, bool>{}.obs;

  // Firestoreの変更を監視するためのサブスクリプションを管理
  final Map<int, StreamSubscription> _subscriptions = {};

  // 日付のカスタムフォーマット
  String _formatTimestamp(Timestamp timestamp) {
    DateTime date = timestamp.toDate();
    String month = date.month.toString();
    String day = date.day.toString();
    String hour = date.hour.toString().padLeft(2, '0');
    String minute = date.minute.toString().padLeft(2, '0');

    return "$month月$day日 $hour時$minute分";
  }

  // 用語記述問題をFirestoreから取得
  Future<List<Map<String, dynamic>>> getDescriptiveQuestions() async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('questions')
          .doc('Descriptive')
          .collection('quiz')
          .orderBy('questionListId')
          .get();

      List<Map<String, dynamic>> questionList = querySnapshot.docs.map((doc) {
        return {
          'questionId': doc['questionListId'],
          'questionName': doc['questionListName'], // 用語記述問題の名前などを保存
        };
      }).toList();

      return questionList;
    } catch (e) {
      print('Error getting descriptive questions: $e');
      throw e;
    }
  }

  // 成績履歴を取得
  Future<List<Map<String, dynamic>>> getPerformanceDescriptiveHistory(
      int questionId) async {
    // currentUser が null でないことを確認
    if (currentUser == null) {
      throw Exception('User is not authenticated');
    }

    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('users')
          .doc(currentUser!.uid)
          .collection('userAnswers')
          .where('questionId', isEqualTo: questionId)
          .where('questionType', isEqualTo: 'Descriptive')
          .get();

      List<Map<String, dynamic>> performanceList =
          querySnapshot.docs.map((doc) {
        final data = doc.data() as Map<String, dynamic>;

        // 'correctCount' フィールドの取得、存在しない場合は 0 を設定
        int correctCount = 0;
        if (data.containsKey('correctCount') && data['correctCount'] is int) {
          correctCount = data['correctCount'] as int;
        }

        // 'timestamp' フィールドの取得とフォーマット、存在しない場合は null を設定
        String? formattedTimestamp;
        if (data.containsKey('timestamp') && data['timestamp'] is Timestamp) {
          formattedTimestamp = _formatTimestamp(data['timestamp'] as Timestamp);
        } else {
          formattedTimestamp = 'データが存在しません';
        }

        return {
          'correctCount': correctCount,
          'docId': doc.id,
          'timestamp': formattedTimestamp,
        };
      }).toList();

      return performanceList;
    } catch (e) {
      print('Error getting performance history: $e');
      throw e;
    }
  }

  //成績詳細を取得
  Future<List<Map<String, dynamic>>?> fetchSingleDocument(String docId) async {
    // Firestoreの特定のドキュメントを取得
    DocumentSnapshot documentSnapshot = await _firestore
        .collection('users')
        .doc(currentUser?.uid)
        .collection('userAnswers')
        .doc(docId)
        .get();

    if (documentSnapshot.exists) {
      // Firestoreから取得したデータ
      Map<String, dynamic>? data =
          documentSnapshot.data() as Map<String, dynamic>?;

      if (data != null) {
        // questionListフィールドをもとの形式（List<Map<String, dynamic>>）に変換
        List<Map<String, dynamic>> questionAnswerList =
            List<Map<String, dynamic>>.from(data['questionList']);

        // questionAnswerListをreturn
        return questionAnswerList;
      }
    }

    // ドキュメントが存在しない場合やデータがnullの場合はnullを返す
    return null;
  }

  /// 指定されたquestionIdに対する成績履歴が存在するかを確認し、キャッシュします。
  Future<bool> hasPerformanceDescriptiveHistory(int questionId) async {
    // 既にキャッシュがある場合はそれを返す
    if (hasHistoryCache.containsKey(questionId)) {
      return hasHistoryCache[questionId]!;
    }

    // currentUser が null でないことを確認
    if (currentUser == null) {
      throw Exception('ユーザーが認証されていません。');
    }

    // Firestoreからデータを取得
    QuerySnapshot querySnapshot = await _firestore
        .collection('users')
        .doc(currentUser!.uid)
        .collection('userAnswers')
        .where('questionId', isEqualTo: questionId)
        .where('questionType', isEqualTo: 'Descriptive')
        .limit(1) // 1件でも存在すれば十分
        .get();

    bool hasHistory = querySnapshot.docs.isNotEmpty;
    hasHistoryCache[questionId] = hasHistory; // キャッシュに保存

    // Firestoreの変更を監視
    _watchPerformanceDescriptiveHistory(questionId);

    return hasHistory;
  }

  /// Firestoreのデータ変更を監視し、キャッシュを更新
  void _watchPerformanceDescriptiveHistory(int questionId) {
    if (_subscriptions.containsKey(questionId)) {
      return; // 既に監視中の場合は何もしない
    }

    final subscription = _firestore
        .collection('users')
        .doc(currentUser!.uid)
        .collection('userAnswers')
        .where('questionId', isEqualTo: questionId)
        .where('questionType', isEqualTo: 'Descriptive')
        .snapshots()
        .listen((snapshot) {
      bool hasHistory = snapshot.docs.isNotEmpty;
      hasHistoryCache[questionId] = hasHistory; // キャッシュを更新
    });

    _subscriptions[questionId] = subscription;
  }

  @override
  void onClose() {
    // メモリリークを防ぐためにすべてのリスナーをキャンセル
    for (var subscription in _subscriptions.values) {
      subscription.cancel();
    }
    _subscriptions.clear();
    super.onClose();
  }
}
