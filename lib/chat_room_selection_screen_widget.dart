import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'chat_screen_widget.dart';
import 'auth_service.dart';
import 'custom_bottom_app_bar.dart';
import 'chat_room_selection_service.dart';

// チュートリアル関連補助ファイル（既存の実装を利用）
import 'tutorial_progress_manager.dart';
import 'blue_square_overlay.dart';
import 'help_overlay_service.dart';
import 'animation_build_display.dart';

class ChatRoomSelectionWidget extends StatefulWidget {
  @override
  _ChatRoomSelectionWidgetState createState() =>
      _ChatRoomSelectionWidgetState();
}

class _ChatRoomSelectionWidgetState extends State<ChatRoomSelectionWidget> {
  // Firebase等の基本設定
  final User? currentUser = FirebaseAuth.instance.currentUser;
  final ChatRoomSelectionService _service = ChatRoomSelectionService();

  // 検索関連
  String _searchQuery = '';
  final TextEditingController _searchController = TextEditingController();
  Timer? _debounce;

  // 並び替え関連
  String _sortOption = 'order';

  // チャットルームデータ
  List<DocumentSnapshot> _chatRooms = [];
  late Stream<QuerySnapshot> _chatRoomsStream;

  // FABタップのデバウンス用
  DateTime? _lastTapTime;

  ////////////// チュートリアル／オンボーディング用の変数 //////////////
  final tutorialProgressManager _tutorial = tutorialProgressManager();
  bool _isTutorialActive = false; // 操作説明（チュートリアル）が実行中か
  bool _isOnboadingActive = false; // オンボーディング中か
  bool _isShowDialog = false; // 「操作説明を開始しますか？」のダイアログ表示
  bool _isCondition = false; // 操作が完了したかの一時フラグ
  bool showHelp = false; // ヘルプオーバーレイ表示用
  bool _isVisible = false; // 操作説明での青い枠表示制御

  int _currentStep = 0; // 操作説明（チュートリアル）進行管理
  int _guidanceNum = 0; // ガイダンス進行管理（オンボーディング用）

  // ガイダンス用のメッセージ（オンボーディング初期表示）
  final List<String> _guidance = [
    "ようこそ！この画面ではチャットルームの操作方法を説明します。",
    "上部の検索バーでチャットルームを探すことができます。",
    "検索バー下の並び替えオプションでリストの順序を変更できます。",
    "チャットルームリストはスワイプで操作メニューが表示されます。",
    "リストアイテムはドラッグで並び替えが可能です。",
    "右下の『＋』ボタンをタップして新規チャットルームを作成してみましょう。",
    "上部のログアウトボタンからサインアウトも可能です。",
    "以上で説明は終了です。",
  ];

  // 各操作説明（チュートリアル）用のステップ
  final List<Map<String, dynamic>> _steps = [
    {'text': 'チャットルーム選択画面の操作説明を開始します', 'requires': null},
    {'text': '検索バーに「Java」と入力してみましょう', 'requires': 'search'},
    {'text': '検索欄の「×」アイコンをタップして、検索をクリアしてください', 'requires': 'search_clear'},
    {'text': '並び替えオプションから順序を変更してみましょう', 'requires': 'sort'},
    {
      'text': 'リストアイテムをスライドした後に表示される★マークをタップして、削除や編集の操作メニューを確認してみましょう',
      'requires': 'slidable'
    },
    {
      'text': 'この操作でチャットルームがお気に入り登録状態になり、再度スライドして★をタップすれば解除できます。',
      'requires': 'register'
    },
    {'text': 'リストアイテムをドラッグして順序を変更してみましょう', 'requires': 'reorder'},
    {'text': 'これでチャットルーム画面の説明は終了です', 'requires': null},
    {'text': '右下の「＋」ボタンをタップして新規チャットルームを作成してみましょう', 'requires': null},
  ];

  // ヘルプオーバーレイ用メッセージなど（そのまま）
  final List<String> helpMessages = [
    "検索バー: チャットルームを検索できます",
    "並び替えオプション: リストの順序を変更できます",
    "リスト: 各チャットルームの詳細が確認できます",
    "スワイプ: 削除・編集・お気に入り設定が可能です",
    "ドラッグ: チャットルームの並び替えができます",
    "FAB: 新しいチャットルームを作成します",
  ];

  final List<String> backgroundImages = [
    'assets/Help_search.png',
    'assets/Help_sort.png',
    'assets/Help_list.png',
    'assets/Help_slidable.png',
    'assets/Help_reorder.png',
    'assets/Help_fab.png',
  ];

  final List<Alignment> messageAlignment = [
    Alignment.topCenter,
    Alignment.topCenter,
    Alignment.center,
    Alignment.center,
    Alignment.center,
    Alignment.bottomCenter,
  ];

  final List<EdgeInsets> margin = [
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(bottom: 20),
  ];

  // ヘルプ用：ハイライト矩形（例示用の固定値）
  final List<Rect> rectangleSteps = [
    Rect.fromLTWH(20, 100, 360, 50), // 検索バー
    Rect.fromLTWH(20, 160, 360, 50), // 並び替えドロップダウン
    Rect.fromLTWH(20, 220, 360, 400), // チャットルームリスト
    Rect.fromLTWH(300, 600, 60, 60), // FAB（新規作成ボタン）
    Rect.fromLTWH(0, 0, 0, 0), // （オプション）
    Rect.fromLTWH(0, 0, 0, 0), // （オプション）
  ];

  //////////// 対象ウィジェットのGlobalKey（既存） ////////////
  final GlobalKey _searchBarKey = GlobalKey();
  final GlobalKey _sortOptionKey = GlobalKey();
  final GlobalKey _chatRoomListKey = GlobalKey();
  final GlobalKey _fabKey = GlobalKey();
  final GlobalKey _logoutKey = GlobalKey();
  final GlobalKey _firstChatRoomKey = GlobalKey();
  final GlobalKey _searchClearKey = GlobalKey();
  //////////// ここまで

  //////////// ガイダンス用オーバーレイ変数（既存） ////////////
  double _guidanceOverlayTop = 0.0;
  double _guidanceOverlayLeft = 0.0;
  double _guidanceOverlayWidth = 0.0;
  double _guidanceOverlayHeight = 0.0;
  //////////// ここまで

  //////////// 操作説明用オーバーレイ変数（新規） ////////////
  double _tutorialOverlayTop = 0.0;
  double _tutorialOverlayLeft = 0.0;
  double _tutorialOverlayWidth = 0.0;
  double _tutorialOverlayHeight = 0.0;
  //////////// ここまで

  // ガイダンス用の対象ウィジェットのGlobalKeyを返す（既存）
  GlobalKey? _getGuidanceTargetKey() {
    switch (_guidanceNum) {
      case 1:
        return _searchBarKey;
      case 2:
        return _sortOptionKey;
      case 3:
        return _chatRoomListKey;
      case 4:
        return _firstChatRoomKey;
      case 5:
        return _fabKey;
      case 6:
        return _logoutKey;
      default:
        return null;
    }
  }

  // ガイダンス用の対象ウィジェットの位置とサイズを更新する（既存）
  void _updateGuidanceOverlayPosition() {
    GlobalKey? targetKey = _getGuidanceTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      // 特にガイダンスステップ4（ドラッグの説明）の場合、先頭アイテムの描画が未完了なら遅延させる
      if (_guidanceNum == 4) {
        Future.delayed(Duration(milliseconds: 100), () {
          _updateGuidanceOverlayPosition();
        });
      }
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _guidanceOverlayTop = globalPosition.dy;
      _guidanceOverlayLeft = globalPosition.dx;
      _guidanceOverlayWidth = size.width;
      _guidanceOverlayHeight = size.height;
    });
  }

  //////////// 操作説明用の対象ウィジェットのGlobalKeyを返す関数（新規） ////////////
  GlobalKey? _getTutorialTargetKey() {
    final String? req = _steps[_currentStep]['requires'];
    if (req == null) return null;
    switch (req) {
      case 'search':
        return _searchBarKey;
      case 'search_clear':
        return _searchClearKey;
      case 'sort':
        return _sortOptionKey;
      case 'slidable':
        return _firstChatRoomKey;
      case 'reorder':
        return _firstChatRoomKey;
      case 'floatingbutton':
        return _fabKey;
      case 'logout':
        return _logoutKey;
      default:
        return null;
    }
  }

  // 操作説明用の対象ウィジェットの位置とサイズを更新する（新規）
  void _updateTutorialOverlayPosition() {
    GlobalKey? targetKey = _getTutorialTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      if (_steps[_currentStep]['requires'] == 'slidable') {
        Future.delayed(Duration(milliseconds: 100), () {
          _updateTutorialOverlayPosition();
        });
      }
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _tutorialOverlayTop = globalPosition.dy;
      _tutorialOverlayLeft = globalPosition.dx;
      _tutorialOverlayWidth = size.width;
      _tutorialOverlayHeight = size.height;
    });
  }
  //////////// ここまで操作説明用

  @override
  void initState() {
    super.initState();

    _searchController.addListener(_onSearchChanged);
    _chatRoomsStream = _getChatRoomsStream();

    // デバッグ用・初回起動時として強制的にオンボーディング（ガイダンス）を開始
    _checkAndStartTutorial();

    _loadProgress();
  }

  @override
  void dispose() {
    _searchController.removeListener(_onSearchChanged);
    _searchController.dispose();
    _debounce?.cancel();
    super.dispose();
  }

  Future<void> _checkAndStartTutorial() async {
    // ここでは画面ごとに一意なキーを用いる（例：ChatScreenWidget の場合はクラス名や固有のID）
    // 複数の画面で管理する場合、各画面で異なるキーを使用してください。
    final String tutorialKey = 'tutorialShown_${this.runtimeType}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstTime = prefs.getBool(tutorialKey) ?? true;
    if (isFirstTime) {
      // 初回の場合のみチュートリアル／ガイダンスを発火させる
      setState(() {
        _isOnboadingActive = true;
        _guidanceNum = 0;
      });
      // 画面描画後にオーバーレイ位置を更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
      // 一度発火したのでフラグを更新（2回目以降は発火しない）
      await prefs.setBool(tutorialKey, false);
    }
  }

  Future<void> _loadProgress() async {
    int progress = await _tutorial.getProgress('currentStep');
    if (progress < _steps.length) {
      setState(() {
        _currentStep = progress;
      });
    }
  }

  Future<void> _saveProgress(int step) async {
    await _tutorial.saveProgress('currentStep', step);
  }

  Future<void> _resetProgress() async {
    setState(() {
      _currentStep = 0;
    });
    await _tutorial.resetProgress();
  }

  Future<void> _saveTutorialStatus() async {
    await _tutorial.saveTutorialBoolStatus(_isTutorialActive);
    await _tutorial.saveOnboadingBoolStatus(_isOnboadingActive);
    await _tutorial.saveShowDialogBoolStatus(_isShowDialog);
  }

  void _getTutorial() async {
    bool futureTutorialActive = await _tutorial.loadTutorialBoolStatus();
    bool futureOnboardingActive = await _tutorial.loadOnboadingBoolStatus();
    bool futureShowDialogActive = await _tutorial.loadShowDialogBoolStatus();
    setState(() {
      _isTutorialActive = futureTutorialActive;
      _isOnboadingActive = futureOnboardingActive;
      _isShowDialog = futureShowDialogActive;
    });
  }

  void _startTutorial() {
    setState(() {
      _currentStep = 0;
      _isTutorialActive = true;
      _isOnboadingActive = false;
      _isShowDialog = false;
    });
    _saveProgress(0);
    _saveTutorialStatus();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _endTutorial() {
    setState(() {
      _isTutorialActive = false;
      _isOnboadingActive = false;
      _isShowDialog = false;
      _currentStep = 0;
      _isVisible = false;
    });
    _resetProgress();
    _saveTutorialStatus();
  }

  void _guidanceStep() {
    if (_guidanceNum < _guidance.length - 1) {
      setState(() {
        _guidanceNum++;
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
    } else {
      setState(() {
        _isShowDialog = true;
        _isOnboadingActive = true;
      });
    }
  }

  void _noTap() {
    setState(() {
      _currentStep = _steps.length - 1;
      _isTutorialActive = true;
    });
  }

  void _endGuidance() {
    setState(() {
      _isShowDialog = false;
      _isOnboadingActive = false;
    });
  }

  void _nextStep() {
    if (_currentStep < _steps.length - 1) {
      setState(() {
        _currentStep++;
        _isCondition = false;
      });
      _saveProgress(_currentStep);
    } else {
      _endTutorial();
      _saveProgress(_currentStep);
    }
    _checkVisible();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _checkAndProceed() {
    final requires = _steps[_currentStep]['requires'];
    if (requires == null || _isCondition) {
      _nextStep();
    }
  }

  void _conditionMet() {
    if (_isTutorialActive) {
      setState(() {
        _isCondition = true;
      });
      _nextStep();
    }
  }

  // 従来の青い枠表示制御（操作説明用）
  void _checkVisible() {
    if (_currentStep >= 1 && _currentStep <= 10) {
      setState(() {
        _isVisible = true;
      });
    } else {
      setState(() {
        _isVisible = false;
      });
    }
  }
  //////////////////////////////////////////////////////////////////////

  Stream<QuerySnapshot> _getChatRoomsStream() {
    if (currentUser == null) {
      return Stream.empty();
    }
    Query query = FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser!.uid)
        .collection('chatRooms')
        .where('isDeleted', isEqualTo: false);

    if (_sortOption == 'name') {
      query = query.orderBy('isFavorite', descending: true).orderBy('name');
    } else if (_sortOption == 'timestamp') {
      query = query
          .orderBy('isFavorite', descending: true)
          .orderBy('timestamp', descending: true);
    } else {
      query = query.orderBy('isFavorite', descending: true).orderBy('order');
    }
    return query.snapshots();
  }

  void _onSearchChanged() {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(const Duration(milliseconds: 300), () {
      setState(() {
        _searchQuery = _searchController.text.trim().toLowerCase();
      });
      if (_isTutorialActive &&
          _steps[_currentStep]['requires'] == 'search' &&
          _searchQuery == "java") {
        _conditionMet();
      }
    });
  }

  String _formatTimestamp(Timestamp timestamp) {
    DateTime date = timestamp.toDate();
    return "${date.month}月${date.day}日 ${date.hour}時${date.minute}分";
  }

  void _showLogoutDialog() {
    if (_isTutorialActive && _steps[_currentStep]['requires'] == 'logout') {
      _conditionMet();
    }
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('ログアウト'),
          content: Text('本当にログアウトしますか？'),
          actions: <Widget>[
            TextButton(
              child: Text('キャンセル'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('ログアウト'),
              onPressed: () {
                Navigator.of(context).pop();
                AuthService.logout(context);
              },
            ),
          ],
        );
      },
    );
  }

  void _onSortOptionChanged(String? newValue) {
    setState(() {
      _sortOption = newValue!;
      _chatRoomsStream = _getChatRoomsStream();
    });
    if (_isTutorialActive && _steps[_currentStep]['requires'] == 'sort') {
      _conditionMet();
    }
  }

  void _onFabPressed() async {
    final now = DateTime.now();
    if (_lastTapTime == null ||
        now.difference(_lastTapTime!) > Duration(seconds: 1)) {
      _lastTapTime = now;
      if (_isTutorialActive &&
          _steps[_currentStep]['requires'] == 'floatingbutton') {
        _conditionMet();
      }
      try {
        await _service.createChatRoom(context, '新規ルーム', currentUser);
      } catch (e, stack) {
        print("createChatRoomエラー: $e\n$stack");
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text('チャットルームの作成に失敗しました。')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            title: Text('ChatGPTに質問する', style: TextStyle(color: Colors.black)),
            backgroundColor: Colors.white,
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(Icons.help_outline,
                    color: _isTutorialActive ? Colors.grey : Colors.black),
                onPressed: () {
                  if (!_isTutorialActive) {
                    // オンボーディング（ガイダンス）を開始
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      setState(() {
                        _isOnboadingActive = true;
                        _guidanceNum = 0;
                      });
                      _updateGuidanceOverlayPosition();
                    });

                    _loadProgress();
                  }
                },
              ),
              IconButton(
                key: _logoutKey,
                onPressed: _showLogoutDialog,
                icon: Icon(Icons.logout, color: Colors.black),
              ),
            ],
          ),
          body: Column(
            children: [
              // 検索バー（GlobalKey _searchBarKey）
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  key: _searchBarKey,
                  child: TextField(
                    controller: _searchController,
                    decoration: InputDecoration(
                      labelText: 'チャットルームを検索',
                      prefixIcon: Icon(Icons.search),
                      suffixIcon: _searchQuery.isNotEmpty
                          ? IconButton(
                              key: _searchClearKey,
                              icon: Icon(Icons.clear),
                              onPressed: () {
                                _searchController.clear();
                                FocusScope.of(context).unfocus();
                                if (_isTutorialActive &&
                                    _steps[_currentStep]['requires'] ==
                                        'search_clear') {
                                  _conditionMet();
                                }
                              },
                            )
                          : null,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                  ),
                ),
              ),
              // 並び替えオプション（GlobalKey _sortOptionKey）
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Container(
                  key: _sortOptionKey,
                  child: Row(
                    children: [
                      Icon(Icons.sort, color: Colors.black),
                      SizedBox(width: 8),
                      Expanded(
                        child: DropdownButton<String>(
                          value: _sortOption,
                          underline: Container(),
                          isExpanded: true,
                          items: [
                            DropdownMenuItem(
                                value: 'order', child: Text('カスタム順')),
                            DropdownMenuItem(
                                value: 'timestamp', child: Text('最新順')),
                            DropdownMenuItem(value: 'name', child: Text('名前順')),
                          ],
                          onChanged: _onSortOptionChanged,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // チャットルームリスト（GlobalKey _chatRoomListKey）
              Expanded(
                child: Container(
                  key: _chatRoomListKey,
                  child: StreamBuilder<QuerySnapshot>(
                    stream: _chatRoomsStream,
                    builder: (ctx, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(child: CircularProgressIndicator());
                      }
                      if (snapshot.hasError) {
                        return Center(child: Text('エラーが発生しました。'));
                      }
                      if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                        return Center(child: Text('右下の「＋」からAIとの会話を始めましょう'));
                      }
                      _chatRooms = snapshot.data!.docs.where((doc) {
                        final roomName = (doc['name'] as String).toLowerCase();
                        return roomName.contains(_searchQuery);
                      }).toList();
                      if (_chatRooms.isEmpty) {
                        return Center(child: Text('一致するチャットルームがありません'));
                      }
                      return ReorderableListView.builder(
                        onReorder: (int oldIndex, int newIndex) async {
                          if (oldIndex < newIndex) {
                            newIndex -= 1;
                          }
                          final movedItem = _chatRooms.removeAt(oldIndex);
                          _chatRooms.insert(newIndex, movedItem);
                          WriteBatch batch = FirebaseFirestore.instance.batch();
                          for (int i = 0; i < _chatRooms.length; i++) {
                            DocumentSnapshot doc = _chatRooms[i];
                            batch.update(doc.reference, {'order': i});
                          }
                          await batch.commit();
                          setState(() {});
                          if (_isTutorialActive &&
                              _steps[_currentStep]['requires'] == 'reorder') {
                            _conditionMet();
                          }
                        },
                        itemCount: _chatRooms.length,
                        itemBuilder: (ctx, index) {
                          final chatRoom = _chatRooms[index];
                          final isFavorite = chatRoom['isFavorite'] ?? false;
                          final isDetailExists =
                              (chatRoom.data() as Map<String, dynamic>?)
                                      ?.containsKey('isDetail') ??
                                  false;
                          final avatarColor =
                              isDetailExists ? Colors.green : Colors.blueAccent;

                          Widget listTile = Slidable(
                            key: ValueKey(chatRoom.id),
                            endActionPane: ActionPane(
                              motion: const ScrollMotion(),
                              children: [
                                if (!isFavorite)
                                  SlidableAction(
                                    onPressed: (context) {
                                      if (!_isTutorialActive)
                                        _service.deleteChatRoom(
                                            chatRoom.id, currentUser);
                                    },
                                    backgroundColor: Colors.red,
                                    foregroundColor: Colors.white,
                                    icon: Icons.delete,
                                  ),
                                SlidableAction(
                                  onPressed: (context) {
                                    if (!_isTutorialActive)
                                      _service.changeChatRoomName(
                                          context, chatRoom.id, currentUser);
                                  },
                                  backgroundColor: Colors.blue,
                                  foregroundColor: Colors.white,
                                  icon: Icons.edit,
                                ),
                                SlidableAction(
                                  onPressed: (context) {
                                    _service.toggleFavoriteChatRoom(
                                        chatRoom.id, currentUser, isFavorite);
                                    if (_isTutorialActive &&
                                        _steps[_currentStep]['requires'] ==
                                            'slidable') {
                                      _conditionMet();
                                    } else if (_isTutorialActive &&
                                        _steps[_currentStep]['requires'] ==
                                            'register') {
                                      _conditionMet();
                                    }
                                  },
                                  backgroundColor: Colors.orange,
                                  foregroundColor: Colors.white,
                                  icon: isFavorite
                                      ? Icons.star
                                      : Icons.star_border,
                                ),
                              ],
                            ),
                            child: ListTile(
                              key: ValueKey(chatRoom.id),
                              leading: Stack(
                                alignment: Alignment.center,
                                children: [
                                  CircleAvatar(
                                    backgroundColor: avatarColor,
                                    child: Text(
                                      (chatRoom['name'] as String).isNotEmpty
                                          ? (chatRoom['name'] as String)[0]
                                              .toUpperCase()
                                          : 'C',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  if (isFavorite)
                                    Positioned(
                                      right: 0,
                                      bottom: 0,
                                      child: Icon(Icons.star,
                                          size: 16, color: Colors.yellow),
                                    ),
                                ],
                              ),
                              title: Text(chatRoom['name'],
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                              subtitle: chatRoom['timestamp'] != null
                                  ? Text(
                                      _formatTimestamp(chatRoom['timestamp']),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: TextStyle(color: Colors.grey[600]),
                                    )
                                  : Text(
                                      '--:--:--',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: TextStyle(color: Colors.grey[600]),
                                    ),
                              trailing: Icon(Icons.drag_handle,
                                  size: 24, color: Colors.grey),
                              onTap: () {
                                String roomId = chatRoom.id;
                                if (!_isTutorialActive)
                                  _service.navigateToChatRoom(context, roomId);
                              },
                            ),
                          );
                          // 操作説明「reorder」で、対象はリストの先頭アイテム
                          if (index == 0) {
                            listTile = Container(
                              key: _firstChatRoomKey,
                              child: listTile,
                            );
                          }
                          return listTile;
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
          floatingActionButton: FloatingActionButton(
            key: _fabKey,
            child: Icon(Icons.add, color: Colors.black),
            onPressed: _onFabPressed,
            backgroundColor: Colors.white,
            elevation: 4.0,
          ),
        ),
        // チュートリアル／オンボーディングオーバーレイ（ガイダンス部分）
        if (_isOnboadingActive && !_isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isOnboadingActive && !_isTutorialActive)
          GestureDetector(
            onTap: _guidanceStep,
            child: Center(
              child: Stack(
                children: [
                  _buildBottomPanel(_guidance[_guidanceNum]),
                  if (_currentStep < _steps.length - 1)
                    Positioned(
                      right: 0,
                      top: 0,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          if (_isOnboadingActive) {
                            _endGuidance();
                          } else if (_isTutorialActive) {
                            _endTutorial();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                            side: BorderSide(color: Colors.black, width: 2.5),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/Skip.png',
                          width: 25,
                          height: 25,
                        ),
                        label: Text(
                          "スキップ",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
        if (_isOnboadingActive && _isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isShowDialog) Center(child: _buildTutorialDialog()),
        // 操作説明（チュートリアル）パネル
        if (_isTutorialActive && !_isOnboadingActive)
          GestureDetector(
            onTap: _checkAndProceed,
            child: Padding(
              padding: EdgeInsets.only(bottom: 80), // FABと被らないよう下部に余白を追加
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Stack(
                  children: [
                    _buildBottomPanel(_steps[_currentStep]['text']),
                    if (_currentStep < _steps.length - 1)
                      Positioned(
                        right: 0,
                        top: 0,
                        child: ElevatedButton.icon(
                          onPressed: () {
                            if (_isOnboadingActive) {
                              _endGuidance();
                            } else if (_isTutorialActive) {
                              _endTutorial();
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                              side: BorderSide(color: Colors.black, width: 2.5),
                            ),
                          ),
                          icon: Image.asset(
                            'assets/Skip.png',
                            width: 25,
                            height: 25,
                          ),
                          label: Text(
                            "スキップ",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        // FABタップ促進のアニメーション
        // ヘルプオーバーレイ
        if (showHelp)
          HelpOverlay(
            existingWidget: Container(),
            helpMessages: helpMessages,
            backgroundImages: backgroundImages,
            onSkip: () {
              setState(() {
                showHelp = false;
              });
            },
            messageAlignment: messageAlignment,
            margin: margin,
            rectangleSteps: rectangleSteps,
          ),
        // ガイダンス中なら、ガイダンス用の青い枠を表示する
        if (_isOnboadingActive)
          BlueSquareOverlay(
            top: _guidanceOverlayTop,
            left: _guidanceOverlayLeft,
            width: _guidanceOverlayWidth,
            height: _guidanceOverlayHeight,
            flashing: true,
            isVisible: true,
          ),

        // 操作説明用の青い枠（動的に対象ウィジェットの位置を取得）
        if (_isTutorialActive && _isVisible)
          BlueSquareOverlay(
            top: _tutorialOverlayTop,
            left: _tutorialOverlayLeft,
            width: _tutorialOverlayWidth,
            height: _tutorialOverlayHeight,
            flashing: true,
            isVisible: _isVisible,
          ),
      ],
    );
  }

  Widget _buildBackgroundOverlay() {
    return Container(
      color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Widget _buildBottomPanel(String text) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.lightBlue, width: 3),
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 16, color: Colors.black, fontWeight: FontWeight.w600),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTutorialDialog() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 252, 246, 251),
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black26, blurRadius: 10.0, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '操作説明を開始しますか？',
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.black87,
                fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  _startTutorial();
                  _endGuidance();
                },
                child: Text(
                  'はい',
                  style: TextStyle(color: Colors.purple, fontSize: 16.0),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _noTap();
                  _endGuidance();
                },
                child: Text(
                  'いいえ',
                  style: TextStyle(color: Colors.purple, fontSize: 16.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
