// auth_screen_widget.dart
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'auth_service.dart';
import 'chat_room_selection_screen_widget.dart';
import 'password_reset_screen_widget.dart'; // パスワードリセット画面のインポート
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthScreenWidget extends StatefulWidget {
  @override
  _AuthScreenWidgetState createState() => _AuthScreenWidgetState();
}

class _AuthScreenWidgetState extends State<AuthScreenWidget> {
  final AuthService _authService = AuthService(); // AuthServiceのインスタンスを生成
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _obscurePassword = true;
  bool _isLoading = false; // ローディング状態を管理するフラグ
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // ログイン試行回数とロックアウト時間の管理
  int _failedAttemptCount = 0;
  DateTime? _lockoutExpiryTime;
  Timer? _lockoutTimer;
  Duration _remainingLockout = Duration.zero;

  // 定数
  static const int maxFailedAttempts = 5;
  static const Duration lockoutDuration = Duration(minutes: 5);

  final FlutterSecureStorage _storage = FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    _loadLoginAttempts(); // セキュアストレージからログイン試行データをロード
    _loadEmailFromStorage(); // セキュアストレージからメールアドレスをロード
  }

  /// セキュアストレージからログイン試行データをロードします。
  Future<void> _loadLoginAttempts() async {
    String? failedAttemptsStr = await _storage.read(key: 'failedAttemptCount');
    String? lockoutExpiryStr = await _storage.read(key: 'lockoutExpiryTime');

    if (failedAttemptsStr != null) {
      _failedAttemptCount = int.tryParse(failedAttemptsStr) ?? 0;
    }

    if (lockoutExpiryStr != null) {
      _lockoutExpiryTime = DateTime.tryParse(lockoutExpiryStr);
      if (_lockoutExpiryTime != null &&
          DateTime.now().isBefore(_lockoutExpiryTime!)) {
        _startLockoutTimer();
      } else {
        // ロックアウトが期限切れの場合、リセット
        _resetLoginAttempts();
      }
    }
  }

  /// ログイン試行データをセキュアストレージに保存します。
  Future<void> _saveLoginAttempts() async {
    await _storage.write(
        key: 'failedAttemptCount', value: _failedAttemptCount.toString());
    if (_lockoutExpiryTime != null) {
      await _storage.write(
          key: 'lockoutExpiryTime',
          value: _lockoutExpiryTime!.toIso8601String());
    }
  }

  /// ログイン試行データをリセットします。
  Future<void> _resetLoginAttempts() async {
    _failedAttemptCount = 0;
    _lockoutExpiryTime = null;
    await _storage.delete(key: 'failedAttemptCount');
    await _storage.delete(key: 'lockoutExpiryTime');
    if (_lockoutTimer != null) {
      _lockoutTimer!.cancel();
      _lockoutTimer = null;
    }
    setState(() {
      _remainingLockout = Duration.zero;
    });
  }

  /// ロックアウトタイマーを開始します。
  void _startLockoutTimer() {
    if (_lockoutExpiryTime == null) return;

    setState(() {
      _remainingLockout = _lockoutExpiryTime!.difference(DateTime.now());
    });

    _lockoutTimer?.cancel();
    _lockoutTimer = Timer.periodic(Duration(seconds: 1), (timer) {
      final now = DateTime.now();
      if (_lockoutExpiryTime != null && now.isBefore(_lockoutExpiryTime!)) {
        setState(() {
          _remainingLockout = _lockoutExpiryTime!.difference(now);
        });
      } else {
        timer.cancel();
        setState(() {
          _remainingLockout = Duration.zero;
          _lockoutExpiryTime = null;
          _failedAttemptCount = 0;
        });
        _saveLoginAttempts();
      }
    });
  }

  /// ログイン処理を実行します。
  Future<void> _login() async {
    if (_isLockedOut()) {
      // ロックアウト中の場合は処理を中断
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('ログインは一時的にロックされています。後でもう一度お試しください。')),
      );
      return;
    }

    if (_isLoading) return; // 既にローディング中の場合は処理を中断

    setState(() {
      _isLoading = true; // ローディング開始
    });

    try {
      var result = await _authService.login(
        _emailController.text.trim(),
        _passwordController.text.trim(),
      );

      if (result['success']) {
        // ログイン成功時にログイン試行データをリセット
        await _resetLoginAttempts();
        // ローディング終了
        setState(() {
          _isLoading = false;
        });
        // ナビゲーションを行う（ChatRoomSelectionWidget へ遷移）
        Get.offAllNamed('/', arguments: {"initialIndex": 0});
      } else {
        // ログイン失敗時に試行回数をインクリメント
        await _incrementFailedAttempts();

        // エラーメッセージを表示し、ボタンを無効化
        String errorMessage = result['message'];
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(errorMessage)),
        );

        // スナックバーの表示時間中はボタンを無効化
        await Future.delayed(Duration(seconds: 3)); // スナックバーのデフォルト表示時間

        setState(() {
          _isLoading = false; // ローディング終了
        });
      }
    } catch (e) {
      // 予期しないエラー
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('ログインに失敗しました。')),
      );
      await _incrementFailedAttempts();

      // スナックバーの表示時間中はボタンを無効化
      await Future.delayed(Duration(seconds: 3));

      setState(() {
        _isLoading = false; // ローディング終了
      });
    }
  }

  /// ログイン試行回数をインクリメントし、必要に応じてロックアウトを設定します。
  Future<void> _incrementFailedAttempts() async {
    _failedAttemptCount += 1;

    if (_failedAttemptCount >= maxFailedAttempts) {
      _lockoutExpiryTime = DateTime.now().add(lockoutDuration);
      _startLockoutTimer();
      // ロックアウトメッセージを表示
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('ログイン試行回数が上限に達しました。5分間ログインできなくなります。')),
      );
    }

    await _saveLoginAttempts();
  }

  /// ログインがロックアウトされているかどうかを確認します。
  bool _isLockedOut() {
    if (_lockoutExpiryTime == null) return false;
    return DateTime.now().isBefore(_lockoutExpiryTime!);
  }

  /// パスワードリセット画面に遷移します。
  void _forgetPassword() {
    Get.to(() => PasswordResetScreen());
  }

  /// セキュアストレージからメールアドレスをロードします。
  Future<void> _loadEmailFromStorage() async {
    String? email = await _storage.read(key: 'email');
    String? password = await _storage.read(key: 'password');
    if (email != null) {
      setState(() {
        _emailController.text = email; // メールアドレスを入力フィールドにセット
      });
    }
    if (password != null) {
      setState(() {
        _passwordController.text = password; // パスワードを入力フィールドにセット
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    bool isLockedOut = _isLockedOut();

    String lockoutMessage = '';
    if (isLockedOut) {
      final minutes = _remainingLockout.inMinutes;
      final seconds = _remainingLockout.inSeconds % 60;
      lockoutMessage = 'ログインは5分間ロックされています。\n残り時間: ${minutes}分${seconds}秒';
    }

    return Scaffold(
      key: _scaffoldKey,
      body: Center(
        child: SingleChildScrollView(
          // キーボード表示時のオーバーフロー防止
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // メールアドレス入力フィールド
                TextField(
                  key: Key('emailField'),
                  controller: _emailController,
                  keyboardType: TextInputType.emailAddress, // キーボードタイプを追加
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(
                      RegExp('[a-zA-Z0-9@._-]'),
                    ), // 半角英数字と特定の記号のみを許可
                  ],
                  decoration: InputDecoration(
                    labelText: 'メールアドレス',
                    border: OutlineInputBorder(),
                  ),
                ),
                SizedBox(height: 16),
                // パスワード入力フィールド
                TextField(
                  key: Key('passwordField'),
                  obscureText: _obscurePassword,
                  controller: _passwordController,
                  decoration: InputDecoration(
                    labelText: 'パスワード',
                    border: OutlineInputBorder(),
                    suffixIcon: IconButton(
                      key: Key('togglePasswordVisibility'),
                      icon: Icon(
                        _obscurePassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          _obscurePassword = !_obscurePassword;
                        });
                      },
                    ),
                  ),
                ),
                SizedBox(height: 20),
                // ロックアウトメッセージ
                if (isLockedOut)
                  Text(
                    lockoutMessage,
                    style: TextStyle(color: Colors.red),
                    textAlign: TextAlign.center,
                  ),
                if (isLockedOut) SizedBox(height: 10),
                // ログインボタン
                ElevatedButton(
                  key: Key('loginButton'),
                  onPressed:
                      (_isLoading || isLockedOut) ? null : _login, // ログイン処理の実行
                  child: _isLoading
                      ? SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                          ),
                        )
                      : Text(
                          'ログイン',
                          style: TextStyle(color: Colors.white),
                        ),
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.indigo),
                    padding: MaterialStateProperty.all<EdgeInsets>(
                      EdgeInsets.symmetric(vertical: 12, horizontal: 24),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                // パスワードリセットボタン
                TextButton(
                  key: Key('passwordResetButton'),
                  onPressed:
                      isLockedOut ? null : _forgetPassword, // パスワードリセットメールの送信
                  child: Text(
                    'パスワードの再設定はこちら',
                    style: TextStyle(color: Colors.indigo),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _lockoutTimer?.cancel(); // タイマーをキャンセル
    super.dispose();
  }
}
