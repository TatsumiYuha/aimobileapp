import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/level_data.dart';
import 'package:ai_mobile_app/xp_gain_overlay.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:intl/intl.dart';
import 'firebase_options.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

// 各ウィジェットのインポート
import 'auth_screen_widget.dart';
import 'chat_room_selection_screen_widget.dart';
import 'question_selection_screen_widget.dart';
import 'glossary_screen_widget.dart';
import 'menu_screen_widget.dart';
import 'question_description_screen_widget.dart';
import 'question_programming_screen_widget.dart';
import 'question_debugging_screen_widget.dart';

import 'questions_provider.dart';
import 'glossary_provider.dart';
import 'inquiry_provider.dart';
import 'bottom_nav_controller.dart';
import 'interruption_data_controller.dart';
import 'performance_management_screen_service.dart';
import 'route_observer.dart';
import 'auth_service.dart';

/// 経験値付与結果を保持するためのデータクラス
class XpRewardDetails {
  final int oldLevel;
  final int newLevel;
  final int oldXp;
  final int newXp;
  final int xpNeededForNextLevel;
  final int gainedXp;
  final int streakDays; // 連続ログイン日数
  XpRewardDetails({
    required this.oldLevel,
    required this.newLevel,
    required this.oldXp,
    required this.newXp,
    required this.xpNeededForNextLevel,
    required this.gainedXp,
    required this.streakDays,
  });
}

Future<void> main() async {
  // Flutter の初期化と向きの固定
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  // BottomNavController を GetX に登録
  Get.put(BottomNavController());

  // Firebase の初期化
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  // ログイン状態および直前の BottomNavigationBar の選択状態を取得
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isLoggedIn = prefs.getBool('isLoggedIn') ?? false;
  String lastActiveItem = prefs.getString('activeBottomBarItem') ?? 'ホーム';
  int initialIndex = 0;
  switch (lastActiveItem) {
    case 'ホーム':
      initialIndex = 0;
      break;
    case 'チャット':
      initialIndex = 1;
      break;
    case '問題':
      initialIndex = 2;
      break;
    case '用語集':
      initialIndex = 3;
      break;
    default:
      initialIndex = 0;
  }

  // ログイン時はスプラッシュ画面（非同期処理待ち）を表示
  String initialRoute = isLoggedIn ? '/splash' : '/auth';

  // 必要な Provider を登録しアプリケーションを起動
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => QuestionsProvider()),
        ChangeNotifierProvider(create: (_) => GlossaryProvider()),
        ChangeNotifierProvider(create: (_) => InquiryFormProvider()),
      ],
      child: MyApp(
        initialRoute: initialRoute,
        initialIndex: initialIndex,
      ),
    ),
  );
}

//updateLoginStreak の戻り値を bool から新しいストリーク数（int?）に変更
Future<int?> updateLoginStreak(User? currentUser) async {
  if (currentUser == null) return null;
  final firestore = FirebaseFirestore.instance;
  final userId = currentUser.uid;

  final streakTimeDocRef = firestore
      .collection('users')
      .doc(userId)
      .collection('meta')
      .doc('streakServerTimeDoc');

  await streakTimeDocRef.set({
    'timestamp': FieldValue.serverTimestamp(),
  }, SetOptions(merge: true));

  final streakTimeSnap = await streakTimeDocRef.get();
  final serverTimestamp = streakTimeSnap['timestamp'] as Timestamp?;
  if (serverTimestamp == null) {
    throw Exception('サーバー時刻が取得できませんでした');
  }
  final serverDateTime = serverTimestamp.toDate();
  final jstDateTime = serverDateTime.toUtc().add(const Duration(hours: 9));
  final currentDateStr = DateFormat('yyyy-MM-dd').format(jstDateTime);

  final userDocRef = firestore.collection('users').doc(userId);
  int? newStreak;
  await firestore.runTransaction((transaction) async {
    final userSnapshot = await transaction.get(userDocRef);
    if (!userSnapshot.exists) {
      newStreak = 1;
      transaction.set(userDocRef, {
        'streak': newStreak,
        'lastStreakDate': currentDateStr,
        'maxStreak': newStreak,
      });
      return;
    }
    final userData = userSnapshot.data() as Map<String, dynamic>;
    final lastStreakDate = userData['lastStreakDate'] as String? ?? '';
    int currentStreak = userData['streak'] as int? ?? 0;
    int maxStreak = userData['maxStreak'] as int? ?? 0;

    DateTime? lastDate;
    try {
      lastDate = DateFormat('yyyy-MM-dd').parse(lastStreakDate);
    } catch (e) {
      lastDate = null;
    }
    final currentDate = DateFormat('yyyy-MM-dd').parse(currentDateStr);
    if (lastDate == null) {
      currentStreak = 1;
      newStreak = currentStreak;
    } else {
      final diffDays = currentDate.difference(lastDate).inDays;
      if (diffDays == 0) {
        newStreak = null; // すでに本日のログイン済み
        return;
      } else if (diffDays == 1) {
        currentStreak += 1;
        newStreak = currentStreak;
      } else {
        currentStreak = 1;
        newStreak = currentStreak;
      }
    }
    if (currentStreak > maxStreak) {
      maxStreak = currentStreak;
    }
    transaction.update(userDocRef, {
      'streak': currentStreak,
      'lastStreakDate': currentDateStr,
      'maxStreak': maxStreak,
    });
  });
  return newStreak;
}

//updateLoginStreakAndReward でストリーク数に応じた経験値を付与
Future<XpRewardDetails?> handleDailyLogin(User? user) async {
  if (user == null) return null;
  final firestore = FirebaseFirestore.instance;
  final userId = user.uid;
  final userDocRef = firestore.collection('users').doc(userId);

  // updateLoginStreak を1回だけ呼び出して、今日初回のログインかどうかを判定する
  int? streak = await updateLoginStreak(user);
  if (streak == null) return null; // 本日の初回ログインでなければ何もしない

  // updateLoginStreak 内と同じサーバー時刻取得ロジックで現在日付を算出
  final streakTimeDocRef =
      userDocRef.collection('meta').doc('streakServerTimeDoc');
  await streakTimeDocRef.set({
    'timestamp': FieldValue.serverTimestamp(),
  }, SetOptions(merge: true));
  final streakTimeSnap = await streakTimeDocRef.get();
  final serverTimestamp = streakTimeSnap['timestamp'] as Timestamp?;
  if (serverTimestamp == null) {
    throw Exception('サーバー時刻が取得できませんでした');
  }
  final serverDateTime = serverTimestamp.toDate();
  final jstDateTime = serverDateTime.toUtc().add(const Duration(hours: 9));
  final currentDateStr = DateFormat('yyyy-MM-dd').format(jstDateTime);

  // 日付が変わっている（＝本日初回ログイン）場合、dailyXpCount と dailyMissionGlossary_numbers をリセットする
  await userDocRef.update({
    'dailyXpCount': 0,
    'dailyMissionGlossary_numbers': [],
    'dailyCountersDate': currentDateStr,
    'dailyQuestion': 0,
  });

  // ストリークに応じた経験値を計算
  int rewardXp;
  if (streak >= 1 && streak <= 3) {
    rewardXp = 5;
  } else if (streak >= 4 && streak <= 7) {
    rewardXp = 10;
  } else if (streak >= 8 && streak <= 14) {
    rewardXp = 15;
  } else if (streak >= 15 && streak <= 30) {
    rewardXp = 20;
  } else {
    // streak >= 31
    rewardXp = 30;
  }

  final gamificationService = GamificationService();
  final int oldXp = await gamificationService.getExperience();
  final oldProgressMap = getCurrentLevelProgress(oldXp);
  final oldProgressXp = oldProgressMap['currentProgress'] ?? 0;
  final oldLevel = getLevelFromXp(oldXp);

  // 経験値を加算
  await gamificationService.addExperience(rewardXp);

  final int newXp = await gamificationService.getExperience();
  final newLevel = getLevelFromXp(newXp);
  final newProgressMap = getCurrentLevelProgress(newXp);
  final newProgressXp = newProgressMap['currentProgress'] ?? 0;
  final xpNeededForNextLevel = newProgressMap['maxProgress'] ?? 0;

  return XpRewardDetails(
    oldLevel: oldLevel,
    newLevel: newLevel,
    oldXp: oldProgressXp,
    newXp: newProgressXp,
    xpNeededForNextLevel: xpNeededForNextLevel,
    gainedXp: rewardXp,
    streakDays: streak,
  );
}

/// MyApp では、初期ルートとしてスプラッシュ画面を使用
class MyApp extends StatelessWidget {
  final String initialRoute;
  final int initialIndex;
  const MyApp(
      {Key? key, required this.initialRoute, required this.initialIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // GetX 用の各コントローラーを登録
    Get.put(InterruptionDataController());
    Get.put(PerformanceManagementScreenService());

    return GetMaterialApp(
      locale: const Locale('ja', 'JP'),
      supportedLocales: const [
        Locale('ja', 'JP'),
      ],
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      initialRoute: initialRoute,
      debugShowCheckedModeBanner: false,
      getPages: [
        // ログインしていない場合の認証画面
        GetPage(name: '/auth', page: () => AuthScreenWidget()),
        // スプラッシュ画面：非同期処理完了後にホーム画面へ遷移
        GetPage(
            name: '/splash',
            page: () => SplashScreen(initialIndex: initialIndex)),
        // ホーム画面
        GetPage(
            name: '/',
            page: () => HomePage(
                  initialIndex: initialIndex,
                )),
      ],
      navigatorObservers: [routeObserver],
    );
  }
}

/// スプラッシュ画面：ストリーク更新と経験値付与処理を実施し、完了後にホーム画面へ遷移
class SplashScreen extends StatefulWidget {
  final int initialIndex;
  const SplashScreen({Key? key, required this.initialIndex}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _initStreakAndReward();
  }

  void _initStreakAndReward() async {
    final User? user = FirebaseAuth.instance.currentUser;
    XpRewardDetails? xpRewardDetails;
    try {
      xpRewardDetails = await handleDailyLogin(user);
    } catch (e) {
      print('Error updating streak and reward xp: $e');
    }
    // スプラッシュ処理完了後、ホーム画面へ遷移（xpRewardDetails を渡す）
    Get.offAll(() => HomePage(
          initialIndex: widget.initialIndex,
          xpRewardDetails: xpRewardDetails,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Image.asset('assets/Icon.png')),
    );
  }
}

/// ホーム画面（BottomNavigationBar を持つ）
/// xpRewardDetails が渡されていれば、画面表示後にオーバーレイで経験値付与結果を表示
class HomePage extends StatefulWidget {
  final int initialIndex;
  final XpRewardDetails? xpRewardDetails;
  const HomePage({Key? key, required this.initialIndex, this.xpRewardDetails})
      : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late int _selectedIndex;

  @override
  void initState() {
    super.initState();
    _selectedIndex = widget.initialIndex;

    // xpRewardDetails がある場合、画面表示完了後にオーバーレイ表示
    if (widget.xpRewardDetails != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        Future.delayed(const Duration(seconds: 1), () {
          showXpGainOverlay(
            context: context,
            oldLevel: widget.xpRewardDetails!.oldLevel,
            newLevel: widget.xpRewardDetails!.newLevel,
            oldXp: widget.xpRewardDetails!.oldXp,
            newXp: widget.xpRewardDetails!.newXp,
            xpNeededForNextLevel: widget.xpRewardDetails!.xpNeededForNextLevel,
            gainedXp: widget.xpRewardDetails!.gainedXp,
            streakDays: widget.xpRewardDetails!.streakDays,
          );
        });
      });
    }
  }

  // 下記4画面を IndexedStack で管理
  final List<Widget> _pages = [
    MenuScreenWidget(),
    ChatRoomSelectionWidget(),
    QuestionSelectionScreen(),
    GlossaryScreenWidget(),
  ];

  // BottomNavigationBar の選択状態変更時の処理
  void _onItemTapped(int index) async {
    if (index != _selectedIndex) {
      setState(() {
        _selectedIndex = index;
      });
      final prefs = await SharedPreferences.getInstance();
      String activeItem;
      switch (index) {
        case 0:
          activeItem = 'ホーム';
          break;
        case 1:
          activeItem = 'チャット';
          break;
        case 2:
          activeItem = '問題';
          break;
        case 3:
          activeItem = '用語集';
          break;
        default:
          activeItem = 'ホーム';
      }
      await prefs.setString('activeBottomBarItem', activeItem);
    }
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      child: Scaffold(
        body: IndexedStack(
          index: _selectedIndex,
          children: _pages,
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
          type: BottomNavigationBarType.fixed,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'ホーム',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              label: 'チャット',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.question_answer),
              label: '問題',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.book),
              label: '用語集',
            ),
          ],
        ),
      ),
    );
  }
}
