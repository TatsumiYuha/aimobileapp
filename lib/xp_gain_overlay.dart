import 'dart:async';
import 'package:flutter/material.dart';
import 'package:confetti/confetti.dart';

/// フルスクリーンのオーバーレイを表示し、XP獲得を演出する関数。
///
/// [oldLevel], [newLevel]   : レベルアップの判定に使用 (コンフェッティ発火条件)
/// [oldXp], [newXp]         : XPの数値アニメーション用 (old → new)
/// [xpNeededForNextLevel]   : 次のレベルに必要なXP (ゲージ計算用)
/// [gainedXp]               : 今回獲得したXP (表示に使うだけ)
Future<void> showXpGainOverlay({
  required BuildContext context,
  required int oldLevel,
  required int newLevel,
  required int oldXp,
  required int newXp,
  required int xpNeededForNextLevel,
  required int gainedXp,
  int? streakDays,
  Duration duration = const Duration(seconds: 3),
}) async {
  // 1) OverlayEntry の作成
  final overlay = OverlayEntry(
    builder: (BuildContext context) {
      return XpGainOverlayWidget(
        oldLevel: oldLevel,
        newLevel: newLevel,
        oldXp: oldXp,
        newXp: newXp,
        xpNeededForNextLevel: xpNeededForNextLevel,
        gainedXp: gainedXp,
        streakDays: streakDays,
        duration: duration,
      );
    },
  );

  // 2) Overlay に挿入
  Overlay.of(context)!.insert(overlay);

  // 3) 演出が終わるのを待ち、取り除く
  await Future.delayed(duration + const Duration(milliseconds: 500));
  overlay.remove();
}

class XpGainOverlayWidget extends StatefulWidget {
  final int oldLevel;
  final int newLevel;
  final int oldXp;
  final int newXp;
  final int xpNeededForNextLevel;
  final int gainedXp;
  final Duration duration;
  final int? streakDays;

  const XpGainOverlayWidget({
    Key? key,
    required this.oldLevel,
    required this.newLevel,
    required this.oldXp,
    required this.newXp,
    required this.xpNeededForNextLevel,
    required this.gainedXp,
    required this.duration,
    this.streakDays,
  }) : super(key: key);

  @override
  State<XpGainOverlayWidget> createState() => _XpGainOverlayWidgetState();
}

class _XpGainOverlayWidgetState extends State<XpGainOverlayWidget>
    with SingleTickerProviderStateMixin {
  late final ConfettiController _confettiController;
  late final AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    // コンフェッティコントローラー初期化（レベルアップ時のみ再生）
    _confettiController =
        ConfettiController(duration: const Duration(seconds: 1));
    if (widget.newLevel > widget.oldLevel) {
      _confettiController.play();
    }

    // XPアニメーション用コントローラー（2秒間）
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );

    _animationController.forward();
  }

  @override
  void dispose() {
    _confettiController.dispose();
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // 半透明の背景で全画面を覆う
    return Material(
      color: Colors.black54,
      child: Stack(
        children: [
          Center(child: _buildAnimatedXpPanel()),
          Align(
            alignment: Alignment.center,
            child: ConfettiWidget(
              confettiController: _confettiController,
              shouldLoop: false,
              blastDirectionality: BlastDirectionality.explosive,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAnimatedXpPanel() {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        final t = _animationController.value; // 0.0 → 1.0

        // レベルアップ判定： newXp が oldXp 未満ならレベルアップ
        final isLevelUp = widget.newXp < widget.oldXp;
        int displayedXp;
        double clampedRatio;

        if (!isLevelUp) {
          // 通常の場合： oldXp から newXp への線形補間
          displayedXp =
              (widget.oldXp + (widget.newXp - widget.oldXp) * t).round();
          final oldRatio = widget.oldXp / widget.xpNeededForNextLevel;
          final newRatio = widget.newXp / widget.xpNeededForNextLevel;
          final currentRatio = oldRatio + (newRatio - oldRatio) * t;
          clampedRatio = currentRatio.clamp(0.0, 1.0);
        } else {
          // レベルアップの場合：
          // ・アニメーションの最初の10%は旧値（例：90）を表示
          // ・それ以降は、即座に0へ切り替え、0から newXp への補間で進捗を示す
          const double threshold = 0.1;
          if (t < threshold) {
            displayedXp = widget.oldXp;
            clampedRatio = widget.oldXp / widget.xpNeededForNextLevel;
          } else {
            final t2 = (t - threshold) / (1 - threshold);
            displayedXp = (0 + (widget.newXp - 0) * t2).round();
            clampedRatio =
                ((0 + (widget.newXp) / widget.xpNeededForNextLevel * t2))
                    .clamp(0.0, 1.0);
          }
        }

        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            // メッセージ
            Text(
              isLevelUp ? 'LEVEL UP!' : '+${widget.gainedXp} XP',
              style: const TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 16),
            // レベル表示
            _buildLevelDisplay(didLevelUp: isLevelUp),
            if (widget.streakDays != null)
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  '連続ログイン：${widget.streakDays}日',
                  style: const TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
              ),
            const SizedBox(height: 16),
            // XPゲージ
            _buildXpGauge(
              ratio: clampedRatio,
              displayedXp: displayedXp,
            ),
          ],
        );
      },
    );
  }

  Widget _buildLevelDisplay({required bool didLevelUp}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Lv.${widget.oldLevel}',
          style: const TextStyle(color: Colors.white, fontSize: 20),
        ),
        const Icon(Icons.arrow_right_alt, color: Colors.white),
        Text(
          'Lv.${widget.newLevel}',
          style: TextStyle(
            color: didLevelUp ? Colors.yellowAccent : Colors.white,
            fontSize: 20,
          ),
        ),
      ],
    );
  }

  Widget _buildXpGauge({
    required double ratio,
    required int displayedXp,
  }) {
    return Container(
      width: 250,
      height: 24,
      decoration: BoxDecoration(
        color: Colors.grey[800],
        borderRadius: BorderRadius.circular(12),
      ),
      child: Stack(
        children: [
          FractionallySizedBox(
            widthFactor: ratio,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.lightGreenAccent,
                borderRadius: BorderRadius.circular(12),
              ),
            ),
          ),
          Center(
            child: Text(
              '$displayedXp / ${widget.xpNeededForNextLevel} XP',
              style: const TextStyle(
                color: Colors.white,
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
