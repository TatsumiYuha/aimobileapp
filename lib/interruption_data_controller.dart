import 'dart:convert';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

// 中断データを保存するためのコントローラー
class InterruptionDataController extends GetxController {
  Map<String, String> _interruptions = {};

  @override
  void onInit() async {
    super.onInit();
    await _loadInterruptions();
  }

  Future<void> _loadInterruptions() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? interruptionsJson = prefs.getString('interruptions');
    if (interruptionsJson != null) {
      Map<String, dynamic> jsonMap = json.decode(interruptionsJson);
      _interruptions =
          jsonMap.map((key, value) => MapEntry(key, value.toString()));
    }
    update();
  }

  void saveInterruption(String questionId, String answer) async {
    _interruptions[questionId] = answer;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson = json.encode(_interruptions);
    await prefs.setString('interruptions', interruptionsJson);
    update();
  }

  // 中断データの保存メソッド
  void saveAllInterruptions(
      String questionId, Map<String, String> allAnswers) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson = json.encode(allAnswers);
    await prefs.setString('interruptions_$questionId', interruptionsJson);
    update();
  }

  // 中断データの取得メソッド
  Future<Map<String, String>> getInterruptions(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson =
        prefs.getString('interruptions_$questionId') ?? '{}';
    Map<String, String> interruptions =
        Map<String, String>.from(json.decode(interruptionsJson));
    return interruptions;
  }

  // 中断データの破棄メソッド
  Future<void> clearInterruptions(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('interruptions_$questionId');
    // シャッフル順序もクリア
    await prefs.remove('shuffled_order_$questionId');
    update();
  }

  String getInterruption(String questionId) {
    return _interruptions[questionId] ?? '';
  }

  // 中断データの有無を確認
  Future<bool> hasInterruption(int questionId, int selectedQuestionType) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    if (selectedQuestionType == 0) {
      return prefs.containsKey('interruptions_$questionId') ||
          prefs.containsKey('shuffled_order_$questionId'); // シャッフル順序も確認
    } else if (selectedQuestionType == 1) {
      return prefs.containsKey('implementation_interruptions_$questionId');
    } else if (selectedQuestionType == 2) {
      return prefs.containsKey('debugging_interruptions_$questionId');
    }
    return false;
  }

  // 実装問題の中断データ保存メソッド
  void saveImplementationInterruption(
      String questionId, Map<String, dynamic> data) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson = json.encode(data);
    await prefs.setString(
        'implementation_interruptions_$questionId', interruptionsJson);
    update();
  }

  // 実装問題の中断データ取得メソッド
  Future<Map<String, dynamic>> getImplementationInterruptions(
      int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson =
        prefs.getString('implementation_interruptions_$questionId') ?? '{}';
    return Map<String, dynamic>.from(json.decode(interruptionsJson));
  }

  // 実装問題の中断データ破棄メソッド
  Future<void> clearImplementationInterruption(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('implementation_interruptions_$questionId');
    await prefs.remove('programming_${questionId}_user_code');
    update();
  }

  // デバッグ問題の中断データ保存メソッド
  void saveDebuggingInterruption(
      String questionId, Map<String, dynamic> data) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson = json.encode(data);
    await prefs.setString(
        'debugging_interruptions_$questionId', interruptionsJson);
    update();
  }

  // デバッグ問題の中断データ取得メソッド
  Future<Map<String, dynamic>> getDebuggingInterruptions(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String interruptionsJson =
        prefs.getString('debugging_interruptions_$questionId') ?? '{}';
    return Map<String, dynamic>.from(json.decode(interruptionsJson));
  }

  // デバッグ問題の中断データ破棄メソッド
  Future<void> clearDebuggingInterruption(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('debugging_interruptions_$questionId');
    // シャッフル順序もクリア（必要に応じて）
    await prefs.remove('shuffled_order_$questionId');
    // await prefs.remove('programming_${questionId}_user_code');
    update();
  }

  // シャッフル順序を保存するメソッド
  Future<void> saveShuffledOrder(
      int questionId, List<int> shuffledOrder) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String orderJson = json.encode(shuffledOrder);
    await prefs.setString('shuffled_order_$questionId', orderJson);
    update();
  }

  // シャッフル順序を取得するメソッド
  Future<List<int>?> getShuffledOrder(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? orderJson = prefs.getString('shuffled_order_$questionId');
    if (orderJson != null) {
      List<dynamic> decodedList = json.decode(orderJson);
      return decodedList.map((e) => e as int).toList();
    }
    return null;
  }
}
