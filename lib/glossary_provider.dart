// glossary_provider.dart

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GlossaryProvider with ChangeNotifier {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final User? currentUser = FirebaseAuth.instance.currentUser;
  List<Map<String, dynamic>> _glossary = [];
  Set<String> _favorites = {};
  List<WordList> _wordLists = [];
  bool _isLoading = false;
  bool _hasError = false;
  String _error = '';

  List<Map<String, dynamic>> get glossary => _glossary;
  Set<String> get favorites => _favorites;
  List<WordList> get wordLists => _wordLists;
  bool get isLoading => _isLoading;
  bool get hasError => _hasError;
  String get error => _error;

  // コンストラクタ
  GlossaryProvider() {}

  // 初期化メソッド
  void initialize() {
    _loadFavorites();
    _fetchWordLists();
  }

  // お気に入りをロード
  Future<void> _loadFavorites() async {
    final prefs = await SharedPreferences.getInstance();
    _favorites = prefs.getStringList('favorites')?.toSet() ?? {};
    notifyListeners();
  }

  // お気に入りを登録
  Future<void> addFavorite(String term) async {
    _favorites.add(term);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('favorites', _favorites.toList());
    notifyListeners();
  }

  // お気に入りを解除
  Future<void> removeFavorite(String term) async {
    _favorites.remove(term);
    final prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('favorites', _favorites.toList());
    notifyListeners();
  }

  bool isFavorite(String term) => _favorites.contains(term);

  // アルファベットかどうかを判定するヘルパー関数
  bool _isAlphabet(String str) {
    return RegExp(r'^[a-zA-Z]+$').hasMatch(str);
  }

  Future<void> fetchGlossary() async {
    if (_isLoading) return; // 既にローディング中の場合は処理を行わない

    _isLoading = true;
    _hasError = false;
    _error = '';
    notifyListeners();

    if (_glossary.isNotEmpty) {
      // キャッシュされたデータが存在する場合はそれを使用
      _isLoading = false;
      notifyListeners();
      return;
    }

    try {
      QuerySnapshot querySnapshot =
          await _firestore.collection('glossary').get();
      _glossary = querySnapshot.docs
          .map((doc) => doc.data() as Map<String, dynamic>)
          .toList();

      // カスタムソート関数
      _glossary.sort((a, b) {
        // 'letter' フィールドの値を比較
        String letterA = a['letter'];
        String letterB = b['letter'];
        if (letterA == letterB) {
          // 'letter' が同じ場合は 'number' で比較
          return a['number'].compareTo(b['number']);
        } else if (letterA == "Φ") {
          return -1; // 'Φ' は常に先頭
        } else if (letterB == "Φ") {
          return 1; // 'Φ' は常に先頭
        } else if (_isAlphabet(letterA) && !_isAlphabet(letterB)) {
          return -1; // アルファベットはカタカナより先
        } else if (!_isAlphabet(letterA) && _isAlphabet(letterB)) {
          return 1; // アルファベットはカタカナより先
        } else {
          // 両方アルファベットまたはカタカナの場合は通常の文字列比較
          return letterA.compareTo(letterB);
        }
      });
    } catch (e) {
      _hasError = true;
      _error = e.toString();
    } finally {
      _isLoading = false;
      notifyListeners();
    }
  }

  // 単語帳関連のメソッド

  // 単語帳をFirestoreから取得
  Future<void> _fetchWordLists() async {
    try {
      QuerySnapshot querySnapshot = await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('wordLists')
          .get();
      _wordLists = querySnapshot.docs.map((doc) {
        return WordList(
          id: doc.id,
          name: doc['name'],
          terms: List<String>.from(doc['terms']),
        );
      }).toList();
      notifyListeners();
    } catch (e) {
      // エラーハンドリング
      print('単語帳の取得に失敗しました: $e');
    }
  }

  // 単語帳を作成
  Future<void> addWordList(String name) async {
    try {
      DocumentReference docRef = await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('wordLists')
          .add({'name': name, 'terms': []});
      _wordLists.add(WordList(id: docRef.id, name: name, terms: []));
      notifyListeners();
    } catch (e) {
      print('単語帳の作成に失敗しました: $e');
    }
  }

  // 単語帳の名前を変更
  Future<void> renameWordList(String id, String newName) async {
    try {
      await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('wordLists')
          .doc(id)
          .update({'name': newName});
      int index = _wordLists.indexWhere((list) => list.id == id);
      if (index != -1) {
        _wordLists[index].name = newName;
        notifyListeners();
      }
    } catch (e) {
      print('単語帳の名前変更に失敗しました: $e');
    }
  }

  // 単語帳を削除
  Future<void> deleteWordList(String id) async {
    try {
      await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('wordLists')
          .doc(id)
          .delete();
      _wordLists.removeWhere((list) => list.id == id);
      notifyListeners();
    } catch (e) {
      print('単語帳の削除に失敗しました: $e');
    }
  }

  // 単語帳に用語を追加
  Future<void> addTermToWordList(String wordListId, String term) async {
    try {
      DocumentReference docRef = _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('wordLists')
          .doc(wordListId);
      await docRef.update({
        'terms': FieldValue.arrayUnion([term])
      });
      // ローカルデータの更新
      int index = _wordLists.indexWhere((list) => list.id == wordListId);
      if (index != -1 && !_wordLists[index].terms.contains(term)) {
        _wordLists[index].terms.add(term);
        notifyListeners();
      }
    } catch (e) {
      print('単語帳への用語追加に失敗しました: $e');
    }
  }

  // 単語帳から用語を削除
  Future<void> removeTermFromWordList(String wordListId, String term) async {
    try {
      DocumentReference docRef = _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('wordLists')
          .doc(wordListId);
      await docRef.update({
        'terms': FieldValue.arrayRemove([term])
      });
      // ローカルデータの更新
      int index = _wordLists.indexWhere((list) => list.id == wordListId);
      if (index != -1 && _wordLists[index].terms.contains(term)) {
        _wordLists[index].terms.remove(term);
        notifyListeners();
      }
    } catch (e) {
      print('単語帳からの用語削除に失敗しました: $e');
    }
  }

  // 関連用語を取得するメソッド
  List<String> getRelatedTerms(String term) {
    final currentTerm =
        _glossary.firstWhere((item) => item['term'] == term, orElse: () => {});
    if (currentTerm.isEmpty ||
        !currentTerm.containsKey('related_terms_registered')) {
      return [];
    }

    List<String> relatedTermNames =
        List<String>.from(currentTerm['related_terms_registered']);
    return relatedTermNames;
  }
}

// 単語帳クラスの定義
class WordList {
  String id;
  String name;
  List<String> terms;

  WordList({required this.id, required this.name, required this.terms});
}
