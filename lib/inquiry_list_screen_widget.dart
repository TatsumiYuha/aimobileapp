import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart'; // intlパッケージのインポート

class InquiryListScreenWidget extends StatefulWidget {
  @override
  _InquiryListScreenWidgetState createState() =>
      _InquiryListScreenWidgetState();
}

class _InquiryListScreenWidgetState extends State<InquiryListScreenWidget> {
  // カテゴリー、サブカテゴリー、および要望の展開状態を保持するマップ
  Map<String, bool> _isCategoryExpanded = {};
  Map<String, Map<String, bool>> _isSubCategoryExpanded = {};
  Map<String, bool> _isInquiryExpanded = {};
  late Future<Map<String, Map<String, List<Map<String, dynamic>>>>>
      _inquiriesFuture;

  @override
  void initState() {
    super.initState();
    _initializeExpansionStates(); // 展開状態の初期化
    _inquiriesFuture = _fetchInquiriesFromFirestore(); // 画面遷移時にデータ取得
  }

  // 展開状態を初期化するメソッド
  void _initializeExpansionStates() {
    _isCategoryExpanded = {
      '要望': false,
      '不具合': false,
      'その他': false,
      '解決済み': false,
    };

    _isSubCategoryExpanded = {'要望': {}, '不具合': {}, 'その他': {}, '解決済み': {}};
  }

  // Firestoreからデータを取得して構造化するメソッド
  Future<Map<String, Map<String, List<Map<String, dynamic>>>>>
      _fetchInquiriesFromFirestore() async {
    QuerySnapshot snapshot =
        await FirebaseFirestore.instance.collection('inquiries').get();

    Map<String, Map<String, List<Map<String, dynamic>>>> inquiries = {
      '要望': {},
      '不具合': {},
      'その他': {},
      '解決済み': {},
    };

    for (var doc in snapshot.docs) {
      Map<String, dynamic> data = doc.data() as Map<String, dynamic>;
      String mainCategory = data['type'] ?? '未分類';
      String subCategory = data['subtype'] ?? '未分類';

      // サブカテゴリーがまだ存在しない場合は新規作成
      inquiries.putIfAbsent(mainCategory, () => {});
      inquiries[mainCategory]!.putIfAbsent(subCategory, () => []);

      // 要望データをサブカテゴリーに追加
      inquiries[mainCategory]![subCategory]!.add({
        'title': data['title'] ??
            _formatDate((data['timestamp'] as Timestamp).toDate()),
        'details': data['content'] ?? '',
        'date': _formatDate((data['timestamp'] as Timestamp).toDate()),
        'status': data['status'] ?? '',
        'severity': data['severity'] ?? '',
        'adminComment': data['adminComment'] ?? '',
      });

      // サブカテゴリーの展開状態を初期化
      _isSubCategoryExpanded[mainCategory]
          ?.putIfAbsent(subCategory, () => false);
    }

    return inquiries;
  }

  // 日付をフォーマットするメソッド
  String _formatDate(DateTime dateTime) {
    return DateFormat('yyyy年M月d日H時m分s秒').format(dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () async {
              Get.back();
            },
          ),
          title: Text('要望リスト'),
          centerTitle: true,
        ),
        // Firestoreからデータを取得し、FutureBuilderでUIを構築
        body:
            FutureBuilder<Map<String, Map<String, List<Map<String, dynamic>>>>>(
          future: _inquiriesFuture,
          builder: (context, snapshot) {
            //このインジケーターが存在するとサブ以降が読めこめなくなる
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator()); // データ取得中
            } else if (snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('エラーが発生しました。'),
                    SizedBox(height: 10),
                    ElevatedButton(
                      onPressed: () {
                        setState(() {
                          _inquiriesFuture = _fetchInquiriesFromFirestore();
                        });
                      },
                      child: Text('再試行'),
                    ),
                  ],
                ),
              );
            } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
              return Center(child: Text('要望が見つかりません。')); // データなし
            }

            Map<String, Map<String, List<Map<String, dynamic>>>> inquiries =
                snapshot.data!;

            return ListView(
              children: inquiries.keys.map((mainCategory) {
                return _buildCategoryTile(mainCategory, inquiries);
              }).toList(),
            );
          },
        ),
      ),
    );
  }

  // メインカテゴリーのタイルを作成するメソッド
  Widget _buildCategoryTile(String mainCategory,
      Map<String, Map<String, List<Map<String, dynamic>>>> inquiries) {
    int count = inquiries[mainCategory]!
        .values
        .fold(0, (previousValue, element) => previousValue + element.length);

    return ExpansionTile(
      title: Text('$mainCategory ($count)'),
      leading: _isCategoryExpanded[mainCategory]!
          ? Icon(Icons.remove_circle_outline)
          : Icon(Icons.add_circle_outline),
      onExpansionChanged: (expanded) {
        setState(() {
          _isCategoryExpanded[mainCategory] = expanded;
        });
      },
      children: _isCategoryExpanded[mainCategory]!
          ? _buildSubCategoryList(mainCategory, inquiries)
          : [],
    );
  }

  // サブカテゴリーリストを作成するメソッド
  List<Widget> _buildSubCategoryList(String mainCategory,
      Map<String, Map<String, List<Map<String, dynamic>>>> inquiries) {
    return inquiries[mainCategory]?.keys.map((subCategory) {
          int count = inquiries[mainCategory]?[subCategory]?.length ?? 0;
          bool isSubCategoryExpanded =
              _isSubCategoryExpanded[mainCategory]?[subCategory] ?? false;

          return Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: ExpansionTile(
              title: Text('$subCategory ($count)'),
              leading: isSubCategoryExpanded
                  ? Icon(Icons.remove_circle_outline)
                  : Icon(Icons.add_circle_outline),
              onExpansionChanged: (expanded) {
                setState(() {
                  _isSubCategoryExpanded[mainCategory]?[subCategory] = expanded;
                });
              },
              children: isSubCategoryExpanded
                  ? _buildInquiryList(mainCategory, subCategory,
                      inquiries[mainCategory]?[subCategory] ?? [])
                  : [],
            ),
          );
        }).toList() ??
        [];
  }

  // 要望リストを作成するメソッド
  List<Widget> _buildInquiryList(String mainCategory, String subCategory,
      List<Map<String, dynamic>> inquiries) {
    return inquiries.map((inquiry) {
      String inquiryId = '${mainCategory}_${subCategory}_${inquiry['title']}';
      bool isExpanded = _isInquiryExpanded[inquiryId] ?? false;

      return ExpansionTile(
        title: Text(inquiry['title']),
        leading: isExpanded
            ? Icon(Icons.remove_circle_outline)
            : Icon(Icons.add_circle_outline),
        onExpansionChanged: (expanded) {
          setState(() {
            _isInquiryExpanded[inquiryId] = expanded;
          });
        },
        children: isExpanded
            ? _buildInquiryDetails(inquiry, subCategory == '未分類')
            : [],
      );
    }).toList();
  }

  // 要望の詳細を作成するメソッド
  List<Widget> _buildInquiryDetails(
      Map<String, dynamic> inquiry, bool isUncategorized) {
    List<Widget> details = [];

    details.add(ListTile(
      title: Text('要望詳細'),
      subtitle: Text(inquiry['details']),
    ));

    if (!isUncategorized) {
      details.add(ListTile(
        title: Text('日付'),
        subtitle: Text(inquiry['date']),
      ));
      details.add(ListTile(
        title: Text('状態'),
        subtitle: Text(inquiry['status']),
      ));
      details.add(ListTile(
        title: Text('重度'),
        subtitle: Text(
          inquiry['severity'],
          style: TextStyle(
            color: _getSeverityColor(inquiry['severity']),
          ),
        ),
      ));
      details.add(ListTile(
        title: Text('管理者からのコメント'),
        subtitle: Text(inquiry['adminComment']),
      ));
    }

    return details;
  }

  // 重度に応じたカラーを返すメソッド
  Color _getSeverityColor(String severity) {
    switch (severity) {
      case '軽':
        return Colors.green;
      case '中':
        return Colors.orange;
      case '重':
        return Colors.red;
      default:
        return Colors.black; // デフォルトの色
    }
  }
}
