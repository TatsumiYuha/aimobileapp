import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'auth_service.dart';

class QuestionDebuggingScreenService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final User? currentUser = FirebaseAuth.instance.currentUser;
  final authService = AuthService();
  // デバッグ問題採点処理
  Future<String> sendDebuggingTask(http.Client client, String sentence,
      String codeSmell, String userInput, List<dynamic> requirements) async {
    try {
      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        return "認証トークンが無効です。"; // ここで処理を終了
      }
      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp/debug'),
        body: utf8.encode(jsonEncode({
          'sentence': sentence,
          'code_smell': codeSmell,
          'user_input': userInput,
          'special_conditions': requirements,
        })),
        headers: {"Content-Type": "application/json"},
      );
      if (response.statusCode == 200) {
        return utf8.decode(response.bodyBytes);
      } else {
        throw Exception('Failed to load post');
      }
    } catch (e) {
      throw Exception('Error occurred while sending data: $e');
    }
  }

  // コードを保存するメソッド
  Future<void> saveUserCode(String code, int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('debugging_${questionId}_user_code', code);
  }

  // コードを読み込むメソッド
  Future<String> loadUserCode(int questionId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('debugging_${questionId}_user_code') ?? '';
  }

  Future<void> addNumberToCompleteList(int questionId) async {
    DocumentReference userCompleteRef = _firestore
        .collection('users')
        .doc(currentUser?.uid)
        .collection('userComplete')
        .doc('Debugging');

    try {
      // ドキュメントの現在のデータを取得
      DocumentSnapshot snapshot = await userCompleteRef.get();

      if (snapshot.exists) {
        Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
        List<dynamic> currentList = data['completeList'] as List<dynamic>;
        //リストに既に同一のIDが存在する場合は登録しない
        if (!currentList.contains(questionId)) {
          // 新しい数値をリストに追加
          currentList.add(questionId);
          // 更新されたリストをFirestoreに保存
          await userCompleteRef.update({'completeList': currentList});
        }
      } else {
        // ドキュメントが存在しない場合、新しいドキュメントを作成
        await userCompleteRef.set({
          'completeList': [questionId]
        });
      }
    } catch (e) {
      print('Error adding number to complete list: $e');
      throw e; // またはエラーを適切に処理
    }
  }
}
