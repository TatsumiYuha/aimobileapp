import 'package:flutter/material.dart';
import 'package:flutter_highlight/themes/ir-black.dart';

class HelpOverlay extends StatefulWidget {
  final Widget existingWidget; // 背景画像1: 既存のWidget
  final List<String> helpMessages; // ヘルプで表示するテキストのリスト
  final List<String> backgroundImages;
  final VoidCallback onSkip; // メッセージを背景画像の下側に表示するか
  final List<Alignment> messageAlignment;
  final List<EdgeInsets> margin;
  final List<Rect> rectangleSteps;

  HelpOverlay({
    required this.existingWidget,
    required this.helpMessages,
    required this.backgroundImages,
    required this.onSkip,
    required this.messageAlignment,
    required this.margin,
    required this.rectangleSteps,
  });

  @override
  _HelpOverlayState createState() => _HelpOverlayState();
}

class _HelpOverlayState extends State<HelpOverlay> {
  int currentStep = 0;

  void _nextStep() {
    setState(() {
      if (currentStep < widget.helpMessages.length - 1) {
        currentStep++;
      } else {
        widget.onSkip(); // 最後のヘルプが終わったらコールバックを実行
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final currentRect = widget.rectangleSteps[currentStep];

    return GestureDetector(
      onTap: _nextStep, // 画面をタップすると次のステップへ
      child: Stack(
        children: [
          Positioned.fill(
            child: SizedBox(
              /*          width: screenWidth,
              height: screenHeight, */
              child: Stack(
                children: [
                  widget.existingWidget, // 既存のWidget
                  Container(
                    color: Colors.black54, // グレーアウト用の半透明レイヤー
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: screenHeight * 0.15, // 画面の25%から表示
            left: screenWidth * 0.1, // 画面の左右10%のマージン

            child: SizedBox(
              width: screenWidth * 0.8, // 幅を画面幅の80%に
              height: screenHeight * 0.7, // 高さを画面高さの50%に
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Image.asset(
                      widget.backgroundImages[currentStep],
                      fit: BoxFit.contain, // 画像を枠内に収める
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: currentRect.top,
            left: currentRect.left,
            child: Container(
              width: currentRect.width,
              height: currentRect.height,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.lightBlue,
                  width: 3,
                ),
                color: Colors.transparent, // 中を透過
              ),
            ),
          ),
          Align(
            alignment: widget.messageAlignment[currentStep],
            child: Container(
              width: screenWidth * 0.7,
              margin: widget.margin[currentStep],
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.lightBlue, width: 3),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/Icon.png', // メッセージボックス内の画像
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Text(widget.helpMessages[currentStep],
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                decoration: TextDecoration.none,
                                fontWeight: FontWeight.w400)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            right: 20,
            child: ElevatedButton.icon(
              onPressed: widget.onSkip,
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(color: Colors.black, width: 2.5),
                ),
              ),
              icon: Image.asset(
                'assets/Skip.png',
                width: 25,
                height: 25,
              ),
              label: Text(
                "スキップ",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class QuestionHelpOverlay extends StatefulWidget {
  final String iamgeWidget; // 背景画像1: 既存のWidget
  final List<String> helpMessages; // ヘルプで表示するテキストのリスト
  final List<String> backgroundImages;
  final VoidCallback onSkip; // メッセージを背景画像の下側に表示するか
  final List<Alignment> messageAlignment;
  final List<EdgeInsets> margin;
  final List<Rect> rectangleSteps;

  QuestionHelpOverlay({
    required this.iamgeWidget,
    required this.helpMessages,
    required this.backgroundImages,
    required this.onSkip,
    required this.messageAlignment,
    required this.margin,
    required this.rectangleSteps,
  });

  @override
  _QuestionHelpOverlay createState() => _QuestionHelpOverlay();
}

class _QuestionHelpOverlay extends State<QuestionHelpOverlay> {
  int currentStep = 0;

  void _nextStep() {
    setState(() {
      if (currentStep < widget.helpMessages.length - 1) {
        currentStep++;
      } else {
        widget.onSkip(); // 最後のヘルプが終わったらコールバックを実行
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    final currentRect = widget.rectangleSteps[currentStep];

    return GestureDetector(
      onTap: _nextStep, // 画面をタップすると次のステップへ
      child: Stack(
        children: [
          Positioned.fill(
            child: SizedBox(
              /*          width: screenWidth,
              height: screenHeight, */
              child: Stack(
                children: [
                  Image.asset(widget.iamgeWidget), // 既存のWidget
                  Container(
                    color: Colors.black54, // グレーアウト用の半透明レイヤー
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: screenHeight * 0.15, // 画面の25%から表示
            left: screenWidth * 0.1, // 画面の左右10%のマージン

            child: SizedBox(
              width: screenWidth * 0.8, // 幅を画面幅の80%に
              height: screenHeight * 0.7, // 高さを画面高さの50%に
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Image.asset(
                      widget.backgroundImages[currentStep],
                      fit: BoxFit.contain, // 画像を枠内に収める
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: currentRect.top,
            left: currentRect.left,
            child: Container(
              width: currentRect.width,
              height: currentRect.height,
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.lightBlue,
                  width: 3,
                ),
                color: Colors.transparent, // 中を透過
              ),
            ),
          ),
          Align(
            alignment: widget.messageAlignment[currentStep],
            child: Container(
              width: screenWidth * 0.7,
              margin: widget.margin[currentStep],
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.lightBlue, width: 3),
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        'assets/Icon.png', // メッセージボックス内の画像
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Text(widget.helpMessages[currentStep],
                            style: TextStyle(
                                fontSize: 15,
                                color: Colors.black,
                                decoration: TextDecoration.none,
                                fontWeight: FontWeight.w400)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 20,
            right: 20,
            child: ElevatedButton.icon(
              onPressed: widget.onSkip,
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.white,
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                  side: BorderSide(color: Colors.black, width: 2.5),
                ),
              ),
              icon: Image.asset(
                'assets/Skip.png',
                width: 25,
                height: 25,
              ),
              label: Text(
                "スキップ",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
