import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'custom_bottom_app_bar.dart';
import 'auth_service.dart';
import 'package:get/get.dart';
import 'question_description_screen_widget.dart';
import 'custom_tap_effect_overlay.dart';
import 'question_programming_screen_widget.dart';
import 'question_debugging_screen_widget.dart';
import 'question_selection_screen_service.dart';
import 'interruption_data_controller.dart';
import 'questions_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'performance_management_screen_widget.dart';
import 'performance_management_screen_service.dart';
import 'history_icon_button.dart';
import 'glossary_provider.dart'; // GlossaryProviderをインポート
import 'route_observer.dart';
// ★ 追加
import 'question_programming_screen_service.dart';

// チュートリアル関連補助ファイル
import 'tutorial_progress_manager.dart';
import 'blue_square_overlay.dart';
import 'help_overlay_service.dart';
import 'animation_build_display.dart';

class QuestionSelectionScreenWidget extends StatelessWidget {
  const QuestionSelectionScreenWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false, // デバッグバナーを非表示
      title: '問題選択',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: const QuestionSelectionScreen(),
    );
  }
}

class QuestionSelectionScreen extends StatefulWidget {
  const QuestionSelectionScreen({Key? key}) : super(key: key);

  @override
  _QuestionSelectionScreenState createState() =>
      _QuestionSelectionScreenState();
}

class _QuestionSelectionScreenState extends State<QuestionSelectionScreen>
    with TickerProviderStateMixin, RouteAware {
  late AnimationController _indicatorController;
  late AnimationController _practiceButtonController;
  late Animation<double> _practiceButtonAnimation;
  AnimationController? _fadeController;
  late Future<List<Map<String, dynamic>>> _descriptiveQuestionsFuture;
  late Future<List<dynamic>> _completeListFuture;

  final ScrollController _scrollController = ScrollController(); // スクロール位置管理

  final QuestionSelectionScreenService _questionSelectionScreenService =
      QuestionSelectionScreenService();

  final PerformanceManagementScreenService _performanceManagementScreenService =
      PerformanceManagementScreenService();

  final List<Color> cardColors = [
    Colors.amber[200]!,
    Colors.blue[200]!,
    Colors.green[200]!,
    Colors.pink[200]!,
    Colors.purple[200]!,
  ];

  List<bool> _selections = [true, false, false];
  int _selectedQuestionType = 0; // 0: 用語記述, 1: 実装処理, 2: デバッグ
  double _answeredPercentage = 0;

  final Duration _swipeCooldownDuration = const Duration(milliseconds: 500);
  DateTime? _lastSwipeTime;
  bool _isGlossaryLoaded = false;

  // フィルター状態 0:すべて、1:完了、2:未完了、3:中断
  int _filterIndex = 0;

  // プラクティスモードの状態管理
  bool _isPracticeMode = false;

  // 用語記述問題の詳細表示管理 (質問IDを保持)
  final Set<int> _expandedDescriptiveQuestions = {};

  // 実装問題の詳細表示管理 (質問IDを保持)
  final Set<int> _expandedImplementationQuestions = {};

  // ★ 追加：採点結果の未読フラグをチェックするService
  final QuestionProgrammingScreenService _programmingService =
      QuestionProgrammingScreenService();

  ////////////// チュートリアル／オンボーディング用の変数 //////////////
  final tutorialProgressManager _tutorial = tutorialProgressManager();
  bool _isTutorialActive = false; // 操作説明（チュートリアル）が実行中か
  bool _isOnboadingActive = false; // オンボーディング中か
  bool _isShowDialog = false; // 「操作説明を開始しますか？」のダイアログ表示
  bool _isCondition = false; // 操作が完了したかの一時フラグ
  bool showHelp = false; // ヘルプオーバーレイ表示用
  bool _isVisible = false; // 操作説明での青い枠表示制御

  int _currentStep = 0; // 操作説明（チュートリアル）進行管理
  int _guidanceNum = 0; // ガイダンス進行管理（オンボーディング用）

  // ガイダンス用のメッセージ（オンボーディング初期表示）
  final List<String> _guidance = [
    "ようこそ！この画面では問題選択や達成率の確認ができます。",
    "中央のタブで、用語記述、実装処理、デバッグの各問題タイプを切り替えられます。",
    "画面を左右にスワイプして、問題タイプを簡単に切り替えてみましょう。",
    "フィルターボタンで、完了済み、未完了、または中断された問題を絞り込むことができます。",
    "『プラクティス』モードでは、問題の詳細情報やヒントが表示され、復習に役立ちます。",
    "リセットボタンで、問題達成率を初期化する操作が行えます。",
    "中断フラグアイコンは、前回の解答が中断された場合に表示され、回答再開の選択肢を示します。",
    "成績履歴アイコンは、過去の採点結果や解答履歴を確認できる機能を表しています。",
    "完了アイコンは、該当の問題が既に完了済みであることを示します。",
    "各問題カードをタップして、詳細な解答画面に進んでみましょう。",
    "以上で問題選択画面の基本操作の説明は終了です。",
  ];

  // 各操作説明（チュートリアル）用のステップ
  final List<Map<String, dynamic>> _steps = [
    {'text': '問題選択画面の操作説明を開始します', 'requires': null},
    {'text': '中央のタブから「実装処理」や「デバッグ」を選択して、問題タイプを切り替えられます', 'requires': 'toggle'},
    {'text': '画面の左右スワイプでも、他の問題タイプにも切り替えが可能です', 'requires': 'swipe'},
    {
      'text': 'フィルターボタンをタップして、表示される問題リストを「すべて」「完了」「未完了」「中断」で絞り込んでみましょう',
      'requires': 'filter'
    },
    {'text': '「プラクティス」モードのボタンをタップして、詳細表示機能を有効にしてください', 'requires': 'practice'},
    {'text': 'このモードでは、練習に最適な出題形式に変更されます。ぜひ活用してください。', 'requires': null},
    {
      'text': '中央のタブで「用語記述」が選択された状態で、このパネルをタップしてください。',
      'requires': 'descriptive'
    },
    {
      'text': '用語記述問題では成績履歴アイコンから、その問題の過去の採点結果を確認できます。',
      'requires': 'historyIcon'
    },
    {'text': '以上で問題選択画面の操作説明は終了です', 'requires': null},
  ];

  // ヘルプオーバーレイ用メッセージなど（そのまま）
  final List<String> helpMessages = [
    "検索バー: チャットルームを検索できます",
  ];

  final List<String> backgroundImages = [
    'assets/Help_search.png',
  ];

  final List<Alignment> messageAlignment = [
    Alignment.topCenter,
  ];

  final List<EdgeInsets> margin = [
    EdgeInsets.only(top: 20),
  ];

  // ヘルプ用：ハイライト矩形（例示用の固定値）
  final List<Rect> rectangleSteps = [
    Rect.fromLTWH(20, 100, 360, 50), // 検索バー
  ];

////////// 対象ウィジェットのGlobalKey ////////////
  // ログアウトボタン用（画面上部のログアウトアイコン）
  final GlobalKey logoutButtonKey = GlobalKey();
  final GlobalKey toggleTabKey = GlobalKey();
  final GlobalKey swipeAreaKey = GlobalKey();
  final GlobalKey filterButtonKey = GlobalKey();
  final GlobalKey practiceButtonKey = GlobalKey();
  final GlobalKey questionCardKey = GlobalKey();
  final GlobalKey interruptionIndicatorKey = GlobalKey();
  final GlobalKey historyIconKey = GlobalKey();
  final GlobalKey completeIconKey = GlobalKey();
  final GlobalKey resetButtonKey = GlobalKey();
  final GlobalKey iconAreaKey = GlobalKey();
  final GlobalKey descriptiveKey = GlobalKey();

////////// ここまで

  //////////// ガイダンス用オーバーレイ変数 ////////////
  double _guidanceOverlayTop = 0.0;
  double _guidanceOverlayLeft = 0.0;
  double _guidanceOverlayWidth = 0.0;
  double _guidanceOverlayHeight = 0.0;
  //////////// ここまで

  //////////// 操作説明用オーバーレイ変数 ////////////
  double _tutorialOverlayTop = 0.0;
  double _tutorialOverlayLeft = 0.0;
  double _tutorialOverlayWidth = 0.0;
  double _tutorialOverlayHeight = 0.0;
  //////////// ここまで

  GlobalKey? _getGuidanceTargetKey() {
    switch (_guidanceNum) {
      case 1:
        return toggleTabKey;
      case 2:
        return swipeAreaKey;
      case 3:
        return filterButtonKey;
      case 4:
        return practiceButtonKey;
      case 5:
        return resetButtonKey;
      case 6:
        return interruptionIndicatorKey;
      case 7:
        return historyIconKey;
      case 8:
        return completeIconKey;
      case 9:
        return questionCardKey;
      default:
        return null;
    }
  }

  // ガイダンス用の対象ウィジェットの位置とサイズを更新する
  void _updateGuidanceOverlayPosition() {
    GlobalKey? targetKey = _getGuidanceTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _guidanceOverlayTop = globalPosition.dy;
      _guidanceOverlayLeft = globalPosition.dx;
      _guidanceOverlayWidth = size.width;
      _guidanceOverlayHeight = size.height;
    });
  }

// 操作説明用の対象ウィジェットのGlobalKeyを返す
  GlobalKey? _getTutorialTargetKey() {
    final String? req = _steps[_currentStep]['requires'];
    if (req == null) return null;
    switch (req) {
      case 'toggle':
        return toggleTabKey;
      case 'swipe':
        return swipeAreaKey;
      case 'filter':
        return filterButtonKey;
      case 'practice':
        return practiceButtonKey;
      case 'question':
        return questionCardKey;
      case 'interruption':
        return interruptionIndicatorKey;
      case 'reset':
        return resetButtonKey;
      case 'iconArea':
        return iconAreaKey;
      case 'descriptive':
        return descriptiveKey;
      case 'historyIcon':
        return historyIconKey;
      default:
        return null;
    }
  }

  // 操作説明用の対象ウィジェットの位置とサイズを更新する（新規）
  void _updateTutorialOverlayPosition() {
    GlobalKey? targetKey = _getTutorialTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      Future.delayed(Duration(milliseconds: 100), () {
        _updateTutorialOverlayPosition();
      });

      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _tutorialOverlayTop = globalPosition.dy;
      _tutorialOverlayLeft = globalPosition.dx;
      _tutorialOverlayWidth = size.width;
      _tutorialOverlayHeight = size.height;
    });
  }
  //////////// ここまで操作説明用

  @override
  void initState() {
    super.initState();
    const int initialCategory = 0;
    _loadToggleState();
    _loadScrollPosition();
    var provider = Provider.of<QuestionsProvider>(context, listen: false);
    _descriptiveQuestionsFuture = provider.fetchQuestions(initialCategory);
    _completeListFuture = provider.fetchCompleteLists(initialCategory);
    _calculateAchievementRate();
    _initAnimationControllers();
    _fadeController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
      value: 1.0,
    );
    _loadPracticeModeState();

    // デバッグ用・初回起動時として強制的にオンボーディング（ガイダンス）を開始
    _checkAndStartTutorial();
    _loadProgress();
  }

  Future<void> _checkAndStartTutorial() async {
    // ここでは画面ごとに一意なキーを用いる（例：ChatScreenWidget の場合はクラス名や固有のID）
    // 複数の画面で管理する場合、各画面で異なるキーを使用してください。
    final String tutorialKey = 'tutorialShown_${this.runtimeType}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstTime = prefs.getBool(tutorialKey) ?? true;
    if (isFirstTime) {
      // 初回の場合のみチュートリアル／ガイダンスを発火させる
      setState(() {
        _isOnboadingActive = true;
        _guidanceNum = 0;
      });
      // 画面描画後にオーバーレイ位置を更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
      // 一度発火したのでフラグを更新（2回目以降は発火しない）
      await prefs.setBool(tutorialKey, false);
    }
  }

  Future<void> _loadProgress() async {
    int progress = await _tutorial.getProgress('currentStep');
    if (progress < _steps.length) {
      setState(() {
        _currentStep = progress;
      });
    }
  }

  Future<void> _saveProgress(int step) async {
    await _tutorial.saveProgress('currentStep', step);
  }

  Future<void> _resetProgress() async {
    setState(() {
      _currentStep = 0;
    });
    await _tutorial.resetProgress();
  }

  Future<void> _saveTutorialStatus() async {
    await _tutorial.saveTutorialBoolStatus(_isTutorialActive);
    await _tutorial.saveOnboadingBoolStatus(_isOnboadingActive);
    await _tutorial.saveShowDialogBoolStatus(_isShowDialog);
  }

  void _getTutorial() async {
    bool futureTutorialActive = await _tutorial.loadTutorialBoolStatus();
    bool futureOnboardingActive = await _tutorial.loadOnboadingBoolStatus();
    bool futureShowDialogActive = await _tutorial.loadShowDialogBoolStatus();
    setState(() {
      _isTutorialActive = futureTutorialActive;
      _isOnboadingActive = futureOnboardingActive;
      _isShowDialog = futureShowDialogActive;
    });
  }

  void _startTutorial() {
    setState(() {
      _currentStep = 0;
      _isTutorialActive = true;
      _isOnboadingActive = false;
      _isShowDialog = false;
    });
    _saveProgress(0);
    _saveTutorialStatus();
    _checkVisible();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _endTutorial() {
    setState(() {
      _isTutorialActive = false;
      _isOnboadingActive = false;
      _isShowDialog = false;
      _currentStep = 0;
      _isVisible = false;
    });
    _resetProgress();
    _saveTutorialStatus();
  }

  void _guidanceStep() {
    if (_guidanceNum < _guidance.length - 1) {
      setState(() {
        _guidanceNum++;
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
    } else {
      setState(() {
        _isShowDialog = true;
        _isOnboadingActive = true;
      });
    }
  }

  void _noTap() {
    setState(() {
      _currentStep = _steps.length - 1;
      _isTutorialActive = true;
    });
  }

  void _endGuidance() {
    setState(() {
      _isShowDialog = false;
      _isOnboadingActive = false;
    });
  }

  void _nextStep() {
    if (_currentStep < _steps.length - 1) {
      setState(() {
        _currentStep++;
        _isCondition = false;
      });
      _saveProgress(_currentStep);
    } else {
      _endTutorial();
      _saveProgress(_currentStep);
    }
    _checkVisible();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _checkAndProceed() {
    final requires = _steps[_currentStep]['requires'];
    if (requires == 'descriptive' || requires == 'historyIcon') {
      if (_selectedQuestionType == 0) {
        _conditionMet();
      }
      return;
    }
    if (requires == null || _isCondition) {
      _nextStep();
    }
  }

  void _conditionMet() {
    if (_isTutorialActive) {
      setState(() {
        _isCondition = true;
      });
      _nextStep();
    }
  }

  // 従来の青い枠表示制御（操作説明用）
  void _checkVisible() {
    if (_currentStep >= 0 && _currentStep <= 8) {
      setState(() {
        _isVisible = true;
      });
    } else {
      setState(() {
        _isVisible = false;
      });
    }
  }
  //////////////////////////////////////////////////////////////////////

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context)!);
    if (!_isGlossaryLoaded) {
      _isGlossaryLoaded = true;
      WidgetsBinding.instance.addPostFrameCallback((_) {
        final provider = Provider.of<GlossaryProvider>(context, listen: false);
        provider.initialize();
        provider.fetchGlossary();
      });
    }
  }

  @override
  void dispose() {
    _indicatorController.dispose();
    _practiceButtonController.dispose();
    _fadeController?.dispose();
    _scrollController.dispose();
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  void _initAnimationControllers() {
    _indicatorController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..forward();

    _practiceButtonController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );

    _practiceButtonAnimation = Tween<double>(begin: 1.0, end: 0.5).animate(
      CurvedAnimation(
        parent: _practiceButtonController,
        curve: Curves.easeInOut,
      ),
    );

    _practiceButtonController.addStatusListener((status) {
      if (_isPracticeMode) {
        if (status == AnimationStatus.completed) {
          _practiceButtonController.reverse();
        } else if (status == AnimationStatus.dismissed) {
          _practiceButtonController.forward();
        }
      } else {
        _practiceButtonController.reset();
      }
    });

    if (_isPracticeMode) {
      _practiceButtonController.forward();
    }
  }

  void _resetAndStartAnimation() {
    _indicatorController.reset();
    _indicatorController.forward();
  }

  void _toggleQuestionType() {
    _fadeController!.reverse().then((_) {
      setState(() {
        _selectedQuestionType = _selections.indexOf(true);
        _updateQuestionsFuture();
      });
      _fadeController!.forward(from: 0.0);
    });
  }

  void _loadToggleState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int selectedIndex = prefs.getInt('selectedQuestionType') ?? 0;
    setState(() {
      _selections =
          List.generate(_selections.length, (index) => index == selectedIndex);
      _selectedQuestionType = selectedIndex;
    });
    _updateQuestionsFuture();
  }

  void _updateQuestionsFuture() {
    var provider = Provider.of<QuestionsProvider>(context, listen: false);
    _descriptiveQuestionsFuture =
        provider.fetchQuestions(_selectedQuestionType);
    _completeListFuture = provider.fetchCompleteLists(_selectedQuestionType);
    _calculateAchievementRate();
    _completeListFuture.whenComplete(() {
      _loadScrollPosition();
    });
  }

  void _calculateAchievementRate() async {
    try {
      List<Map<String, dynamic>> allQuestions =
          await _descriptiveQuestionsFuture;
      List<dynamic> completedQuestions = await _completeListFuture;
      int totalQuestions = allQuestions.length;
      int completedQuestionsCount = completedQuestions.length;
      double achievementRate = totalQuestions > 0
          ? (completedQuestionsCount / totalQuestions) * 100
          : 0.0;
      setState(() {
        _answeredPercentage = achievementRate;
      });
      _resetAndStartAnimation();
    } catch (e) {
      debugPrint("Error calculating achievement rate: $e");
    }
  }

  void _handleSwipe(DragUpdateDetails details) {
    final now = DateTime.now();
    if (_lastSwipeTime != null &&
        now.difference(_lastSwipeTime!) < _swipeCooldownDuration) {
      return;
    }

    if (details.primaryDelta! < 0) {
      if (_selectedQuestionType < _selections.length - 1) {
        setState(() {
          _selectedQuestionType++;
          _selections = List.generate(
              _selections.length, (index) => index == _selectedQuestionType);
          _updateQuestionsFuture();
          SharedPreferences.getInstance().then((prefs) {
            prefs.setInt('selectedQuestionType', _selectedQuestionType);
          });
        });
      }
    } else if (details.primaryDelta! > 0) {
      if (_selectedQuestionType > 0) {
        setState(() {
          _selectedQuestionType--;
          _selections = List.generate(
              _selections.length, (index) => index == _selectedQuestionType);
          _updateQuestionsFuture();
          SharedPreferences.getInstance().then((prefs) {
            prefs.setInt('selectedQuestionType', _selectedQuestionType);
          });
        });
      }
    }
    _lastSwipeTime = now;
  }

  void _saveScrollPosition() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setDouble(
        'scrollPosition_$_selectedQuestionType', _scrollController.offset);
  }

  void _loadScrollPosition() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    double savedPosition =
        prefs.getDouble('scrollPosition_$_selectedQuestionType') ?? 0.0;
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_scrollController.hasClients) {
        _scrollController.jumpTo(savedPosition);
      }
    });
  }

  String _getFilterLabel() {
    switch (_filterIndex) {
      case 0:
        return "すべて";
      case 1:
        return "完了";
      case 2:
        return "未完了";
      case 3:
        return "中断";
      default:
        return "すべて";
    }
  }

  void _loadPracticeModeState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isPractice = prefs.getBool('isPracticeMode') ?? false;
    setState(() {
      _isPracticeMode = isPractice;
    });

    if (_isPracticeMode) {
      _practiceButtonController.forward();
    } else {
      _practiceButtonController.reset();
      _expandedDescriptiveQuestions.clear();
      _expandedImplementationQuestions.clear();
    }
  }

  void _savePracticeModeState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isPracticeMode', _isPracticeMode);
  }

  /// 画面に戻ってきた時（子画面からpopされたとき）に再描画 -> 未読フラグ再チェック
  @override
  void didPopNext() {
    super.didPopNext();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            backgroundColor:
                _isPracticeMode ? Colors.lightBlue[100] : Colors.white,
            iconTheme: const IconThemeData(color: Colors.black),
            title: const Text(
              '問題選択',
              style: TextStyle(color: Colors.black),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(Icons.help_outline,
                    color: _isTutorialActive ? Colors.grey : Colors.black),
                onPressed: () {
                  if (!_isTutorialActive) {
                    // オンボーディング（ガイダンス）を開始
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      setState(() {
                        _isOnboadingActive = true;
                        _guidanceNum = 0;
                      });
                      _updateGuidanceOverlayPosition();
                    });

                    _loadProgress();
                  }
                },
              ),
              IconButton(
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('ログアウト'),
                        content: const Text('本当にログアウトしますか？'),
                        actions: <Widget>[
                          TextButton(
                            child: const Text('キャンセル'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          TextButton(
                            child: const Text('ログアウト'),
                            onPressed: () {
                              Navigator.of(context).pop();
                              AuthService.logout(context);
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                icon: const Icon(
                  Icons.logout,
                  color: Colors.black,
                ),
                tooltip: 'ログアウト',
              ),
            ],
          ),
          backgroundColor:
              _isPracticeMode ? Colors.lightBlue[50] : Colors.grey[200],
          body: GestureDetector(
            key: swipeAreaKey,
            onHorizontalDragUpdate: _handleSwipe,
            onHorizontalDragEnd: (details) {
              if (_isTutorialActive &&
                  _steps[_currentStep]['requires'] == 'swipe') {
                _conditionMet();
              }
            },
            child: Stack(
              children: [
                CustomTapEffectOverlay(
                  child: Column(
                    children: [
                      // 達成率インジケーター
                      Expanded(
                        flex: 3,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 0),
                          child: Container(
                            color: _isPracticeMode
                                ? Colors.lightBlue[100]
                                : const Color.fromARGB(255, 19, 26, 61),
                            child: Center(
                              child: CustomIndicator(
                                controller: _indicatorController,
                                label: '問題達成率',
                                percentage: _answeredPercentage,
                              ),
                            ),
                          ),
                        ),
                      ),
                      // 質問タイプ切替
                      Padding(
                        padding: const EdgeInsets.only(top: 12.5),
                        child: Container(
                          decoration: const BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                          ),
                          padding: const EdgeInsets.only(bottom: 12.5),
                          child: Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 30.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Visibility(
                                  visible: _selectedQuestionType != 0,
                                  child: const Icon(Icons.chevron_left,
                                      size: 24.0),
                                  replacement: const SizedBox(width: 24.0),
                                ),
                                Expanded(
                                  child: Center(
                                    child: ToggleButtons(
                                      key: toggleTabKey,
                                      borderColor: const Color.fromARGB(
                                          255, 78, 106, 153),
                                      fillColor: const Color.fromARGB(
                                          255, 78, 106, 153),
                                      selectedBorderColor: const Color.fromARGB(
                                          255, 78, 106, 153),
                                      selectedColor: Colors.white,
                                      borderRadius: BorderRadius.circular(4.0),
                                      children: [
                                        Container(
                                          key:
                                              descriptiveKey, // トグル内全域をカバーするため、Containerでラップ
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 12.0),
                                          alignment: Alignment.center,
                                          child: const Text('用語記述'),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12.0),
                                          child: Text('実装処理'),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12.0),
                                          child: Text('デバッグ'),
                                        ),
                                      ],
                                      isSelected: _selections,
                                      onPressed: (int index) {
                                        setState(() {
                                          for (int buttonIndex = 0;
                                              buttonIndex < _selections.length;
                                              buttonIndex++) {
                                            _selections[buttonIndex] =
                                                buttonIndex == index;
                                          }
                                          _toggleQuestionType();
                                        });
                                        SharedPreferences.getInstance()
                                            .then((prefs) {
                                          prefs.setInt(
                                              'selectedQuestionType', index);
                                        });
                                        if (_isTutorialActive &&
                                            _steps[_currentStep]['requires'] ==
                                                'toggle') {
                                          _conditionMet();
                                        }
                                      },
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: _selectedQuestionType != 2,
                                  child: const Icon(Icons.chevron_right,
                                      size: 24.0),
                                  replacement: const SizedBox(width: 24.0),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      // フィルターボタン・プラクティス・リセット
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 30.0, vertical: 8.0),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              ElevatedButton.icon(
                                key: filterButtonKey,
                                onPressed: () {
                                  setState(() {
                                    _filterIndex = (_filterIndex + 1) % 4;
                                  });
                                  _scrollController.jumpTo(0.0);
                                  if (_isTutorialActive &&
                                      _steps[_currentStep]['requires'] ==
                                          'filter') {
                                    _conditionMet();
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  foregroundColor: Colors.white,
                                  backgroundColor: Colors.black,
                                  side: const BorderSide(color: Colors.grey),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                                icon: Icon(
                                  _filterIndex == 0
                                      ? Icons.filter_alt
                                      : _filterIndex == 1
                                          ? Icons.check_circle
                                          : _filterIndex == 2
                                              ? Icons.remove_circle
                                              : Icons.flag,
                                  color: Colors.white,
                                ),
                                label: Text(_getFilterLabel()),
                              ),
                              const SizedBox(width: 10.0),
                              FadeTransition(
                                opacity: _practiceButtonAnimation,
                                key: practiceButtonKey,
                                child: ElevatedButton.icon(
                                  onPressed: () {
                                    setState(() {
                                      _isPracticeMode = !_isPracticeMode;
                                      _savePracticeModeState();
                                      if (_isPracticeMode) {
                                        _practiceButtonController.forward();
                                      } else {
                                        _practiceButtonController.reset();
                                        _expandedDescriptiveQuestions.clear();
                                        _expandedImplementationQuestions
                                            .clear();
                                      }
                                    });
                                    if (_isTutorialActive &&
                                        _steps[_currentStep]['requires'] ==
                                            'practice') {
                                      _conditionMet();
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                    foregroundColor: Colors.white,
                                    backgroundColor: _isPracticeMode
                                        ? Colors.blueAccent
                                        : Colors.lightBlue,
                                    side: const BorderSide(color: Colors.grey),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8.0),
                                    ),
                                  ),
                                  icon: const Icon(
                                    Icons.school,
                                    color: Colors.white,
                                  ),
                                  label: const Text('プラクティス'),
                                ),
                              ),
                              const SizedBox(width: 10.0),
                              ElevatedButton.icon(
                                key: resetButtonKey,
                                onPressed: () async {
                                  bool confirm = await showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return AlertDialog(
                                            title: const Text('問題達成率をリセット'),
                                            content: const Text(
                                                '本当に問題達成率をリセットしますか？'),
                                            actions: <Widget>[
                                              TextButton(
                                                child: const Text('キャンセル'),
                                                onPressed: () {
                                                  Navigator.of(context)
                                                      .pop(false);
                                                },
                                              ),
                                              TextButton(
                                                child: const Text('リセット'),
                                                onPressed: () {
                                                  Navigator.of(context)
                                                      .pop(true);
                                                },
                                              ),
                                            ],
                                          );
                                        },
                                      ) ??
                                      false;

                                  if (confirm) {
                                    final provider =
                                        Provider.of<QuestionsProvider>(context,
                                            listen: false);
                                    try {
                                      await provider.reset();
                                      setState(() {
                                        _answeredPercentage = 0;
                                        _descriptiveQuestionsFuture =
                                            provider.fetchQuestions(
                                                _selectedQuestionType);
                                        _completeListFuture =
                                            provider.fetchCompleteLists(
                                                _selectedQuestionType);
                                      });
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        const SnackBar(
                                            content: Text('問題達成率をリセットしました')),
                                      );
                                    } catch (e) {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(
                                        SnackBar(
                                            content: Text('リセットに失敗しました: $e')),
                                      );
                                    }
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  foregroundColor: Colors.white,
                                  backgroundColor: Colors.red,
                                  side: const BorderSide(color: Colors.grey),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                ),
                                icon: const Icon(
                                  Icons.refresh,
                                  color: Colors.white,
                                ),
                                label: const Text('リセット'),
                              ),
                            ],
                          ),
                        ),
                      ),
                      // 質問リスト
                      Expanded(
                        flex: 7,
                        child: FadeTransition(
                          opacity: _fadeController ??
                              const AlwaysStoppedAnimation(1.0),
                          child: FutureBuilder<List<Map<String, dynamic>>>(
                            future: _descriptiveQuestionsFuture,
                            builder: (context, snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.waiting) {
                                return const Center(
                                    child: CircularProgressIndicator());
                              }
                              if (snapshot.hasError) {
                                return Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      const Text('エラーが発生しました'),
                                      const SizedBox(height: 10),
                                      ElevatedButton(
                                        onPressed: () {
                                          setState(() {
                                            var provider =
                                                Provider.of<QuestionsProvider>(
                                                    context,
                                                    listen: false);
                                            _descriptiveQuestionsFuture =
                                                provider.fetchQuestions(
                                                    _selectedQuestionType);
                                            _completeListFuture =
                                                provider.fetchCompleteLists(
                                                    _selectedQuestionType);
                                          });
                                        },
                                        child: const Text('再試行'),
                                      ),
                                    ],
                                  ),
                                );
                              }
                              if (!snapshot.hasData || snapshot.data!.isEmpty) {
                                return const Center(child: Text('問題がありません'));
                              } else {
                                List<Map<String, dynamic>> questions =
                                    snapshot.data!;
                                return FutureBuilder<List<dynamic>>(
                                  future: _completeListFuture,
                                  builder: (context, completeSnapshot) {
                                    if (completeSnapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      return const Center(
                                          child: CircularProgressIndicator());
                                    }
                                    if (completeSnapshot.hasError) {
                                      return Center(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            const Text('エラーが発生しました'),
                                            const SizedBox(height: 10),
                                            ElevatedButton(
                                              onPressed: () {
                                                setState(() {
                                                  var provider = Provider.of<
                                                          QuestionsProvider>(
                                                      context,
                                                      listen: false);
                                                  _completeListFuture = provider
                                                      .fetchCompleteLists(
                                                          _selectedQuestionType);
                                                });
                                              },
                                              child: const Text('再試行'),
                                            ),
                                          ],
                                        ),
                                      );
                                    }
                                    List<dynamic> completeList =
                                        completeSnapshot.data ?? [];

                                    final InterruptionDataController
                                        interruptionController = Get.find();

                                    return NotificationListener<
                                        ScrollNotification>(
                                      onNotification:
                                          (ScrollNotification notification) {
                                        if (notification
                                            is ScrollEndNotification) {
                                          _saveScrollPosition();
                                        }
                                        return false;
                                      },
                                      child: Scrollbar(
                                        controller: _scrollController,
                                        thumbVisibility: true,
                                        interactive: true,
                                        thickness: 12.0,
                                        radius: const Radius.circular(4.0),
                                        child: ListView.separated(
                                          controller: _scrollController,
                                          itemCount: questions.length,
                                          separatorBuilder: (context, index) =>
                                              const Divider(
                                                  color: Colors.transparent),
                                          itemBuilder: (context, index) {
                                            Color currentCardColor = cardColors[
                                                index % cardColors.length];
                                            Map<String, dynamic> question =
                                                questions[index];

                                            // フィルタリング前
                                            bool isCompleted =
                                                completeList.contains(
                                              _selectedQuestionType == 0
                                                  ? question['questionListId']
                                                  : question['questionId'],
                                            );

                                            return FutureBuilder<bool>(
                                              future: interruptionController
                                                  .hasInterruption(
                                                _selectedQuestionType == 0
                                                    ? question['questionListId']
                                                    : question['questionId'],
                                                _selectedQuestionType,
                                              ),
                                              builder: (context,
                                                  interruptionSnapshot) {
                                                bool interruptionExists =
                                                    interruptionSnapshot.data ??
                                                        false;

                                                // 追加: 採点結果の未読フラグをチェック
                                                int questionId =
                                                    (_selectedQuestionType == 0)
                                                        ? question[
                                                            'questionListId']
                                                        : question[
                                                            'questionId'];

                                                return FutureBuilder<bool>(
                                                  future: _programmingService
                                                      .isGradingResultUnread(
                                                          questionId),
                                                  builder: (context,
                                                      gradingSnapshot) {
                                                    bool hasUnreadResult =
                                                        gradingSnapshot.data ??
                                                            false;

                                                    // フィルタリングロジック
                                                    if (_filterIndex == 1 &&
                                                        !isCompleted) {
                                                      return const SizedBox
                                                          .shrink();
                                                    }
                                                    if (_filterIndex == 2 &&
                                                        isCompleted) {
                                                      return const SizedBox
                                                          .shrink();
                                                    }
                                                    if (_filterIndex == 3 &&
                                                        !interruptionExists) {
                                                      return const SizedBox
                                                          .shrink();
                                                    }

                                                    return _buildListItem(
                                                      currentCardColor,
                                                      question,
                                                      isCompleted,
                                                      interruptionExists,
                                                      hasUnreadResult, // 未読フラグ
                                                      index,
                                                    );
                                                  },
                                                );
                                              },
                                            );
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        // チュートリアル／オンボーディングオーバーレイ（ガイダンス部分）
        if (_isOnboadingActive && !_isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isOnboadingActive && !_isTutorialActive)
          GestureDetector(
            onTap: _guidanceStep,
            child: Padding(
              padding: EdgeInsets.only(bottom: 80), // FABと被らないよう下部に余白を追加
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Stack(
                  children: [
                    _buildBottomPanel(_guidance[_guidanceNum]),
                    if (_currentStep < _steps.length - 1)
                      Positioned(
                        right: 0,
                        top: 0,
                        child: ElevatedButton.icon(
                          onPressed: () {
                            if (_isOnboadingActive) {
                              _endGuidance();
                            } else if (_isTutorialActive) {
                              _endTutorial();
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                              side: BorderSide(color: Colors.black, width: 2.5),
                            ),
                          ),
                          icon: Image.asset(
                            'assets/Skip.png',
                            width: 25,
                            height: 25,
                          ),
                          label: Text(
                            "スキップ",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        if (_isOnboadingActive && _isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isShowDialog) Center(child: _buildTutorialDialog()),
        // 操作説明（チュートリアル）パネル
        if (_isTutorialActive && !_isOnboadingActive)
          GestureDetector(
            onTap: _checkAndProceed,
            child: Padding(
              padding: EdgeInsets.only(bottom: 80), // FABと被らないよう下部に余白を追加
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Stack(
                  children: [
                    _buildBottomPanel(_steps[_currentStep]['text']),
                    if (_currentStep < _steps.length - 1)
                      Positioned(
                        right: 0,
                        top: 0,
                        child: ElevatedButton.icon(
                          onPressed: () {
                            if (_isOnboadingActive) {
                              _endGuidance();
                            } else if (_isTutorialActive) {
                              _endTutorial();
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                              side: BorderSide(color: Colors.black, width: 2.5),
                            ),
                          ),
                          icon: Image.asset(
                            'assets/Skip.png',
                            width: 25,
                            height: 25,
                          ),
                          label: Text(
                            "スキップ",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        // FABタップ促進のアニメーション
        // ヘルプオーバーレイ
        if (showHelp)
          HelpOverlay(
            existingWidget: Container(),
            helpMessages: helpMessages,
            backgroundImages: backgroundImages,
            onSkip: () {
              setState(() {
                showHelp = false;
              });
            },
            messageAlignment: messageAlignment,
            margin: margin,
            rectangleSteps: rectangleSteps,
          ),
        // ガイダンス中なら、ガイダンス用の青い枠を表示する
        if (_isOnboadingActive && !_isShowDialog)
          BlueSquareOverlay(
            top: _guidanceOverlayTop,
            left: _guidanceOverlayLeft,
            width: _guidanceOverlayWidth,
            height: _guidanceOverlayHeight,
            flashing: true,
            isVisible: true,
          ),

        // 操作説明用の青い枠（動的に対象ウィジェットの位置を取得）
        if (_isTutorialActive && _isVisible)
          BlueSquareOverlay(
            top: _tutorialOverlayTop,
            left: _tutorialOverlayLeft,
            width: _tutorialOverlayWidth,
            height: _tutorialOverlayHeight,
            flashing: true,
            isVisible: _isVisible,
          ),
      ],
    );
  }

  Widget _buildBackgroundOverlay() {
    return Container(
      color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Widget _buildBottomPanel(String text) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.lightBlue, width: 3),
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 16, color: Colors.black, fontWeight: FontWeight.w600),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTutorialDialog() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 252, 246, 251),
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black26, blurRadius: 10.0, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '操作説明を開始しますか？',
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.black87,
                fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  _startTutorial();
                  _endGuidance();
                },
                child: Text(
                  'はい',
                  style: TextStyle(color: Colors.purple, fontSize: 16.0),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _noTap();
                  _endGuidance();
                },
                child: Text(
                  'いいえ',
                  style: TextStyle(color: Colors.purple, fontSize: 16.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildListItem(
    Color cardColor,
    Map<String, dynamic> question,
    bool isCompleted,
    bool interruptionExists,
    bool hasUnreadResult,
    int index,
  ) {
    int questionId;
    String questionText;

    if (_selectedQuestionType == 0) {
      questionId = question['questionListId'];
      questionText = question['questionListName'] ?? '未定義の質問';
    } else {
      questionId = question['questionId'];
      questionText = question['questionName'] ?? '未定義の質問';
    }

    final InterruptionDataController interruptionController = Get.find();

    return Column(
      children: [
        InkWell(
          onTap: () async {
            // プラクティスモード時
            if (_isPracticeMode && _selectedQuestionType == 0) {
              setState(() {
                if (_expandedDescriptiveQuestions.contains(questionId)) {
                  _expandedDescriptiveQuestions.remove(questionId);
                } else {
                  _expandedDescriptiveQuestions.add(questionId);
                }
              });
              return;
            } else if (_isPracticeMode && _selectedQuestionType == 1) {
              setState(() {
                if (_expandedImplementationQuestions.contains(questionId)) {
                  _expandedImplementationQuestions.remove(questionId);
                } else {
                  _expandedImplementationQuestions.add(questionId);
                }
              });
              return;
            }

            // 中断がある場合
            if (interruptionExists) {
              bool? result = await showDialog(
                context: context,
                barrierDismissible: true,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('回答を再開しますか？'),
                    content:
                        const Text('この問題は前回の回答が中断されています。中断されたところから再開しますか？'),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('最初から'),
                        onPressed: () async {
                          if (_selectedQuestionType == 0) {
                            await interruptionController
                                .clearInterruptions(questionId);
                          } else if (_selectedQuestionType == 1) {
                            await interruptionController
                                .clearImplementationInterruption(questionId);
                          } else if (_selectedQuestionType == 2) {
                            await interruptionController
                                .clearDebuggingInterruption(questionId);
                          }
                          Navigator.of(context).pop(false);
                        },
                      ),
                      TextButton(
                        child: const Text('中断データから'),
                        onPressed: () {
                          Navigator.of(context).pop(true);
                        },
                      ),
                      TextButton(
                        child: const Text('閉じる'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
              if (result == null) {
                return;
              }
              bool proceed = result;

              Map<String, String> interruptions = {};
              Map<String, dynamic> implementationInterruption = {};
              Map<String, dynamic> debuggingInterruption = {};

              if (proceed) {
                if (_selectedQuestionType == 0) {
                  interruptions =
                      await interruptionController.getInterruptions(questionId);
                } else if (_selectedQuestionType == 1) {
                  implementationInterruption = await interruptionController
                      .getImplementationInterruptions(questionId);
                } else if (_selectedQuestionType == 2) {
                  debuggingInterruption = await interruptionController
                      .getDebuggingInterruptions(questionId);
                }
              }

              if (_selectedQuestionType == 0) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => QuestionDescriptionScreenWidget(
                            questionData: question,
                            interruptions: proceed ? interruptions : const {},
                          )),
                ).then((_) => setState(() {}));
              } else if (_selectedQuestionType == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionProgrammingScreenWidget(
                        questionData: question,
                        interruption:
                            proceed ? implementationInterruption : const {},
                        resumeFromInterruption: proceed),
                  ),
                ).then((_) => setState(() {}));
              } else if (_selectedQuestionType == 2) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionDebuggingScreenWidget(
                      questionData: question,
                      interruption: proceed ? debuggingInterruption : const {},
                    ),
                  ),
                ).then((_) => setState(() {}));
              }
            } else {
              // 中断がない場合
              if (_selectedQuestionType == 0) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionDescriptionScreenWidget(
                      questionData: question,
                      interruptions: const {},
                    ),
                  ),
                ).then((_) => setState(() {}));
              } else if (_selectedQuestionType == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionProgrammingScreenWidget(
                      questionData: question,
                      interruption: const {},
                      resumeFromInterruption: false,
                    ),
                  ),
                ).then((_) => setState(() {}));
              } else if (_selectedQuestionType == 2) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionDebuggingScreenWidget(
                      questionData: question,
                      interruption: const {},
                    ),
                  ),
                ).then((_) => setState(() {}));
              }
            }
          },
          child: Container(
            key: index == 0 ? questionCardKey : null,
            height: 80,
            margin: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
            child: Card(
              elevation: 4.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      cardColor.withOpacity(0.6),
                      cardColor,
                      cardColor.withOpacity(0.8),
                      cardColor.withOpacity(0.6),
                    ],
                    stops: const [0.0, 0.3, 0.6, 1.0],
                  ),
                  backgroundBlendMode: BlendMode.overlay,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[600]!.withOpacity(0.5),
                      blurRadius: 8.0,
                      spreadRadius: 2.0,
                      offset: const Offset(4.0, 4.0),
                    ),
                  ],
                ),
                child: ListTile(
                  contentPadding: const EdgeInsets.symmetric(
                      horizontal: 30.0, vertical: 10.0),
                  title: Text(
                    questionText,
                    style: const TextStyle(
                      color: Colors.black87,
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                  trailing: Row(
                    key: index == 0 ? iconAreaKey : null,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // 追加: 未読アイコン
                      if (hasUnreadResult)
                        const Icon(
                          Icons.notification_important,
                          color: Colors.red,
                          semanticLabel: '未確認の採点結果あり',
                        )
                      else
                        const SizedBox(width: 0),
                      const SizedBox(width: 8.0),

                      // 中断フラグ
                      interruptionExists
                          ? Icon(
                              Icons.flag,
                              color: Colors.orange,
                              semanticLabel: '中断あり',
                            )
                          : Icon(
                              Icons.flag,
                              color: Colors.grey.withOpacity(0.6),
                              semanticLabel: '中断なし',
                              key: index == 0 ? interruptionIndicatorKey : null,
                            ),

                      _selectedQuestionType == 0
                          ? HistoryIconButton(
                              key: index == 0 ? historyIconKey : null,
                              questionId: questionId,
                              selectedQuestionType: _selectedQuestionType,
                            )
                          : const SizedBox(width: 10.0),

                      isCompleted
                          ? const Icon(Icons.check_circle,
                              color: Colors.green, semanticLabel: '完了')
                          : Icon(
                              Icons.check_circle,
                              color: Colors.grey.withOpacity(0.6),
                              semanticLabel: '未完了',
                              key: index == 0 ? completeIconKey : null,
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        // プラクティスモード時の詳細表示
        if (_isPracticeMode &&
            _selectedQuestionType == 0 &&
            _expandedDescriptiveQuestions.contains(questionId))
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: ExpansionTile(
              title: const Text(
                '詳細',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0,
                ),
              ),
              initiallyExpanded: true,
              children: [
                ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: question['questionList'].length,
                  itemBuilder: (context, detailIndex) {
                    final Map<String, dynamic> selectedQuestion =
                        question['questionList'][detailIndex];
                    return ListTile(
                      leading:
                          const Icon(Icons.question_answer, color: Colors.blue),
                      title: Text(
                        selectedQuestion['relatedTerms'].isNotEmpty
                            ? selectedQuestion['relatedTerms'][0]
                            : '詳細未設定',
                      ),
                      onTap: () {
                        Map<String, dynamic> individualQuestion =
                            Map<String, dynamic>.from(question);
                        individualQuestion['questionList'] = [selectedQuestion];
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                QuestionDescriptionScreenWidget(
                              questionData: individualQuestion,
                              interruptions: const {},
                            ),
                          ),
                        );
                      },
                      trailing: const Icon(Icons.arrow_forward_ios, size: 16.0),
                      contentPadding: const EdgeInsets.symmetric(
                          horizontal: 16.0, vertical: 4.0),
                      dense: true,
                      visualDensity: VisualDensity.compact,
                      hoverColor: Colors.blue.withOpacity(0.1),
                      focusColor: Colors.blue.withOpacity(0.1),
                      tileColor: Colors.white,
                    );
                  },
                ),
              ],
            ),
          ),
        if (_isPracticeMode &&
            _selectedQuestionType == 1 &&
            _expandedImplementationQuestions.contains(questionId))
          _buildImplementationPracticeView(question),
      ],
    );
  }

  Widget _buildImplementationPracticeView(Map<String, dynamic> question) {
    final List<dynamic>? subTasks = question['subTaskList'];
    if (subTasks == null || subTasks.isEmpty) {
      return const SizedBox.shrink();
    }

    final int displayCount = subTasks.length - 1;
    if (displayCount <= 0) {
      return const SizedBox.shrink();
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: ExpansionTile(
        title: const Text(
          '詳細',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16.0,
          ),
        ),
        initiallyExpanded: true,
        children: [
          ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemCount: displayCount,
            itemBuilder: (context, i) {
              final int currentIndex = i + 1;
              final dynamic previousSubTask = subTasks[i];
              final dynamic currentSubTask = subTasks[currentIndex];
              final String subTaskLabel = 'サブタスク${currentIndex}';

              return ListTile(
                leading: const Icon(Icons.task, color: Colors.blue),
                title: Text(subTaskLabel),
                onTap: () {
                  final Map<String, dynamic> individualQuestion =
                      Map<String, dynamic>.from(question);

                  individualQuestion['subTaskList'] = [
                    previousSubTask,
                    currentSubTask,
                  ];

                  final List<dynamic> modelAnswers =
                      (question['modelAnswer'] ?? []) as List<dynamic>;
                  if (modelAnswers.length > currentIndex) {
                    individualQuestion['modelAnswer'] = [
                      modelAnswers[i],
                      modelAnswers[currentIndex]
                    ];
                  }

                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => QuestionProgrammingScreenWidget(
                        questionData: individualQuestion,
                        interruption: const {},
                        resumeFromInterruption: false,
                      ),
                    ),
                  );
                },
                trailing: const Icon(Icons.arrow_forward_ios, size: 16.0),
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 4.0),
                dense: true,
                visualDensity: VisualDensity.compact,
                hoverColor: Colors.blue.withOpacity(0.1),
                focusColor: Colors.blue.withOpacity(0.1),
                tileColor: Colors.white,
              );
            },
          ),
        ],
      ),
    );
  }

  // 用語記述用のダイアログなどは元のコードをそのまま維持してください。
  // 問題情報をダイアログで表示
  void _showQuestionInfoDialog(Map<String, dynamic> question) {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    int questionId = _selectedQuestionType == 0
        ? question['questionListId']
        : question['questionId'];
    List<dynamic> questionList = question['questionList'] ?? [];
    Set<String> relatedTermsSet = {};
    for (var item in questionList) {
      List<String> terms = List<String>.from(item['relatedTerms'] ?? []);
      relatedTermsSet.addAll(terms);
    }
    String difficulty = question['difficulty'];
    List<String> relatedTerms = relatedTermsSet.toList();
    String questionName = _selectedQuestionType == 0
        ? (question['questionListName'] ?? '未定義の問題')
        : (question['questionName'] ?? '未定義の問題');

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('問題情報'),
          content: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('難易度：$difficulty'),
                const SizedBox(height: 20.0),
                // 対象となる用語
                const Text(
                  '対象となる用語：',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                const SizedBox(height: 10),
                _buildRelatedTermsWrap(relatedTerms),
              ],
            ),
          ),
          actions: [
            TextButton(
              child: const Text('単語帳を作成'),
              onPressed: () async {
                await _createWordListFromTerms(relatedTerms, questionName);
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: const Text('閉じる'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget _buildRelatedTermsWrap(List<String> relatedTerms) {
    if (relatedTerms.isEmpty) {
      return const Text('用語が登録されていません');
    }
    return Wrap(
      spacing: 8.0,
      runSpacing: 8.0,
      children: relatedTerms.map((term) {
        return Semantics(
          label: '用語: $term',
          button: true,
          child: GestureDetector(
            onTap: () {
              _showTermDefinitionDialog(term);
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.grey[300]!),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 1,
                    blurRadius: 5,
                    offset: const Offset(0, 3),
                  ),
                ],
              ),
              child: Text(
                term,
                style: const TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Future<void> _createWordListFromTerms(
      List<String> terms, String questionName) async {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    String initialWordListName = '$questionName 単語帳';
    String wordListName = initialWordListName;

    await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        TextEditingController controller =
            TextEditingController(text: initialWordListName);
        return AlertDialog(
          title: const Text('単語帳の名前を入力'),
          content: TextField(
            onChanged: (value) {
              wordListName = value;
            },
            controller: controller,
            decoration: const InputDecoration(
              hintText: '単語帳名',
              border: OutlineInputBorder(),
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('キャンセル'),
              onPressed: () {
                Navigator.of(context).pop();
                wordListName = '';
              },
            ),
            TextButton(
              child: const Text('作成'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );

    if (wordListName.trim().isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('単語帳名を入力してください')),
      );
      return;
    }

    await glossaryProvider.addWordList(wordListName.trim());
    final newWordList = glossaryProvider.wordLists.last;

    for (String term in terms) {
      await glossaryProvider.addTermToWordList(newWordList.id, term);
    }

    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(content: Text('単語帳を作成しました')),
    );
  }

  void _showTermDefinitionDialog(String term) {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    try {
      Map<String, dynamic> termData =
          glossaryProvider.glossary.firstWhere((item) => item['term'] == term);

      String definition = termData['definition'] ?? '定義がありません';
      List<String> relatedTerms = glossaryProvider.getRelatedTerms(term);

      showDialog(
        context: context,
        builder: (BuildContext context) {
          bool isFavorite = glossaryProvider.isFavorite(term);

          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return AlertDialog(
                title: Text(term),
                content: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        definition,
                        style: const TextStyle(fontSize: 16.0),
                      ),
                      const SizedBox(height: 20),
                      if (relatedTerms.isNotEmpty) ...[
                        const Text(
                          '関連用語',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Wrap(
                          spacing: 8,
                          children: relatedTerms.map<Widget>((relatedTerm) {
                            return Semantics(
                              label: '関連用語: $relatedTerm',
                              button: true,
                              child: ActionChip(
                                label: Text(relatedTerm),
                                onPressed: () {
                                  _showTermDefinitionDialog(relatedTerm);
                                },
                                backgroundColor: Colors.blue[50],
                                tooltip: '関連用語: $relatedTerm',
                              ),
                            );
                          }).toList(),
                        ),
                      ],
                    ],
                  ),
                ),
                actions: <Widget>[
                  TextButton(
                    child: Text(isFavorite ? 'お気に入りを解除' : 'お気に入りに追加'),
                    onPressed: () async {
                      if (isFavorite) {
                        await glossaryProvider.removeFavorite(term);
                      } else {
                        await glossaryProvider.addFavorite(term);
                      }
                      setState(() {});
                    },
                  ),
                  TextButton(
                    child: const Text('単語帳に追加'),
                    onPressed: () {
                      _showAddToWordListDialog(term);
                    },
                  ),
                  TextButton(
                    child: const Text('閉じる'),
                    onPressed: () {
                      Navigator.of(context).pop(); // ダイアログを閉じる
                    },
                  ),
                ],
              );
            },
          );
        },
      );
    } catch (e, stackTrace) {
      // エラーが発生した場合、エラーログを表示
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const SelectableText('エラーが発生しました'),
            content: SingleChildScrollView(
              child: SelectableText('エラー内容: $e\n\nスタックトレース:\n$stackTrace'),
            ),
            actions: <Widget>[
              TextButton(
                child: const Text('閉じる'),
                onPressed: () {
                  Navigator.of(context).pop(); // ダイアログを閉じる
                },
              ),
            ],
          );
        },
      );
    }
  }

  // 単語帳に追加ダイアログ
  void _showAddToWordListDialog(String term) {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        bool createNew = false;
        String? selectedWordListId;
        String newWordListName = '';
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: const Text('単語帳に追加'),
              content: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min, // サイズを最小に
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          value: createNew,
                          onChanged: (bool? value) {
                            setState(() {
                              createNew = value ?? false;
                              if (createNew) {
                                selectedWordListId = null;
                              }
                            });
                          },
                          activeColor: Colors.blueAccent,
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                createNew = !createNew;
                                if (createNew) {
                                  selectedWordListId = null;
                                }
                              });
                            },
                            child: const Text('新しい単語帳を作成する'),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    if (!createNew)
                      DropdownButtonFormField<String>(
                        decoration: const InputDecoration(
                          labelText: '単語帳を選択',
                          border: OutlineInputBorder(),
                        ),
                        value: selectedWordListId,
                        onChanged: (String? newValue) {
                          setState(() {
                            selectedWordListId = newValue;
                          });
                        },
                        items: glossaryProvider.wordLists
                            .map<DropdownMenuItem<String>>((WordList wordList) {
                          return DropdownMenuItem<String>(
                            value: wordList.id,
                            child: Text(wordList.name),
                          );
                        }).toList(),
                      ),
                    if (createNew)
                      TextFormField(
                        maxLength: 20,
                        decoration: const InputDecoration(
                          labelText: '新しい単語帳の名前',
                          hintText: '最大20文字',
                          border: OutlineInputBorder(),
                        ),
                        onChanged: (value) {
                          newWordListName = value;
                        },
                      ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: const Text('キャンセル'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: const Text('追加'),
                  onPressed: () async {
                    if (createNew) {
                      if (newWordListName.trim().isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('単語帳名を入力してください')),
                        );
                        return;
                      }
                      await glossaryProvider
                          .addWordList(newWordListName.trim());
                      final newWordList = glossaryProvider.wordLists.last;
                      await glossaryProvider.addTermToWordList(
                          newWordList.id, term);
                    } else {
                      if (selectedWordListId == null) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(content: Text('単語帳を選択してください')),
                        );
                        return;
                      }
                      await glossaryProvider.addTermToWordList(
                          selectedWordListId!, term);
                    }
                    Navigator.of(context).pop();
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('単語帳に追加しました')),
                    );
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }
} // ...

class CustomIndicator extends AnimatedWidget {
  final String label;
  final double percentage;

  const CustomIndicator({
    Key? key,
    required AnimationController controller,
    required this.label,
    required this.percentage,
  }) : super(key: key, listenable: controller);

  Color _getColorBasedOnPercentage(double percentage) {
    if (percentage < 20) return Colors.grey;
    if (percentage < 36) return Colors.green;
    if (percentage < 63) return Colors.blue;
    if (percentage < 76) return Colors.purple;
    if (percentage < 96) return Colors.orange;
    if (percentage < 100) return Colors.pink;
    return Colors.red;
  }

  Gradient _getGradientBasedOnPercentage(double percentage) {
    if (percentage < 20) {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFBDBDBD),
          Color(0xFF757575),
        ],
      );
    } else if (percentage < 36) {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFF43A047),
          Color(0xFF2E7D32),
        ],
      );
    } else if (percentage < 63) {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFF64B5F6),
          Color(0xFF1976D2),
        ],
      );
    } else if (percentage < 76) {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFBA68C8),
          Color(0xFF8E24AA),
        ],
      );
    } else if (percentage < 96) {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFFFA726),
          Color(0xFFF57C00),
        ],
      );
    } else if (percentage < 100) {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFF48FB1),
          Color(0xFFD81B60),
        ],
      );
    } else {
      return const LinearGradient(
        begin: Alignment.topLeft,
        end: Alignment.bottomRight,
        colors: [
          Color(0xFFEF5350),
          Color(0xFFB71C1C),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final AnimationController controller = listenable as AnimationController;
    double currentPercentage = percentage * controller.value;
    Color indicatorColor = _getColorBasedOnPercentage(currentPercentage);
    Gradient indicatorGradientColor =
        _getGradientBasedOnPercentage(currentPercentage);

    double screenWidth = MediaQuery.of(context).size.width;
    double indicatorSize = screenWidth < 360 ? 100 : 150;

    return Container(
      width: indicatorSize,
      height: indicatorSize,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        gradient: indicatorGradientColor,
        border: Border.all(color: indicatorColor, width: 3.0),
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
            width: indicatorSize,
            height: indicatorSize,
            child: CircularProgressIndicator(
              value: currentPercentage / 100,
              strokeWidth: 15,
              valueColor: AlwaysStoppedAnimation(indicatorColor),
              backgroundColor: Colors.grey[150],
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                label,
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
              Text(
                '${currentPercentage.toStringAsFixed(1)}%',
                style: const TextStyle(fontSize: 24, color: Colors.white),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
