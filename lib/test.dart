import 'package:flutter/material.dart';
import 'package:confetti/confetti.dart'; // pubspec.yamlに confetti: ^0.7.0 を追加

void main() {
  runApp(const AchievementApp());
}

/// メインアプリ
class AchievementApp extends StatelessWidget {
  const AchievementApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Achievements',
      debugShowCheckedModeBanner: false,
      home: const AchievementScreen(),
    );
  }
}

///
/// アチーブメント一覧画面
///
/// - 背景はメニューと同じグラデーション
/// - 4x4(16個)のGridView
/// - アイコンは "未取得" → 黒アイコン, 取得済み → レベルに応じた色
/// - 縦方向の余白をやや大きめにして見やすく
///
class AchievementScreen extends StatelessWidget {
  const AchievementScreen({Key? key}) : super(key: key);

  /// 16個のアチーブ (4段x4列) を想定
  final List<Achievement> _achievements = const [
    Achievement(title: "Streak 3 Days", level: 1, isAcquired: true),
    Achievement(title: "Streak 7 Days", level: 2, isAcquired: true),
    Achievement(title: "Streak 14 Days", level: 3, isAcquired: false),
    Achievement(title: "Streak 30 Days", level: 4, isAcquired: false),
    Achievement(title: "Solve 10 Problems", level: 1, isAcquired: true),
    Achievement(title: "Solve 30 Problems", level: 2, isAcquired: true),
    Achievement(title: "Solve 50 Problems", level: 3, isAcquired: true),
    Achievement(title: "Solve 100 Problems", level: 4, isAcquired: false),
    Achievement(title: "Daily Login 5", level: 1, isAcquired: true),
    Achievement(title: "Daily Login 15", level: 2, isAcquired: false),
    Achievement(title: "Daily Login 30", level: 3, isAcquired: false),
    Achievement(title: "Daily Login 60", level: 4, isAcquired: false),
    Achievement(title: "Win 3 Quizzes", level: 1, isAcquired: true),
    Achievement(title: "Win 5 Quizzes", level: 2, isAcquired: false),
    Achievement(title: "Win 10 Quizzes", level: 3, isAcquired: false),
    Achievement(title: "Win 20 Quizzes", level: 4, isAcquired: false),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // 同じグラデーション背景
      body: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color(0xFF211C54),
              Color(0xFF3B327F),
              Color(0xFF4E4376),
            ],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: SafeArea(
          child: Column(
            children: [
              // ヘッダー
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  'アチーブメント一覧',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: GridView.builder(
                    // 4列
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      childAspectRatio: 0.85, // 縦に余裕を持たせる
                      crossAxisSpacing: 8,
                      mainAxisSpacing: 20, // 縦を大きめに
                    ),
                    itemCount: _achievements.length,
                    itemBuilder: (context, index) {
                      final achievement = _achievements[index];
                      return _buildAchievementTile(context, achievement, index);
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  /// アチーブメント1つ分のタイル
  Widget _buildAchievementTile(BuildContext context, Achievement a, int index) {
    // "未取得"なら黒アイコン、取得済みならレベル色
    final iconColor = a.isAcquired ? _getIconColor(a.level) : Colors.black;

    return InkWell(
      onTap: () {
        // 画面遷移 (ヒーローアニメーション)
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (_) => AchievementDetailScreen(
              achievement: a,
              heroTag: 'achievement-$index',
            ),
          ),
        );
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // ヒーローアニメーション
          Hero(
            tag: 'achievement-$index',
            child: Icon(
              Icons.emoji_events,
              color: iconColor,
              size: 40,
            ),
          ),
          const SizedBox(height: 8),
          // タイトル
          Text(
            a.title,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 12, color: Colors.white),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }

  /// レベルに応じた色
  Color _getIconColor(int level) {
    switch (level) {
      case 1:
        return Colors.grey;
      case 2:
        return Colors.blue;
      case 3:
        return Colors.purple;
      case 4:
        return Colors.amber;
      default:
        return Colors.grey;
    }
  }
}

/// アチーブメント詳細画面
/// - 取得済みなら紙吹雪を飛ばす
/// - 未取得ならアイコンは黒のまま
class AchievementDetailScreen extends StatefulWidget {
  final Achievement achievement;
  final String heroTag;

  const AchievementDetailScreen({
    Key? key,
    required this.achievement,
    required this.heroTag,
  }) : super(key: key);

  @override
  State<AchievementDetailScreen> createState() =>
      _AchievementDetailScreenState();
}

class _AchievementDetailScreenState extends State<AchievementDetailScreen> {
  late ConfettiController _confettiController;

  @override
  void initState() {
    super.initState();
    // 取得済みの場合は、画面表示直後に紙吹雪を発生
    _confettiController =
        ConfettiController(duration: const Duration(seconds: 3));
    if (widget.achievement.isAcquired) {
      // 画面に入るタイミングで紙吹雪スタート
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _confettiController.play();
      });
    }
  }

  @override
  void dispose() {
    _confettiController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // アイコン色 (取得済み or 黒)
    final iconColor = widget.achievement.isAcquired
        ? _getIconColor(widget.achievement.level)
        : Colors.black;

    return Scaffold(
      // 同じグラデーション背景
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Color(0xFF211C54),
                  Color(0xFF3B327F),
                  Color(0xFF4E4376),
                ],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
          ),
          // 紙吹雪 (取得済みのみ)
          if (widget.achievement.isAcquired)
            Align(
              alignment: Alignment.center,
              child: ConfettiWidget(
                confettiController: _confettiController,
                blastDirectionality: BlastDirectionality.explosive,
                shouldLoop: false,
                emissionFrequency: 0.05,
                numberOfParticles: 30,
                gravity: 0.3,
              ),
            ),
          SafeArea(
            child: Column(
              children: [
                // ヘッダー
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      icon: const Icon(Icons.arrow_back, color: Colors.white),
                      onPressed: () => Navigator.pop(context),
                    ),
                    const Text(
                      'アチーブ詳細',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(width: 48), // 戻るボタンのスペース合わせ
                  ],
                ),
                const SizedBox(height: 20),

                // ヒーローアニメーションで拡大
                Hero(
                  tag: widget.heroTag,
                  child: Icon(
                    Icons.emoji_events,
                    color: iconColor,
                    size: 100,
                  ),
                ),
                const SizedBox(height: 16),

                // タイトル
                Text(
                  widget.achievement.title,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 16),

                // 詳細内容
                Expanded(
                  child: Center(
                    child: Text(
                      widget.achievement.isAcquired
                          ? 'レベル: ${widget.achievement.level}\n\n'
                              'ストリークや問題達成率など、\nここで詳細な情報を見られます！\n'
                              '取得おめでとうございます！'
                          : 'まだ未取得のアチーブです。\n'
                              'ストリークや問題達成率を上げて解放しましょう！',
                      style: const TextStyle(
                        color: Colors.white70,
                        fontSize: 16,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// レベルに応じた色
  Color _getIconColor(int level) {
    switch (level) {
      case 1:
        return Colors.grey;
      case 2:
        return Colors.blue;
      case 3:
        return Colors.purple;
      case 4:
        return Colors.amber;
      default:
        return Colors.grey;
    }
  }
}

/// アチーブメント情報
class Achievement {
  final String title; // 例: "Streak 3 Days"
  final int level; // 1 ~ 4
  final bool isAcquired; // 未取得(false) or 取得済み(true)

  const Achievement({
    required this.title,
    required this.level,
    required this.isAcquired,
  });
}
