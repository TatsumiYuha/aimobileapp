// lib/glossary_screen_widget.dart

import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/tutorial_progress_manager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'blue_square_overlay.dart';
import 'help_overlay_service.dart';
import 'animation_build_display.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'glossary_provider.dart';
import 'custom_bottom_app_bar.dart';
import 'auth_service.dart';
import 'word_list_detail_screen_widget.dart';
import 'package:keyboard_actions/keyboard_actions.dart';

class GlossaryScreenWidget extends StatefulWidget {
  @override
  _GlossaryScreenWidgetState createState() => _GlossaryScreenWidgetState();
}

class _GlossaryScreenWidgetState extends State<GlossaryScreenWidget>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  String _searchQuery = '';
  final FocusNode _searchFocusNode = FocusNode();
  final TextEditingController _searchController = TextEditingController();

  final ScrollController _glossaryScrollController = ScrollController();
  final ScrollController _favoritesScrollController = ScrollController();
  final gamificationService = GamificationService();

  // ドロップダウンフィルターの変数
  String _selectedFilter = '全表示'; // 初期値は'全表示'
  List<String> _filterOptions = ['全表示', '索引'];

  // 索引要素
  List<String> _indexElements = [];
  String _selectedIndex = ''; // 選択された索引要素

  bool _isGlossaryLoaded = false; // 用語集がロードされたかどうかのフラグ

  ////////////// チュートリアル／オンボーディング用の変数 //////////////
  final tutorialProgressManager _tutorial = tutorialProgressManager();
  bool _isTutorialActive = false; // 操作説明（チュートリアル）が実行中か
  bool _isOnboadingActive = false; // オンボーディング中か
  bool _isShowDialog = false; // 「操作説明を開始しますか？」のダイアログ表示
  bool _isCondition = false; // 操作が完了したかの一時フラグ
  bool showHelp = false; // ヘルプオーバーレイ表示用
  bool _isVisible = false; // 操作説明での青い枠表示制御

  int _currentStep = 0; // 操作説明（チュートリアル）進行管理
  int _guidanceNum = 0; // ガイダンス進行管理（オンボーディング用）

  bool _isDefinitionDialogOpen = false; //ダイアログ表示中かどうかを管理するフラグ
  final ValueNotifier<int> _stepNotifier = ValueNotifier<int>(0);

  // ガイダンス用のメッセージ（オンボーディング初期表示）
  final List<String> _guidance = [
    "ようこそ！この画面では用語集の操作方法を説明します。",
    "上部の検索バーで用語を検索することができます。",
    "検索バー下のドロップダウンから、一覧のフィルター（全表示、カテゴリー、索引）を選択できます。",
    "「索引」を選ぶと、横スクロールの索引バーが表示され、タップすることで絞り込みが可能です。",
    "用語カードをタップすると、用語の定義や詳細が表示されます。",
    "定義ダイアログ内では、関連用語をタップしてさらに詳しい内容を確認できます。",
    "用語カード上の星アイコンで、お気に入りの登録・解除ができます。",
    "また、用語カードの「単語帳に追加」ボタンから、用語を自分の単語帳に登録できます。",
    "単語帳タブでは、単語帳の作成、編集、削除が行えます。",
    "以上で用語集の説明は終了です。"
  ];

  // 各操作説明（チュートリアル）用のステップ
  final List<Map<String, dynamic>> _steps = [
    {'text': '用語集画面の操作説明を開始します', 'requires': null},
    {'text': '検索バーに「コンパイラ」と入力して、検索機能を試してみましょう', 'requires': 'search'},
    {'text': '検索バー内の「×」アイコンをタップして、検索内容をクリアしてください', 'requires': 'search_clear'},
    {'text': 'ドロップダウンメニューから「索引」を選択して、索引バーを表示させましょう', 'requires': 'filter'},
    {'text': '索引バーをスクロールして「M」をタップし、用語をフィルタリングしてみましょう', 'requires': 'index'},
    {'text': '用語カードをタップして、定義ダイアログを開いてみましょう', 'requires': 'detail'},
    {'text': '関連用語をタップして、さらに詳細を確認してみましょう', 'requires': 'related'},
    {
      'text': 'ダイアログをすべて閉じ、用語カードの★アイコンをタップしてお気に入り登録してみましょう',
      'requires': 'favorite'
    },
    {'text': '「単語帳に追加」ボタンをタップして、用語を単語帳に追加してみましょう', 'requires': 'add_wordlist'},
    {'text': '単語帳タブに移動し、登録済みの単語帳を確認してみましょう', 'requires': 'wordlist_tab'},
    {'text': 'この画面では用語をまとめた単語帳をタップすることでアクセスできます', 'requires': null},
    {'text': '単語帳カードの編集アイコンをタップして、単語帳名を変更してみましょう', 'requires': 'rename'},
    {'text': '単語帳カードの削除アイコンをタップして、不要な単語帳を削除してみましょう', 'requires': 'delete'},
    {'text': 'これで用語集画面の操作説明は終了です', 'requires': null},
  ];

  // ヘルプオーバーレイ用メッセージなど（そのまま）
  final List<String> helpMessages = [
    "検索バー: 用語を検索できます",
  ];

  final List<String> backgroundImages = [
    'assets/Help_search.png',
  ];

  final List<Alignment> messageAlignment = [
    Alignment.topCenter,
  ];

  final List<EdgeInsets> margin = [
    EdgeInsets.only(top: 20),
  ];

  // ヘルプ用：ハイライト矩形（例示用の固定値）
  final List<Rect> rectangleSteps = [
    Rect.fromLTWH(20, 100, 360, 50), // 検索バー
  ];

  //////////// 対象ウィジェットのGlobalKey ////////////
  // 検索バー用のGlobalKey
  final GlobalKey searchBarKey = GlobalKey();

  //検索バーのクリアアイコン用のGlobalKey
  final GlobalKey searchClearKey = GlobalKey();

// フィルター（ドロップダウン）用のGlobalKey
  final GlobalKey filterDropdownKey = GlobalKey();

// 索引バー用のGlobalKey
  final GlobalKey indexBarKey = GlobalKey();

// 用語カード（一覧内の各用語表示用）の代表的なGlobalKey
  final GlobalKey glossaryCardKey = GlobalKey();

// 定義ダイアログ全体用のGlobalKey
  final GlobalKey definitionDialogKey = GlobalKey();

// 定義ダイアログ内の関連用語チップ用のGlobalKey
  final GlobalKey relatedChipKey = GlobalKey();

// 定義ダイアログ内のお気に入り（星）アイコン用のGlobalKey
  final GlobalKey favoriteIconKey = GlobalKey();

// 定義ダイアログ内の「単語帳に追加」ボタン用のGlobalKey
  final GlobalKey addToWordListButtonKey = GlobalKey();

// 単語帳タブ（単語帳一覧表示エリア）用のGlobalKey
  final GlobalKey wordListTabKey = GlobalKey();

// 単語帳カード内の編集（名前変更）アイコン用のGlobalKey
  final GlobalKey renameIconKey = GlobalKey();

// 単語帳カード内の削除アイコン用のGlobalKey
  final GlobalKey deleteIconKey = GlobalKey();

// キーボードアクション内の「閉じる」ボタン用のGlobalKey（必要に応じて）
  final GlobalKey keyboardCloseButtonKey = GlobalKey();

  //////////// ここまで

  //////////// ガイダンス用オーバーレイ変数（既存） ////////////
  double _guidanceOverlayTop = 0.0;
  double _guidanceOverlayLeft = 0.0;
  double _guidanceOverlayWidth = 0.0;
  double _guidanceOverlayHeight = 0.0;
  //////////// ここまで

  //////////// 操作説明用オーバーレイ変数（新規） ////////////
  double _tutorialOverlayTop = 0.0;
  double _tutorialOverlayLeft = 0.0;
  double _tutorialOverlayWidth = 0.0;
  double _tutorialOverlayHeight = 0.0;
  //////////// ここまで

// ガイダンス用の対象ウィジェットのGlobalKeyを返す関数
  GlobalKey? _getGuidanceTargetKey() {
    switch (_guidanceNum) {
      case 0:
        // ウェルカムメッセージは対象なし
        return null;
      case 1:
        // 「上部の検索バーで用語を検索できます」
        return searchBarKey;
      case 2:
        // 「検索バー下のドロップダウンから、一覧のフィルターを選択できます」
        return filterDropdownKey;
      case 3:
        // 「『索引』を選ぶと、横スクロールの索引バーが表示されます」
        return indexBarKey;
      case 4:
        // 「用語カードをタップすると、用語の定義や詳細が表示されます」
        return glossaryCardKey;
      case 5:
        // 「定義ダイアログ内では、関連用語をタップしてさらに詳しい内容を確認できます」
        return relatedChipKey;
      case 6:
        // 「用語カード上の星アイコンで、お気に入りの登録・解除ができます」
        return favoriteIconKey;
      case 7:
        // 「単語帳に追加」ボタンを指示
        return addToWordListButtonKey;
      case 8:
        // 「単語帳タブでは、単語帳の作成、編集、削除が行えます」
        return wordListTabKey;
      default:
        return null;
    }
  }

  // ガイダンス用の対象ウィジェットの位置とサイズを更新する
  void _updateGuidanceOverlayPosition() {
    GlobalKey? targetKey = _getGuidanceTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      // 特にガイダンスステップ4（ドラッグの説明）の場合、先頭アイテムの描画が未完了なら遅延させる

      Future.delayed(Duration(milliseconds: 100), () {
        _updateGuidanceOverlayPosition();
      });

      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _guidanceOverlayTop = globalPosition.dy;
      _guidanceOverlayLeft = globalPosition.dx;
      _guidanceOverlayWidth = size.width;
      _guidanceOverlayHeight = size.height;
    });
  }

// 操作説明（チュートリアル）用の対象ウィジェットのGlobalKeyを返す関数
  GlobalKey? _getTutorialTargetKey() {
    final String? req = _steps[_currentStep]['requires'];
    if (req == null) return null;
    switch (req) {
      case 'search':
        return searchBarKey;
      case 'search_clear':
        return searchClearKey;
      case 'filter':
        return filterDropdownKey;
      case 'index':
        return indexBarKey;
      case 'detail':
        return glossaryCardKey;
      case 'related':
        return relatedChipKey;
      case 'favorite':
        return favoriteIconKey;
      case 'add_wordlist':
        return addToWordListButtonKey;
      case 'wordlist_tab':
        return wordListTabKey;
      case 'rename':
        return renameIconKey;
      case 'delete':
        return deleteIconKey;
      default:
        return null;
    }
  }

  // 操作説明用の対象ウィジェットの位置とサイズを更新する
  void _updateTutorialOverlayPosition() {
    GlobalKey? targetKey = _getTutorialTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      Future.delayed(Duration(milliseconds: 100), () {
        _updateTutorialOverlayPosition();
      });

      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _tutorialOverlayTop = globalPosition.dy;
      _tutorialOverlayLeft = globalPosition.dx;
      _tutorialOverlayWidth = size.width;
      _tutorialOverlayHeight = size.height;
    });
  }
  //////////// ここまで操作説明用

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(_handleTabIndex);

    // 検索コントローラーのリスナーを追加
    _searchController.addListener(() {
      setState(() {
        _searchQuery = _searchController.text;
      });
    });
    // デバッグ用・初回起動時として強制的にオンボーディング（ガイダンス）を開始
    _checkAndStartTutorial();

    _loadProgress();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (!_isGlossaryLoaded) {
      _isGlossaryLoaded = true;

      WidgetsBinding.instance.addPostFrameCallback((_) {
        final provider = Provider.of<GlossaryProvider>(context, listen: false);
        provider.initialize();
        provider.fetchGlossary();
      });
    }
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabIndex);
    _tabController.dispose();
    _searchFocusNode.dispose();
    _searchController.dispose();
    _glossaryScrollController.dispose();
    _favoritesScrollController.dispose();
    super.dispose();
  }

  Future<void> _checkAndStartTutorial() async {
    // ここでは画面ごとに一意なキーを用いる（例：ChatScreenWidget の場合はクラス名や固有のID）
    // 複数の画面で管理する場合、各画面で異なるキーを使用してください。
    final String tutorialKey = 'tutorialShown_${this.runtimeType}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstTime = prefs.getBool(tutorialKey) ?? true;
    if (isFirstTime) {
      // 初回の場合のみチュートリアル／ガイダンスを発火させる
      setState(() {
        _isOnboadingActive = true;
        _guidanceNum = 0;
      });
      // 画面描画後にオーバーレイ位置を更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
      // 一度発火したのでフラグを更新（2回目以降は発火しない）
      await prefs.setBool(tutorialKey, false);
    }
  }

  Future<void> _loadProgress() async {
    int progress = await _tutorial.getProgress('currentStep');
    if (progress < _steps.length) {
      setState(() {
        _currentStep = progress;
      });
    }
  }

  Future<void> _saveProgress(int step) async {
    await _tutorial.saveProgress('currentStep', step);
  }

  Future<void> _resetProgress() async {
    setState(() {
      _currentStep = 0;
    });
    await _tutorial.resetProgress();
  }

  Future<void> _saveTutorialStatus() async {
    await _tutorial.saveTutorialBoolStatus(_isTutorialActive);
    await _tutorial.saveOnboadingBoolStatus(_isOnboadingActive);
    await _tutorial.saveShowDialogBoolStatus(_isShowDialog);
  }

  void _getTutorial() async {
    bool futureTutorialActive = await _tutorial.loadTutorialBoolStatus();
    bool futureOnboardingActive = await _tutorial.loadOnboadingBoolStatus();
    bool futureShowDialogActive = await _tutorial.loadShowDialogBoolStatus();
    setState(() {
      _isTutorialActive = futureTutorialActive;
      _isOnboadingActive = futureOnboardingActive;
      _isShowDialog = futureShowDialogActive;
    });
  }

  void _startTutorial() {
    setState(() {
      _currentStep = 0;
      _isTutorialActive = true;
      _isOnboadingActive = false;
      _isShowDialog = false;
    });
    _saveProgress(0);
    _saveTutorialStatus();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _endTutorial() {
    setState(() {
      _isTutorialActive = false;
      _isOnboadingActive = false;
      _isShowDialog = false;
      _currentStep = 0;
      _isVisible = false;
    });
    _resetProgress();
    _saveTutorialStatus();
  }

  void _guidanceStep() {
    if (_guidanceNum < _guidance.length - 1) {
      setState(() {
        _guidanceNum++;
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
    } else {
      setState(() {
        _isShowDialog = true;
        _isOnboadingActive = true;
      });
    }
  }

  void _noTap() {
    setState(() {
      _currentStep = _steps.length - 1;
      _isTutorialActive = true;
    });
  }

  void _endGuidance() {
    setState(() {
      _isShowDialog = false;
      _isOnboadingActive = false;
    });
  }

  void _nextStep() {
    if (_currentStep < _steps.length - 1) {
      setState(() {
        _currentStep++;
        _isCondition = false;
      });
      _saveProgress(_currentStep);
    } else {
      _endTutorial();
      _saveProgress(_currentStep);
    }
    _checkVisible();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _checkAndProceed() {
    final requires = _steps[_currentStep]['requires'];
    if (requires == null || _isCondition) {
      _nextStep();
    }
  }

  void _conditionMet() {
    if (_isTutorialActive) {
      setState(() {
        _isCondition = true;
      });
      _nextStep();
      _stepNotifier.value = _currentStep;
    }
  }

  // 従来の青い枠表示制御（操作説明用）
  void _checkVisible() {
    if (_currentStep >= 1 && _currentStep <= 15) {
      setState(() {
        _isVisible = true;
      });
    } else {
      setState(() {
        _isVisible = false;
      });
    }
  }

  // タブインデックスの変更を処理
  void _handleTabIndex() {
    setState(() {
      _searchQuery = '';
      _searchController.clear();
      _selectedFilter = '全表示';
      _selectedIndex = '';
    });
  }

  // 索引要素を'letter'の値から動的に生成
  void _buildIndexElements(GlossaryProvider provider) {
    // 'letter'フィールドを抽出し、重複を除去
    Set<String> lettersSet = provider.glossary
        .map<String>((item) => item['letter'].toString())
        .toSet();

    // SetをListに変換
    List<String> letters = lettersSet.toList();

    // カスタムソート関数
    int compareLetters(String letterA, String letterB) {
      if (letterA == letterB) {
        return 0;
      } else if (letterA == "Φ") {
        return -1; // 'Φ'が先頭に来る
      } else if (letterB == "Φ") {
        return 1; // 'Φ'が先頭に来る
      } else if (_isAlphabet(letterA) && !_isAlphabet(letterB)) {
        return -1; // アルファベットが他の文字より前
      } else if (!_isAlphabet(letterA) && _isAlphabet(letterB)) {
        return 1; // 他の文字がアルファベットより後
      } else {
        // 両方アルファベットまたはその他の場合、通常の比較
        return letterA.compareTo(letterB);
      }
    }

    letters.sort(compareLetters);

    // setState()を使用せずに_indexElementsを更新
    _indexElements = letters;
  }

  // 文字がアルファベットかどうかを判定
  bool _isAlphabet(String str) {
    return RegExp(r'^[a-zA-Z]+$').hasMatch(str);
  }

  // KeyboardActionsの設定
  KeyboardActionsConfig _buildKeyboardActionsConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardSeparatorColor: Colors.grey,
      actions: [
        KeyboardActionsItem(
          focusNode: _searchFocusNode,
          toolbarButtons: [
            (node) {
              return GestureDetector(
                key: keyboardCloseButtonKey, // キーボード閉じる用
                onTap: () {
                  node.unfocus();
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "閉じる",
                    style: TextStyle(color: Theme.of(context).primaryColor),
                  ),
                ),
              );
            },
          ],
        ),
      ],
    );
  }

  // 検索バーまたは索引バーを決定
  Widget _buildIndexOrSearchBar(GlossaryProvider glossaryProvider) {
    // 索引要素をデータロード後に構築
    if (_indexElements.isEmpty && !glossaryProvider.isLoading) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _buildIndexElements(glossaryProvider);
        if (mounted) {
          setState(() {});
        }
      });
    }

    // 検索バーまたは索引バーを選択
    if (_selectedFilter == '索引') {
      return _buildHorizontalIndexBar();
    } else {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.08,
          child: KeyboardActions(
            config: _buildKeyboardActionsConfig(context),
            child: TextField(
              key: searchBarKey, // 検索バー用GlobalKey
              focusNode: _searchFocusNode,
              controller: _searchController,

              decoration: InputDecoration(
                hintText: '検索したい用語を入力してください',
                prefixIcon: Icon(Icons.search),
                suffixIcon: _searchQuery.isNotEmpty
                    ? IconButton(
                        key: searchClearKey,
                        icon: Icon(Icons.close),
                        onPressed: () {
                          setState(() {
                            _searchController.clear();
                          });
                          if (_isTutorialActive &&
                              _steps[_currentStep]['requires'] ==
                                  'search_clear') {
                            _conditionMet();
                          }
                        },
                      )
                    : null,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              onChanged: (value) {
                setState(() {
                  _searchQuery = value;
                });
                // 余分な空白を取り除く
                final trimmedValue = value.trim();
                if (trimmedValue == "コンパイラ") {
                  // 現在のステップが 'search' で、チュートリアルが有効なら _conditionMet() を呼び出す
                  if (_isTutorialActive &&
                      _steps[_currentStep]['requires'] == 'search') {
                    _conditionMet();
                  }
                }
              },
              textInputAction: TextInputAction.done,
            ),
          ),
        ),
      );
    }
  }

  // 索引バーを横スクロールで表示
  Widget _buildHorizontalIndexBar() {
    return Container(
      key: indexBarKey, // 索引バー用GlobalKey
      height: 50,
      color: Colors.white.withOpacity(0.8),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _indexElements.length,
        itemBuilder: (context, index) {
          String letter = _indexElements[index];
          bool isSelected = _selectedIndex == letter;
          return GestureDetector(
            onTap: () {
              setState(() {
                _selectedIndex = letter;
                _searchQuery = '';
                _searchController.clear();
              });
              if (letter == 'M') {
                if (_isTutorialActive &&
                    _steps[_currentStep]['requires'] == 'index') {
                  _conditionMet();
                }
              }
            },
            child: Container(
              width: 40,
              alignment: Alignment.center,
              color: isSelected
                  ? Theme.of(context).primaryColor.withOpacity(0.5)
                  : Colors.transparent,
              child: Text(
                letter,
                style: TextStyle(
                  color: isSelected ? Colors.white : Colors.black,
                  fontSize: 16,
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  // 定義ダイアログを表示
  void _showDefinitionDialog(String term, String definition) {
    setState(() {
      _isDefinitionDialogOpen = true;
    });

    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        final glossaryProvider =
            Provider.of<GlossaryProvider>(dialogContext, listen: false);
        bool isFavorite = glossaryProvider.isFavorite(term);
        List<String> relatedTerms = glossaryProvider.getRelatedTerms(term);

        final String? currentReq = _steps[_currentStep]['requires'];
        final bool isSearchRelated = (currentReq == 'favorite');

        return ValueListenableBuilder<int>(
          valueListenable: _stepNotifier,
          builder: (context, currentStepValue, child) {
            final requires = _steps[currentStepValue]['requires'];
            final stepText = _steps[currentStepValue]['text'];
            return Stack(
              children: [
                // AlertDialog本体
                AlertDialog(
                  title: Text(term),
                  content: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(definition),
                        SizedBox(height: 20),
                        if (relatedTerms.isNotEmpty) ...[
                          Text(
                            '関連用語',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          SizedBox(height: 10),
                          // 定義ダイアログ内の関連用語チップ部分（抜粋）
                          Wrap(
                            spacing: 8,
                            children: relatedTerms
                                .asMap()
                                .entries
                                .map<Widget>((entry) {
                              String relatedTerm = entry.value;
                              return Builder(
                                builder: (context) {
                                  return ActionChip(
                                    label: Text(relatedTerm),
                                    onPressed: () {
                                      final glossaryItem =
                                          glossaryProvider.glossary.firstWhere(
                                        (item) => item['term'] == relatedTerm,
                                        orElse: () => {
                                          'definition': '定義なし',
                                          'number': '0'
                                        },
                                      );
                                      // 関連用語の閲覧カウントを進める
                                      gamificationService.addGlossaryView(
                                          glossaryItem['number'].toString());
                                      // 新しいダイアログを開く（現在のダイアログは閉じない）

                                      _showDefinitionDialog(
                                        relatedTerm,
                                        glossaryProvider.glossary.firstWhere(
                                              (item) =>
                                                  item['term'] == relatedTerm,
                                              orElse: () =>
                                                  {'definition': '定義なし'},
                                            )['definition'] ??
                                            '定義なし',
                                      );
                                      if (_isTutorialActive &&
                                          _steps[_currentStep]['requires'] ==
                                              'related') {
                                        _conditionMet();
                                      }
                                    },
                                  );
                                },
                              );
                            }).toList(),
                          ),
                        ],
                      ],
                    ),
                  ),
                  actions: <Widget>[
                    TextButton(
                      child: Text(isFavorite ? 'お気に入りを解除する' : 'お気に入りに登録する'),
                      onPressed: () async {
                        if (isFavorite) {
                          await glossaryProvider.removeFavorite(term);
                        } else {
                          await glossaryProvider.addFavorite(term);
                        }
                        setState(() {
                          isFavorite = !isFavorite;
                        });
                        // ダイアログは閉じない
                      },
                    ),
                    TextButton(
                      child: Text('単語帳に追加'),
                      onPressed: () {
                        _showAddToWordListDialog(term);
                      },
                    ),
                    TextButton(
                      child: Text('閉じる'),
                      onPressed: () {
                        Navigator.of(dialogContext).pop(); // このダイアログのみ閉じる
                      },
                    ),
                  ],
                ),
                // BottomPanel をダイアログ上部に表示（下部寄せ）
                if (_isTutorialActive)
                  Positioned(
                    bottom: isSearchRelated ? 300 : 300, // 下部から30ピクセル上げた位置に表示
                    left: 30,
                    right: 30,
                    child: _buildBottomPanel(
                      _isTutorialActive
                          ? _steps[_currentStep]['text']
                          : _guidance[_guidanceNum],
                    ),
                  ),
              ],
            );
          },
        );
      },
    ).then((_) {
      // ダイアログが閉じたあとに呼ばれる
      setState(() {
        _isDefinitionDialogOpen = false;
      });
    });
  }

  // 用語一覧タブのビルド
  Widget _buildGlossaryTab(GlossaryProvider provider) {
    if (provider.isLoading) {
      return Center(child: CircularProgressIndicator());
    }

    if (provider.hasError) {
      return Center(child: Text('エラーが発生しました: ${provider.error}'));
    }

    List<dynamic> filteredGlossary = provider.glossary;

    // フィルターを適用
    if (_selectedFilter == '索引' && _selectedIndex.isNotEmpty) {
      filteredGlossary = filteredGlossary.where((item) {
        String letter = item['letter'].toString();
        return letter == _selectedIndex;
      }).toList();
    } else if (_searchQuery.isNotEmpty) {
      filteredGlossary = filteredGlossary.where((item) {
        String term = item['term'].toString().toLowerCase();
        return term.contains(_searchQuery.toLowerCase());
      }).toList();
    }

    // 検索・フィルターの結果、用語が見つからなかった場合の処理
    if (filteredGlossary.isEmpty) {
      return Center(
        child: Text(
          '該当する用語はありません',
          style: TextStyle(fontSize: 18, color: Colors.grey),
        ),
      );
    }

    return Scrollbar(
      controller: _glossaryScrollController,
      thumbVisibility: true,
      interactive: true,
      thickness: 12.0,
      child: ListView.builder(
        key: PageStorageKey<String>('glossaryList'),
        controller: _glossaryScrollController,
        itemCount: filteredGlossary.length,
        itemBuilder: (BuildContext context, int index) {
          final item = filteredGlossary[index];
          final isFavorite = provider.favorites.contains(item['term']);
          return GestureDetector(
            key: index == 0 ? glossaryCardKey : null, // 用語カード用GlobalKey（代表例）
            onTap: () {
              gamificationService.addGlossaryView(item['number'].toString());
              _showDefinitionDialog(item['term'], item['definition']);
              if (_isTutorialActive &&
                  _steps[_currentStep]['requires'] == 'detail') {
                _conditionMet();
              }
            },
            child: SizedBox(
              height: 100, // 固定高さを設定
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 3,
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                color: Colors.blue[50], // カードの背景色を薄い青色に変更
                child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Row(
                    children: [
                      CircleAvatar(
                        child: Text(
                          item['letter'],
                          style: TextStyle(color: Colors.white),
                        ),
                        backgroundColor: Colors.teal, // アバターの背景色を変更
                      ),
                      SizedBox(width: 15),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // 用語テキストをスクロール可能にする
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                item['term'],
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 5),
                            // 定義テキストをスクロール可能にする
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                item['definition'],
                                style: TextStyle(
                                    fontSize: 12, color: Colors.grey[700]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      IconButton(
                        key: index == 0
                            ? favoriteIconKey
                            : null, // お気に入り用GlobalKey
                        icon: Icon(
                          isFavorite ? Icons.star : Icons.star_border,
                          color: isFavorite ? Colors.orange : Colors.grey,
                        ),
                        onPressed: () async {
                          if (isFavorite) {
                            await provider.removeFavorite(item['term']);
                          } else {
                            await provider.addFavorite(item['term']);
                          }
                          setState(() {});
                          if (_isTutorialActive &&
                              _steps[_currentStep]['requires'] == 'favorite') {
                            _conditionMet();
                          }
                        },
                      ),
                      SizedBox(width: 5),
                      IconButton(
                        key: index == 0
                            ? addToWordListButtonKey
                            : null, // 単語帳追加用GlobalKey
                        icon: Icon(Icons.bookmark_add, color: Colors.blue),
                        onPressed: () {
                          _showAddToWordListDialog(item['term']);
                        },
                        tooltip: '単語帳に追加',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  // お気に入りタブのビルド
  Widget _buildFavoritesTab(GlossaryProvider provider) {
    final favoritesSet = provider.favorites;
    final favoritesList = provider.glossary
        .where((item) => favoritesSet.contains(item['term']))
        .toList();

    if (favoritesList.isEmpty) {
      return Center(child: Text('お気に入りがありません'));
    }

    return Scrollbar(
      controller: _favoritesScrollController,
      thumbVisibility: true,
      interactive: true,
      thickness: 12.0,
      child: ListView.builder(
        key: PageStorageKey<String>('favoritesList'),
        controller: _favoritesScrollController,
        itemCount: favoritesList.length,
        itemBuilder: (BuildContext context, int index) {
          final item = favoritesList[index];
          bool isFavorite = provider.isFavorite(item['term']);
          return GestureDetector(
            onTap: () =>
                _showDefinitionDialog(item['term'], item['definition']),
            child: SizedBox(
              height: 100, // 固定高さを設定
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 3,
                margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                color: Colors.purple[50], // カードの背景色を薄い紫色に変更
                child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Row(
                    children: [
                      CircleAvatar(
                        child: Text(
                          item['letter'],
                          style: TextStyle(color: Colors.white),
                        ),
                        backgroundColor: Colors.deepPurple, // アバターの背景色を変更
                      ),
                      SizedBox(width: 15),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            // 用語テキストをスクロール可能にする
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                item['term'],
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                            SizedBox(height: 5),
                            // 定義テキストをスクロール可能にする
                            SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                item['definition'],
                                style: TextStyle(
                                    fontSize: 12, color: Colors.grey[700]),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      IconButton(
                        icon: Icon(
                          isFavorite ? Icons.star : Icons.star_border,
                          color: isFavorite ? Colors.orange : Colors.grey,
                        ),
                        onPressed: () async {
                          if (isFavorite) {
                            await provider.removeFavorite(item['term']);
                          } else {
                            await provider.addFavorite(item['term']);
                          }
                          setState(() {});
                        },
                      ),
                      SizedBox(width: 5),
                      IconButton(
                        icon: Icon(Icons.bookmark_add, color: Colors.blue),
                        onPressed: () {
                          _showAddToWordListDialog(item['term']);
                        },
                        tooltip: '単語帳に追加',
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  // 単語帳タブのビルド
  Widget _buildWordListsTab(GlossaryProvider provider) {
    return Column(
      children: [
        Expanded(
          child: provider.wordLists.isEmpty
              ? Center(child: Text('単語帳がありません'))
              : ListView.builder(
                  itemCount: provider.wordLists.length,
                  itemBuilder: (context, index) {
                    final wordList = provider.wordLists[index];
                    return Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      elevation: 3,
                      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      child: ListTile(
                        leading: Icon(Icons.book,
                            color: Colors.orangeAccent, size: 40),
                        title: Text(
                          wordList.name,
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text('${wordList.terms.length} 単語'),
                        trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            IconButton(
                              key: index == 0
                                  ? deleteIconKey
                                  : null, // 削除用GlobalKey
                              icon: Icon(Icons.delete, color: Colors.red),
                              onPressed: () {
                                _showDeleteWordListDialog(wordList);
                              },
                              tooltip: '単語帳を削除',
                            ),
                            IconButton(
                              key: index == 0
                                  ? renameIconKey
                                  : null, // 名前変更用GlobalKey
                              icon: Icon(Icons.edit, color: Colors.blueGrey),
                              onPressed: () {
                                _showRenameWordListDialog(wordList);
                              },
                              tooltip: '名前を変更',
                            ),
                          ],
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  WordListDetailScreen(wordList: wordList),
                            ),
                          );
                        },
                      ),
                    );
                  },
                ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton.icon(
            onPressed: _showCreateWordListDialog,
            icon: Icon(Icons.add),
            label: Text('単語帳を作成'),
            style: ElevatedButton.styleFrom(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              textStyle: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }

  // 単語帳に追加ダイアログ
  void _showAddToWordListDialog(String term) {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        bool createNew = false;
        String? selectedWordListId;
        String newWordListName = '';
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text('単語帳に追加'),
              content: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min, // サイズを最小に
                  children: [
                    // チェックボックスとテキストをRowで統合
                    Row(
                      children: [
                        Checkbox(
                          value: createNew,
                          onChanged: (bool? value) {
                            setState(() {
                              createNew = value ?? false;
                              if (createNew) {
                                selectedWordListId = null;
                              }
                            });
                          },
                        ),
                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              setState(() {
                                createNew = !createNew;
                                if (createNew) {
                                  selectedWordListId = null;
                                }
                              });
                            },
                            child: Text('新しい単語帳を作成する'),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    if (!createNew)
                      DropdownButtonFormField<String>(
                        decoration: InputDecoration(
                          labelText: '単語帳を選択',
                        ),
                        value: selectedWordListId,
                        onChanged: (String? newValue) {
                          setState(() {
                            selectedWordListId = newValue;
                          });
                        },
                        items: glossaryProvider.wordLists
                            .map<DropdownMenuItem<String>>((WordList wordList) {
                          return DropdownMenuItem<String>(
                            value: wordList.id,
                            child: Text(wordList.name),
                          );
                        }).toList(),
                      ),
                    if (createNew)
                      TextFormField(
                        maxLength: 20,
                        decoration: InputDecoration(
                          labelText: '新しい単語帳の名前',
                          hintText: '最大20文字',
                        ),
                        onChanged: (value) {
                          newWordListName = value;
                        },
                      ),
                  ],
                ),
              ),
              actions: <Widget>[
                TextButton(
                  child: Text('キャンセル'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                TextButton(
                  child: Text('追加'),
                  onPressed: () async {
                    if (createNew) {
                      if (newWordListName.trim().isEmpty) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text('単語帳名を入力してください')),
                        );
                        return;
                      }
                      await glossaryProvider
                          .addWordList(newWordListName.trim());
                      final newWordList = glossaryProvider.wordLists.last;
                      await glossaryProvider.addTermToWordList(
                          newWordList.id, term);
                    } else {
                      if (selectedWordListId == null) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(content: Text('単語帳を選択してください')),
                        );
                        return;
                      }
                      await glossaryProvider.addTermToWordList(
                          selectedWordListId!, term);
                    }
                    Navigator.of(context).pop();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text('単語帳に追加しました')),
                    );
                    if (_isTutorialActive &&
                        _steps[_currentStep]['requires'] == 'add_wordlist') {
                      _conditionMet();
                    }
                  },
                ),
              ],
            );
          },
        );
      },
    );
  }

  // 単語帳作成ダイアログ
  void _showCreateWordListDialog() {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    String wordListName = '';
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('新しい単語帳を作成'),
          content: TextField(
            maxLength: 20,
            onChanged: (value) {
              wordListName = value;
            },
            decoration: InputDecoration(
              hintText: '単語帳の名前 (最大20文字)',
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('キャンセル'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('作成'),
              onPressed: () async {
                if (wordListName.trim().isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('単語帳名を入力してください')),
                  );
                  return;
                }
                await glossaryProvider.addWordList(wordListName.trim());
                Navigator.of(context).pop();
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('単語帳を作成しました')),
                );
              },
            ),
          ],
        );
      },
    );
  }

  // 単語帳名変更ダイアログ
  void _showRenameWordListDialog(WordList wordList) {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    String newName = wordList.name;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('単語帳の名前を変更'),
          content: TextField(
            maxLength: 20,
            onChanged: (value) {
              newName = value;
            },
            controller: TextEditingController(text: wordList.name),
            decoration: InputDecoration(
              hintText: '新しい名前 (最大20文字)',
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('キャンセル'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('変更'),
              onPressed: () async {
                if (newName.trim().isEmpty) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('新しい名前を入力してください')),
                  );
                  return;
                }
                await glossaryProvider.renameWordList(
                    wordList.id, newName.trim());
                Navigator.of(context).pop();
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('単語帳の名前を変更しました')),
                );
                if (_isTutorialActive &&
                    _steps[_currentStep]['requires'] == 'rename') {
                  _conditionMet();
                }
              },
            ),
          ],
        );
      },
    );
  }

  // 単語帳削除ダイアログ
  void _showDeleteWordListDialog(WordList wordList) {
    final glossaryProvider =
        Provider.of<GlossaryProvider>(context, listen: false);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('単語帳を削除'),
          content: Text('「${wordList.name}」を本当に削除しますか？'),
          actions: <Widget>[
            TextButton(
              child: Text('キャンセル'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('削除', style: TextStyle(color: Colors.red)),
              onPressed: () async {
                await glossaryProvider.deleteWordList(wordList.id);
                Navigator.of(context).pop();
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(content: Text('単語帳を削除しました')),
                );
                if (_isTutorialActive &&
                    _steps[_currentStep]['requires'] == 'delete') {
                  _conditionMet();
                }
              },
            ),
          ],
        );
      },
    );
  }

  // ビルドメソッド
  @override
  Widget build(BuildContext context) {
    final glossaryProvider = Provider.of<GlossaryProvider>(context);

    return Stack(
      children: [
        Scaffold(
          appBar: AppBar(
            title: Text('用語集', style: TextStyle(color: Colors.black)),
            backgroundColor: Colors.white,
            centerTitle: true,
            iconTheme: IconThemeData(color: Colors.black),
            actions: [
              IconButton(
                icon: Icon(Icons.help_outline,
                    color: _isTutorialActive ? Colors.grey : Colors.black),
                onPressed: () {
                  if (!_isTutorialActive) {
                    // オンボーディング（ガイダンス）を開始
                    WidgetsBinding.instance.addPostFrameCallback((_) {
                      setState(() {
                        _isOnboadingActive = true;
                        _guidanceNum = 0;
                      });
                      _updateGuidanceOverlayPosition();
                    });

                    _loadProgress();
                  }
                },
              ),
              IconButton(
                onPressed: () => {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('ログアウト'),
                        content: Text('本当にログアウトしますか？'),
                        actions: <Widget>[
                          TextButton(
                            child: Text('キャンセル'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                          TextButton(
                            child: Text('ログアウト'),
                            onPressed: () {
                              Navigator.of(context).pop();
                              AuthService.logout(context);
                            },
                          ),
                        ],
                      );
                    },
                  ),
                },
                icon: Icon(
                  Icons.logout,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          body: Consumer<GlossaryProvider>(
            builder: (context, provider, child) {
              return Column(
                children: <Widget>[
                  // タブバー
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Material(
                      color: Colors.white,
                      child: TabBar(
                        controller: _tabController,
                        onTap: (int index) {
                          // 3番目のタブは index=2
                          if (index == 2) {
                            if (_isTutorialActive &&
                                _steps[_currentStep]['requires'] ==
                                    'wordlist_tab') {
                              _conditionMet();
                            }
                          }
                        },
                        labelColor: Colors.black,
                        unselectedLabelColor: Colors.grey,
                        indicatorColor: Theme.of(context).colorScheme.secondary,
                        tabs: [
                          Tab(text: '用語一覧'),
                          Tab(text: 'お気に入り'),
                          Tab(
                            child: Container(
                              key: wordListTabKey, // ここでGlobalKeyを付与
                              alignment: Alignment.center,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 4.0),
                              child: Text('単語帳'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  // ドロップダウンフィルター
                  if (_tabController.index == 0) // 用語一覧タブのみフィルターを表示
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: DropdownButton<String>(
                        key: filterDropdownKey, // ドロップダウン用GlobalKey
                        value: _selectedFilter,
                        isExpanded: true,
                        onChanged: (String? newValue) {
                          setState(() {
                            _selectedFilter = newValue!;
                            _searchQuery = '';
                            _searchController.clear();

                            if (_selectedFilter == '索引') {
                              // 索引要素が生成されていない場合は生成
                              if (_indexElements.isEmpty &&
                                  !provider.isLoading) {
                                _buildIndexElements(provider);
                              }
                              // 選択された索引が未設定の場合のみ先頭の要素を選択
                              if (_indexElements.isNotEmpty &&
                                  _selectedIndex.isEmpty) {
                                _selectedIndex = _indexElements.first;
                              }
                              if (_isTutorialActive &&
                                  _steps[_currentStep]['requires'] ==
                                      'filter') {
                                _conditionMet();
                              }
                            }
                          });
                        },
                        items: _filterOptions
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  // 検索バーまたは索引バー
                  if (_tabController.index == 0) // 用語一覧タブのみバーを表示
                    _buildIndexOrSearchBar(provider),
                  Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        _buildGlossaryTab(provider),
                        _buildFavoritesTab(provider),
                        _buildWordListsTab(provider),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ),
        // チュートリアル／オンボーディングオーバーレイ（ガイダンス部分）
        if (_isOnboadingActive && !_isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isOnboadingActive && !_isTutorialActive)
          GestureDetector(
            onTap: _guidanceStep,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Stack(
                children: [
                  _buildBottomPanel(_guidance[_guidanceNum]),
                  if (_currentStep < _steps.length - 1)
                    Positioned(
                      right: 0,
                      top: 0,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          if (_isOnboadingActive) {
                            _endGuidance();
                          } else if (_isTutorialActive) {
                            _endTutorial();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                            side: BorderSide(color: Colors.black, width: 2.5),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/Skip.png',
                          width: 25,
                          height: 25,
                        ),
                        label: Text(
                          "スキップ",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
        if (_isOnboadingActive && _isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isShowDialog) Center(child: _buildTutorialDialog()),
        // 操作説明（チュートリアル）パネル
        if (_isTutorialActive && !_isOnboadingActive)
          GestureDetector(
            onTap: _checkAndProceed,
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Stack(
                children: [
                  _buildBottomPanel(_steps[_currentStep]['text']),
                  if (_currentStep < _steps.length - 1)
                    Positioned(
                      right: 0,
                      top: 0,
                      child: ElevatedButton.icon(
                        onPressed: () {
                          if (_isOnboadingActive) {
                            _endGuidance();
                          } else if (_isTutorialActive) {
                            _endTutorial();
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          padding:
                              EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15),
                            side: BorderSide(color: Colors.black, width: 2.5),
                          ),
                        ),
                        icon: Image.asset(
                          'assets/Skip.png',
                          width: 25,
                          height: 25,
                        ),
                        label: Text(
                          "スキップ",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),

        // FABタップ促進のアニメーション
        // ヘルプオーバーレイ
        if (showHelp)
          HelpOverlay(
            existingWidget: Container(),
            helpMessages: helpMessages,
            backgroundImages: backgroundImages,
            onSkip: () {
              setState(() {
                showHelp = false;
              });
            },
            messageAlignment: messageAlignment,
            margin: margin,
            rectangleSteps: rectangleSteps,
          ),
        // ガイダンス中なら、ガイダンス用の青い枠を表示する
        if (_isOnboadingActive && !_isShowDialog)
          BlueSquareOverlay(
            top: _guidanceOverlayTop,
            left: _guidanceOverlayLeft,
            width: _guidanceOverlayWidth,
            height: _guidanceOverlayHeight,
            flashing: true,
            isVisible: true,
          ),

        // 操作説明用の青い枠（動的に対象ウィジェットの位置を取得）
        if (_isTutorialActive && _isVisible && !_isShowDialog)
          BlueSquareOverlay(
            top: _tutorialOverlayTop,
            left: _tutorialOverlayLeft,
            width: _tutorialOverlayWidth,
            height: _tutorialOverlayHeight,
            flashing: true,
            isVisible: _isVisible,
          ),
      ],
    );
  }

  Widget _buildBackgroundOverlay() {
    return Container(
      color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Widget _buildBottomPanel(String text) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.lightBlue, width: 3),
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 16, color: Colors.black, fontWeight: FontWeight.w600),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTutorialDialog() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 252, 246, 251),
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black26, blurRadius: 10.0, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '操作説明を開始しますか？',
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.black87,
                fontWeight: FontWeight.normal),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  _startTutorial();
                  _endGuidance();
                },
                child: Text(
                  'はい',
                  style: TextStyle(color: Colors.purple, fontSize: 16.0),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _noTap();
                  _endGuidance();
                },
                child: Text(
                  'いいえ',
                  style: TextStyle(color: Colors.purple, fontSize: 16.0),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
