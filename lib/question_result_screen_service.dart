import 'dart:convert';
import 'package:http/http.dart' as http;
import 'auth_service.dart';
import 'dart:io';

class QuestionResultScreenService {
  final List<Map<String, dynamic>> questions;
  List<String> answers;
  final authService = AuthService();

  QuestionResultScreenService({required this.questions, required this.answers});

  List<Map<String, dynamic>> getQuestionAnswerPairs() {
    return List.generate(questions.length, (index) {
      return {
        "question": questions[index]['questionText'] ?? '',
        "answer": answers[index],
        "modelAnswer": questions[index]['modelAnswer'] ?? '',
      };
    });
  }

  Future<String> sendQuestionAnswerPairs(http.Client client) async {
    try {
      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        throw Exception("認証トークンが無効です。再ログインしてください。"); // ここで処理を終了
      }
      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp//score'),
        body: utf8.encode(jsonEncode({
          'question_answer_pairs': getQuestionAnswerPairs(),
        })),
        headers: {"Content-Type": "application/json"},
      );
      if (response.statusCode == 200) {
        return utf8.decode(response.bodyBytes);
      } else {
        throw Exception('Failed to load post');
      }
    } on SocketException {
      throw Exception('ネットワークエラーが発生しました。インターネット接続を確認してください。');
    } on FormatException {
      throw Exception('サーバーの応答の解析に失敗しました。');
    } catch (e) {
      throw Exception('Error occurred while sending data: $e');
    }
  }
}
