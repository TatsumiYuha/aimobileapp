import 'package:shared_preferences/shared_preferences.dart';

class tutorialProgressManager {
  static const String _progressKey = "tutorialProgress";
  static const String _tutorialBoolKey = "tutorial_bool_key";
  static const String _onboadingBoolKey = "onboading_bool_key";
  static const String _showDialogBoolKey = "showDialog_bool_key";
  static const String _isFirstBoolKey = "isFirst_bool_key";
  int debugNum = 0;
  static SharedPreferences? _prefs;

  /*  // 非同期で初期化するメソッド
  static Future<void> initialize() async {
    if (_prefs == null) {
      _prefs = await SharedPreferences.getInstance();
    }
  }

  // SharedPreferencesインスタンスを取得する
  static SharedPreferences get instance {
    if (_prefs == null) {
      throw Exception(
          'SharedPreferencesService is not initialized. Call initialize() first.');
    }
    return _prefs!;
  }

  Future<void> saveProgress(String key, int step) async {
    await instance.setInt(key, step);
  }

  int? getProgress(String key) {
    return instance.getInt(key);
  }

  Future<void> resetProgress() async {
    await instance.remove(_progressKey);
  }

  Future<void> boolResets() async {
    instance.remove(_tutorialBoolKey);
    instance.remove(_onboadingBoolKey);
    instance.remove(_showDialogBoolKey);
    instance.remove(_isFirstBoolKey);
  }

  Future<void> saveTutorialBoolStatus(bool status) async {
    await instance.setBool(_tutorialBoolKey, status);
  }

  Future<void> saveOnboadingBoolStatus(bool status) async {
    await instance.setBool(_onboadingBoolKey, status);
  }

  Future<void> saveShowDialogBoolStatus(bool status) async {
    await instance.setBool(_showDialogBoolKey, status);
  }

  Future<void> saveString(String key, String value) async {
    await instance.setString(key, value);
  }

  Future<void> saveIsFirstBoolStatus(bool status) async {
    await instance.setBool(_isFirstBoolKey, status);
  }

  // bool値を読み込むメソッド（デフォルトはfalse）
  bool loadTutorialBoolStatus() {
    return instance.getBool(_tutorialBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  bool loadOnboadingBoolStatus() {
    return instance.getBool(_onboadingBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  bool loadShowDialogBoolStatus() {
    return instance.getBool(_showDialogBoolKey) ??
        false; // 値が存在しない場合は false を返す
  }

  bool loadIsFirstBoolStatus() {
    return instance.getBool(_isFirstBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  Future<bool> isFirstVisit(String screenKey) async {
    final prefs = await SharedPreferences.getInstance();
    final hasVisited = prefs.getBool(screenKey) ?? false;

    if (!hasVisited) {
      await prefs.setBool(screenKey, true);
    }
    return !hasVisited;
  }

  Future<void> resetAllVisits() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  String? getString(String key) {
    return instance.getString(key); // null の場合もあるため、nullable 型で扱う
  }
} */

  //currentStep 保存用
  Future<void> saveProgress(String key, int step) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(key, step);
  }

//currentStep入手用
  Future<int> getProgress(String key) async {
    final prefs = await SharedPreferences.getInstance();
    debugNum = prefs.getInt(key) ?? 0;
    print(debugNum);
    return debugNum;

    //return prefs.getInt(key) ?? 0;
  }

//currentStepリセット用
  Future<void> resetProgress() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(_progressKey);
  }

  Future<void> boolResets() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(_tutorialBoolKey);
    prefs.remove(_onboadingBoolKey);
    prefs.remove(_showDialogBoolKey);
    prefs.remove(_isFirstBoolKey);
  }

  Future<void> saveTutorialBoolStatus(bool status) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_tutorialBoolKey, status);
  }

  Future<void> saveOnboadingBoolStatus(bool status) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_onboadingBoolKey, status);
  }

  Future<void> saveShowDialogBoolStatus(bool status) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_showDialogBoolKey, status);
  }

  Future<void> saveString(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  Future<void> saveIsFirstBoolStatus(bool status) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(_isFirstBoolKey, status);
  }

  // bool値を読み込むメソッド（デフォルトはfalse）
  Future<bool> loadTutorialBoolStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_tutorialBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  Future<bool> loadOnboadingBoolStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_onboadingBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  Future<bool> loadShowDialogBoolStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_showDialogBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  Future<bool> loadIsFirstBoolStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isFirstBoolKey) ?? false; // 値が存在しない場合は false を返す
  }

  Future<bool> isFirstVisit(String screenKey) async {
    final prefs = await SharedPreferences.getInstance();
    final hasVisited = prefs.getBool(screenKey) ?? false;

    if (!hasVisited) {
      await prefs.setBool(screenKey, true);
    }
    return !hasVisited;
  }

  Future<void> resetAllVisits() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  Future<String?> getString(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(key); // null の場合もあるため、nullable 型で扱う
  }
}
