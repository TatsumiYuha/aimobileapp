import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/level_data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'auth_service.dart';
import 'question_completion_screen.dart';
import 'package:flutter_code_editor/flutter_code_editor.dart';
import 'package:highlight/languages/java.dart'; // 言語定義をインポート
import 'package:flutter_highlight/themes/monokai-sublime.dart';
import 'question_selection_screen_widget.dart';
import 'question_programming_screen_service.dart';
import 'question_debugging_screen_widget.dart';
import 'question_selection_screen_service.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'interruption_data_controller.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart'; // sliding_up_panelをインポート

// チュートリアル関連補助ファイル（既存の実装を利用）
import 'tutorial_progress_manager.dart';
import 'blue_square_overlay.dart';
import 'help_overlay_service.dart';
import 'animation_build_display.dart';

class QuestionProgrammingScreenWidget extends StatefulWidget {
  final Map<String, dynamic> questionData;
  final Map<String, dynamic> interruption;

  final bool resumeFromInterruption;

  QuestionProgrammingScreenWidget({
    required this.questionData,
    required this.interruption,
    required this.resumeFromInterruption,
  });

  @override
  _QuestionProgrammingScreenWidgetState createState() =>
      _QuestionProgrammingScreenWidgetState();
}

class _QuestionProgrammingScreenWidgetState
    extends State<QuestionProgrammingScreenWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _opacityAnimation;

  /// CodeField
  late CodeController _codeController;
  final FocusNode _codeFocusNode = FocusNode();
  final double _baseFontSize = 14.0;
  double _scale = 1.0;
  final double _minScale = 0.5;
  final double _maxScale = 3.0;

  /// 質問関連
  String mainTask = "";
  List<Map<String, dynamic>> subTasks = [];
  int currentSubTaskIndex = 1; // サブタスクは1から始める
  double progress = 0.0; // 進捗率

  /// 回答関連
  late String _initialCode;
  bool _judge = false;
  String _comment = '';
  String _compilation = '';
  String _execution = '';
  String _subtaskCheck = '';
  bool _showGradingResult = false;
  bool _isGradingInProgress = false;
  bool _animateGradingResult = false;
  bool _isButtonDisabled = false;

  // サブタスクごとの「開始時に記録したコード」を保持するMap
  Map<int, String> _subTaskCodeState = {};

  /// 正答表示状態
  bool _answerUnlocked = false; // 正解が表示可能状態であるとき true
  bool _hasShownAnswer = false; // 正解を表示しているとき true

  String _previousText = ''; // 前回のテキスト状態を保持する変数
  bool _isHandlingAutoPair = false; // 再帰呼び出し防止用フラグ

  /// Services
  final QuestionProgrammingScreenService _service =
      QuestionProgrammingScreenService();
  final QuestionSelectionScreenService _questionSelectionScreenService =
      QuestionSelectionScreenService();
  final gamificationService = GamificationService();

  final PanelController _panelController = PanelController();

  final tutorialProgressManager _tutorial = tutorialProgressManager();
  bool _isTutorialActive = false; // 操作説明（チュートリアル）が実行中か
  bool _isOnboadingActive = false; // オンボーディング中か
  bool _isShowDialog = false; // 「操作説明を開始しますか？」のダイアログ表示
  bool _isCondition = false; // 操作が完了したかの一時フラグ
  bool showHelp = false; // ヘルプオーバーレイ表示用
  bool _isVisible = false; // 操作説明での青い枠表示制御

  int _currentStep = 0; // 操作説明（チュートリアル）進行管理
  int _guidanceNum = 0; // ガイダンス進行管理（オンボーディング用）

  // ガイダンス用のメッセージ（オンボーディング初期表示）
  final List<String> _guidance = [
    "ようこそ！この画面ではプログラミングチャレンジに挑戦できます。",
    "上部のコードエディタで自由にコードを記述してください。",
    "画面左上の戻るボタンから、いつでも問題一覧に戻ることができます。",
    "下部のパネルをスワイプすると、回答送信やズーム調整などの補助機能が表示されます。",
    "進捗バーで、あなたのチャレンジの進行状況を確認できます。",
    "採点結果をタップすると、詳細なフィードバックが表示されます。",
    "それでは、操作説明を開始しましょう！",
  ];

  // 各操作説明（チュートリアル）用のステップ
  final List<Map<String, dynamic>> _steps = [
    {'text': 'プログラミングチャレンジの操作説明を開始します。', 'requires': null},
    {'text': 'まず、上部のコードエディタにコードを記入してみましょう。', 'requires': 'editor'},
    {'text': 'キーボード上の「回答を送信」ボタンをタップして、採点を実行してください。', 'requires': 'submit'},
    {'text': 'もし入力内容を初期状態に戻したい場合は、リセットボタンを押してください。', 'requires': 'reset'},
    {'text': '下部のパネルを上にスライドしてみましょう', 'requires': 'panel'},
    {
      'text': 'ズームスライダーでコード表示の拡大・縮小ができます。リセットボタンで標準倍率に戻せます。',
      'requires': 'zoom'
    },
    {'text': '上下にスワイプすることで各種機能を確認できます。', 'requires': null},
    {'text': '採点結果をタップすると、詳細なフィードバックが表示されます。', 'requires': 'grading'},
    {'text': 'これで操作説明は終了です。チャレンジに挑戦してみましょう！', 'requires': null},
  ];

  // ヘルプオーバーレイ用メッセージなど（そのまま）
  final List<String> helpMessages = [
    "検索バー: チャットルームを検索できます",
    "並び替えオプション: リストの順序を変更できます",
    "リスト: 各チャットルームの詳細が確認できます",
    "スワイプ: 削除・編集・お気に入り設定が可能です",
    "ドラッグ: チャットルームの並び替えができます",
    "FAB: 新しいチャットルームを作成します",
  ];

  final List<String> backgroundImages = [
    'assets/Help_search.png',
    'assets/Help_sort.png',
    'assets/Help_list.png',
    'assets/Help_slidable.png',
    'assets/Help_reorder.png',
    'assets/Help_fab.png',
  ];

  final List<Alignment> messageAlignment = [
    Alignment.topCenter,
    Alignment.topCenter,
    Alignment.center,
    Alignment.center,
    Alignment.center,
    Alignment.bottomCenter,
  ];

  final List<EdgeInsets> margin = [
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(top: 20),
    EdgeInsets.only(bottom: 20),
  ];

  // ヘルプ用：ハイライト矩形（例示用の固定値）
  final List<Rect> rectangleSteps = [
    Rect.fromLTWH(20, 100, 360, 50), // 検索バー
    Rect.fromLTWH(20, 160, 360, 50), // 並び替えドロップダウン
    Rect.fromLTWH(20, 220, 360, 400), // チャットルームリスト
    Rect.fromLTWH(300, 600, 60, 60), // FAB（新規作成ボタン）
    Rect.fromLTWH(0, 0, 0, 0), // （オプション）
    Rect.fromLTWH(0, 0, 0, 0), // （オプション）
  ];

  //////////// 対象ウィジェットのGlobalKey ////////////
  // コードエディタ用（例：エディタ部分全体を対象）
  final GlobalKey codeEditorKey = GlobalKey();

// 戻るボタン用（AppBarの先頭に配置される戻るボタン）
  final GlobalKey backButtonKey = GlobalKey();

// 下部パネル用（スライディングアップパネル全体）
  final GlobalKey bottomPanelKey = GlobalKey();

// 進捗バー用（チャレンジ進行状況を示すバー）
  final GlobalKey progressBarKey = GlobalKey();

// 採点結果表示用（採点結果のフィードバックエリア）
  final GlobalKey gradingResultKey = GlobalKey();

// キーボード上の「回答を送信」ボタン用
  final GlobalKey submitButtonKey = GlobalKey();

// 回答リセットボタン用
  final GlobalKey resetButtonKey = GlobalKey();

// ズームスライダー用（コードの拡大縮小を操作するスライダー）
  final GlobalKey zoomSliderKey = GlobalKey();

// 補助メニューとしてのパネルスワイプ領域用（必要に応じて）
  final GlobalKey panelSwipeKey = GlobalKey();

  //////////// ここまで

  //////////// ガイダンス用オーバーレイ変数（既存） ////////////
  double _guidanceOverlayTop = 0.0;
  double _guidanceOverlayLeft = 0.0;
  double _guidanceOverlayWidth = 0.0;
  double _guidanceOverlayHeight = 0.0;
  //////////// ここまで

  //////////// 操作説明用オーバーレイ変数（新規） ////////////
  double _tutorialOverlayTop = 0.0;
  double _tutorialOverlayLeft = 0.0;
  double _tutorialOverlayWidth = 0.0;
  double _tutorialOverlayHeight = 0.0;
  //////////// ここまで

// ガイダンス用の対象ウィジェットのGlobalKeyを返す
  GlobalKey? _getGuidanceTargetKey() {
    switch (_guidanceNum) {
      case 1:
        // 「上部のコードエディタで自由にコードを記入してください。」
        return codeEditorKey;
      case 2:
        // 「画面左上の戻るボタンから、いつでも問題一覧に戻ることができます。」
        return backButtonKey;
      case 3:
        // 「下部のパネルをスワイプすると、回答送信やズーム調整などの補助機能が表示されます。」
        return bottomPanelKey;
      case 4:
        // 「進捗バーで、あなたのチャレンジの進行状況を確認できます。」
        return progressBarKey;
      case 5:
        // 「採点結果をタップすると、詳細なフィードバックが表示されます。」
        return gradingResultKey;
      default:
        return null;
    }
  }

  // ガイダンス用の対象ウィジェットの位置とサイズを更新する（既存）
  void _updateGuidanceOverlayPosition() {
    GlobalKey? targetKey = _getGuidanceTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      // 特にガイダンスステップ4（ドラッグの説明）の場合、先頭アイテムの描画が未完了なら遅延させる

      Future.delayed(Duration(milliseconds: 100), () {
        _updateGuidanceOverlayPosition();
      });

      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _guidanceOverlayTop = globalPosition.dy;
      _guidanceOverlayLeft = globalPosition.dx;
      _guidanceOverlayWidth = size.width;
      _guidanceOverlayHeight = size.height;
    });
  }

// 操作説明用の対象ウィジェットのGlobalKeyを返す関数
  GlobalKey? _getTutorialTargetKey() {
    final String? req = _steps[_currentStep]['requires'];
    if (req == null) return null;
    switch (req) {
      case 'editor':
        return codeEditorKey;
      case 'submit':
        return submitButtonKey;
      case 'reset':
        return resetButtonKey;
      case 'zoom':
        return zoomSliderKey;
      case 'panel':
        return bottomPanelKey;
      case 'grading':
        return gradingResultKey;
      default:
        return null;
    }
  }

  // 操作説明用の対象ウィジェットの位置とサイズを更新する（新規）
  void _updateTutorialOverlayPosition() {
    GlobalKey? targetKey = _getTutorialTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      if (_steps[_currentStep]['requires'] == 'slidable') {
        Future.delayed(Duration(milliseconds: 100), () {
          _updateTutorialOverlayPosition();
        });
      }
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _tutorialOverlayTop = globalPosition.dy;
      _tutorialOverlayLeft = globalPosition.dx;
      _tutorialOverlayWidth = size.width;
      _tutorialOverlayHeight = size.height;
    });
  }
  //////////// ここまで操作説明用

  @override
  void initState() {
    super.initState();

    // ====== (1) 質問データの設定 ======
    subTasks = List<Map<String, dynamic>>.from(
      widget.questionData['subTaskList'] ?? [],
    );
    mainTask = widget.questionData['mainTask'] ?? "";

    // modelAnswerの初期セット
    if (widget.questionData['modelAnswer'] != null &&
        widget.questionData['modelAnswer'] is List &&
        widget.questionData['modelAnswer'].isNotEmpty) {
      _initialCode = widget.questionData['modelAnswer'][0]
          .replaceAll('\\n', '\n')
          .replaceAll('\\', '');
    } else {
      _initialCode = "";
    }

    // サブタスク1の初期コードを保存
    _subTaskCodeState[1] = _initialCode;

    // ====== (2) AnimationController ======
    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);
    _opacityAnimation = Tween(begin: 0.2, end: 0.7).animate(_controller);

    // ====== (3) CodeController ======
    _codeController = CodeController(
      language: java,
      text: _initialCode,
    );
    _codeController.popupController.enabled = false;
    _codeController.addListener(_onTextChanged);

    // ====== (4) 中断データの適用 ======
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _applyInterruptions();
      _service.saveUserCode(
          _codeController.text, widget.questionData['questionId']);
    });

    // ====== (5) 画面を開いた段階で前回の採点結果があれば読み込む ======
    if (widget.resumeFromInterruption) {
      // 「中断データから再開する」場合のみ、過去の採点結果を読み込む
      _loadPreviousGradingResult();
    } else {
      final questionId = widget.questionData['questionId'] as int;
      _service.markGradingResultAsRead(questionId);
    }

    // ====== (6) 初期表示後、ダイアログ表示 ======
    WidgetsBinding.instance.addPostFrameCallback((_) => _showProgressDialog());

    // デバッグ用・初回起動時として強制的にオンボーディング（ガイダンス）を開始
    _checkAndStartTutorial();
    _loadProgress();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // キーボードが閉じた（viewInsets.bottomが0）場合に座標更新をスケジュールする
    if (MediaQuery.of(context).viewInsets.bottom == 0) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateTutorialOverlayPosition();
      });
    }
  }

  Future<void> _checkAndStartTutorial() async {
    // ここでは画面ごとに一意なキーを用いる（例：ChatScreenWidget の場合はクラス名や固有のID）
    // 複数の画面で管理する場合、各画面で異なるキーを使用してください。
    final String tutorialKey = 'tutorialShown_${this.runtimeType}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstTime = prefs.getBool(tutorialKey) ?? true;
    if (isFirstTime) {
      // 初回の場合のみチュートリアル／ガイダンスを発火させる
      setState(() {
        _isOnboadingActive = true;
        _guidanceNum = 0;
      });
      // 画面描画後にオーバーレイ位置を更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
      // 一度発火したのでフラグを更新（2回目以降は発火しない）
      await prefs.setBool(tutorialKey, false);
    }
  }

  Future<void> _loadProgress() async {
    int progress = await _tutorial.getProgress('currentStep');
    if (progress < _steps.length) {
      setState(() {
        _currentStep = progress;
      });
    }
  }

  Future<void> _saveProgress(int step) async {
    await _tutorial.saveProgress('currentStep', step);
  }

  Future<void> _resetProgress() async {
    setState(() {
      _currentStep = 0;
    });
    await _tutorial.resetProgress();
  }

  Future<void> _saveTutorialStatus() async {
    await _tutorial.saveTutorialBoolStatus(_isTutorialActive);
    await _tutorial.saveOnboadingBoolStatus(_isOnboadingActive);
    await _tutorial.saveShowDialogBoolStatus(_isShowDialog);
  }

  void _getTutorial() async {
    bool futureTutorialActive = await _tutorial.loadTutorialBoolStatus();
    bool futureOnboardingActive = await _tutorial.loadOnboadingBoolStatus();
    bool futureShowDialogActive = await _tutorial.loadShowDialogBoolStatus();
    setState(() {
      _isTutorialActive = futureTutorialActive;
      _isOnboadingActive = futureOnboardingActive;
      _isShowDialog = futureShowDialogActive;
    });
  }

  void _startTutorial() {
    setState(() {
      _currentStep = 0;
      _isTutorialActive = true;
      _isOnboadingActive = false;
      _isShowDialog = false;
    });
    _saveProgress(0);
    _saveTutorialStatus();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _endTutorial() {
    setState(() {
      _isTutorialActive = false;
      _isOnboadingActive = false;
      _isShowDialog = false;
      _currentStep = 0;
      _isVisible = false;
    });
    _resetProgress();
    _saveTutorialStatus();
  }

  void _guidanceStep() {
    if (_guidanceNum < _guidance.length - 1) {
      setState(() {
        _guidanceNum++;
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
    } else {
      setState(() {
        _isShowDialog = true;
        _isOnboadingActive = true;
      });
    }
  }

  void _noTap() {
    setState(() {
      _currentStep = _steps.length - 1;
      _isTutorialActive = true;
    });
  }

  void _endGuidance() {
    setState(() {
      _isShowDialog = false;
      _isOnboadingActive = false;
    });
  }

  void _nextStep() {
    if (_currentStep < _steps.length - 1) {
      setState(() {
        _currentStep++;
        _isCondition = false;
      });
      _saveProgress(_currentStep);
    } else {
      _endTutorial();
      _saveProgress(_currentStep);
    }
    _checkVisible();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      _updateTutorialOverlayPosition();
    });
  }

  void _checkAndProceed() {
    final requires = _steps[_currentStep]['requires'];
    if (requires == null || _isCondition) {
      _nextStep();
    }
  }

  void _conditionMet() {
    if (_isTutorialActive) {
      setState(() {
        _isCondition = true;
      });
      _nextStep();
    }
  }

  // 従来の青い枠表示制御（操作説明用）
  void _checkVisible() {
    if (_steps[_currentStep]['requires'] == null) {
      setState(() {
        _isVisible = false;
      });
    } else if (_currentStep >= 1 && _currentStep <= 10) {
      setState(() {
        _isVisible = true;
      });
    } else {
      setState(() {
        _isVisible = false;
      });
    }
  }
  //////////////////////////////////////////////////////////////////////

  /// (A) 前回保存された採点結果を読み込む & 未読→既読化
  Future<void> _loadPreviousGradingResult() async {
    final questionId = widget.questionData['questionId'] as int;
    final savedJson = await _service.getGradingResult(questionId);

    if (savedJson != null) {
      final Map<String, dynamic> responseJson = jsonDecode(savedJson);

      final String compilationString = _buildCompilationResult(responseJson);
      final String executionString = _buildExecutionResult(responseJson);
      final String subtaskCheckString = _buildSubtaskCheckResult(responseJson);

      // 画面が破棄されていないか確認
      if (!mounted) return;
      setState(() {
        _compilation = compilationString;
        _execution = executionString;
        _subtaskCheck = subtaskCheckString;
        _showGradingResult = true; // 前回の結果を即表示
      });

      // 前回の結果を見せたので未読→既読化
      await _service.markGradingResultAsRead(questionId);
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _codeController.removeListener(_onTextChanged);
    _codeController.dispose();
    _codeFocusNode.dispose();
    super.dispose();
  }

  /// (B) 中断データを適用
  void _applyInterruptions() {
    if (widget.interruption.isNotEmpty && !_hasShownAnswer) {
      _codeController.text = widget.interruption['userCode'];
      setState(() {
        progress = widget.interruption['progress'];
        currentSubTaskIndex = widget.interruption['currentSubTaskIndex'];
        _subTaskCodeState[currentSubTaskIndex] =
            widget.interruption['userCode'];
      });
    }
  }

  /// (C) 回答状況リセット
  void _resetAnswerStatus() {
    setState(() {
      String codeAtSubtaskStart;
      if (_subTaskCodeState.containsKey(currentSubTaskIndex)) {
        codeAtSubtaskStart = _subTaskCodeState[currentSubTaskIndex]!;
      } else {
        codeAtSubtaskStart = _initialCode;
        _subTaskCodeState[currentSubTaskIndex] = _initialCode;
      }
      _codeController.text = codeAtSubtaskStart;
    });
  }

  /// (D) キーボードアクション設定
  KeyboardActionsConfig _buildConfig(BuildContext context) {
    return KeyboardActionsConfig(
      keyboardActionsPlatform: KeyboardActionsPlatform.ALL,
      actions: [
        KeyboardActionsItem(
          focusNode: _codeFocusNode,
          toolbarButtons: [
            // 回答を送信ボタン
            if (progress < 1.0)
              (node) {
                return GestureDetector(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                    _updateCode();
                    if (_isTutorialActive &&
                        _steps[_currentStep]['requires'] == 'submit') {
                      _conditionMet();
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "回答を送信",
                      style: TextStyle(
                          color: Colors.blue, fontWeight: FontWeight.bold),
                    ),
                  ),
                );
              },
            // キーボード閉じる
            (node) {
              return GestureDetector(
                onTap: () => FocusScope.of(context).unfocus(),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "閉じる",
                    style: TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                ),
              );
            },
          ],
        ),
      ],
    );
  }

  /// (E) クォーテーション修正 && 括弧自動ペアリング機能
  void _onTextChanged() {
    if (_isHandlingAutoPair) return;
    String currentText = _codeController.text;
    int oldTextLength = _previousText.length;

    // クォーテーション修正
    String correctedText = currentText
        .replaceAll('′', '\'')
        .replaceAll('′', '\'')
        .replaceAll('‘', '\'')
        .replaceAll('’', '\'')
        .replaceAll('“', '"')
        .replaceAll('”', '"');

    if (currentText != correctedText) {
      _codeController.value = _codeController.value.copyWith(
        text: correctedText,
        selection: TextSelection.fromPosition(
          TextPosition(offset: correctedText.length),
        ),
      );
      _previousText = correctedText;
      return;
    }

    // 自動ペアリング
    final selection = _codeController.selection;
    if (currentText.length == _previousText.length + 1 &&
        selection.isCollapsed) {
      final insertIndex = selection.start - 1;
      if (insertIndex >= 0 && insertIndex < currentText.length) {
        final insertedChar = currentText[insertIndex];
        String? matching;
        if (insertedChar == '(') {
          matching = ')';
        } else if (insertedChar == '{') {
          matching = '}';
        } else if (insertedChar == '[') {
          matching = ']';
        }

        if (matching != null) {
          if (selection.start < currentText.length &&
              currentText[selection.start] == matching) {
            _previousText = currentText;
            return;
          }
          _isHandlingAutoPair = true;
          final newText = currentText.substring(0, selection.start) +
              matching +
              currentText.substring(selection.start);
          final newSelection = TextSelection.collapsed(offset: selection.start);
          _codeController.value = TextEditingValue(
            text: newText,
            selection: newSelection,
          );
          _previousText = newText;
          _isHandlingAutoPair = false;
          return;
        }
      }
    }
    _previousText = currentText;

    if (_isTutorialActive &&
        _steps[_currentStep]['requires'] == 'editor' &&
        currentText.length > oldTextLength) {
      _conditionMet();
    }
  }

  /// (F) 進捗ダイアログ
  void _showProgressDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("メインタスク", style: TextStyle(fontWeight: FontWeight.bold)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(mainTask, style: TextStyle(color: Colors.blueGrey)),
                SizedBox(height: 10),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('閉じる', style: TextStyle(color: Colors.blue)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  /// (G) 回答送信（正解表示中も可能）
  Future<void> _updateCode() async {
    // (1) ボタン無効 & 採点中表示
    if (!mounted) return;
    setState(() {
      _isButtonDisabled = true;
      _showGradingResult = true;
      _isGradingInProgress = true;
    });

    try {
      // (2) 採点API呼び出し
      final response = await _service.sendProgrammingHighTask(
        http.Client(),
        subTasks,
        currentSubTaskIndex,
        _codeController.text,
      );

      // (2.1) 採点結果をSharedPreferencesへ保存 + 未読フラグON
      final questionId = widget.questionData['questionId'] as int;
      await _service.storeGradingResult(questionId, response);

      // (2.2) 画面が既に破棄されているか確認
      if (!mounted) {
        // 画面がないので、グローバル通知だけ出す
        Get.snackbar(
          "採点完了",
          "結果が届きました。問題一覧で確認してください。",
          snackPosition: SnackPosition.TOP,
        );
        return;
      }

      // (3) JSONパース
      final Map<String, dynamic> responseJson = jsonDecode(response);

      // (4) 結果文字列生成
      final String compilationString = _buildCompilationResult(responseJson);
      final String executionString = _buildExecutionResult(responseJson);
      final String subtaskCheckString = _buildSubtaskCheckResult(responseJson);

      // (5) 画面に反映 (mountedチェック)
      if (!mounted) return;
      setState(() {
        _judge = responseJson['judge'] ?? false;
        _compilation = compilationString;
        _execution = executionString;
        _subtaskCheck = subtaskCheckString;
        _isGradingInProgress = false;
        _animateGradingResult = true;
      });

      // (5.1) 「画面上で結果を表示した＝ユーザーが見た」→ 既読化
      await _service.markGradingResultAsRead(questionId);

      // (6) サブタスクの進捗判定
      _updateSubTaskProgress(responseJson);

      // (7) 全サブタスク完了なら次へ
      if (currentSubTaskIndex >= subTasks.length) {
        await _service.saveUserCode(
          _codeController.text,
          widget.questionData['questionId'],
        );
        final interruptionController = Get.find<InterruptionDataController>();
        interruptionController.clearImplementationInterruption(
          widget.questionData['questionId'],
        );

        if (!_hasShownAnswer) {
          await _service.addNumberToCompleteList(
            widget.questionData['questionId'],
          );
        }

        final nextQ = await _questionSelectionScreenService
            .fetchImplementiveNextQuestion(widget.questionData['questionId']);

        final int oldXp = await gamificationService.getExperience();
        final oldprogressMap = getCurrentLevelProgress(oldXp);
        final oldprogressXp = oldprogressMap['currentProgress'] ?? 0;
        final oldLevel = getLevelFromXp(oldXp);

        await gamificationService.addPoint(10);
        await gamificationService.addExperience(subTasks.length * 5);

        final int newXp = await gamificationService.getExperience();
        final newLevel = getLevelFromXp(newXp);
        final newprogressMap = getCurrentLevelProgress(newXp);
        final newprogressXp = newprogressMap['currentProgress'] ?? 0;
        final xpNeededForNextLevel = newprogressMap['maxProgress'] ?? 0;
        final xpGainResult = XpGainResult(
          oldXp: oldprogressXp,
          newXp: newprogressXp,
          oldLevel: oldLevel,
          newLevel: newLevel,
          xpNeededForNextLevel: xpNeededForNextLevel,
        );

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CompletionScreen(
              compilation: _compilation,
              execution: _execution,
              specialCheck: _subtaskCheck,
              onNextChallenge: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionProgrammingScreenWidget(
                      questionData: nextQ,
                      interruption: {},
                      resumeFromInterruption: false,
                    ),
                  ),
                );
              },
              onFinish: () {
                Get.offAllNamed('/', arguments: {'initialIndex': 1});
              },
              implementiveQuestionId: widget.questionData['questionId'],
              xpResult: xpGainResult,
              questionName: widget.questionData['questionName'],
            ),
          ),
        );
      }
    } catch (e) {
      Get.snackbar(
        'エラー',
        '採点中にエラーが発生しました: $e',
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.redAccent,
        colorText: Colors.white,
      );
    } finally {
      // (8) 処理終了後のUI復帰
      if (!mounted) return;
      setState(() {
        _isButtonDisabled = false;
        _isGradingInProgress = false;
        _animateGradingResult = false;
      });
    }
  }

  /// コンパイル結果テキスト生成
  String _buildCompilationResult(Map<String, dynamic> responseJson) {
    if (!responseJson.containsKey('compilation')) return '';
    final compilation = responseJson['compilation'];

    final errors = compilation['errors'] ?? [];
    final warnings = compilation['warnings'] ?? [];

    final sb = StringBuffer('[コンパイル結果]\n');
    for (var error in errors) {
      sb.writeln(
          '${error['file']}:${error['line']}: error: ${error['message']}');
    }
    for (var warning in warnings) {
      sb.writeln(
          '${warning['file']}:${warning['line']}: warning: ${warning['message']}');
    }
    if (errors.isNotEmpty) {
      sb.writeln('${errors.length} errors');
    }
    if (warnings.isNotEmpty) {
      sb.writeln('${warnings.length} warnings');
    }

    return sb.toString();
  }

  /// 実行結果テキスト生成
  String _buildExecutionResult(Map<String, dynamic> responseJson) {
    if (!responseJson.containsKey('execution')) return '';
    final execution = responseJson['execution'];

    final stdoutStr = execution['stdout'] ?? '';
    final stderrStr = execution['stderr'] ?? '';
    final exception = execution['exception'];

    final sb = StringBuffer('[実行結果]\n');
    sb.writeln('status: ${execution['status']}');

    if (stdoutStr.isNotEmpty) {
      sb.writeln('stdout:\n$stdoutStr');
    }
    if (stderrStr.isNotEmpty) {
      sb.writeln('stderr:\n$stderrStr');
    }
    if (exception != null) {
      sb.writeln('Exception:');
      sb.writeln('${exception['type']}: ${exception['message']}');
      final stackTraceLines = exception['stackTrace'] ?? [];
      if (stackTraceLines is List) {
        sb.writeln('StackTrace:');
        for (var line in stackTraceLines) {
          sb.writeln(line);
        }
      }
    }

    return sb.toString();
  }

  /// 特殊条件チェックテキスト生成
  String _buildSubtaskCheckResult(Map<String, dynamic> responseJson) {
    if (!responseJson.containsKey('check')) return '';
    final check = responseJson['check'];
    final conditions = check['conditions'];

    if (conditions == null) return '';

    final sb = StringBuffer('[結果]\n');

    for (int i = currentSubTaskIndex; i < subTasks.length; i++) {
      sb.writeln('${conditions[i]['subtask']}:${conditions[i]['result']}:');
      if (i + 1 < subTasks.length) {
        if (conditions[i + 1]['result'] != 'passed') {
          break;
        }
      }
    }
    return sb.toString();
  }

  /// サブタスク進捗更新
  Future<void> _updateSubTaskProgress(Map<String, dynamic> responseJson) async {
    if (!responseJson.containsKey('check')) return;
    final check = responseJson['check'];
    final conditions = check['conditions'];
    if (conditions == null || conditions.isEmpty) return;

    int newIndex = currentSubTaskIndex;
    for (int i = currentSubTaskIndex; i < subTasks.length; i++) {
      if (i >= conditions.length) break;
      final String? result = conditions[i]['result'];
      if (result != 'passed') {
        gamificationService.addPoint(1);
        break;
      }
      newIndex++;
    }

    if (newIndex > currentSubTaskIndex) {
      for (int idx = currentSubTaskIndex; idx <= newIndex; idx++) {
        _subTaskCodeState[idx] = _codeController.text;
      }
      setState(() {
        currentSubTaskIndex = newIndex;
        _updateProgress();
      });
    }
  }

  /// 進捗率更新
  void _updateProgress() {
    if (currentSubTaskIndex >= subTasks.length) {
      progress = 1.0;
      return;
    }
    double denom = (subTasks.length - 1).toDouble();
    double numerator = (currentSubTaskIndex - 1).toDouble();
    progress = (numerator / denom).clamp(0.0, 1.0);
  }

  /// ズームリセット
  void _resetZoom() {
    setState(() {
      _scale = 1.0;
    });
  }

  /// キーボード可視判定
  bool isKeyboardVisible(BuildContext context) {
    return MediaQuery.of(context).viewInsets.bottom > 0;
  }

  /// 戻るボタン押下時
  void _handleBackButton() {
    if (!_hasShownAnswer) {
      final interruptionController = Get.find<InterruptionDataController>();
      if (_codeController.text.isNotEmpty) {
        Map<String, dynamic> interruptionData = {
          'userCode': _codeController.text,
          'currentSubTaskIndex': currentSubTaskIndex,
          'progress': progress,
        };
        interruptionController.saveImplementationInterruption(
          widget.questionData['questionId'].toString(),
          interruptionData,
        );
      }
    }
    Get.back();
  }

  @override
  Widget build(BuildContext context) {
    bool keyboardIsVisible = isKeyboardVisible(context);

    return Stack(
      children: [
        PopScope(
          canPop: false,
          child: Scaffold(
            appBar: AppBar(
              title: Text(widget.questionData['questionName'],
                  style: TextStyle(color: Colors.black)),
              backgroundColor: Colors.white,
              centerTitle: true,
              leading: IconButton(
                key: backButtonKey,
                icon: Icon(Icons.arrow_back, color: Colors.black),
                onPressed: _handleBackButton,
              ),
              actions: [
                IconButton(
                  icon: Icon(Icons.help_outline,
                      color: _isTutorialActive ? Colors.grey : Colors.black),
                  onPressed: () {
                    if (!_isTutorialActive) {
                      // オンボーディング（ガイダンス）を開始
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        setState(() {
                          _isOnboadingActive = true;
                          _guidanceNum = 0;
                        });
                        _updateGuidanceOverlayPosition();
                      });
                    }
                  },
                ),
              ],
            ),
            backgroundColor: Colors.grey.shade900,
            body: GestureDetector(
              onTap: () => FocusScope.of(context).unfocus(),
              child: SlidingUpPanel(
                controller: _panelController,
                minHeight: keyboardIsVisible ? 0 : 150,
                maxHeight: keyboardIsVisible
                    ? 0
                    : MediaQuery.of(context).size.height * 0.4,
                borderRadius: BorderRadius.vertical(top: Radius.circular(24.0)),
                panel: _panel(),
                body: Column(
                  children: [
                    // 進捗表示部分
                    Container(
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 19, 26, 61),
                        border: Border.all(color: Colors.grey, width: 5.0),
                      ),
                      padding:
                          EdgeInsets.symmetric(horizontal: 16, vertical: 6),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // 進捗率 + Info
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "進捗達成率: ${(progress * 100).toStringAsFixed(0)}%",
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white),
                              ),
                              IconButton(
                                icon: Icon(Icons.info_outline,
                                    color: Colors.white),
                                onPressed: _showProgressDialog,
                              ),
                            ],
                          ),
                          SizedBox(height: 3),
                          LinearProgressIndicator(
                            key: progressBarKey,
                            value: progress,
                            minHeight: 5,
                            color: Colors.amber,
                            backgroundColor: Color.fromARGB(255, 247, 232, 179),
                          ),
                          SizedBox(height: 3),
                          // 現在のサブタスク
                          Text(
                            (currentSubTaskIndex < subTasks.length)
                                ? subTasks[currentSubTaskIndex]['subTask']
                                : "すべてのサブタスクが完了しました",
                            style: TextStyle(color: Colors.white, fontSize: 13),
                          ),
                        ],
                      ),
                    ),

                    // 採点結果表示
                    Visibility(
                      visible: _showGradingResult,
                      maintainSize: true,
                      maintainAnimation: true,
                      maintainState: true,
                      child: SizedBox(
                        key: gradingResultKey,
                        height: MediaQuery.of(context).size.height * 0.1,
                        child: OverlaidGradingResultWidget(
                          judge: _judge,
                          compilation: _compilation,
                          execution: _execution,
                          subtaskCheck: _subtaskCheck,
                          isGradingInProgress: _isGradingInProgress,
                          animate: _animateGradingResult,
                          onConditionMet: _conditionMet,
                          step: _steps[_currentStep]['requires'],
                        ),
                      ),
                    ),

                    // コード入力エリア
                    Expanded(
                      child: KeyboardActions(
                        config: _buildConfig(context),
                        child: CodeTheme(
                          data: CodeThemeData(styles: monokaiSublimeTheme),
                          child: InteractiveViewer(
                            panEnabled: true,
                            scaleEnabled: false,
                            child: CodeField(
                              key: codeEditorKey,
                              wrap: false,
                              textStyle: GoogleFonts.jetBrainsMono(
                                fontSize: _baseFontSize * _scale,
                                height: 1.5,
                              ),
                              controller: _codeController,
                              focusNode: _codeFocusNode,
                              gutterStyle: const GutterStyle(
                                margin: 0,
                                width: 65,
                                showFoldingHandles: false,
                                textStyle: TextStyle(height: 1.5),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                onPanelOpened: () {
                  if (_steps[_currentStep]['requires'] == 'panel') {
                    _conditionMet();
                  }
                },
                color: Colors.black,
                backdropEnabled: true,
                backdropOpacity: 0.5,
              ),
            ),
          ),
        ),
        // チュートリアル／オンボーディングオーバーレイ（ガイダンス部分）
        if (_isOnboadingActive && !_isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isOnboadingActive && !_isTutorialActive)
          GestureDetector(
            onTap: _guidanceStep,
            child: Center(
              child: Stack(
                children: [
                  _buildBottomPanel(_guidance[_guidanceNum]),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: ElevatedButton.icon(
                      onPressed: () {
                        if (_isOnboadingActive) {
                          _endGuidance();
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                          side: BorderSide(color: Colors.black, width: 2.5),
                        ),
                      ),
                      icon: Image.asset(
                        'assets/Skip.png',
                        width: 25,
                        height: 25,
                      ),
                      label: Text(
                        "スキップ",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        if (_isOnboadingActive && _isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isShowDialog) Center(child: _buildTutorialDialog()),
        // 操作説明（チュートリアル）パネル
        if (_isTutorialActive && !_isOnboadingActive)
          GestureDetector(
            onTap: _checkAndProceed,
            child: Padding(
              padding: EdgeInsets.only(
                bottom: keyboardIsVisible ? 450 : 125,
              ), // FABと被らないよう下部に余白を追加
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Stack(
                  children: [
                    _buildBottomPanel(_steps[_currentStep]['text']),
                    if (_currentStep < _steps.length - 1)
                      Positioned(
                        right: 0,
                        top: 0,
                        child: ElevatedButton.icon(
                          onPressed: () {
                            if (_isOnboadingActive) {
                              _endGuidance();
                            } else if (_isTutorialActive) {
                              _endTutorial();
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.white,
                            padding: EdgeInsets.symmetric(
                                horizontal: 16, vertical: 8),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                              side: BorderSide(color: Colors.black, width: 2.5),
                            ),
                          ),
                          icon: Image.asset(
                            'assets/Skip.png',
                            width: 25,
                            height: 25,
                          ),
                          label: Text(
                            "スキップ",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        // FABタップ促進のアニメーション
        // ヘルプオーバーレイ
        if (showHelp)
          HelpOverlay(
            existingWidget: Container(),
            helpMessages: helpMessages,
            backgroundImages: backgroundImages,
            onSkip: () {
              setState(() {
                showHelp = false;
              });
            },
            messageAlignment: messageAlignment,
            margin: margin,
            rectangleSteps: rectangleSteps,
          ),
        // ガイダンス中なら、ガイダンス用の青い枠を表示する
        if (_isOnboadingActive)
          BlueSquareOverlay(
            top: _guidanceOverlayTop,
            left: _guidanceOverlayLeft,
            width: _guidanceOverlayWidth,
            height: _guidanceOverlayHeight,
            flashing: true,
            isVisible: true,
          ),

        // 操作説明用の青い枠（動的に対象ウィジェットの位置を取得）
        if (_isTutorialActive && _isVisible)
          BlueSquareOverlay(
            top: _tutorialOverlayTop,
            left: _tutorialOverlayLeft,
            width: _tutorialOverlayWidth,
            height: _tutorialOverlayHeight,
            flashing: true,
            isVisible: _isVisible,
          ),
      ],
    );
  }

  Widget _buildBackgroundOverlay() {
    return Container(
      color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Widget _buildBottomPanel(String text) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.lightBlue, width: 3),
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: Text(
        text,
        style: TextStyle(
            fontSize: 16,
            color: Colors.black,
            fontWeight: FontWeight.w600,
            decoration: TextDecoration.none),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildTutorialDialog() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 50),
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 252, 246, 251),
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
              color: Colors.black26, blurRadius: 10.0, offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            '操作説明を開始しますか？',
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.black87,
                fontWeight: FontWeight.normal,
                decoration: TextDecoration.none),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              GestureDetector(
                onTap: () {
                  _startTutorial();
                  _endGuidance();
                },
                child: Text(
                  'はい',
                  style: TextStyle(
                      color: Colors.purple,
                      fontSize: 16.0,
                      decoration: TextDecoration.none),
                ),
              ),
              GestureDetector(
                onTap: () {
                  _noTap();
                  _endGuidance();
                },
                child: Text(
                  'いいえ',
                  style: TextStyle(
                      color: Colors.purple,
                      fontSize: 16.0,
                      decoration: TextDecoration.none),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  /// 下部パネル
  Widget _panel() {
    final displayedSubTasks = subTasks.sublist(1); // 1番目〜末尾まで
    return Column(
      key: bottomPanelKey,
      children: [
        // 上スワイプ用の"三本線"
        GestureDetector(
          key: panelSwipeKey,
          onTap: () {
            if (_panelController.isPanelOpen) {
              _panelController.close();
            } else {
              _panelController.open();
            }
          },
          child: BreathingBorder(opacityAnimation: _opacityAnimation),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(16),
              child: Column(
                children: [
                  SizedBox(height: 10),

                  // 回答リセット + 回答送信
                  if (progress < 1.0)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          key: resetButtonKey,
                          onPressed: () {
                            _resetAnswerStatus();
                            if (_isTutorialActive &&
                                _steps[_currentStep]['requires'] == 'reset') {
                              _conditionMet();
                            }
                          },
                          child: Text("回答状況リセット"),
                        ),
                        SizedBox(width: 16),
                        ElevatedButton(
                          onPressed: _isButtonDisabled ? null : _updateCode,
                          child: Text("回答を送信"),
                        ),
                      ],
                    ),
                  SizedBox(height: 20),

                  // ズームスライダー
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Text(
                                  "ズーム: ${(_scale * 100).toInt()}%",
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                ),
                                SizedBox(width: 8),
                                IconButton(
                                  icon: Icon(Icons.refresh),
                                  color: Colors.white,
                                  tooltip: "ズームをリセット",
                                  onPressed: _resetZoom,
                                ),
                              ],
                            ),
                            Slider(
                              key: zoomSliderKey,
                              value: _scale,
                              min: _minScale,
                              max: _maxScale,
                              divisions: ((_maxScale - _minScale) * 10).toInt(),
                              label: "${(_scale * 100).toInt()}%",
                              onChanged: (double value) {
                                setState(() {
                                  _scale = value;
                                });
                                if (_isTutorialActive &&
                                    _steps[_currentStep]['requires'] ==
                                        'zoom') {
                                  _conditionMet();
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  if (progress < 1.0)
                    // 「正解を表示」ボタン
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          onPressed: () async {
                            // 回答表示がまだロックされていない場合、30pt消費してロック解除を試みる
                            if (!_answerUnlocked) {
                              // ※ gamificationServiceに30ptを消費する処理がある前提です
                              bool success =
                                  await gamificationService.spendPoints(30);
                              if (success) {
                                setState(() {
                                  _answerUnlocked = true;
                                });
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text("30ptを使用して回答表示可能になりました")));
                              } else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(content: Text("ポイントが足りません")));
                              }
                              return;
                            }
                            currentSubTaskIndex++;
                            setState(() {
                              if (widget.questionData['modelAnswer'] != null &&
                                  widget.questionData['modelAnswer'] is List &&
                                  widget.questionData['modelAnswer'].length >=
                                      currentSubTaskIndex) {
                                final newCode = widget
                                    .questionData['modelAnswer']
                                        [currentSubTaskIndex]
                                    .replaceAll('\\n', '\n')
                                    .replaceAll('\\', '');
                                _codeController.value = TextEditingValue(
                                  text: newCode,
                                  selection: TextSelection.fromPosition(
                                    TextPosition(offset: newCode.length),
                                  ),
                                );
                                _subTaskCodeState[currentSubTaskIndex] =
                                    _codeController.text;
                              }

                              if (currentSubTaskIndex >= subTasks.length) {
                                currentSubTaskIndex = subTasks.length;
                              }
                              _updateProgress();
                              _hasShownAnswer = true;
                              _answerUnlocked = false;
                            });
                          },
                          child: Text(
                              _answerUnlocked ? "正解を表示する" : "回答を見る（30pt 消費）"),
                        ),
                      ],
                    ),

                  // サブタスク一覧表示
                  SizedBox(height: 20),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "サブタスク一覧",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(height: 8),

                  ListView.builder(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: displayedSubTasks.length,
                    itemBuilder: (context, index) {
                      int realIndex = index + 1;
                      bool isCompleted = realIndex < currentSubTaskIndex;
                      bool isCurrent = realIndex == currentSubTaskIndex;

                      Icon subtaskIcon;
                      if (isCompleted) {
                        subtaskIcon =
                            Icon(Icons.check_circle, color: Colors.green);
                      } else if (isCurrent) {
                        subtaskIcon =
                            Icon(Icons.play_circle_fill, color: Colors.orange);
                      } else {
                        subtaskIcon = Icon(Icons.radio_button_unchecked,
                            color: Colors.grey);
                      }

                      return Container(
                        margin: EdgeInsets.symmetric(vertical: 4),
                        decoration: BoxDecoration(
                          color: Colors.transparent,
                          border: Border.all(
                            color: Colors.grey,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            subtaskIcon,
                            SizedBox(width: 8),
                            Expanded(
                              child: Text(
                                subTasks[realIndex]['subTask'] ?? '',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  if (_hasShownAnswer && progress >= 1.0) ...[
                    SizedBox(height: 30),
                    Text(
                      "正解を表示し、すべてのサブタスクを確認しました。",
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                    SizedBox(height: 10),
                    ElevatedButton(
                      onPressed: () {
                        Get.back();
                      },
                      child: Text("解説を終了して問題一覧へ戻る"),
                    ),
                  ],
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class BreathingBorder extends StatelessWidget {
  final Animation<double> opacityAnimation;
  BreathingBorder({required this.opacityAnimation});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: AnimatedBuilder(
        animation: opacityAnimation,
        builder: (context, child) {
          return CustomPaint(
            painter: TripleLinePainter(opacityAnimation.value),
            size: Size(double.infinity, 50),
          );
        },
      ),
    );
  }
}

class TripleLinePainter extends CustomPainter {
  final double opacity;
  TripleLinePainter(this.opacity);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Colors.blueGrey.withOpacity(opacity)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    final double lineSpacing = 10.0;
    final double centerY = size.height / 2;
    final double startY = centerY - lineSpacing;
    final double endY = centerY + lineSpacing;

    // 中央の線
    canvas.drawLine(
      Offset(20, centerY),
      Offset(size.width - 20, centerY),
      paint,
    );
    // 上部の線
    canvas.drawLine(
      Offset(20, startY),
      Offset(size.width - 20, startY),
      paint,
    );
    // 下部の線
    canvas.drawLine(
      Offset(20, endY),
      Offset(size.width - 20, endY),
      paint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

///
/// 採点結果表示Widget
///
class OverlaidGradingResultWidget extends StatefulWidget {
  final bool judge;
  final String compilation;
  final String execution;
  final String subtaskCheck;
  final bool isGradingInProgress;
  final bool animate;
  final VoidCallback? onConditionMet;
  final String? step;

  OverlaidGradingResultWidget({
    required this.judge,
    required this.compilation,
    required this.execution,
    required this.subtaskCheck,
    required this.isGradingInProgress,
    this.animate = false,
    this.onConditionMet,
    this.step,
  });

  @override
  _OverlaidGradingResultWidgetState createState() =>
      _OverlaidGradingResultWidgetState();
}

class _OverlaidGradingResultWidgetState
    extends State<OverlaidGradingResultWidget> with TickerProviderStateMixin {
  late AnimationController _iconAnimController;
  late Animation<Color?> _iconColorAnimation;
  late Animation<Color?> _textColorAnimation;
  OverlayEntry? _overlayEntry;

  @override
  void initState() {
    super.initState();
    _iconAnimController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );

    _iconColorAnimation = ColorTween(
      begin: Colors.transparent,
      end: widget.judge ? Colors.green : Colors.red,
    ).animate(_iconAnimController)
      ..addListener(() {
        setState(() {});
      });

    _textColorAnimation = ColorTween(
      begin: Colors.transparent,
      end: Colors.white,
    ).animate(_iconAnimController)
      ..addListener(() {
        setState(() {});
      });

    _iconAnimController.forward();
  }

  @override
  void didUpdateWidget(OverlaidGradingResultWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.judge != widget.judge || widget.animate) {
      _iconColorAnimation = ColorTween(
        begin: Colors.transparent,
        end: widget.judge ? Colors.green : Colors.red,
      ).animate(_iconAnimController);

      _iconAnimController
        ..reset()
        ..forward();
    }
  }

  @override
  void dispose() {
    _overlayEntry?.remove();
    _iconAnimController.dispose();
    super.dispose();
  }

  /// "success" "failure" "passed" "failed" をハイライト
  List<TextSpan> _buildHighlightedSpans(String text) {
    final List<TextSpan> spans = [];
    final pattern = RegExp(r'(success|failure|passed|failed)');
    int lastIndex = 0;
    text = text.replaceAll('\\n', '\n').replaceAll('\\', '');

    for (final match in pattern.allMatches(text)) {
      if (match.start > lastIndex) {
        spans.add(TextSpan(text: text.substring(lastIndex, match.start)));
      }
      final matchedText = match.group(0);
      Color highlightColor = Colors.white;
      if (matchedText == 'success' || matchedText == 'passed') {
        highlightColor = Colors.green;
      } else if (matchedText == 'failure' || matchedText == 'failed') {
        highlightColor = Colors.red;
      }
      spans.add(
          TextSpan(text: matchedText, style: TextStyle(color: highlightColor)));
      lastIndex = match.end;
    }
    if (lastIndex < text.length) {
      spans.add(TextSpan(text: text.substring(lastIndex)));
    }
    return spans;
  }

  OverlayEntry _createOverlayEntry(BuildContext context) {
    return OverlayEntry(
      builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            _overlayEntry?.remove();
            _overlayEntry = null;
          },
          behavior: HitTestBehavior.translucent,
          child: Stack(
            children: [
              Container(color: Colors.black54), // 背景
              Center(
                child: Material(
                  color: Colors.transparent,
                  child: Container(
                    margin: EdgeInsets.all(16.0),
                    padding: EdgeInsets.all(16.0),
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // compilation
                          Text(
                            widget.compilation,
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                          SizedBox(height: 8),
                          // execution
                          RichText(
                            text: TextSpan(
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white),
                              children:
                                  _buildHighlightedSpans(widget.execution),
                            ),
                          ),
                          SizedBox(height: 8),
                          // subtaskCheck
                          RichText(
                            text: TextSpan(
                              style:
                                  TextStyle(fontSize: 14, color: Colors.white),
                              children:
                                  _buildHighlightedSpans(widget.subtaskCheck),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _showOverlay(BuildContext context) {
    if (_overlayEntry != null) return;
    _overlayEntry = _createOverlayEntry(context);
    Overlay.of(context).insert(_overlayEntry!);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isGradingInProgress) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          CircularProgressIndicator(),
          SizedBox(width: 10),
          Text("判定中...", style: TextStyle(color: Colors.white)),
        ],
      );
    }

    return GestureDetector(
      onTap: () {
        _showOverlay(context);
        // タップ時に外部から渡された _conditionMet コールバックを呼び出す
        if (widget.onConditionMet != null && widget.step != null) {
          if (widget.step == 'grading') {
            widget.onConditionMet!();
          }
        }
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AnimatedBuilder(
            animation: _iconColorAnimation,
            builder: (context, child) {
              return Icon(
                widget.judge ? Icons.check_circle : Icons.cancel,
                color: _iconColorAnimation.value,
                size: 40.0,
              );
            },
          ),
          SizedBox(width: 8),
          AnimatedBuilder(
            animation: _textColorAnimation,
            builder: (context, child) {
              return Text(
                "タップしてコメントを表示",
                style: TextStyle(
                  fontSize: 16.0,
                  color: _textColorAnimation.value,
                  fontWeight: FontWeight.bold,
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
