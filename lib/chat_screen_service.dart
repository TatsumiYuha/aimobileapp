// chat_screen_service.dart
import 'dart:convert';
import 'dart:io';
import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:http/io_client.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'auth_service.dart';

class ChatScreenService {
  late Future<http.Client> _trustedClientFuture;
  List<Map<String, dynamic>> recentChats = [];
  final authService = AuthService();
  final gamificationService = GamificationService();

  ChatScreenService() {
    _trustedClientFuture = _createTrustedClient();
  }

  Future<http.Client> _createTrustedClient() async {
    final SecurityContext context = SecurityContext.defaultContext;
    ByteData data = await rootBundle.load('assets/server-chain.crt');
    context.setTrustedCertificatesBytes(data.buffer.asUint8List());
    final HttpClient httpClient = HttpClient(context: context);
    return IOClient(
        httpClient); // http package's IOClient wraps around HttpClient
  }

  // メッセージとGPTのレスポンスをFirestoreに保存するメソッド
  Future<DocumentReference> storeChat(String userMessage,
      String? chatGptResponse, String roomId, User? currentUser) {
    if (currentUser == null) {
      throw Exception('ユーザーがログインしていません。');
    }
    return FirebaseFirestore.instance
        .collection('users')
        .doc(currentUser.uid)
        .collection('chatRooms')
        .doc(roomId)
        .collection('chats')
        .add({
      'user_message': userMessage,
      'chat_gpt_response': chatGptResponse,
      'timestamp': FieldValue.serverTimestamp(), // サーバータイムスタンプを使用
    });
  }

  Future<void> fetchRecentChats(String roomId, User? currentUser) async {
    if (currentUser == null) {
      throw Exception('ユーザーがログインしていません。');
    }
    final chats = await getChatMessages(roomId, currentUser).first;
    recentChats = chats
        .map((doc) => {
              'user_message': doc['user_message'],
              'chat_gpt_response': doc['chat_gpt_response'],
            })
        .toList();
  }

  // メッセージを送信するメソッド
  Future<Map<String, dynamic>> sendMessage(
      String message, User? currentUser, String roomId) async {
    if (currentUser == null) {
      throw Exception('ユーザーがログインしていません。');
    }
    try {
      print("Starting sendMessage process...");
      // レスポンスがnullの状態で登録
      DocumentReference ref =
          await storeChat(message, null, roomId, currentUser);

      // firestoreからチャットログを取得
      await fetchRecentChats(roomId, currentUser);

      // 直近5回のチャットを取得
      final lastFiveChats =
          (recentChats.length >= 5) ? recentChats.sublist(0, 5) : recentChats;

      // ルームの isNameUpdated フィールドをチェック
      DocumentSnapshot roomDoc = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .doc(roomId)
          .get();

      if (roomDoc.exists && !(roomDoc['isNameUpdated'] ?? false)) {
        // ルーム名の要約を取得するためのメッセージをChatGPTに送信
        String summarizedMessage = await getSummaryFromChatGPT(message);

        // Firestoreにルーム名と isNameUpdated フィールドを更新
        await FirebaseFirestore.instance
            .collection('users')
            .doc(currentUser.uid)
            .collection('chatRooms')
            .doc(roomId)
            .update({
          'name': summarizedMessage,
          'isNameUpdated': true, // フィールドを更新
          'timestamp': FieldValue.serverTimestamp(), // サーバータイムスタンプを使用
        });
      }

      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        throw Exception("認証トークンが無効です。再ログインしてください。"); // 処理を終了
      }

      final client = await _trustedClientFuture;
      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp/ask'),
        body: utf8.encode(jsonEncode({
          'recent_chats': lastFiveChats,
          'message': message,
        })),
        headers: {"Content-Type": "application/json"},
      );

      // 応答を解析
      Map<String, dynamic> decodedResponse =
          jsonDecode(utf8.decode(response.bodyBytes));

      // レスポンスをfirestoreに更新し、timestampも更新
      await ref.update({
        'chat_gpt_response': decodedResponse['response'],
        'timestamp': FieldValue.serverTimestamp(), // サーバータイムスタンプを使用
      });

      // チャットルームの timestamp を更新
      await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .doc(roomId)
          .update({
        'timestamp': FieldValue.serverTimestamp(), // サーバータイムスタンプを使用
      });

      await gamificationService.addDailyExperience(2);

      return decodedResponse;
    } on SocketException {
      throw Exception('ネットワークエラーが発生しました。インターネット接続を確認してください。');
    } on FirebaseException catch (e) {
      throw Exception('Firebase error: ${e.message}');
    } on FormatException {
      throw Exception('サーバーの応答の解析に失敗しました。');
    } catch (e) {
      print("Error in sendMessage: $e");
      throw Exception('Unknown error occurred: $e');
    }
  }

  // メッセージを送信するメソッド
  Future<Map<String, dynamic>> sendDetailMessage(
      String message, User? currentUser, String roomId) async {
    if (currentUser == null) {
      throw Exception('ユーザーがログインしていません。');
    }
    try {
      print("Starting sendDetailMessage process...");

      // レスポンスがnullの状態で登録
      DocumentReference ref =
          await storeChat(message, null, roomId, currentUser);

      // ルームの詳細情報を取得
      DocumentSnapshot roomDoc = await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .doc(roomId)
          .get();

      if (!roomDoc.exists) {
        throw Exception('Room does not exist');
      }

      // firestoreからチャットログを取得
      await fetchRecentChats(roomId, currentUser);

      // 直近5回のチャットを取得
      final lastFiveChats =
          (recentChats.length >= 5) ? recentChats.sublist(0, 5) : recentChats;

      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        throw Exception("認証トークンが無効です。再ログインしてください。"); // 処理を終了
      }

      // Firestoreからルームの詳細情報を取得
      Map<String, dynamic> roomData = roomDoc.data() as Map<String, dynamic>;
      String questionText = roomData['questionText'];
      String userAnswer = roomData['userAnswer'];
      String modelAnswer = roomData['modelAnswer'];

      // リクエストを送信
      final client = await _trustedClientFuture;
      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp/analyze'),
        body: utf8.encode(jsonEncode({
          'questionText': questionText,
          'userAnswer': userAnswer,
          'modelAnswer': modelAnswer,
          'recent_chats': lastFiveChats,
          'message': message,
        })),
        headers: {"Content-Type": "application/json"},
      );

      // 応答を解析
      var decodedResponse =
          jsonDecode(utf8.decode(response.bodyBytes)) as Map<String, dynamic>;

      // レスポンスをfirestoreに更新し、timestampも更新
      await ref.update({
        'chat_gpt_response': decodedResponse['analysis'],
        'timestamp': FieldValue.serverTimestamp(), // サーバータイムスタンプを使用
      });

      // チャットルームの timestamp を更新
      await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .doc(roomId)
          .update({
        'timestamp': FieldValue.serverTimestamp(), // サーバータイムスタンプを使用
      });

      await gamificationService.addDailyExperience(2);

      return decodedResponse;
    } on SocketException {
      throw Exception('ネットワークエラーが発生しました。インターネット接続を確認してください。');
    } on FirebaseException catch (e) {
      throw Exception('Firebase error: ${e.message}');
    } on FormatException {
      throw Exception('サーバーの応答の解析に失敗しました。');
    } catch (e) {
      print("Error in sendDetailMessage: $e");
      throw Exception('Unknown error occurred: $e');
    }
  }

  // メッセージとレスポンスのリストを取得する
  Stream<List<QueryDocumentSnapshot>> getChatMessages(
      String roomId, User? currentUser,
      {DocumentSnapshot? startAfter}) {
    if (currentUser == null) {
      throw Exception('ユーザーがログインしていません。');
    }
    try {
      Query query = FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid) // 正しくuidを参照
          .collection('chatRooms')
          .doc(roomId)
          .collection('chats')
          .orderBy('timestamp', descending: true)
          .limit(20);

      // startAfterが指定されている場合、それを基点に次の20件を取得
      if (startAfter != null) {
        query = query.startAfterDocument(startAfter);
      }

      return query.snapshots().map((snapshot) => snapshot.docs);
    } catch (e) {
      throw Exception('Failed to fetch chat messages: $e');
    }
  }

  Future<String> getSummaryFromChatGPT(String message) async {
    try {
      final client = await _trustedClientFuture;

      // ユーザーの認証状態とトークンの有効性を確認
      bool isAuthenticatedAndValid =
          await authService.isUserAuthenticatedAndTokenValid();
      if (!isAuthenticatedAndValid) {
        return "認証トークンが無効です。"; // 処理を終了
      }

      final response = await client.post(
        Uri.parse('https://flap.axiz.co.jp/summarize'),
        body: utf8.encode(jsonEncode({
          'message': message,
        })),
        headers: {"Content-Type": "application/json"},
      );

      // 応答を解析
      String decodedResponse = jsonDecode(utf8.decode(response.bodyBytes));

      return decodedResponse;
    } on SocketException {
      throw Exception('No Internet connection');
    } on FormatException {
      throw Exception('Failed to decode server response');
    } catch (e) {
      throw Exception('Unknown error occurred: $e');
    }
  }

  Future<void> isDetailed(User? currentUser, String chatRoomId) async {
    if (currentUser == null) {
      throw Exception('ユーザーがログインしていません。');
    }
    if (chatRoomId.isNotEmpty) {
      await FirebaseFirestore.instance
          .collection('users')
          .doc(currentUser.uid)
          .collection('chatRooms')
          .doc(chatRoomId)
          .update({'isDetail': false});
    }
  }

  // 予測クイックレスポンスを取得するメソッド
  Future<List<String>> getPredictedQuickResponses(
      Map<String, dynamic> response) async {
    // 簡単なキーワードベースの予測ロジック
    // ここでは例として、特定のキーワードに基づいてレスポンスを生成しています
    List<String> predictions = List<String>.from(response['qcr']);

    // 必要に応じてさらに予測を追加・調整可能
    return predictions;
  }
}
