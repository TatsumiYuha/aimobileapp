import 'package:flutter/material.dart';
import 'auth_service.dart';
import 'chat_screen_service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AnswerDetailScreen extends StatefulWidget {
  final bool isCorrect;
  final String questionText;
  final String userAnswer;
  final double score;
  final String commentary;

  AnswerDetailScreen({
    required this.isCorrect,
    required this.questionText,
    required this.userAnswer,
    required this.score,
    required this.commentary,
  });

  @override
  _AnswerDetailScreenState createState() => _AnswerDetailScreenState();
}

class _AnswerDetailScreenState extends State<AnswerDetailScreen>
    with TickerProviderStateMixin {
  List<Map<String, dynamic>> messages = [
    {'role': 'user', 'text': 'この問題について詳しく教えてください。'}
  ];

  late AnimationController _animationController;
  late Animation<double> _opacityAnimation;
  bool _showChatButton = true;

  late AnimationController _borderAnimationController;
  late AnimationController _inputAnimationController;
  late AnimationController _chatAnimationController;

  // ボーダーアニメーション用の変数
  late Animation<double> _borderThickness;

  final TextEditingController _controller = TextEditingController();
  final ChatScreenService _service = ChatScreenService(); // Serviceのインスタンス化
  final User? currentUser = FirebaseAuth.instance.currentUser; // 現在のユーザーを取得
  Stream<List<QueryDocumentSnapshot>>? _chatStream; // チャットのStream
  bool _isLoading = false; // ローディング中か判定する
  final ScrollController _scrollController = ScrollController();
  bool _showScrollButton = false; //ボタン表示状態を管理する
  bool _isVisible = true; // セクションの表示状態を管理する変数

  @override
  void initState() {
    super.initState();
    _loadChatStream(); // チャットストリームの読み込み

    _animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    _borderAnimationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    _inputAnimationController = AnimationController(
      duration: const Duration(seconds: 3),
      vsync: this,
    );
    _chatAnimationController = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    );

    _borderThickness = Tween<double>(begin: 0.0, end: 2.0).animate(
      CurvedAnimation(parent: _borderAnimationController, curve: Curves.easeIn),
    );

    _borderAnimationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _inputAnimationController.forward();
      }
    });

    _inputAnimationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _chatAnimationController.forward();
      }
    });

    _opacityAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _animationController, curve: Curves.easeIn),
    );

    _scrollController.addListener(() {
      if (_scrollController.offset > 0 && !_showScrollButton) {
        setState(() {
          _showScrollButton = true;
        });
      } else if (_scrollController.offset == 0 && _showScrollButton) {
        setState(() {
          _showScrollButton = false;
        });
      }
    });
  }

  _resetScrollPosition() {
    _scrollController.jumpTo(0.0); //初期値へジャンプ
  }

  void _loadChatStream() {
    // ここでチャットストリームを初期化
    _chatStream =
        _service.getChatMessages("roomId", currentUser); // roomIdは適切なIDに置き換える
  }

  void _sendMessage(String message) async {
    _controller.clear();

    await _service.sendMessage(
        message, currentUser, "roomId"); // roomIdは適切なIDに置き換える
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          '問題詳細',
          style: TextStyle(color: Colors.black),
        ),
        centerTitle: true,
        actions: [
          IconButton(
            onPressed: () => {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('ログアウト'),
                    content: Text('本当にログアウトしますか？'),
                    actions: <Widget>[
                      TextButton(
                        child: Text('キャンセル'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      TextButton(
                        child: Text('ログアウト'),
                        onPressed: () {
                          Navigator.of(context).pop(); // ダイアログを閉じる
                          AuthService.logout(context); // ログアウト処理を呼び出す
                        },
                      ),
                    ],
                  );
                },
              ),
            },
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            // トグルアイコンとセクションを含むコンテナ
            Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.blueGrey[50], // 背景色
                borderRadius: BorderRadius.circular(10), // 角の丸み
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      IconButton(
                        icon: Icon(_isVisible
                            ? Icons.toggle_on
                            : Icons.toggle_off_outlined),
                        onPressed: () {
                          setState(() {
                            _isVisible = !_isVisible; // 表示状態を切り替える
                          });
                        },
                      ),
                    ],
                  ),
                  if (_isVisible) _buildQuestionSection(), // 問題セクションの構築
                  if (_isVisible) _buildAnswerSection(), // 回答セクションの構築
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildQuestionSection() {
    return Card(
      elevation: 4.0,
      margin: EdgeInsets.all(8.0), // カードの外側のマージンを設定
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('問題文:',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
            const SizedBox(height: 8.0),
            Text(widget.questionText),
          ],
        ),
      ),
    );
  }

  Widget _buildAnswerSection() {
    return Container(
      // CardをContainerでラップ
      width: double.infinity, // 横幅を画面いっぱいに設定
      child: Card(
        elevation: 4.0,
        margin: EdgeInsets.all(8.0), // カードの外側のマージンを設定
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('あなたの回答:',
                  style:
                      TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
              const SizedBox(height: 8.0),
              Text(widget.userAnswer),
              const SizedBox(height: 8.0),
              Text('得点: ${widget.score}'), // 得点
              Text(widget.isCorrect ? '正解' : '不正解',
                  style: TextStyle(
                    color: widget.isCorrect ? Colors.green : Colors.red,
                  )),
              const SizedBox(height: 8.0),
              Text('採点者からのコメント:',
                  style: TextStyle(fontWeight: FontWeight.bold)),
              SizedBox(height: 8.0),
              Text(widget.commentary),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildGPTChatSection() {
    return Card(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'GPTとの会話:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          StreamBuilder<List<QueryDocumentSnapshot>>(
            stream: _chatStream,
            builder: (BuildContext context, snapshot) {
              if (!snapshot.hasData) {
                return Center(child: CircularProgressIndicator());
              }
              var messages = snapshot.data!;
              return Stack(
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: messages.length,
                    itemBuilder: (context, index) {
                      var messageData =
                          messages[index].data() as Map<String, dynamic>;
                      return ListTile(
                        title:
                            Text(messageData['user_message'] ?? 'No message'),
                        subtitle: Text(messageData['chat_gpt_response'] ??
                            'Waiting for response...'),
                      );
                    },
                  ),
                  if (_showScrollButton) // スクロールボタンの追加
                    Positioned(
                      right: 10.0,
                      bottom: 10.0,
                      child: FloatingActionButton(
                        onPressed: _resetScrollPosition,
                        child: Icon(Icons.arrow_downward),
                        mini: true,
                        backgroundColor: Colors.grey,
                      ),
                    ),
                ],
              );
            },
          ),
          Row(
            //テキストフィールドと送信アイコンを配置
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: TextField(
                  controller: _controller,
                  onSubmitted: (message) {
                    _sendMessage(message); // Serviceクラスのメソッドを使用
                  },
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: const InputDecoration(
                    hintText: 'メッセージを入力...',
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
              IconButton(
                icon: Icon(_isLoading ? Icons.hourglass_empty : Icons.send,
                    color: _isLoading ? Colors.grey : null), // 「メッセージを送信する」アイコン
                onPressed: _isLoading
                    ? null
                    : () {
                        _sendMessage(_controller.text);
                      },
              ),
            ],
          ),
        ],
      ),
    );
  }

  // メッセージの背景とテキストの色を設定する関数
  Color getMessageBackgroundColor(bool isUserMessage) {
    return isUserMessage ? Colors.blueGrey : Colors.black;
  }

  Color getMessageTextColor(bool isUserMessage) {
    return isUserMessage ? Colors.white : Colors.white;
  }

  Widget bubbleDesign(String? message, bool isUserMessage, [Widget? child]) {
    return Align(
      alignment: isUserMessage ? Alignment.centerRight : Alignment.centerLeft,
      child: IntrinsicWidth(
        child: Container(
          constraints:
              BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.7),
          margin: isUserMessage
              ? const EdgeInsets.only(left: 50, right: 10, top: 10, bottom: 10)
              : const EdgeInsets.only(left: 10, right: 50, top: 10, bottom: 10),
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          decoration: BoxDecoration(
            color: getMessageBackgroundColor(isUserMessage),
            borderRadius: isUserMessage
                ? const BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(0),
                  )
                : const BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(0),
                  ),
          ),
          child: child ??
              Text(
                message ?? "",
                style: TextStyle(
                  color: getMessageTextColor(isUserMessage),
                ),
              ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    _borderAnimationController.dispose();
    _inputAnimationController.dispose();
    _chatAnimationController.dispose();
    super.dispose();
  }
}
