import 'package:flutter/material.dart';

class BlueSquareOverlay extends StatefulWidget {
  final double top;
  final double left;
  final double width;
  final double height;
  final bool flashing;
  final bool isVisible;

  const BlueSquareOverlay({
    required this.top,
    required this.left,
    required this.width,
    required this.height,
    required this.flashing,
    required this.isVisible,
    Key? key,
  }) : super(key: key);

  @override
  State<BlueSquareOverlay> createState() => _BlueSquareOverlayState();
}

class _BlueSquareOverlayState extends State<BlueSquareOverlay>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 1),
    );

    _opacityAnimation = Tween<double>(begin: 1.0, end: 0.0).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeInOut),
    );

    _updateAnimationState();
  }

  @override
  void didUpdateWidget(covariant BlueSquareOverlay oldWidget) {
    super.didUpdateWidget(oldWidget);
    _updateAnimationState();
  }

  void _updateAnimationState() {
    if (widget.isVisible && widget.flashing) {
      if (!_controller.isAnimating) {
        _controller.repeat(reverse: true);
      }
    } else {
      if (_controller.isAnimating) {
        _controller.stop();
      }
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!widget.isVisible) {
      return const SizedBox.shrink(); // 表示されていない場合は空のウィジェットを返す
    }

    return Positioned(
      top: widget.top,
      left: widget.left,
      child: IgnorePointer(
        child: AnimatedBuilder(
          animation: _opacityAnimation,
          builder: (context, child) {
            return Opacity(
              opacity: widget.flashing ? _opacityAnimation.value : 1.0,
              child: Container(
                width: widget.width,
                height: widget.height,
                decoration: BoxDecoration(
                  color: Colors.transparent, // 中身は透明
                  border: Border.all(
                    color: Colors.blue, // 青い枠線
                    width: 3.0, // 枠線の太さ
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
