import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'question_result_screen_widget.dart';
import 'question_description_screen_service.dart';
import 'question_result_screen_service.dart';
import 'dart:convert';
import 'interruption_data_controller.dart';
import 'question_selection_screen_widget.dart';
import 'package:http/http.dart' as http;

// チュートリアル関連補助ファイル
import 'tutorial_progress_manager.dart';
import 'blue_square_overlay.dart';
import 'help_overlay_service.dart';
import 'animation_build_display.dart';

class QuestionDescriptionScreenWidget extends StatefulWidget {
  final Map<String, dynamic> questionData;
  final Map<String, String> interruptions;

  QuestionDescriptionScreenWidget({
    required this.questionData,
    required this.interruptions,
  });

  @override
  _QuestionDescriptionScreenWidgetState createState() =>
      _QuestionDescriptionScreenWidgetState();
}

class _QuestionDescriptionScreenWidgetState
    extends State<QuestionDescriptionScreenWidget>
    with SingleTickerProviderStateMixin {
  final QuestionDescriptionService _service = QuestionDescriptionService();
  bool _isLoading = true;

  late AnimationController _controller;
  late Animation<double> _opacityAnimation;

  ////////////// チュートリアル／オンボーディング用の変数 //////////////
  final tutorialProgressManager _tutorial = tutorialProgressManager();
  bool _isTutorialActive = false; // 操作説明（チュートリアル）が実行中か
  bool _isOnboadingActive = false; // オンボーディング中か
  bool _isShowDialog = false; // 「操作説明を開始しますか？」のダイアログ表示
  bool _isCondition = false; // 操作が完了したかの一時フラグ
  bool showHelp = false; // ヘルプオーバーレイ表示用
  bool _isVisible = false; // 操作説明での青い枠表示制御

  int _currentStep = 0; // 操作説明（チュートリアル）進行管理
  int _guidanceNum = 0; // ガイダンス進行管理（オンボーディング用）

  // ガイダンス用のメッセージ（オンボーディング初期表示）
  final List<String> _guidance = [
    "ようこそ！この画面では問題文の確認と回答の入力ができます。",
    "上部に表示される問題番号で、全体の進捗状況を把握できます。",
    "問題文は画面上部に表示されています。しっかりと読みましょう。",
    "画面下部のテキスト入力欄に、あなたの回答を入力してください。",
    "左下の『前へ』ボタンで前の問題に戻り、右下の『次へ』ボタンで次の問題に進むことができます。",
    "中央下部の『回答終了』ボタンをタップすると、回答の採点が開始されます。",
    "画面上部の戻るボタンを使うと、途中までの回答を保存して問題選択画面に戻ります。",
    "以上で説明は終了です。",
  ];

  // ヘルプオーバーレイ用メッセージなど（そのまま）
  final List<String> helpMessages = [
    "検索バー: チャットルームを検索できます",
  ];

  final List<String> backgroundImages = [
    'assets/Help_search.png',
  ];

  final List<Alignment> messageAlignment = [
    Alignment.topCenter,
  ];

  final List<EdgeInsets> margin = [
    EdgeInsets.only(top: 20),
  ];

  // ヘルプ用：ハイライト矩形（例示用の固定値）
  final List<Rect> rectangleSteps = [
    Rect.fromLTWH(20, 100, 360, 50), // 検索バー
  ];

////////// 対象ウィジェットのGlobalKey ////////////
// 1. 画面上部の進捗表示（例：AppBarのタイトル部分など、問題番号・進捗状況を示すウィジェット）
  final GlobalKey progressDisplayKey = GlobalKey();

// 2. 問題文表示エリア（問題のテキストが表示される部分）
  final GlobalKey questionTextKey = GlobalKey();

// 3. 回答入力フィールド（ユーザーがテキスト入力するエリア）
  final GlobalKey answerInputFieldKey = GlobalKey();

// 4. 「前へ」ボタン（前の問題に戻るためのボタン）
  final GlobalKey previousButtonKey = GlobalKey();

// 5. 「次へ」ボタン（次の問題に進むためのボタン）
  final GlobalKey nextButtonKey = GlobalKey();

// 6. 「回答終了」ボタン（採点処理へ進むためのボタン）
  final GlobalKey submitButtonKey = GlobalKey();

// 7. 画面上部の戻るボタン（AppBarの左側に配置されるナビゲーション用のボタン）
  final GlobalKey backButtonKey = GlobalKey();

////////// ここまで

  //////////// ガイダンス用オーバーレイ変数 ////////////
  double _guidanceOverlayTop = 0.0;
  double _guidanceOverlayLeft = 0.0;
  double _guidanceOverlayWidth = 0.0;
  double _guidanceOverlayHeight = 0.0;
  //////////// ここまで

  //////////// 操作説明用オーバーレイ変数 ////////////
  double _tutorialOverlayTop = 0.0;
  double _tutorialOverlayLeft = 0.0;
  double _tutorialOverlayWidth = 0.0;
  double _tutorialOverlayHeight = 0.0;
  //////////// ここまで

// ガイダンス用の対象ウィジェットのGlobalKeyを返す
  GlobalKey? _getGuidanceTargetKey() {
    switch (_guidanceNum) {
      case 0:
        return null;
      case 1:
        return progressDisplayKey;
      case 2:
        return questionTextKey;
      case 3:
        return answerInputFieldKey;
      case 4:
        return nextButtonKey;
      case 5:
        return submitButtonKey;
      case 6:
        return backButtonKey;
      case 7:
        return null;
      default:
        return null;
    }
  }

  // ガイダンス用の対象ウィジェットの位置とサイズを更新する
  void _updateGuidanceOverlayPosition() {
    GlobalKey? targetKey = _getGuidanceTargetKey();
    if (targetKey == null) return;
    final context = targetKey.currentContext;
    if (context == null) {
      return;
    }
    final RenderBox renderBox = context.findRenderObject() as RenderBox;
    final Offset globalPosition = renderBox.localToGlobal(Offset.zero);
    final Size size = renderBox.size;
    setState(() {
      _guidanceOverlayTop = globalPosition.dy;
      _guidanceOverlayLeft = globalPosition.dx;
      _guidanceOverlayWidth = size.width;
      _guidanceOverlayHeight = size.height;
    });
  }

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat(reverse: true);

    _opacityAnimation = Tween(begin: 0.2, end: 0.7).animate(_controller);

    _service.fetchQuestions(widget.questionData).then((_) {
      setState(() {
        _isLoading = false;
        _applyInterruptions();
      });
    });

    // デバッグ用・初回起動時として強制的にオンボーディング（ガイダンス）を開始
    _checkAndStartTutorial();
  }

  Future<void> _checkAndStartTutorial() async {
    // ここでは画面ごとに一意なキーを用いる（例：ChatScreenWidget の場合はクラス名や固有のID）
    // 複数の画面で管理する場合、各画面で異なるキーを使用してください。
    final String tutorialKey = 'tutorialShown_${this.runtimeType}';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isFirstTime = prefs.getBool(tutorialKey) ?? true;
    if (isFirstTime) {
      // 初回の場合のみチュートリアル／ガイダンスを発火させる
      setState(() {
        _isOnboadingActive = true;
        _guidanceNum = 0;
      });
      // 画面描画後にオーバーレイ位置を更新
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
      // 一度発火したのでフラグを更新（2回目以降は発火しない）
      await prefs.setBool(tutorialKey, false);
    }
  }

  Future<void> _saveProgress(int step) async {
    await _tutorial.saveProgress('currentStep', step);
  }

  Future<void> _resetProgress() async {
    setState(() {
      _currentStep = 0;
    });
    await _tutorial.resetProgress();
  }

  void _guidanceStep() {
    if (_guidanceNum < _guidance.length - 1) {
      setState(() {
        _guidanceNum++;
      });
      WidgetsBinding.instance.addPostFrameCallback((_) {
        _updateGuidanceOverlayPosition();
      });
    } else {
      _endGuidance();
    }
  }

  void _endGuidance() {
    setState(() {
      _isShowDialog = false;
      _isOnboadingActive = false;
    });
  }

  //////////////////////////////////////////////////////////////////////

  @override
  void dispose() {
    _controller.dispose();
    _service.questionControllers.forEach((key, controller) {
      controller.dispose();
    });
    super.dispose();
  }

  void _applyInterruptions() {
    _service.initializeControllers();
    if (widget.interruptions.isNotEmpty) {
      widget.interruptions.forEach((questionId, answer) {
        int id = int.tryParse(questionId) ?? -1;
        if (id >= 0 && id < _service.questionControllers.length) {
          _service.questionControllers[id]?.text = answer;
        }
      });
    }
  }

  void _saveAllAnswers() {
    for (int i = 0; i < _service.questions.length; i++) {
      String answer = _service.questionControllers[i]?.text ?? '';
      _service.answers[i] = answer;
    }
  }

  void _goToNextQuestion() {
    _service.goToNextQuestion();
    setState(() {
      // UIを更新
    });
  }

  void _goToPreviousQuestion() {
    _service.goToPreviousQuestion();
    setState(() {
      // UIを更新
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) {
      return Scaffold(
        appBar: AppBar(),
        body: Center(child: CircularProgressIndicator()),
      );
    }

    return Stack(
      children: [
        PopScope(
          canPop: false,
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.white,
              iconTheme: IconThemeData(color: Colors.black),
              leading: IconButton(
                key: backButtonKey,
                icon: Icon(Icons.arrow_back),
                onPressed: () async {
                  final InterruptionDataController interruptionController =
                      Get.find();
                  Map<String, String> allAnswers = {};

                  _service.questionControllers
                      .forEach((questionId, controller) {
                    String answer = controller.text;
                    if (answer.isNotEmpty) {
                      allAnswers[questionId.toString()] = answer;
                    }
                  });

                  if (allAnswers.isNotEmpty) {
                    String currentQuestionId =
                        widget.questionData['questionListId'].toString();
                    interruptionController.saveAllInterruptions(
                        currentQuestionId, allAnswers);

                    // シャッフル順序を保存
                    List<int> shuffledOrder = _service.getShuffledOrder();
                    await interruptionController.saveShuffledOrder(
                        int.parse(currentQuestionId), shuffledOrder);
                  }
                  Get.back();
                },
              ),
              title: Text(
                '${_service.questionListName} : ${_service.currentQuestionNumber}/${_service.questions.length}',
                key: progressDisplayKey,
                style: TextStyle(color: Colors.black),
              ),
              centerTitle: true,
              actions: [
                IconButton(
                  icon: Icon(Icons.help_outline,
                      color: _isTutorialActive ? Colors.grey : Colors.black),
                  onPressed: () {
                    if (!_isTutorialActive) {
                      // オンボーディング（ガイダンス）を開始
                      WidgetsBinding.instance.addPostFrameCallback((_) {
                        setState(() {
                          _isOnboadingActive = true;
                          _guidanceNum = 0;
                        });
                        _updateGuidanceOverlayPosition();
                      });
                    }
                  },
                ),
              ],
            ),
            body: Column(
              children: [
                Expanded(
                  flex: 7,
                  child: Container(
                    key: questionTextKey,
                    padding: EdgeInsets.all(16.0),
                    alignment: Alignment.topLeft,
                    child: Text(
                      _service.currentQuestionText,
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 8.0, vertical: 12.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Visibility(
                        visible: _service.currentQuestionNumber != 1,
                        maintainSize: true,
                        maintainAnimation: true,
                        maintainState: true,
                        child: ElevatedButton(
                          key: previousButtonKey,
                          onPressed: _goToPreviousQuestion,
                          child: Row(
                            children: [
                              Icon(Icons.arrow_back),
                              SizedBox(width: 5),
                              Text("前へ"),
                            ],
                          ),
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                          ).copyWith(
                            backgroundColor: MaterialStateProperty.all(
                                Colors.lightBlueAccent),
                            foregroundColor:
                                MaterialStateProperty.all(Colors.black),
                          ),
                        ),
                      ),
                      ElevatedButton(
                        key: submitButtonKey,
                        onPressed: () async {
                          _saveAllAnswers();
                          // 確認ダイアログを表示
                          bool proceed = await showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('採点を開始しますか？'),
                                    content: Text('回答を送信して採点します。'),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text('キャンセル'),
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pop(false); // ダイアログを閉じ、falseを返す
                                        },
                                      ),
                                      TextButton(
                                        child: Text('OK'),
                                        onPressed: () {
                                          Navigator.of(context)
                                              .pop(true); // ダイアログを閉じ、trueを返す
                                        },
                                      ),
                                    ],
                                  );
                                },
                              ) ??
                              false;

                          if (proceed) {
                            final InterruptionDataController
                                interruptionController = Get.find();

                            int currentQuestionId =
                                widget.questionData['questionListId'];

                            showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return const AlertDialog(
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      CircularProgressIndicator(),
                                      SizedBox(height: 20),
                                      Text("採点中..."),
                                    ],
                                  ),
                                );
                              },
                            );

                            var service = QuestionResultScreenService(
                              questions: _service.questions,
                              answers: _service.answers,
                            );
                            try {
                              String gradingResult = await service
                                  .sendQuestionAnswerPairs(http.Client());

                              Navigator.pop(context); // ダイアログを閉じる

                              // 結果を解析して、結果画面に遷移
                              List<dynamic> decodedJson =
                                  jsonDecode(gradingResult) as List<dynamic>;
                              List<Map<String, dynamic>>
                                  gradedQuestionAnswerPairs = decodedJson
                                      .map((dynamic item) =>
                                          item as Map<String, dynamic>)
                                      .toList();

                              // データ登録サービスを呼び出して結果をFirestoreに登録
                              final xpResult =
                                  await _service.registerQuestionAnswers(
                                      gradedQuestionAnswerPairs,
                                      widget.questionData['questionListId'],
                                      widget.questionData['questionListName']);

                              if (gradingResult.isNotEmpty) {
                                // 成功した場合、中断データを削除
                                interruptionController
                                    .clearInterruptions(currentQuestionId);
                              }
                              /*Get.to(
                              () => QuestionResultScreenWidget(
                                    questionAnswerPairs:
                                        gradedQuestionAnswerPairs,
                                    quesTionListName: _service.questionListName,
                                    originId: 1,
                                    questionId: -1,
                                  ),
                              transition: Transition.downToUp);*/
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) =>
                                      QuestionResultScreenWidget(
                                    questionAnswerPairs:
                                        gradedQuestionAnswerPairs,
                                    quesTionListName: _service.questionListName,
                                    originId: 1,
                                    questionId: -1,
                                    xpResult: xpResult,
                                    questionListName:
                                        widget.questionData['questionListName'],
                                  ),
                                ),
                              );
                            } catch (e) {
                              Navigator.pop(context); // エラー発生時にもダイアログを閉じる
                              Get.snackbar(
                                'エラー',
                                '回答送信中にエラーが発生しました: $e',
                                snackPosition: SnackPosition.BOTTOM,
                                backgroundColor: Colors.redAccent,
                                colorText: Colors.white,
                              );
                            }
                          }
                        },
                        child: Text("回答終了"),
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                        ).copyWith(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.grey[400]),
                          foregroundColor:
                              MaterialStateProperty.all(Colors.black),
                        ),
                      ),
                      Visibility(
                        visible: _service.currentQuestionNumber !=
                            _service.questions.length,
                        maintainSize: true,
                        maintainAnimation: true,
                        maintainState: true,
                        child: ElevatedButton(
                          key: nextButtonKey,
                          onPressed: _goToNextQuestion,
                          child: Row(
                            children: [
                              Text("次へ"),
                              SizedBox(width: 5),
                              Icon(Icons.arrow_forward),
                            ],
                          ),
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                          ).copyWith(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.redAccent),
                            foregroundColor:
                                MaterialStateProperty.all(Colors.black),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                BreathingBorder(opacityAnimation: _opacityAnimation),
                Expanded(
                  flex: 3,
                  child: _buildFullWidthAnswerInputField(),
                ),
              ],
            ),
          ),
        ),
        // チュートリアル／オンボーディングオーバーレイ（ガイダンス部分）
        if (_isOnboadingActive && !_isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (_isOnboadingActive && !_isTutorialActive)
          GestureDetector(
            onTap: _guidanceStep,
            child: Center(
              child: Stack(
                children: [
                  _buildBottomPanel(_guidance[_guidanceNum]),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: ElevatedButton.icon(
                      onPressed: () {
                        if (_isOnboadingActive) {
                          _endGuidance();
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15),
                          side: BorderSide(color: Colors.black, width: 2.5),
                        ),
                      ),
                      icon: Image.asset(
                        'assets/Skip.png',
                        width: 25,
                        height: 25,
                      ),
                      label: Text(
                        "スキップ",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        if (_isOnboadingActive && _isShowDialog)
          Center(child: _buildBackgroundOverlay()),
        if (showHelp)
          HelpOverlay(
            existingWidget: Container(),
            helpMessages: helpMessages,
            backgroundImages: backgroundImages,
            onSkip: () {
              setState(() {
                showHelp = false;
              });
            },
            messageAlignment: messageAlignment,
            margin: margin,
            rectangleSteps: rectangleSteps,
          ),
        // ガイダンス中なら、ガイダンス用の青い枠を表示する
        if (_isOnboadingActive)
          BlueSquareOverlay(
            top: _guidanceOverlayTop,
            left: _guidanceOverlayLeft,
            width: _guidanceOverlayWidth,
            height: _guidanceOverlayHeight,
            flashing: true,
            isVisible: true,
          ),

        // 操作説明用の青い枠（動的に対象ウィジェットの位置を取得）
        if (_isTutorialActive && _isVisible)
          BlueSquareOverlay(
            top: _tutorialOverlayTop,
            left: _tutorialOverlayLeft,
            width: _tutorialOverlayWidth,
            height: _tutorialOverlayHeight,
            flashing: true,
            isVisible: _isVisible,
          ),
      ],
    );
  }

  Widget _buildBackgroundOverlay() {
    return Container(
      color: Colors.black54,
      width: double.infinity,
      height: double.infinity,
    );
  }

  Widget _buildBottomPanel(String text) {
    String displayText = text;

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 30),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.lightBlue, width: 3),
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
      ),
      child: Text(
        displayText,
        style: TextStyle(
            fontSize: 16,
            color: Colors.black,
            fontWeight: FontWeight.w600,
            decoration: TextDecoration.none),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildFullWidthAnswerInputField() {
    TextEditingController? currentController =
        _service.questionControllers[_service.currentQuestionIndex];

    if (currentController == null) {
      currentController = TextEditingController();
      _service.questionControllers[_service.currentQuestionIndex] =
          currentController;
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        key: answerInputFieldKey,
        controller: currentController,
        onChanged: (value) {
          _service.answers[_service.currentQuestionIndex] = value;
        },
        decoration: InputDecoration(
          hintText: 'あなたの回答を入力してください',
          hintStyle: TextStyle(
            fontSize: 12,
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey.shade800),
          ),
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        ),
        keyboardType: TextInputType.multiline,
        maxLines: 7,
      ),
    );
  }
}

class BreathingBorder extends StatelessWidget {
  final Animation<double> opacityAnimation;

  BreathingBorder({required this.opacityAnimation});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 10,
      child: AnimatedBuilder(
        animation: opacityAnimation,
        builder: (context, child) {
          return CustomPaint(
            painter: TripleLinePainter(opacityAnimation.value),
            size: Size(double.infinity, 10),
          );
        },
      ),
    );
  }
}

class TripleLinePainter extends CustomPainter {
  final double opacity;

  TripleLinePainter(this.opacity);

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = Colors.blueGrey.withOpacity(opacity)
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1;

    final double segmentHeight = size.height / 3;

    // 3本の線を描画
    for (int i = 0; i < 3; i++) {
      canvas.drawLine(
        Offset(0, segmentHeight * (i + 0.5)),
        Offset(size.width, segmentHeight * (i + 0.5)),
        paint,
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
