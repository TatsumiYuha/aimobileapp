import 'package:ai_mobile_app/gamefication_service.dart';
import 'package:ai_mobile_app/level_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'interruption_data_controller.dart';
import 'package:get/get.dart';
import 'dart:math';

class QuestionDescriptionService {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final User? currentUser = FirebaseAuth.instance.currentUser;
  List<Map<String, dynamic>> questions = [];
  List<String> answers = [];
  String questionListName = '';
  int currentQuestionIndex = 0;
  Map<int, TextEditingController> questionControllers = {};
  final gamificationService = GamificationService();

  final InterruptionDataController _interruptionController = Get.find();

  // シャッフル順序を保持するメンバ変数
  List<int> shuffledOrder = [];

  QuestionDescriptionService();

  Future<void> fetchQuestions(Map<String, dynamic> questionData) async {
    questionListName = questionData['questionListName'] ?? '';

    if (questionData.containsKey('questionList') &&
        questionData['questionList'] is List) {
      List<dynamic> fetchedQuestions =
          questionData['questionList'] as List<dynamic>;

      // 各質問をMap<String, dynamic>に変換してquestionsリストに格納
      questions = fetchedQuestions
          .map((question) => Map<String, dynamic>.from(question))
          .toList();

      // 質問数に合わせてanswersを初期化
      answers = List<String>.filled(questions.length, '');

      // 質問リストIDを取得
      int questionId = questionData['questionListId'];

      // 保存されたシャッフル順序を取得
      List<int>? savedShuffledOrder =
          await _interruptionController.getShuffledOrder(questionId);

      if (savedShuffledOrder != null &&
          savedShuffledOrder.length == questions.length) {
        // 保存された順序が存在する場合、その順序で質問を並び替え
        List<Map<String, dynamic>> orderedQuestions = [];
        for (int index in savedShuffledOrder) {
          if (index >= 0 && index < questions.length) {
            orderedQuestions.add(questions[index]);
          }
        }
        questions = orderedQuestions;
        shuffledOrder = List<int>.from(savedShuffledOrder);
      } else {
        // シャッフルして順序を保持
        shuffledOrder = List<int>.generate(questions.length, (index) => index);
        shuffledOrder.shuffle(Random());
        questions = shuffledOrder.map((index) => questions[index]).toList();
        // シャッフル順序は保存しない（バックアローがタップされた際に保存）
      }
    } else {
      questions = [];
      answers = [];
      shuffledOrder = [];
    }
    initializeControllers();
  }

  // シャッフル順序を取得するゲッター
  List<int> getShuffledOrder() {
    return shuffledOrder;
  }

  int get currentQuestionNumber => currentQuestionIndex + 1;

  String get currentQuestionText => questions.isNotEmpty
      ? questions[currentQuestionIndex]['questionText'] ?? ''
      : '';

  void goToNextQuestion() {
    if (currentQuestionIndex < questions.length - 1) {
      currentQuestionIndex++;
    }
  }

  void goToPreviousQuestion() {
    if (currentQuestionIndex > 0) {
      currentQuestionIndex--;
    }
  }

  void saveAnswer(String answer) {
    if (questions.isNotEmpty) {
      answers[currentQuestionIndex] = answer;
    }
  }

  bool isLastQuestion() {
    return currentQuestionIndex == questions.length - 1;
  }

  void initializeControllers() {
    for (int i = 0; i < questions.length; i++) {
      questionControllers[i] = TextEditingController(text: answers[i]);
    }
  }

  Future<XpGainResult> registerQuestionAnswers(
      List<Map<String, dynamic>> gradedQuestionAnswerPairs,
      int questionId,
      String questionListName) async {
    int correctCount = 0;
    int totalExpToAdd = 0;
    int totalPointToAdd = 0;
    final userId = currentUser?.uid;

    if (gradedQuestionAnswerPairs.length != 1 || questionListName == "今日の問題") {
      // ドキュメントに保存するための問題と回答のペアのリストを作成
      List<Map<String, dynamic>> questionAnswerList = [];

      // 各問題ごとに userAnswers に登録し、正答数と回答数をカウント
      for (var pair in gradedQuestionAnswerPairs) {
        if (pair['grade']) {
          correctCount++;
          if (questionListName == "今日の問題") {
            await gamificationService.addDailyQuestion();
          }
        }

        questionAnswerList.add({
          'answer': pair['answer'],
          'isCorrect': pair['grade'],
          'commentary': pair['commentary'],
          'grade': pair['grade'],
          'question': pair['question'],
          'modelAnswer': pair['modelAnswer'],
        });

        totalExpToAdd +=
            gamificationService.calculateXpForAnswer(pair['grade']);
        totalPointToAdd +=
            gamificationService.calculatePointForAnswer(pair['grade']);
      }

      // userAnswers に問題リスト全体を登録
      await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('userAnswers')
          .add({
        'questionList': questionAnswerList,
        'correctCount': correctCount,
        'questionType': 'Descriptive',
        'questionId': questionId,
        'timestamp': DateTime.now(),
      });

      // userPerformance を更新
      await _firestore
          .collection('users')
          .doc(currentUser?.uid)
          .collection('userPerformance')
          .add({
        'questionType': 'Descriptive',
        'questionId': questionId,
        'correctCount': correctCount,
        'totalQuestions': gradedQuestionAnswerPairs.length,
      });

      // 満点だった場合に完了リストに問題番号を追加
      if (correctCount == gradedQuestionAnswerPairs.length &&
          questionListName != "今日の問題") {
        await addNumberToCompleteList(questionId);
      }
    }

    // 2) XPの加算前後で oldXp/newXp/レベル情報を取得
    final userDocRef = _firestore.collection('users').doc(userId);
    final userDocSnap = await userDocRef.get();

    // oldXpを取得（未設定なら0とする）
    final oldXp = (userDocSnap.data()?['experience'] ?? 0) as int;
    final oldprogressMap = getCurrentLevelProgress(oldXp);
    final oldprogressXp = oldprogressMap['currentProgress'] ?? 0;
    final oldLevel = getLevelFromXp(oldXp);

    if (totalExpToAdd > 0 && currentUser?.uid != null) {
      await gamificationService.addExperience(totalExpToAdd);
    }

    if (totalPointToAdd > 0 && currentUser?.uid != null) {
      await gamificationService.addPoint(totalExpToAdd);
    }
    // 4) 加算後のXPを再取得
    final updatedDocSnap = await userDocRef.get();
    final newXp = (updatedDocSnap.data()?['experience'] ?? 0) as int;
    final newLevel = getLevelFromXp(newXp);

    // レベルごとの必要XPを取得する
    // 例: getCurrentLevelProgressを使うか、直接 levelTable を参照するか
    final newprogressMap = getCurrentLevelProgress(newXp);
    final newprogressXp = newprogressMap['currentProgress'] ?? 0;
    final xpNeededForNextLevel = newprogressMap['maxProgress'] ?? 0;

    // 5) XpGainResultを返す
    return XpGainResult(
      oldXp: oldprogressXp,
      newXp: newprogressXp,
      oldLevel: oldLevel,
      newLevel: newLevel,
      xpNeededForNextLevel: xpNeededForNextLevel,
    );
  }

  Future<void> addNumberToCompleteList(int questionId) async {
    DocumentReference userCompleteRef = _firestore
        .collection('users')
        .doc(currentUser?.uid)
        .collection('userComplete')
        .doc('Descriptive');

    try {
      // ドキュメントの現在のデータを取得
      DocumentSnapshot snapshot = await userCompleteRef.get();

      if (snapshot.exists) {
        Map<String, dynamic> data = snapshot.data() as Map<String, dynamic>;
        List<dynamic> currentList = data['completeList'] as List<dynamic>;
        // リストに既に同一のIDが存在する場合は登録しない
        if (!currentList.contains(questionId)) {
          // 新しい数値をリストに追加
          currentList.add(questionId);
          // 更新されたリストをFirestoreに保存
          await userCompleteRef.update({'completeList': currentList});
        }
      } else {
        // ドキュメントが存在しない場合、新しいドキュメントを作成
        await userCompleteRef.set({
          'completeList': [questionId]
        });
      }
    } catch (e) {
      print('Error adding number to complete list: $e');
      throw e; // またはエラーを適切に処理
    }
  }
}
