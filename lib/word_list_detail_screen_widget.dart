// lib/screens/word_list_detail_screen_widget.dart

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'glossary_provider.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

class WordListDetailScreen extends StatelessWidget {
  final WordList wordList;

  WordListDetailScreen({required this.wordList});

  void _showDefinitionDialog(
      BuildContext context, String term, String definition) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        // 新しい変数 dialogContext を使用
        final glossaryProvider =
            Provider.of<GlossaryProvider>(dialogContext, listen: false);
        bool isFavorite = glossaryProvider.isFavorite(term);
        List<String> relatedTerms = glossaryProvider.getRelatedTerms(term);

        return AlertDialog(
          title: Text(term),
          content: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(definition),
                SizedBox(height: 20),
                if (relatedTerms.isNotEmpty) ...[
                  Text(
                    '関連用語',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 10),
                  Wrap(
                    spacing: 8,
                    children: relatedTerms.map<Widget>((relatedTerm) {
                      return ActionChip(
                        label: Text(relatedTerm),
                        onPressed: () {
                          // 現在のダイアログを閉じずに新しいダイアログを開く
                          _showDefinitionDialog(
                            context,
                            relatedTerm,
                            glossaryProvider.glossary.firstWhere(
                                  (item) => item['term'] == relatedTerm,
                                  orElse: () => {
                                    'definition': '定義なし',
                                  },
                                )['definition'] ??
                                '定義なし',
                          );
                        },
                      );
                    }).toList(),
                  ),
                ],
              ],
            ),
          ),
          actions: <Widget>[
            if (!isFavorite)
              TextButton(
                child: Text('お気に入りに登録する'),
                onPressed: () async {
                  await glossaryProvider.addFavorite(term);
                  Navigator.of(dialogContext).pop(); // このダイアログのみ閉じる
                },
              ),
            if (isFavorite)
              TextButton(
                child: Text('お気に入りを解除する'),
                onPressed: () async {
                  await glossaryProvider.removeFavorite(term);
                  Navigator.of(dialogContext).pop(); // このダイアログのみ閉じる
                },
              ),
            TextButton(
              child: Text('閉じる'),
              onPressed: () {
                Navigator.of(dialogContext).pop(); // このダイアログのみ閉じる
              },
            ),
          ],
        );
      },
    );
  }

  void _showDeleteTermDialog(BuildContext context, String term) async {
    bool confirm = false;
    await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('用語を削除'),
          content: Text('「$term」を単語帳から削除しますか？'),
          actions: <Widget>[
            TextButton(
              child: Text('キャンセル'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('削除', style: TextStyle(color: Colors.red)),
              onPressed: () {
                confirm = true;
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );

    if (confirm) {
      await Provider.of<GlossaryProvider>(context, listen: false)
          .removeTermFromWordList(wordList.id, term);
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('用語を単語帳から削除しました')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    final glossaryProvider = Provider.of<GlossaryProvider>(context);
    final terms = wordList.terms;

    return Scaffold(
      appBar: AppBar(
        title: Text(wordList.name),
        backgroundColor: Colors.white, // アプリバーの色を変更
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: terms.isEmpty
          ? Center(child: Text('この単語帳には用語がありません'))
          : ListView.builder(
              itemCount: terms.length,
              itemBuilder: (context, index) {
                final term = terms[index];
                final glossaryItem = glossaryProvider.glossary.firstWhere(
                    (item) => item['term'] == term,
                    orElse: () =>
                        {'term': term, 'definition': '定義なし', 'letter': '?'});

                return ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.teal, // アイコンの背景色を変更
                    child: Text(
                      glossaryItem['letter'],
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  title: Text(
                    glossaryItem['term'],
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: Colors.teal[700],
                    ),
                  ),
                  subtitle: Text(
                    glossaryItem['definition'],
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.grey[700],
                    ),
                  ),
                  trailing: IconButton(
                    icon: Icon(Icons.delete, color: Colors.redAccent),
                    onPressed: () {
                      _showDeleteTermDialog(context, term);
                    },
                    tooltip: '用語を削除',
                  ),
                  onTap: () {
                    _showDefinitionDialog(context, glossaryItem['term'],
                        glossaryItem['definition']);
                  },
                );
              },
            ),
    );
  }
}
