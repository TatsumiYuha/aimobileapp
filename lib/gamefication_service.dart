// gamification_service.dart

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';

class GamificationService {
  final FirebaseFirestore firestore;
  final User? currentUser = FirebaseAuth.instance.currentUser;

  GamificationService({FirebaseFirestore? instance})
      : firestore = instance ?? FirebaseFirestore.instance;

  /// ユーザーにXPを加算する共通メソッド
  Future<void> addExperience(int xp) async {
    await firestore.collection('users').doc(currentUser?.uid).update({
      'experience': FieldValue.increment(xp),
    });
  }

  /// ユーザーにポイントを加算する共通メソッド
  Future<void> addPoint(int point) async {
    await firestore.collection('users').doc(currentUser?.uid).update({
      'point': FieldValue.increment(point),
    });
  }

  /// ユーザーのポイントを減算する共通メソッド
  Future<bool> spendPoints(int point) async {
    final userDoc = firestore.collection('users').doc(currentUser?.uid);
    final snapshot = await userDoc.get();

    if (snapshot.exists) {
      final currentPoints = snapshot.data()?['point'] ?? 0;
      // 現在のポイントが引数のpoint以上の場合のみ更新
      if (currentPoints >= point) {
        await userDoc.update({
          'point': FieldValue.increment(-point),
        });
        return true;
      }
    }
    return false;
  }

  Future<int> getExperience() async {
    final userDocRef = firestore.collection('users').doc(currentUser?.uid);
    final userDocSnap = await userDocRef.get();
    return (userDocSnap.data()?['experience'] ?? 0) as int;
  }

  int calculateXpForAnswer(bool isCorrect) {
    return isCorrect ? 10 : 1;
  }

  int calculatePointForAnswer(bool isCorrect) {
    return isCorrect ? 5 : 1;
  }

  Future<void> addDailyExperience(int xp) async {
    if (currentUser == null) {
      // ログインしていない状態なら何もしない
      return;
    }
    final userId = currentUser!.uid;

    // ユーザードキュメントをトランザクションで更新
    final userDocRef = firestore.collection('users').doc(userId);

    await firestore.runTransaction((transaction) async {
      final userSnapshot = await transaction.get(userDocRef);

      final userData = userSnapshot.data() as Map<String, dynamic>;
      final dailyXpCount = userData['dailyXpCount'] as int? ?? 0;

      // 5回未満なら加算
      if (dailyXpCount < 5) {
        transaction.update(userDocRef, {
          'dailyXpCount': dailyXpCount + 1,
        });
      }
    });
  }

  Future<void> addGlossaryView(String glossaryNumber) async {
    // currentUser, firestore は各自既に初期化されている前提
    if (currentUser == null) return;
    final userId = currentUser!.uid;

    // ユーザードキュメントの dailyMissionGlossary_numbers と を更新
    final userDocRef = firestore.collection('users').doc(userId);
    await firestore.runTransaction((transaction) async {
      final docSnapshot = await transaction.get(userDocRef);
      final data = docSnapshot.data() ?? {};
      List<dynamic> viewedList = data['dailyMissionGlossary_numbers'] ?? [];
      // 既に本日記録されている場合、重複しなければ追加
      if (!viewedList.contains(glossaryNumber) && viewedList.length < 10) {
        viewedList.add(glossaryNumber);
        transaction.update(userDocRef, {
          'dailyMissionGlossary_numbers': viewedList,
        });
      }
    });
  }

  Future<void> addDailyQuestion() async {
    if (currentUser == null) {
      // ログインしていない状態なら何もしない
      return;
    }
    final userId = currentUser!.uid;
    final userDocRef = firestore.collection('users').doc(userId);

    await firestore.runTransaction((transaction) async {
      final docSnapshot = await transaction.get(userDocRef);
      if (docSnapshot.exists) {
        // ドキュメントが存在する場合、dailyQuestion の値を取得（未設定なら 0 とみなす）
        final userData = docSnapshot.data() ?? {};
        final dailyQuestion = userData['dailyQuestion'] as int? ?? 0;
        // 値が 1 の場合は既に加算済みなので、更新しない
        if (dailyQuestion != 1) {
          transaction.update(userDocRef, {
            'dailyQuestion': dailyQuestion + 1,
          });
        }
      } else {
        // ドキュメント自体が存在しない場合は、新規作成して dailyQuestion を 1 とする
        transaction.set(userDocRef, {
          'dailyQuestion': 1,
        });
      }
    });
  }
}

class XpGainResult {
  final int oldXp;
  final int newXp;
  final int oldLevel;
  final int newLevel;
  final int xpNeededForNextLevel;

  XpGainResult({
    required this.oldXp,
    required this.newXp,
    required this.oldLevel,
    required this.newLevel,
    required this.xpNeededForNextLevel,
  });
}
