import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver? driver;

  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    await driver?.close();
  });

  test('TC01: Login with correct email and password', () async {
    await driver?.tap(find.byValueKey('emailField'));
    await driver?.enterText('ai.techno@techno-core.jp');
    await driver?.tap(find.byValueKey('passwordField'));
    await driver?.enterText('AiTechno0609');
    await driver?.tap(find.byValueKey('loginButton'));
    expect(await driver?.getText(find.byValueKey('chatRoomSelectionTitle')),
        'Chat Room Selection');
  });

  test('TC02: Attempt to login with incorrect email', () async {
    await driver?.tap(find.byValueKey('emailField'));
    await driver?.enterText('incorrect_email@example.com');
    await driver?.tap(find.byValueKey('passwordField'));
    await driver?.enterText('any_password');
    await driver?.tap(find.byValueKey('loginButton'));
    expect(await driver?.getText(find.byValueKey('snackBar')), 'ログインに失敗しました。');
  });

  test('TC03: Attempt to login with correct email and incorrect password',
      () async {
    await driver?.tap(find.byValueKey('emailField'));
    await driver?.enterText('ai.techno@techno-core.jp');
    await driver?.tap(find.byValueKey('passwordField'));
    await driver?.enterText('AiTTTTTTTTTT');
    await driver?.tap(find.byValueKey('loginButton'));
    expect(await driver?.getText(find.byValueKey('snackBar')), 'ログインに失敗しました。');
  });

  test('TC04: Toggle password visibility', () async {
    await driver?.tap(find.byValueKey('togglePasswordVisibility'));
    // このケースでは、パスワードの可視性を変更する効果を直接テストする方法はありません。
    // ただし、エラーが発生しないことを確認することは可能です。
  });

  test('TC05: Navigate to Password Reset Screen', () async {
    await driver?.tap(find.byValueKey('passwordResetButton'));
    expect(await driver?.getText(find.byValueKey('passwordResetTitle')),
        'パスワード再設定');
  });
}
