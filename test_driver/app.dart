import 'package:flutter_driver/driver_extension.dart';
import 'package:ai_mobile_app/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}
